package tradesystem.tradeunit.basetestutils;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import tradesystem.tradeunit.dao.AccountDAO;
import tradesystem.tradeunit.dao.CashDAO;
import tradesystem.tradeunit.dao.CountedAttributeDAO;
import tradesystem.tradeunit.dao.CurrencyDAO;
import tradesystem.tradeunit.dao.CustomerDAO;
import tradesystem.tradeunit.dao.DivisionDAO;
import tradesystem.tradeunit.dao.ItemDAO;
import tradesystem.tradeunit.dao.MeasureDAO;
import tradesystem.tradeunit.dao.OrderDAO;
import tradesystem.tradeunit.dao.PriceDAO;
import tradesystem.tradeunit.dao.ProductDAO;
import tradesystem.tradeunit.dao.ProductGroupDAO;
import tradesystem.tradeunit.dao.WarehouseDAO;
import tradesystem.tradeunit.dao.security.UserAccountDAO;
import tradesystem.tradeunit.domain.Account;
import tradesystem.tradeunit.domain.Cash;
import tradesystem.tradeunit.domain.CountedAttribute;
import tradesystem.tradeunit.domain.Currency;
import tradesystem.tradeunit.domain.Customer;
import tradesystem.tradeunit.domain.Division;
import tradesystem.tradeunit.domain.Measure;
import tradesystem.tradeunit.domain.Order;
import tradesystem.tradeunit.domain.Price;
import tradesystem.tradeunit.domain.Product;
import tradesystem.tradeunit.domain.ProductGroup;
import tradesystem.tradeunit.domain.Warehouse;
import tradesystem.tradeunit.domain.security.UserAccount;
import tradesystem.tradeunit.service.AccountService;
import tradesystem.tradeunit.service.CashService;
import tradesystem.tradeunit.service.CountedAttributeService;
import tradesystem.tradeunit.service.CurrencyService;
import tradesystem.tradeunit.service.CustomerService;
import tradesystem.tradeunit.service.DivisionService;
import tradesystem.tradeunit.service.ItemService;
import tradesystem.tradeunit.service.MeasureService;
import tradesystem.tradeunit.service.OrderService;
import tradesystem.tradeunit.service.PriceService;
import tradesystem.tradeunit.service.PrivilegesService;
import tradesystem.tradeunit.service.ProductGroupService;
import tradesystem.tradeunit.service.ProductService;
import tradesystem.tradeunit.service.WarehouseService;
import tradesystem.tradeunit.service.security.UserAccountService;

/**
 * The Class BaseTest.
 */
@Transactional
public class BaseTest {

	// DAO

	@Autowired
	protected UserAccountDAO userAccountDAO;

	@Autowired
	protected AccountDAO accountDAO;

	@Autowired
	protected CashDAO cashDAO;

	@Autowired
	protected DivisionDAO divisionDAO;

	@Autowired
	protected MeasureDAO measureDAO;

	@Autowired
	protected ProductGroupDAO productGroupDAO;

	@Autowired
	protected ProductDAO productDAO;

	@Autowired
	protected CustomerDAO customerDAO;
	
	@Autowired
	protected CurrencyDAO currencyDAO;
	
	@Autowired
	protected WarehouseDAO warehouseDAO;
	
	@Autowired
	protected PriceDAO priceDAO;
	
	@Autowired
	protected CountedAttributeDAO countedAttributeDAO;
	
	@Autowired
	protected OrderDAO orderDAO;
	
	@Autowired
	protected ItemDAO itemDAO;

	// Services

	@Autowired
	protected UserAccountService userAccountService;

	@Autowired
	protected AccountService accountService;

	@Autowired
	protected CashService cashService;

	@Autowired
	protected DivisionService divisionService;

	@Autowired
	protected MeasureService measureService;

	@Autowired
	protected ProductGroupService productGroupService;

	@Autowired
	protected ProductService productService;

	@Autowired
	protected CustomerService customerService;
	
	@Autowired
	protected CurrencyService currencyService;
	
	@Autowired
	protected WarehouseService warehouseService;
	
	@Autowired
	protected CountedAttributeService countedAttributeServece;
	
	@Autowired
	protected PriceService priceServece;
	
	@Autowired
	protected OrderService orderService;
	
	@Autowired
	protected ItemService itemService;
	
	@Autowired
	protected PrivilegesService privilegesService;
	

	// Objects
	protected Division division1;
	protected Division division2;

	protected UserAccount userAccount;

	protected Measure measureInteger;
	protected Measure measureFloat;
	protected Measure measureFraction;

	protected ProductGroup productGroup;

	protected Product productInteger;
	protected Product productFloat;
	protected Product productFraction;
	
	
	// for subaccounts tests
	protected Product productIntegerShoes;
	protected Product productFloatOil;
	protected Product productFractionTablet;
	
	protected CountedAttribute countedAttributeShoesSize;
	protected CountedAttribute countedAttributeColor;
	// ...
	
	protected Currency currencyKZ;
	protected Currency currencyRU;
	protected Currency currencyUA;

	protected Cash cash;

	protected Account account;

	protected Customer customer;
	
	protected Warehouse warehouse1;
	protected Warehouse warehouse2;
	
	protected Warehouse warehouse3;
	
	protected Order orderPpl1, orderPpl2, orderPpl3;
	

	/**
	 * Creates the test entity's.
	 * 
	 * @throws Exception
	 *             the exception
	 */
	@Transactional
	public void createTestEntitys() throws Exception {

		// Create division1
		{
			Division divisionT = new Division();
			divisionT.setKeyName("Division 1");
			divisionT.setDescription("Division description 1 ...");
			division1 = divisionService.persist(divisionT);
		}
		
		{
			Division divisionT = new Division();
			divisionT.setKeyName("Division 2");
			divisionT.setDescription("Division description 2 ...");
			division2 = divisionService.persist(divisionT);
		}

		// Create user account
		{
			UserAccount userAccountT = new UserAccount();
			userAccountT.setUsername("User 1");
			userAccountT.setDescription("User 1 description");
			userAccountT
					.setPassword("460555704309b621576728cab442c5df8233ddbd");
			userAccountT.setfName("Userfname");
			userAccountT.setnName("Usernname");
			userAccountT.setmName("Usermname");
			userAccountT.setEmail("email@test.com");
			userAccountT.setPfone1("87001205230");
			userAccountT.setPfone2("87252437487");
			userAccountT.setPfone3("87054557286");
			userAccountT.setPfone4("87262717171");
			userAccountT.setDivision(division1);

			userAccount = userAccountService.persist(userAccountT);
		}

		// Create cash
		{
			Cash cashT = new Cash();
			cashT.setKeyName("Cass 1");
			cashT.setDescription("Cass 1 description...");
			cashT.setDivision(division1);
			cash = cashService.persist(cashT);
		}

		// Create measure
		{
			Measure measureT = new Measure();
			measureT.setKeyName("INT");
			measureT.setMeasureType(Measure.MeasureType.INTEGER);
			measureT.setDescription("Measure KG description...");
			measureInteger = measureService.persist(measureT);
		}
		{
			Measure measureT = new Measure();
			measureT.setKeyName("FLOAT");
			measureT.setMeasureType(Measure.MeasureType.FLOAT);
			measureT.setDescription("Measure KG description...");
			measureFloat = measureService.persist(measureT);
		}
		{
			Measure measureT = new Measure();
			measureT.setKeyName("FRACTION");
			measureT.setMeasureType(Measure.MeasureType.FRACTION);
			measureT.setDescription("Measure KG description...");
			measureFraction = measureService.persist(measureT);
		}
		
		// Counted attributes
		{
			//protected CountedAttribute CountedAttributeShoesSize;
			{
				countedAttributeShoesSize = new CountedAttribute();
				countedAttributeShoesSize.setKeyName("Shoes sizes");
				countedAttributeShoesSize.setDescription("Description.....");
				
				{
					CountedAttribute countedAttribute = new CountedAttribute();
					countedAttribute.setKeyName("38");
					countedAttribute.setDescription("38 size");
					countedAttribute.setParent(countedAttributeShoesSize);
					countedAttribute = countedAttributeServece.persist(countedAttribute);
					
					countedAttribute.getChildren().size();
					
					countedAttributeShoesSize.getChildren().add(countedAttribute);
				}
				
				{
					CountedAttribute countedAttribute = new CountedAttribute();
					countedAttribute.setKeyName("39");
					countedAttribute.setDescription("39 size");
					countedAttribute.setParent(countedAttributeShoesSize);
					countedAttribute = countedAttributeServece.persist(countedAttribute);
					
					countedAttributeShoesSize.getChildren().add(countedAttribute);
				}
				
				{
					CountedAttribute countedAttribute = new CountedAttribute();
					countedAttribute.setKeyName("40");
					countedAttribute.setDescription("40 size");
					countedAttribute.setParent(countedAttributeShoesSize);
					countedAttribute = countedAttributeServece.persist(countedAttribute);
					
					countedAttributeShoesSize.getChildren().add(countedAttribute);
				}
				
				countedAttributeShoesSize = countedAttributeServece.persist(countedAttributeShoesSize);
			}
			
			
			//protected CountedAttribute countedAttributeColor;
			{
				countedAttributeColor = new CountedAttribute();
				countedAttributeColor.setKeyName("Shoes sizes");
				countedAttributeColor.setDescription("Description.....");
				
				{
					CountedAttribute countedAttribute = new CountedAttribute();
					countedAttribute.setKeyName("RED");
					countedAttribute.setDescription("RED COLOR");
					countedAttribute.setParent(countedAttributeColor);
					countedAttribute = countedAttributeServece.persist(countedAttribute);
					
					countedAttributeColor.getChildren().add(countedAttribute);
				}
				
				{
					CountedAttribute countedAttribute = new CountedAttribute();
					countedAttribute.setKeyName("GREEN");
					countedAttribute.setDescription("GREEN COLOR");
					countedAttribute.setParent(countedAttributeColor);
					countedAttribute = countedAttributeServece.persist(countedAttribute);
					
					countedAttributeColor.getChildren().add(countedAttribute);
				}
				
				{
					CountedAttribute countedAttribute = new CountedAttribute();
					countedAttribute.setKeyName("BLUE");
					countedAttribute.setDescription("BLUE COLOR");
					countedAttribute.setParent(countedAttributeColor);
					countedAttribute = countedAttributeServece.persist(countedAttribute);
					
					countedAttributeColor.getChildren().add(countedAttribute);
				}
				
				countedAttributeColor = countedAttributeServece.persist(countedAttributeColor);
			}
			
		}

		// Product group
		{
			ProductGroup productGroupT = new ProductGroup();
			productGroupT.setKeyName("Product group 1");
			productGroupT.setDescription("Product group 1 description ...");
			productGroup = productGroupService.persist(productGroupT);
		}

		// Product
		{
			{
				Product productT = new Product();
				productT.setKeyName("Product 1");
				productT.setDescription("Product 1 description...");
				productT.setBarcode("TEST01");
				productT.setMeasure(measureInteger);
				productT.setProductGroup(productGroup);
				productT.setDenominator(new BigDecimal(0));

				productInteger = productService.persist(productT);
				
				
				Price price = priceServece.findPrice(division1, productT);
				price.setValue(new BigDecimal(100));
				price = priceServece.merge(price);
			}

			{
				Product productT = new Product();
				productT.setKeyName("Product 2");
				productT.setDescription("Product 2 description...");
				productT.setBarcode("TEST02");
				productT.setMeasure(measureFloat);
				productT.setProductGroup(productGroup);
				productT.setDenominator(new BigDecimal(0));

				productFloat = productService.persist(productT);
				
				Price price = priceServece.findPrice(division1, productT);
				price.setValue(new BigDecimal(200));
				price = priceServece.merge(price);
			}

			{
				Product productT = new Product();
				productT.setKeyName("Product 3");
				productT.setDescription("Product 3 description...");
				productT.setBarcode("TEST03");
				productT.setMeasure(measureFraction);
				productT.setProductGroup(productGroup);
				productT.setDenominator(new BigDecimal(8));

				productFraction = productService.persist(productT);
				
				Price price = priceServece.findPrice(division1, productT);
				price.setValue(new BigDecimal(5));
				price.setValueNumrator(new BigDecimal(1));
				price = priceServece.merge(price);
			}	
			
			// Product with sub accounts 
			{
				Product productT = new Product();
				productT.setKeyName("BOOTS");
				productT.setDescription("BOOTS description...");
				productT.setBarcode("TEST04");
				productT.setMeasure(measureInteger);
				productT.setProductGroup(productGroup);
				productT.setCountedAttribute(countedAttributeShoesSize);

				productIntegerShoes = productService.persist(productT);
				
				Price price = priceServece.findPrice(division1, productT);
				price.setValue(new BigDecimal(4000));
				price = priceServece.merge(price);
			}
			
			// Product with sub accounts 
			{
				Product productT = new Product();
				productT.setKeyName("OIL");
				productT.setDescription("OIL description...");
				productT.setBarcode("TEST06");
				productT.setMeasure(measureFloat);
				productT.setProductGroup(productGroup);
				productT.setCountedAttribute(countedAttributeColor);

				productFloatOil = productService.persist(productT);
				
				Price price = priceServece.findPrice(division1, productT);
				price.setValue(new BigDecimal(150));
				price = priceServece.merge(price);
			}
			
			
			// Product with sub accounts
			{
				Product productT = new Product();
				productT.setKeyName("MULTIVITAMIIN");
				productT.setDescription("Multivitamin tablets description...");
				productT.setBarcode("TEST05");
				productT.setMeasure(measureFraction);
				productT.setProductGroup(productGroup);
				productT.setDenominator(new BigDecimal(16));
				productT.setCountedAttribute(countedAttributeColor);

				productFractionTablet = productService.persist(productT);
				
				Price price = priceServece.findPrice(division1, productT);
				price.setValue(new BigDecimal(160));
				price.setValueNumrator(new BigDecimal(15));
				price = priceServece.merge(price);
			}
			
		}
		
		// Currency
		{
			{
				Currency currencyT = new Currency();
				currencyT.setKeyName("KZ");
				currencyT.setDescription("tenge");
				currencyKZ = currencyService.persist(currencyT);
			}
			
			{
				Currency currencyT = new Currency();
				currencyT.setKeyName("RU");
				currencyT.setDescription("ruble");
				currencyRU = currencyService.persist(currencyT);
			}
			
			{
				Currency currencyT = new Currency();
				currencyT.setKeyName("UA");
				currencyT.setDescription("uane");
				currencyUA = currencyService.persist(currencyT);
			}
		}

		// Customer

		{
			Customer customerT = new Customer();
			customerT.setKeyName("Customer name");
			customerT.setfName("Test fName");
			customerT.setnName("Test nName");
			customerT.setDescription("Customer description...");
			customer = customerService.persist(customerT);

		}
		
		// Warehouse
		{
			Warehouse warehouseT = new Warehouse();
			warehouseT.setKeyName("warehouse 1");
			warehouseT.setDescription("Warehouse description 1...");
			warehouseT.setDivision(division1);
			warehouse1 = warehouseService.persist(warehouseT);
		}
		
		{
			Warehouse warehouseT = new Warehouse();
			warehouseT.setKeyName("warehouse 2");
			warehouseT.setDescription("Warehouse description 2...");
			warehouseT.setDivision(division1);
			warehouse2 = warehouseService.persist(warehouseT);
		}
		
		{
			Warehouse warehouseT = new Warehouse();
			warehouseT.setKeyName("warehouse 3");
			warehouseT.setDescription("Warehouse description 3...");
			warehouseT.setDivision(division2);
			warehouse3 = warehouseService.persist(warehouseT);
		}
		
		
		
		
		// Operations
		
		// BEGIN PRIVILEGES
		
		
		
		
		// END	

	}
	
	
	
	
	
	
}
