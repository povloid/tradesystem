package tradesystem.tradeunit.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import pk.home.libs.combine.dao.ABaseDAO.SortOrderType;
import tradesystem.tradeunit.basetestutils.BaseTest;
import tradesystem.tradeunit.domain.Cash;
import tradesystem.tradeunit.domain.Cash_;

/**
 * JUnit test service class for entity class: Cash Cash - касса
 */
@RunWith(SpringJUnit4ClassRunner.class)
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
		DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class })
@Transactional
@ActiveProfiles({ "Dev" })
@ContextConfiguration(locations = { "file:./src/main/resources/applicationContext.xml" })
public class TestCashService extends BaseTest {

	/**
	 * The DAO being tested, injected by Spring
	 * 
	 */
	private CashService service;

	/**
	 * Method to allow Spring to inject the DAO that will be tested
	 * 
	 */
	@Autowired
	public void setservice(CashService service) {
		this.service = service;
	}

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#getAllEntities()}.
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testGetAllEntities() throws Exception {
		createTestEntitys();

		long index = service.count();
		for (int i = 0; i < 100; i++) {
			Cash cash = new Cash();
			cash.setKeyName("key " + i);
			cash.setDivision(division1);
			service.persist(cash);
			index++;
		}

		List<Cash> list = service.getAllEntities();

		assertTrue(list != null);
		assertTrue(list.size() > 0);
		assertTrue(list.size() == index);
	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#getAllEntities(javax.persistence.metamodel.SingularAttribute, pk.home.libs.combine.dao.ABaseDAO.SortOrderType)}
	 * .
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testGetAllEntitiesSingularAttributeOfTQSortOrderType()
			throws Exception {
		createTestEntitys();

		long index = service.count();
		for (int i = 0; i < 100; i++) {
			Cash cash = new Cash();
			cash.setKeyName("key " + i);
			cash.setDivision(division1);
			service.persist(cash);
			index++;
		}

		List<Cash> list = service.getAllEntities(Cash_.id, SortOrderType.ASC);

		assertTrue(list != null);
		assertTrue(list.size() > 0);
		assertTrue(list.size() == index);

		long lastId = 0;
		for (Cash cash : list) {
			assertTrue(lastId < cash.getId());
			lastId = cash.getId();
		}
	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#getAllEntities(int, int)}.
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testGetAllEntitiesIntInt() throws Exception {
		createTestEntitys();

		// int index = 0;
		for (int i = 0; i < 100; i++) {
			Cash cash = new Cash();
			cash.setKeyName("key " + i);
			cash.setDivision(division1);
			service.persist(cash);
			// index++;
		}

		List<Cash> list = service.getAllEntities(10, 10);

		assertTrue(list != null);
		assertTrue(list.size() > 0);
		assertTrue(list.size() == 10);
	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#getAllEntities(int, int, javax.persistence.metamodel.SingularAttribute, pk.home.libs.combine.dao.ABaseDAO.SortOrderType)}
	 * .
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testGetAllEntitiesIntIntSingularAttributeOfTQSortOrderType()
			throws Exception {
		createTestEntitys();

		// long index = service.count();
		for (int i = 0; i < 100; i++) {
			Cash cash = new Cash();
			cash.setKeyName("key " + i);
			cash.setDivision(division1);
			service.persist(cash);
			// index++;
		}

		List<Cash> list = service.getAllEntities(10, 10, Cash_.id,
				SortOrderType.ASC);

		assertTrue(list != null);
		assertTrue(list.size() > 0);
		assertTrue(list.size() == 10);

		long lastId = 0;
		for (Cash cash : list) {
			assertTrue(lastId < cash.getId());
			lastId = cash.getId();
		}
	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#getAllEntities(boolean, int, int, javax.persistence.metamodel.SingularAttribute, pk.home.libs.combine.dao.ABaseDAO.SortOrderType)}
	 * .
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testGetAllEntitiesBooleanIntIntSingularAttributeOfTQSortOrderType()
			throws Exception {
		createTestEntitys();

		long index = service.count();
		for (int i = 0; i < 100; i++) {
			Cash cash = new Cash();
			cash.setKeyName("key " + i);
			cash.setDivision(division1);
			service.persist(cash);
			index++;
		}

		// all - FALSE
		List<Cash> list = service.getAllEntities(false, 10, 10, Cash_.id,
				SortOrderType.ASC);

		assertTrue(list != null);
		assertTrue(list.size() > 0);
		assertTrue(list.size() == 10);

		long lastId = 0;
		for (Cash cash : list) {
			assertTrue(lastId < cash.getId());
			lastId = cash.getId();
		}

		// all - TRUE
		list = service
				.getAllEntities(true, 10, 10, Cash_.id, SortOrderType.ASC);

		assertTrue(list != null);
		assertTrue(list.size() > 0);
		assertTrue(list.size() == index);

		lastId = 0;
		for (Cash cash : list) {
			assertTrue(lastId < cash.getId());
			lastId = cash.getId();
		}
	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#find(java.lang.Object)}.
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testFind() throws Exception {
		createTestEntitys();

		Cash cash = new Cash();
		cash.setKeyName("key " + 999);
		cash.setDivision(division1);
		cash = service.persist(cash);

		long id = cash.getId();

		Cash cash2 = service.find(id);

		assertEquals(cash, cash2);
		assertTrue(cash.getId() == cash2.getId());
		assertEquals(cash.getKeyName(), cash2.getKeyName());

	}

	/**
	 * Test method for {@link pk.home.libs.combine.dao.ABaseDAO#count()}.
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testCount() throws Exception {
		createTestEntitys();

		long index = service.count();
		for (int i = 0; i < 100; i++) {
			Cash cash = new Cash();
			cash.setKeyName("key " + i);
			cash.setDivision(division1);
			service.persist(cash);
			index++;
		}

		long count = service.count();

		assertTrue(count == index);
	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#persist(java.lang.Object)}.
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testPersist() throws Exception {
		createTestEntitys();

		Cash cash = new Cash();
		cash.setKeyName("key " + 999);
		cash.setDivision(division1);
		cash = service.persist(cash);

		long id = cash.getId();

		Cash cash2 = service.find(id);

		assertEquals(cash, cash2);
		assertTrue(cash.getId() == cash2.getId());
		assertEquals(cash.getKeyName(), cash2.getKeyName());
	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#refresh(java.lang.Object)}.
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testRefresh() throws Exception {
		createTestEntitys();

		Cash cash = new Cash();
		cash.setKeyName("key " + 999);
		cash.setDivision(division1);
		cash = service.persist(cash);

		long id = cash.getId();

		Cash cash2 = service.find(id);

		assertEquals(cash, cash2);
		assertTrue(cash.getId() == cash2.getId());
		assertEquals(cash.getKeyName(), cash2.getKeyName());

		cash2.setKeyName("key 65535");
		cash2 = service.refresh(cash2);

		assertEquals(cash, cash2);
		assertTrue(cash.getId() == cash2.getId());
		assertEquals(cash.getKeyName(), cash2.getKeyName());

	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#merge(java.lang.Object)}.
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testMerge() throws Exception {
		createTestEntitys();

		Cash cash = new Cash();
		cash.setKeyName("key " + 999);
		cash.setDivision(division1);
		cash = service.persist(cash);

		long id = cash.getId();

		Cash cash2 = service.find(id);

		assertEquals(cash, cash2);
		assertTrue(cash.getId() == cash2.getId());
		assertEquals(cash.getKeyName(), cash2.getKeyName());

		cash2.setKeyName("key 65535");
		cash2 = service.merge(cash2);

		cash = service.refresh(cash);

		assertEquals(cash, cash2);
		assertTrue(cash.getId() == cash2.getId());
		assertEquals(cash.getKeyName(), cash2.getKeyName());
	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#remove(java.lang.Object)}.
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testRemove() throws Exception {
		createTestEntitys();

		Cash cash = new Cash();
		cash.setKeyName("key " + 999);
		cash.setDivision(division1);
		cash = service.persist(cash);

		long id = cash.getId();

		Cash cash2 = service.find(id);

		assertEquals(cash, cash2);
		assertTrue(cash.getId() == cash2.getId());
		assertEquals(cash.getKeyName(), cash2.getKeyName());

		service.remove(cash);

		Cash cash3 = service.find(id);
		assertTrue(cash3 == null);

	}

	// -----------------------------------------------------------------------------------------------------------------

	@Test
	@Rollback(true)
	public void insertEntities() throws Exception {
		createTestEntitys();

		long index = service.count();
		for (int i = 200; i < 210; i++) {
			Cash cash = new Cash();
			cash.setKeyName("key " + i);
			cash.setDivision(division1);
			service.persist(cash);
			index++;
		}

		List<Cash> list = service.getAllEntities();

		assertTrue(list != null);
		assertTrue(list.size() > 0);
		assertTrue(list.size() == index);
	}

}
