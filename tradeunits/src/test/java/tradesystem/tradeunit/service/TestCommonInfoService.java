package tradesystem.tradeunit.service;

import java.math.BigDecimal;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import pk.home.libs.combine.dao.ABaseDAO.SortOrderType;
import pk.home.libs.combine.service.tuple.TupleElementS;
import pk.home.libs.combine.service.tuple.TupleS;
import tradesystem.tradeunit.basetestutils.BaseTest;
import tradesystem.tradeunit.domain.Account;
import tradesystem.tradeunit.domain.Account_;
import tradesystem.tradeunit.domain.Price;
import tradesystem.tradeunit.domain.Price_;
import tradesystem.tradeunit.domain.Product_;

/**
 * JUnit test service class for entity class: Account Account - счет
 */
@RunWith(SpringJUnit4ClassRunner.class)
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
		DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class })
@Transactional
@ActiveProfiles({ "Dev" })
@ContextConfiguration(locations = { "file:./src/main/resources/applicationContext.xml" })
public class TestCommonInfoService extends BaseTest{
	
	
	@Autowired
	private CommonInfoService commonInfoService;
	
	
	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}
	
	/**
	 * Prints the list.
	 *
	 * @param list the list
	 */
	private void printList(List<TupleS> list){
		System.out.println(list.size() + " ---------------------------------------------------------------------");
		 
		if(list.size() > 0){
			System.out.print("| ");
			for(TupleS te: list){
				for(TupleElementS tes : te)
					System.out.print(tes.getAlias() + " | ");
				break;
			}
			System.out.println();
		}
		
		for (TupleS tuple : list) {
			System.out.print("| ");
			for (TupleElementS o : tuple) {
				System.out.print(tuple.get(o.getAlias()) + " | ");
			}
			System.out.println();
		}
	}
	
	
	/**
	 * Test products in division1.
	 *
	 * @throws Exception the exception
	 */
	@Test
	@Rollback(true)
	public void testProductsInDivision() throws Exception {
		createTestEntitys();
		
		List<TupleS> list = commonInfoService.productsInDivision( division1, null, false, true,0,0,Product_.id,SortOrderType.ASC);
		 
		printList(list);
		
		Account account = accountService.findAccount(division1, productInteger, warehouse1);
		account = accountService.add(account, new BigDecimal(1), new BigDecimal(0));
		
		account = accountService.findAccount(division1, productFloat, warehouse1);
		account = accountService.add(account, new BigDecimal(1.1), new BigDecimal(0));
		
		account = accountService.findAccount(division1, productFraction, warehouse1);
		account = accountService.add(account, new BigDecimal(1), new BigDecimal(1));
		
		account = accountService.findAccount(division1, productIntegerShoes, warehouse1);
		account = accountService.findAccount(division1, productFractionTablet, warehouse1);
		
		
		accountService.findAccount(division2, productInteger, warehouse1);
		accountService.findAccount(division2, productFloat, warehouse1);
		accountService.findAccount(division2, productFraction, warehouse1);
		accountService.findAccount(division2, productIntegerShoes, warehouse1);
		accountService.findAccount(division2, productFractionTablet, warehouse1);
		
		list =  commonInfoService.productsInDivision(division1, null, false, true,0,0,Account_.id,SortOrderType.ASC);
		
		printList(list);
		
		Price price = priceServece.findPrice(division1, productInteger);
		price.setValue(new BigDecimal(120));
		priceServece.merge(price);
		
		price = priceServece.findPrice(division1, productFloat);
		price.setValue(new BigDecimal(120));
		priceServece.merge(price);
		
		price = priceServece.findPrice(division1, productFraction);
		price.setValue(new BigDecimal(120));
		priceServece.merge(price);
		
		price = priceServece.findPrice(division1, productIntegerShoes);
		price.setValue(new BigDecimal(120));
		priceServece.merge(price);
		
		price = priceServece.findPrice(division1, productFractionTablet);
		price.setValue(new BigDecimal(120));
		priceServece.merge(price);
		
		
		
		priceServece.findPrice(division2, productInteger);
		priceServece.findPrice(division2, productFloat);
		priceServece.findPrice(division2, productFraction);
		priceServece.findPrice(division2, productIntegerShoes);
		priceServece.findPrice(division2, productFractionTablet);
		
		list =  commonInfoService.productsInDivision(division1, null, false, true,0,0, Price_.id ,SortOrderType.ASC);	
		 
		printList(list);
		
		
		account = accountService.findAccount(division1, productInteger, warehouse2);
		account = accountService.add(account, new BigDecimal(1), new BigDecimal(0));
		
		account = accountService.findAccount(division1, productFloat, warehouse2);
		account = accountService.add(account, new BigDecimal(1.1), new BigDecimal(0));
		
		account = accountService.findAccount(division1, productFraction, warehouse2);
		account = accountService.add(account, new BigDecimal(1), new BigDecimal(1));
		
		account = accountService.findAccount(division1, productIntegerShoes, warehouse2);
		account = accountService.findAccount(division1, productFractionTablet, warehouse2);
		

		
		list =  commonInfoService.productsInDivision(division1, null, false, true,0,0, Price_.id ,SortOrderType.ASC);	
		 
		printList(list);
		

	}
	
	
	
	

}
