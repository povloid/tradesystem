package tradesystem.tradeunit.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.transaction.NotSupportedException;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import pk.home.libs.combine.dao.ABaseDAO.SortOrderType;
import tradesystem.tradeunit.basetestutils.BaseTest;
import tradesystem.tradeunit.domain.Account;
import tradesystem.tradeunit.domain.CountedAttribute;
import tradesystem.tradeunit.domain.Item;
import tradesystem.tradeunit.domain.Item_;
import tradesystem.tradeunit.domain.Order;
import tradesystem.tradeunit.domain.OrderType;

/**
 * JUnit test service class for entity class: Item
 * Item - запись
 */
@RunWith(SpringJUnit4ClassRunner.class)
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
		DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class })
@Transactional
@ActiveProfiles({"Dev"})
@ContextConfiguration(locations = { "file:./src/main/resources/applicationContext.xml" })
public class TestItemService extends BaseTest{

	/**
	 * The DAO being tested, injected by Spring
	 * 
	 */
	private ItemService service;

	/**
	 * Method to allow Spring to inject the DAO that will be tested
	 * 
	 */
	@Autowired
	public void setservice(ItemService service) {
		this.service = service;
	}

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}
	
	
	/**
	 * Prints the.
	 *
	 * @param list the list
	 */
	private void print(Collection<Item> list){
		System.out.println();
		for(Item i: list)
			System.out.println(i.getAccount().getId()
					+ "\t" + (i.getAccount().getParentAccount() == null ? "null" : i.getAccount().getParentAccount().getId())
					+ "\t" + i.getAccount().getValue() + " " + i.getAccount().getNumerator() + "/" + i.getAccount().getCountsObject().getDenominator()
					+ "\t" + (i.getAccount().getParentAccount() == null ? i.getAccount().getCountsObject() : i.getAccount().getCountedAttribute().getKeyName()));
	}
	
	
	/**
	 * Test sort accounts.
	 *
	 * @throws Exception the exception
	 */
	@Test
	@Rollback(true)
	public void testSortForCountedAttributes() throws Exception {
		createTestEntitys();
		
		Account accountInteger = accountService.findAccount(division1,
				productInteger, warehouse1);
		
		Account accountFloat = accountService.findAccount(division1,
				productFloat, warehouse1);
		
		Account accountFraction = accountService.findAccount(division1,
				productFraction, warehouse1);
		
		
		Account accountFloatOil = accountService.findAccount(division1,
				productFloatOil, warehouse1);

		Account accountShoes = accountService.findAccount(division1,
				productIntegerShoes, warehouse1);
		
		Account accountFractionTablet = accountService.findAccount(division1,
				productFractionTablet, warehouse1);
		
		
		Collection<Item> list = new ArrayList<>();
		
		list.add(new Item(accountInteger));
		
		list.add(new Item(accountFloatOil));
		
		
		for(Account a: accountFloatOil.getSubAccounts())
			list.add(new Item(a));
		
		list.add(new Item(accountShoes));
		
		list.add(new Item(accountFraction));
		
		for(Account a: accountShoes.getSubAccounts())
			list.add(new Item(a));
		
		for(Account a: accountFractionTablet.getSubAccounts())
			list.add(new Item(a));
		
		list.add(new Item(accountFractionTablet));
		
		list.add(new Item(accountFloat));
		
		
		// TEST
		
		print(list);
		
		list = service.sortForCountedAttributes(list, false);
		
		print(list);
		
		CountedAttribute parent = null;
		for(Item a: list){
			if(a.getAccount().getParentAccount() == null)
				parent = a.getCountedAttribute();
			else {
				assertNotNull(a.getCountedAttribute());
				assertEquals(a.getCountedAttribute().getParent(), parent);
			}
		}
		
		list = service.sortForCountedAttributes(list, true);
		
		parent = null;
		for(Item a: list){
			if(a.getAccount().getParentAccount() == null)
				parent = a.getCountedAttribute();
			else {
				assertNotNull(a.getCountedAttribute());
				assertNotEquals(a.getCountedAttribute().getParent(), parent);
			}
		}
		
		print(list);
		
		
		
		
	}
	
	

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#getAllEntities()}.
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testGetAllEntities() throws Exception {
		createTestEntitys();

		long index = service.count();
		for (int i = 0; i < 100; i++) {
			
			BigDecimal value = new BigDecimal(1);
			BigDecimal numerator = new BigDecimal(0);
		
			Order order = orderDAO.persist(new Order(new Date(), OrderType.PRODUCT_PLUS, division1, userAccount, "Order description"));
			Account account = accountService.findAccount(division1, productInteger, warehouse1);
			account = accountService.add(account, value, numerator);
			
			Item item = new Item();
			item.setAccount(account);
			item.setOrder(order);
			item.setValue(value);
			item.setNumerator(numerator);
			item.setDescription("key " + i);
			item.setMeasure(account.getProduct().getMeasure());
			
			service.persist(item);
			index++;
		}

		List<Item> list = service.getAllEntities();

		assertTrue(list != null);
		assertTrue(list.size() > 0);
		assertTrue(list.size() == index);
	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#getAllEntities(javax.persistence.metamodel.SingularAttribute, pk.home.libs.combine.dao.ABaseDAO.SortOrderType)}
	 * .
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testGetAllEntitiesSingularAttributeOfTQSortOrderType()
			throws Exception {
		createTestEntitys();
		
		long index = service.count();
		for (int i = 0; i < 100; i++) {
			BigDecimal value = new BigDecimal(1);
			BigDecimal numerator = new BigDecimal(0);
		
			Order order = orderDAO.persist(new Order(new Date(), OrderType.PRODUCT_PLUS, division1, userAccount, "Order description"));
			Account account = accountService.findAccount(division1, productInteger, warehouse1);
			account = accountService.add(account, value, numerator);
			
			Item item = new Item();
			item.setAccount(account);
			item.setOrder(order);
			item.setValue(value);
			item.setNumerator(numerator);
			item.setDescription("key " + i);
			item.setMeasure(account.getProduct().getMeasure());
			
			service.persist(item);
			
			index++;
		}

		List<Item> list = service.getAllEntities(Item_.id, SortOrderType.ASC);

		assertTrue(list != null);
		assertTrue(list.size() > 0);
		assertTrue(list.size() == index);

		long lastId = 0;
		for (Item item : list) {
			assertTrue(lastId < item.getId());
			lastId = item.getId();
		}
	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#getAllEntities(int, int)}.
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testGetAllEntitiesIntInt() throws Exception {
		createTestEntitys();

		// int index = 0;
		for (int i = 0; i < 100; i++) {
			BigDecimal value = new BigDecimal(1);
			BigDecimal numerator = new BigDecimal(0);
		
			Order order = orderDAO.persist(new Order(new Date(), OrderType.PRODUCT_PLUS, division1, userAccount, "Order description"));
			Account account = accountService.findAccount(division1, productInteger, warehouse1);
			account = accountService.add(account, value, numerator);
			
			Item item = new Item();
			item.setAccount(account);
			item.setOrder(order);
			item.setValue(value);
			item.setNumerator(numerator);
			item.setDescription("key " + i);
			item.setMeasure(account.getProduct().getMeasure());
			
			service.persist(item);
			// index++;
		}

		List<Item> list = service.getAllEntities(10, 10);

		assertTrue(list != null);
		assertTrue(list.size() > 0);
		assertTrue(list.size() == 10);
	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#getAllEntities(int, int, javax.persistence.metamodel.SingularAttribute, pk.home.libs.combine.dao.ABaseDAO.SortOrderType)}
	 * .
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testGetAllEntitiesIntIntSingularAttributeOfTQSortOrderType()
			throws Exception {
		createTestEntitys();
		
		// long index = service.count();
		for (int i = 0; i < 100; i++) {
			BigDecimal value = new BigDecimal(1);
			BigDecimal numerator = new BigDecimal(0);
		
			Order order = orderDAO.persist(new Order(new Date(), OrderType.PRODUCT_PLUS, division1, userAccount, "Order description"));
			Account account = accountService.findAccount(division1, productInteger, warehouse1);
			account = accountService.add(account, value, numerator);
			
			Item item = new Item();
			item.setAccount(account);
			item.setOrder(order);
			item.setValue(value);
			item.setNumerator(numerator);
			item.setDescription("key " + i);
			item.setMeasure(account.getProduct().getMeasure());
			
			service.persist(item);
			// index++;
		}

		List<Item> list = service.getAllEntities(10, 10, Item_.id,
				SortOrderType.ASC);

		assertTrue(list != null);
		assertTrue(list.size() > 0);
		assertTrue(list.size() == 10);

		long lastId = 0;
		for (Item item : list) {
			assertTrue(lastId < item.getId());
			lastId = item.getId();
		}
	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#getAllEntities(boolean, int, int, javax.persistence.metamodel.SingularAttribute, pk.home.libs.combine.dao.ABaseDAO.SortOrderType)}
	 * .
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testGetAllEntitiesBooleanIntIntSingularAttributeOfTQSortOrderType()
			throws Exception {
		createTestEntitys();
		
		long index = service.count();
		for (int i = 0; i < 100; i++) {
			BigDecimal value = new BigDecimal(1);
			BigDecimal numerator = new BigDecimal(0);
		
			Order order = orderDAO.persist(new Order(new Date(), OrderType.PRODUCT_PLUS, division1, userAccount, "Order description"));
			Account account = accountService.findAccount(division1, productInteger, warehouse1);
			account = accountService.add(account, value, numerator);
			
			Item item = new Item();
			item.setAccount(account);
			item.setOrder(order);
			item.setValue(value);
			item.setNumerator(numerator);
			item.setDescription("key " + i);
			item.setMeasure(account.getProduct().getMeasure());
			
			service.persist(item);
			index++;
		}

		// all - FALSE
		List<Item> list = service.getAllEntities(false, 10, 10, Item_.id,
				SortOrderType.ASC);

		assertTrue(list != null);
		assertTrue(list.size() > 0);
		assertTrue(list.size() == 10);

		long lastId = 0;
		for (Item item : list) {
			assertTrue(lastId < item.getId());
			lastId = item.getId();
		}

		// all - TRUE
		list = service.getAllEntities(true, 10, 10, Item_.id,
				SortOrderType.ASC);

		assertTrue(list != null);
		assertTrue(list.size() > 0);
		assertTrue(list.size() == index);

		lastId = 0;
		for (Item item : list) {
			assertTrue(lastId < item.getId());
			lastId = item.getId();
		}
	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#find(java.lang.Object)}.
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testFind() throws Exception {
		createTestEntitys();

		BigDecimal value = new BigDecimal(1);
		BigDecimal numerator = new BigDecimal(0);
	
		Order order = orderDAO.persist(new Order(new Date(), OrderType.PRODUCT_PLUS, division1, userAccount, "Order description"));
		Account account = accountService.findAccount(division1, productInteger, warehouse1);
		account = accountService.add(account, value, numerator);
		
		Item item = new Item();
		item.setAccount(account);
		item.setOrder(order);
		item.setValue(value);
		item.setNumerator(numerator);
		item.setDescription("key ");
		item.setMeasure(account.getProduct().getMeasure());
		
		item = service.persist(item);

		long id = item.getId();

		Item item2 = service.find(id);

		assertEquals(item, item2);
		assertTrue(item.getId() == item2.getId());
		assertEquals(item.getDescription(), item2.getDescription());

	}

	/**
	 * Test method for {@link pk.home.libs.combine.dao.ABaseDAO#count()}.
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testCount() throws Exception {
		createTestEntitys();
		
		long index = service.count();
		for (int i = 0; i < 100; i++) {
			BigDecimal value = new BigDecimal(1);
			BigDecimal numerator = new BigDecimal(0);
		
			Order order = orderDAO.persist(new Order(new Date(), OrderType.PRODUCT_PLUS, division1, userAccount, "Order description"));
			Account account = accountService.findAccount(division1, productInteger, warehouse1);
			account = accountService.add(account, value, numerator);
			
			Item item = new Item();
			item.setAccount(account);
			item.setOrder(order);
			item.setValue(value);
			item.setNumerator(numerator);
			item.setDescription("key " + i);
			item.setMeasure(account.getProduct().getMeasure());
			
			service.persist(item);
			index++;
		}

		long count = service.count();

		assertTrue(count == index);
	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#persist(java.lang.Object)}.
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testPersist() throws Exception {
		createTestEntitys();
		
		BigDecimal value = new BigDecimal(1);
		BigDecimal numerator = new BigDecimal(0);
	
		Order order = orderDAO.persist(new Order(new Date(), OrderType.PRODUCT_PLUS, division1, userAccount, "Order description"));
		Account account = accountService.findAccount(division1, productInteger, warehouse1);
		account = accountService.add(account, value, numerator);
		
		Item item = new Item();
		item.setAccount(account);
		item.setOrder(order);
		item.setValue(value);
		item.setNumerator(numerator);
		item.setDescription("key ");
		item.setMeasure(account.getProduct().getMeasure());
		
		item = service.persist(item);

		long id = item.getId();

		Item item2 = service.find(id);

		assertEquals(item, item2);
		assertTrue(item.getId() == item2.getId());
		assertEquals(item.getDescription(), item2.getDescription());
	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#refresh(java.lang.Object)}.
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testRefresh() throws Exception {
		createTestEntitys();
		
		BigDecimal value = new BigDecimal(1);
		BigDecimal numerator = new BigDecimal(0);
	
		Order order = orderDAO.persist(new Order(new Date(), OrderType.PRODUCT_PLUS, division1, userAccount, "Order description"));
		Account account = accountService.findAccount(division1, productInteger, warehouse1);
		account = accountService.add(account, value, numerator);
		
		Item item = new Item();
		item.setAccount(account);
		item.setOrder(order);
		item.setValue(value);
		item.setNumerator(numerator);
		item.setDescription("key ");
		item.setMeasure(account.getProduct().getMeasure());
		
		item = service.persist(item);

		long id = item.getId();

		Item item2 = service.find(id);

		assertEquals(item, item2);
		assertTrue(item.getId() == item2.getId());
		assertEquals(item.getDescription(), item2.getDescription());

		item2.setDescription("key 65535");
		item2 = service.refresh(item2);

		assertEquals(item, item2);
		assertTrue(item.getId() == item2.getId());
		assertEquals(item.getDescription(), item2.getDescription());

	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#merge(java.lang.Object)}.
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testMerge() throws Exception {
		createTestEntitys();
		
		BigDecimal value = new BigDecimal(1);
		BigDecimal numerator = new BigDecimal(0);
	
		Order order = orderDAO.persist(new Order(new Date(), OrderType.PRODUCT_PLUS, division1, userAccount, "Order description"));
		Account account = accountService.findAccount(division1, productInteger, warehouse1);
		account = accountService.add(account, value, numerator);
		
		Item item = new Item();
		item.setAccount(account);
		item.setOrder(order);
		item.setValue(value);
		item.setNumerator(numerator);
		item.setDescription("key ");
		item.setMeasure(account.getProduct().getMeasure());
		
		try{
			item = service.merge(item);
			
			assertTrue("Operation merge is supported!",false);
			
		}catch(NotSupportedException ex){
			assertTrue(true);
		}

		
	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#remove(java.lang.Object)}.
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testRemove() throws Exception {
		createTestEntitys();
		
		BigDecimal value = new BigDecimal(1);
		BigDecimal numerator = new BigDecimal(0);
	
		Order order = orderDAO.persist(new Order(new Date(), OrderType.PRODUCT_PLUS, division1, userAccount, "Order description"));
		Account account = accountService.findAccount(division1, productInteger, warehouse1);
		account = accountService.add(account, value, numerator);
		
		Item item = new Item();
		item.setAccount(account);
		item.setOrder(order);
		item.setValue(value);
		item.setNumerator(numerator);
		item.setDescription("key ");
		item.setMeasure(account.getProduct().getMeasure());
		
		try{
			service.remove(item);
			
			assertTrue("Operation merge is supported!",false);
			
		}catch(NotSupportedException ex){
			assertTrue(true);
		}


	}
	
	
	
	// -----------------------------------------------------------------------------------------------------------------
	
	@Test
	@Rollback(true)
	public void insertEntities() throws Exception {
		createTestEntitys();
		

		long index = service.count();
		for (int i = 200; i < 210; i++) {
			BigDecimal value = new BigDecimal(1);
			BigDecimal numerator = new BigDecimal(0);
		
			Order order = orderDAO.persist(new Order(new Date(), OrderType.PRODUCT_PLUS, division1, userAccount, "Order description"));
			Account account = accountService.findAccount(division1, productInteger, warehouse1);
			account = accountService.add(account, value, numerator);
			
			Item item = new Item();
			item.setAccount(account);
			item.setOrder(order);
			item.setValue(value);
			item.setNumerator(numerator);
			item.setDescription("key ");
			item.setMeasure(account.getProduct().getMeasure());
			
			service.persist(item);
			index++;
		}

		List<Item> list = service.getAllEntities();

		assertTrue(list != null);
		assertTrue(list.size() > 0);
		assertTrue(list.size() == index);
	}
	


}
