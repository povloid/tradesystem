package tradesystem.tradeunit.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import pk.home.libs.combine.dao.ABaseDAO.SortOrderType;
import tradesystem.tradeunit.basetestutils.BaseTest;
import tradesystem.tradeunit.domain.Privileges;
import tradesystem.tradeunit.domain.Privileges_;

/**
 * JUnit test service class for entity class: Privileges
 * Privileges - привелегии
 */
@RunWith(SpringJUnit4ClassRunner.class)
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
		DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class })
@Transactional
@ActiveProfiles({"Dev"})
@ContextConfiguration(locations = { "file:./src/main/resources/applicationContext.xml" })
public class TestPrivilegesService extends BaseTest{

	/**
	 * The DAO being tested, injected by Spring
	 * 
	 */
	private PrivilegesService service;

	/**
	 * Method to allow Spring to inject the DAO that will be tested
	 * 
	 */
	@Autowired
	public void setDataStore(PrivilegesService service) {
		this.service = service;
	}

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	
	@Test
	@Rollback(true)
	public void testGetType() throws Exception {
		createTestEntitys();
		
		
		
		{
			Privileges privileges = new Privileges();
			privileges.setDivision(division1);
			privileges.setUserAccount(userAccount);
			privileges.setWarehouse(warehouse1);

			service.persist(privileges);
		}
		
		{
			Privileges privileges = new Privileges();
			privileges.setDivision(division1);
			privileges.setUserAccount(userAccount);
			privileges.setCash(cash);

			service.persist(privileges);
		}	
		
		try {
			Privileges privileges = new Privileges();
			privileges.setDivision(division2);
			privileges.setUserAccount(userAccount);
			privileges.setWarehouse(warehouse2);

			service.persist(privileges);
			
			assertTrue(false);
		} catch (Exception ex) {
			assertTrue(true);
		}
		

		{
			long count = service.count();
			
			List<Privileges> list = service.getAllEntities();

			assertTrue(list != null);
			assertTrue(list.size() > 0);
			assertTrue(list.size() == count);
		}
		
		

		{
			List<Privileges> list = service.getType(userAccount, division1, warehouse1);

			assertTrue(list != null);
			assertTrue(list.size() > 0);
			assertTrue(list.size() == 1);
		}
		
		
		{
			List<Privileges> list = service.getType(userAccount, division1, cash);

			assertTrue(list != null);
			assertTrue(list.size() > 0);
			assertTrue(list.size() == 1);
		}
		
		
		
		
		
		
	}
	
	
	
	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#getAllEntities()}.
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testGetAllEntities() throws Exception {
		createTestEntitys();

		long index = service.count();
		for (int i = 0; i < 100; i++) {
			Privileges privileges = new Privileges();
			privileges.setDivision(division1);
			privileges.setUserAccount(userAccount);
			
			service.persist(privileges);
			index++;
		}

		List<Privileges> list = service.getAllEntities();

		assertTrue(list != null);
		assertTrue(list.size() > 0);
		assertTrue(list.size() == index);
	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#getAllEntities(javax.persistence.metamodel.SingularAttribute, pk.home.libs.combine.dao.ABaseDAO.SortOrderType)}
	 * .
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testGetAllEntitiesSingularAttributeOfTQSortOrderType()
			throws Exception {
		createTestEntitys();
		
		long index = service.count();
		for (int i = 0; i < 100; i++) {
			Privileges privileges = new Privileges();
			privileges.setDivision(division1);
			privileges.setUserAccount(userAccount);
			service.persist(privileges);
			index++;
		}

		List<Privileges> list = service.getAllEntities(Privileges_.id, SortOrderType.ASC);

		assertTrue(list != null);
		assertTrue(list.size() > 0);
		assertTrue(list.size() == index);

		long lastId = 0;
		for (Privileges privileges : list) {
			assertTrue(lastId < privileges.getId());
			lastId = privileges.getId();
		}
	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#getAllEntities(int, int)}.
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testGetAllEntitiesIntInt() throws Exception {
		createTestEntitys();

		// int index = 0;
		for (int i = 0; i < 100; i++) {
			Privileges privileges = new Privileges();
			privileges.setDivision(division1);
			privileges.setUserAccount(userAccount);
			service.persist(privileges);
			// index++;
		}

		List<Privileges> list = service.getAllEntities(10, 10);

		assertTrue(list != null);
		assertTrue(list.size() > 0);
		assertTrue(list.size() == 10);
	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#getAllEntities(int, int, javax.persistence.metamodel.SingularAttribute, pk.home.libs.combine.dao.ABaseDAO.SortOrderType)}
	 * .
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testGetAllEntitiesIntIntSingularAttributeOfTQSortOrderType()
			throws Exception {
		createTestEntitys();
		
		// long index = service.count();
		for (int i = 0; i < 100; i++) {
			Privileges privileges = new Privileges();
			privileges.setDivision(division1);
			privileges.setUserAccount(userAccount);
			service.persist(privileges);
			// index++;
		}

		List<Privileges> list = service.getAllEntities(10, 10, Privileges_.id,
				SortOrderType.ASC);

		assertTrue(list != null);
		assertTrue(list.size() > 0);
		assertTrue(list.size() == 10);

		long lastId = 0;
		for (Privileges privileges : list) {
			assertTrue(lastId < privileges.getId());
			lastId = privileges.getId();
		}
	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#getAllEntities(boolean, int, int, javax.persistence.metamodel.SingularAttribute, pk.home.libs.combine.dao.ABaseDAO.SortOrderType)}
	 * .
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testGetAllEntitiesBooleanIntIntSingularAttributeOfTQSortOrderType()
			throws Exception {
		createTestEntitys();
		
		long index = service.count();
		for (int i = 0; i < 100; i++) {
			Privileges privileges = new Privileges();
			privileges.setDivision(division1);
			privileges.setUserAccount(userAccount);
			service.persist(privileges);
			index++;
		}

		// all - FALSE
		List<Privileges> list = service.getAllEntities(false, 10, 10, Privileges_.id,
				SortOrderType.ASC);

		assertTrue(list != null);
		assertTrue(list.size() > 0);
		assertTrue(list.size() == 10);

		long lastId = 0;
		for (Privileges privileges : list) {
			assertTrue(lastId < privileges.getId());
			lastId = privileges.getId();
		}

		// all - TRUE
		list = service.getAllEntities(true, 10, 10, Privileges_.id,
				SortOrderType.ASC);

		assertTrue(list != null);
		assertTrue(list.size() > 0);
		assertTrue(list.size() == index);

		lastId = 0;
		for (Privileges privileges : list) {
			assertTrue(lastId < privileges.getId());
			lastId = privileges.getId();
		}
	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#find(java.lang.Object)}.
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testFind() throws Exception {
		createTestEntitys();

		Privileges privileges = new Privileges();
		privileges.setDivision(division1);
		privileges.setUserAccount(userAccount);
		privileges = service.persist(privileges);

		long id = privileges.getId();

		Privileges privileges2 = service.find(id);

		assertEquals(privileges, privileges2);
		assertTrue(privileges.getId() == privileges2.getId());

	}

	/**
	 * Test method for {@link pk.home.libs.combine.dao.ABaseDAO#count()}.
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testCount() throws Exception {
		createTestEntitys();
		
		long index = service.count();
		for (int i = 0; i < 100; i++) {
			Privileges privileges = new Privileges();
			privileges.setDivision(division1);
			privileges.setUserAccount(userAccount);
			service.persist(privileges);
			index++;
		}

		long count = service.count();

		assertTrue(count == index);
	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#persist(java.lang.Object)}.
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testPersist() throws Exception {
		createTestEntitys();
		
		Privileges privileges = new Privileges();
		privileges.setDivision(division1);
		privileges.setUserAccount(userAccount);
		privileges = service.persist(privileges);

		long id = privileges.getId();

		Privileges privileges2 = service.find(id);

		assertEquals(privileges, privileges2);
		assertTrue(privileges.getId() == privileges2.getId());
	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#refresh(java.lang.Object)}.
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testRefresh() throws Exception {
		createTestEntitys();
		
		Privileges privileges = new Privileges();
		privileges.setDivision(division1);
		privileges.setUserAccount(userAccount);
		privileges = service.persist(privileges);

		long id = privileges.getId();

		Privileges privileges2 = service.find(id);

		assertEquals(privileges, privileges2);
		assertTrue(privileges.getId() == privileges2.getId());

		privileges2 = service.refresh(privileges2);

		assertEquals(privileges, privileges2);
		assertTrue(privileges.getId() == privileges2.getId());

	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#merge(java.lang.Object)}.
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testMerge() throws Exception {
		createTestEntitys();
		
		Privileges privileges = new Privileges();
		privileges.setDivision(division1);
		privileges.setUserAccount(userAccount);
		privileges = service.persist(privileges);

		long id = privileges.getId();

		Privileges privileges2 = service.find(id);

		assertEquals(privileges, privileges2);
		assertTrue(privileges.getId() == privileges2.getId());

		privileges2 = service.merge(privileges2);

		privileges = service.refresh(privileges);

		assertEquals(privileges, privileges2);
		assertTrue(privileges.getId() == privileges2.getId());
	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#remove(java.lang.Object)}.
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testRemove() throws Exception {
		createTestEntitys();
		
		Privileges privileges = new Privileges();
		privileges.setDivision(division1);
		privileges.setUserAccount(userAccount);
		privileges = service.persist(privileges);

		long id = privileges.getId();

		Privileges privileges2 = service.find(id);

		assertEquals(privileges, privileges2);
		assertTrue(privileges.getId() == privileges2.getId());

		service.remove(privileges);

		Privileges privileges3 = service.find(id);
		assertTrue(privileges3 == null);

	}
	
	
	
	// -----------------------------------------------------------------------------------------------------------------
	
	@Test
	@Rollback(true)
	public void insertEntities() throws Exception {
		createTestEntitys();

		long index = service.count();
		for (int i = 200; i < 210; i++) {
			Privileges privileges = new Privileges();
			privileges.setDivision(division1);
			privileges.setUserAccount(userAccount);
			service.persist(privileges);
			index++;
		}

		List<Privileges> list = service.getAllEntities();

		assertTrue(list != null);
		assertTrue(list.size() > 0);
		assertTrue(list.size() == index);
	}
	

}
