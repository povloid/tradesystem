package tradesystem.tradeunit.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.transaction.NotSupportedException;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import pk.home.libs.combine.dao.ABaseDAO.SortOrderType;
import tradesystem.tradeunit.basetestutils.BaseTest;
import tradesystem.tradeunit.domain.Account;
import tradesystem.tradeunit.domain.Account_;
import tradesystem.tradeunit.domain.CountedAttribute;

/**
 * JUnit test service class for entity class: Account Account - счет
 */
@RunWith(SpringJUnit4ClassRunner.class)
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
		DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class })
@Transactional
@ActiveProfiles({ "Dev" })
@ContextConfiguration(locations = { "file:./src/main/resources/applicationContext.xml" })
public class TestAccountService extends BaseTest {

	// TODO: Написать тесты для различных владельцев Owner

	/**
	 * The DAO being tested, injected by Spring
	 * 
	 */
	private AccountService service;

	/**
	 * Method to allow Spring to inject the DAO that will be tested
	 * 
	 */
	@Autowired
	public void setDataStore(AccountService service) {
		this.service = service;
	}

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test create test entitys.
	 * 
	 * @throws Exception
	 *             the exception
	 */
	@Test
	@Rollback(true)
	public void testCreateTestEntitys() throws Exception {
		createTestEntitys();
	}

	/**
	 * Testfind account.
	 * 
	 * @throws Exception
	 *             the exception
	 */
	@Test
	@Rollback(true)
	public void testfindAccount() throws Exception {
		createTestEntitys();

		long size = service.count();

		Account accountProduct = service.findAccount(division1, productInteger,
				warehouse1);
		Account accountProduct2 = service.findAccount(division1, productInteger,
				warehouse1);
		++size;
		assertEquals(accountProduct, accountProduct2);
		assertTrue(size == service.count());

		Account accountCurrencyKZ = service.findAccount(division1, currencyKZ,
				cash);
		Account accountCurrencyKZ2 = service.findAccount(division1, currencyKZ,
				cash);
		++size;
		assertEquals(accountCurrencyKZ, accountCurrencyKZ2);
		assertTrue(size == service.count());

		assertNotEquals(accountProduct, accountCurrencyKZ);
	}

	
	
	/**
	 * Prints the.
	 *
	 * @param list the list
	 */
	private void print(Collection<Account> list){
		System.out.println();
		for(Account a: list)
			System.out.println(a.getId()
					+ "\t" + (a.getParentAccount() == null ? "null" : a.getParentAccount().getId())
					+ "\t" + a.getValue() + " " + a.getNumerator() + "/" + a.getCountsObject().getDenominator()
					+ "\t" + (a.getParentAccount() == null ? a.getCountsObject() : a.getCountedAttribute().getKeyName()));
	}
	
	
	/**
	 * Test sort accounts.
	 *
	 * @throws Exception the exception
	 */
	@Test
	@Rollback(true)
	public void testSortAccounts() throws Exception {
		createTestEntitys();
		
		Account accountInteger = service.findAccount(division1,
				productInteger, warehouse1);
		
		Account accountFloat = service.findAccount(division1,
				productFloat, warehouse1);
		
		Account accountFraction = service.findAccount(division1,
				productFraction, warehouse1);
		
		
		Account accountFloatOil = service.findAccount(division1,
				productFloatOil, warehouse1);

		Account accountShoes = service.findAccount(division1,
				productIntegerShoes, warehouse1);
		
		Account accountFractionTablet = service.findAccount(division1,
				productFractionTablet, warehouse1);
		
		
		Collection<Account> list = new ArrayList<>();
		
		list.add(accountInteger);
		
		list.add(accountFloatOil);
		list.addAll(accountFloatOil.getSubAccounts());
		
		list.add(accountShoes);
		
		list.add(accountFraction);
		
		list.addAll(accountShoes.getSubAccounts());
		
		list.addAll(accountFractionTablet.getSubAccounts());
		list.add(accountFractionTablet);
		
		list.add(accountFloat);
		
		
		// TEST
		
		print(list);
		
		list = accountService.sortAccounts(list, false);
		
		print(list);
		
		Account parent = null;
		for(Account a: list){
			if(a.getParentAccount() == null)
				parent = a;
			else {
				assertNotNull(a.getParentAccount());
				assertEquals(a.getParentAccount(), parent);
			}
		}
		
		list = accountService.sortAccounts(list, true);
		
		parent = null;
		for(Account a: list){
			if(a.getParentAccount() == null)
				parent = a;
			else {
				assertNotNull(a.getParentAccount());
				assertNotEquals(a.getParentAccount(), parent);
			}
		}
		
		print(list);
		
		
		
		
	}
	
	
	
	
	
	/**
	 * Test add.
	 * 
	 * @throws Exception
	 *             the exception
	 */
	@Test
	@Rollback(true)
	public void testAdd() throws Exception {
		createTestEntitys();

		final BigDecimal zero = new BigDecimal(0);

		final BigDecimal one = new BigDecimal(1);
		final BigDecimal _one = new BigDecimal(-1);

		final BigDecimal two = new BigDecimal(2);
		final BigDecimal _two = new BigDecimal(-2);

		final BigDecimal v8 = new BigDecimal(8);
		final BigDecimal _v8 = new BigDecimal(-8);

		final BigDecimal v10 = new BigDecimal(10);
		final BigDecimal _v10 = new BigDecimal(-10);

		final BigDecimal v15 = new BigDecimal(15);
		final BigDecimal _v15 = new BigDecimal(-15);

		final BigDecimal v25 = new BigDecimal(25);
		final BigDecimal _v25 = new BigDecimal(-25);

		final BigDecimal v100 = new BigDecimal(100);
		final BigDecimal _v100 = new BigDecimal(-100);

		final BigDecimal v101 = new BigDecimal(101);
		final BigDecimal _v101 = new BigDecimal(-101);

		final BigDecimal v2_5 = new BigDecimal(2.5);
		final BigDecimal _v2_5 = new BigDecimal(-2.5);

		final BigDecimal v10_25 = new BigDecimal(100.256666).setScale(Account.SCALE, RoundingMode.HALF_UP);
		final BigDecimal _v10_25 = new BigDecimal(-100.256666).setScale(Account.SCALE, RoundingMode.HALF_UP);

		Account accountProductInteger = service.findAccount(division1,
				productInteger, warehouse1);
		Account accountProductFloat = service.findAccount(division1,
				productFloat, warehouse1);
		Account accountProductFraction = service.findAccount(division1,
				productFraction, warehouse1);
		
		// Neens for math tests
		accountProductInteger.setCredit(true);
		accountProductFloat.setCredit(true);
		accountProductFraction.setCredit(true);
		

		{
			Account account = service.add(accountProductInteger, zero, null);
			assertTrue(account.getValue().longValue() == zero.longValue());
		}

		{
			Account account = service.add(accountProductInteger, one, null);
			assertTrue(account.getValue().longValue() == one.longValue());
			
			assertTrue(account.getValue().compareTo(one) == 0);

			account = service.add(accountProductInteger, one, null);
			assertTrue(account.getValue().longValue() == two.longValue());
			
			assertTrue(account.getValue().compareTo(two) == 0);

			account = service.add(accountProductInteger, _one, null);
			assertTrue(account.getValue().longValue() == one.longValue());
			
			assertTrue(account.getValue().compareTo(one) == 0);

			account = service.add(accountProductInteger, one, null);
			assertTrue(account.getValue().longValue() == two.longValue());
			
			assertTrue(account.getValue().compareTo(two) == 0);

			account = service.add(accountProductInteger, v8, null);
			assertTrue(account.getValue().longValue() == v10.longValue());
			
			assertTrue(account.getValue().compareTo(v10) == 0);

			account = service.add(accountProductInteger, _v8, null);
			account = service.add(accountProductInteger, _two, null);
			assertTrue(account.getValue().longValue() == zero.longValue());
			
			assertTrue(account.getValue().compareTo(zero) == 0);

			account = service.add(accountProductInteger, _v100, null);
			assertTrue(account.getValue().longValue() == _v100.longValue());
			
			assertTrue(account.getValue().compareTo(_v100) == 0);

			account = service.add(accountProductInteger, _v15, null);
			assertTrue(account.getValue().longValue() == new BigDecimal(-115)
					.longValue());
			
			assertTrue(account.getValue().compareTo(new BigDecimal(-115)) == 0);

			account = service.add(accountProductInteger, _v25, null);
			assertTrue(account.getValue().longValue() == new BigDecimal(-140)
					.longValue());
			
			assertTrue(account.getValue().compareTo(new BigDecimal(-140)) == 0);

			account = service.add(accountProductInteger, v25, null);
			assertTrue(account.getValue().longValue() == new BigDecimal(-115)
					.longValue());
			
			assertTrue(account.getValue().compareTo(new BigDecimal(-115)) == 0);

			try {
				account = service.add(accountProductInteger, v10_25, null);
				assertTrue("Was added not integer value for INTEGER", false);
			} catch (Exception ex) {
				assertTrue(true);
			}

		}

		{
			Account account = service.add(accountProductFloat, one, null);
			assertTrue(account.getValue().doubleValue() == one.doubleValue());
			
			assertTrue(account.getValue().compareTo(one) == 0);

			account = service.add(accountProductFloat, one, null);
			assertTrue(account.getValue().doubleValue() == two.doubleValue());
			
			assertTrue(account.getValue().compareTo(two) == 0);

			account = service.add(accountProductFloat, _one, null);
			assertTrue(account.getValue().doubleValue() == one.doubleValue());
			
			assertTrue(account.getValue().compareTo(one) == 0);

			account = service.add(accountProductFloat, one, null);
			assertTrue(account.getValue().doubleValue() == two.doubleValue());
			
			assertTrue(account.getValue().compareTo(two) == 0);

			account = service.add(accountProductFloat, v8, null);
			assertTrue(account.getValue().doubleValue() == v10.doubleValue());
			
			assertTrue(account.getValue().compareTo(v10) == 0);

			account = service.add(accountProductFloat, _v8, null);
			account = service.add(accountProductFloat, _two, null);
			assertTrue(account.getValue().doubleValue() == zero.doubleValue());
			
			assertTrue(account.getValue().compareTo(zero) == 0);

			account = service.add(accountProductFloat, _v100, null);
			assertTrue(account.getValue().doubleValue() == _v100.doubleValue());
			
			assertTrue(account.getValue().compareTo(_v100) == 0);

			account = service.add(accountProductFloat, v100, null);
			assertTrue(account.getValue().doubleValue() == zero.doubleValue());
			
			assertTrue(account.getValue().compareTo(zero) == 0);

			account = service.add(accountProductFloat, v2_5, null);
			assertTrue(account.getValue().doubleValue() == v2_5.doubleValue());
			
			assertTrue(account.getValue().compareTo(v2_5) == 0);

			account = service.add(accountProductFloat, v2_5, null);
			account = service.add(accountProductFloat, v2_5, null);
			account = service.add(accountProductFloat, v2_5, null);

			assertTrue(account.getValue().doubleValue() == v10.doubleValue());
			assertTrue(account.getValue().compareTo(v10) == 0);

			account = service.add(accountProductFloat, v2_5, null);
			account = service.add(accountProductFloat, v2_5, null);

			assertTrue(account.getValue().doubleValue() == v15.doubleValue());
			assertTrue(account.getValue().compareTo(v15) == 0);

			account = service.add(accountProductFloat, _v2_5, null);
			account = service.add(accountProductFloat, _v2_5, null);

			assertTrue(account.getValue().doubleValue() == v10.doubleValue());
			assertTrue(account.getValue().compareTo(v10) == 0);

			account = service.add(accountProductFloat, _v10, null);
			assertTrue(account.getValue().doubleValue() == zero.doubleValue());
			assertTrue(account.getValue().compareTo(zero) == 0);

			account = service.add(accountProductFloat, v10_25, null);
			assertTrue(account.getValue().doubleValue() == v10_25.doubleValue());
			assertTrue(account.getValue().compareTo(v10_25) == 0);

			account = service.add(accountProductFloat, _v10_25, null);
			assertTrue(account.getValue().doubleValue() == zero.doubleValue());
			assertTrue(account.getValue().compareTo(zero) == 0);

		}

		{
			Account account = service.add(accountProductFraction, one, null);
			assertTrue(account.getValue().longValue() == one.longValue());
			
			assertTrue(account.getValue().compareTo(one) == 0);
			// 1

			account = service.add(accountProductFraction, one, null);
			assertTrue(account.getValue().longValue() == two.longValue());
			
			assertTrue(account.getValue().compareTo(two) == 0);
			// 2

			account = service.add(accountProductFraction, _one, null);
			assertTrue(account.getValue().longValue() == one.longValue());
			
			assertTrue(account.getValue().compareTo(one) == 0);
			// 1

			account = service.add(accountProductFraction, one, null);
			assertTrue(account.getValue().longValue() == two.longValue());
			
			assertTrue(account.getValue().compareTo(two) == 0);
			// 2

			account = service.add(accountProductFraction, v8, null);
			assertTrue(account.getValue().longValue() == v10.longValue());
			
			assertTrue(account.getValue().compareTo(v10) == 0);
			// 10

			account = service.add(accountProductFraction, _v8, null);
			account = service.add(accountProductFraction, _two, null);
			assertTrue(account.getValue().longValue() == zero.longValue());
			
			assertTrue(account.getValue().compareTo(zero) == 0);
			// 0

			account = service.add(accountProductFraction, _v100, null);
			assertTrue(account.getValue().longValue() == _v100.longValue());
			
			assertTrue(account.getValue().compareTo(_v100) == 0);

			// -100

			account = service.add(accountProductFraction, v100, null);
			// 0

			account = service.add(accountProductFraction, v15, two);
			assertTrue(account.getValue().longValue() == v15.longValue());
			assertTrue(account.getNumerator().longValue() == two.longValue());
			
			
			assertTrue(account.getValue().compareTo(new BigDecimal(15)) == 0);
			assertTrue(account.getNumerator().compareTo(new BigDecimal(2)) == 0);
			// 15 2/8

			// 15 2/8 + 10 10/8
			account = service.add(accountProductFraction, v10, v10);
			assertTrue(account.getValue().longValue() == new BigDecimal(26)
					.longValue());
			assertTrue(account.getNumerator().longValue() == new BigDecimal(4)
					.longValue());
			
			
			assertTrue(account.getValue().compareTo(new BigDecimal(26)) == 0);
			assertTrue(account.getNumerator().compareTo(new BigDecimal(4)) == 0);
			// 26 4/8

			// 26 4/8 + 0 100/8
			account = service.add(accountProductFraction, zero, v100);
			assertTrue(account.getValue().longValue() == new BigDecimal(13 + 26)
					.longValue());
			assertTrue(account.getNumerator().longValue() == zero.longValue());
			
			
			assertTrue(account.getValue().compareTo(new BigDecimal(13 + 26)) == 0);
			assertTrue(account.getNumerator().compareTo(new BigDecimal(0)) == 0);
			// 39 0/8

			// 39 0/8 - 0 -100/8
			account = service.add(accountProductFraction, zero, _v100);
			System.out.println(accountProductFraction.getValue().longValue());
			System.out.println(accountProductFraction.getNumerator()
					.longValue());

			assertTrue(account.getValue().longValue() == new BigDecimal(26)
					.longValue());
			assertTrue(account.getNumerator().longValue() == new BigDecimal(4)
					.longValue());
			
			
			assertTrue(account.getValue().compareTo(new BigDecimal(26)) == 0);
			assertTrue(account.getNumerator().compareTo(new BigDecimal(4)) == 0);
			// 26 4/8

			// 26 4/8 + 0 101/8
			account = service.add(accountProductFraction, zero, v101);
			assertTrue(account.getValue().longValue() == new BigDecimal(13 + 26)
					.longValue());
			assertTrue(account.getNumerator().longValue() == one.longValue());
			
			
			assertTrue(account.getValue().compareTo(new BigDecimal(13 + 26)) == 0);
			assertTrue(account.getNumerator().compareTo(new BigDecimal(1)) == 0);
			// 39 1/8

			// 39 1/8 - 0 -101/8
			account = service.add(accountProductFraction, zero, _v101);
			System.out.println(accountProductFraction.getValue().longValue());
			System.out.println(accountProductFraction.getNumerator()
					.longValue());

			assertTrue(account.getValue().longValue() == new BigDecimal(26)
					.longValue());
			assertTrue(account.getNumerator().longValue() == new BigDecimal(4)
					.longValue());
			
			
			assertTrue(account.getValue().compareTo(new BigDecimal(26)) == 0);
			assertTrue(account.getNumerator().compareTo(new BigDecimal(4)) == 0);
			// 26 4/8

			// 26 4/8 - 26 -4/8
			account = service.add(accountProductFraction, new BigDecimal(-26),
					new BigDecimal(-4));
			System.out.println(accountProductFraction.getValue().longValue());
			System.out.println(accountProductFraction.getNumerator()
					.longValue());

			assertTrue(account.getValue().longValue() == zero.longValue());
			assertTrue(account.getNumerator().longValue() == zero.longValue());
			
			
			assertTrue(account.getValue().compareTo(new BigDecimal(0)) == 0);
			assertTrue(account.getNumerator().compareTo(new BigDecimal(0)) == 0);
			// 0 0/8

			// 0 0/8 -6 -5/8
			account = service.add(accountProductFraction, new BigDecimal(-6),
					new BigDecimal(-5));
			System.out.println(accountProductFraction.getValue().longValue());
			System.out.println(accountProductFraction.getNumerator()
					.longValue());

			assertTrue(account.getValue().longValue() == new BigDecimal(-6)
					.longValue());
			assertTrue(account.getNumerator().longValue() == new BigDecimal(-5)
					.longValue());
			
			
			assertTrue(account.getValue().compareTo(new BigDecimal(-6)) == 0);
			assertTrue(account.getNumerator().compareTo(new BigDecimal(-5)) == 0);
			// -6 -5/8

			// -6 -5/8 2 3/8
			account = service.add(accountProductFraction, new BigDecimal(2),
					new BigDecimal(3));
			System.out.println(accountProductFraction.getValue().longValue());
			System.out.println(accountProductFraction.getNumerator()
					.longValue());

			assertTrue(account.getValue().longValue() == new BigDecimal(-4)
					.longValue());
			assertTrue(account.getNumerator().longValue() == new BigDecimal(-2)
					.longValue());
			
			
			assertTrue(account.getValue().compareTo(new BigDecimal(-4)) == 0);
			assertTrue(account.getNumerator().compareTo(new BigDecimal(-2)) == 0);
			// -4 -2/8

			// -4 -2/8 0 13/8 (1 5/8) ()
			account = service.add(accountProductFraction, new BigDecimal(0),
					new BigDecimal(13));
			System.out.println(accountProductFraction.getValue().longValue());
			System.out.println(accountProductFraction.getNumerator()
					.longValue());

			assertTrue(account.getValue().longValue() == new BigDecimal(-2)
					.longValue());
			assertTrue(account.getNumerator().longValue() == new BigDecimal(-5)
					.longValue());
			
			
			assertTrue(account.getValue().compareTo(new BigDecimal(-2)) == 0);
			assertTrue(account.getNumerator().compareTo(new BigDecimal(-5)) == 0);
			// -2 -5/8

			// -2 -5/8 2 2/8
			account = service.add(accountProductFraction, new BigDecimal(2),
					new BigDecimal(2));
			System.out.println(accountProductFraction.getValue().longValue());
			System.out.println(accountProductFraction.getNumerator()
					.longValue());

			assertTrue(account.getValue().longValue() == new BigDecimal(0)
					.longValue());
			assertTrue(account.getNumerator().longValue() == new BigDecimal(-3)
					.longValue());
			
			
			assertTrue(account.getValue().compareTo(new BigDecimal(0)) == 0);
			assertTrue(account.getNumerator().compareTo(new BigDecimal(-3)) == 0);
			// -0 -3/8

			// -0 -3/8 -0 4/8
			account = service.add(accountProductFraction, new BigDecimal(0),
					new BigDecimal(4));
			System.out.println(accountProductFraction.getValue().longValue());
			System.out.println(accountProductFraction.getNumerator()
					.longValue());

			assertTrue(account.getValue().longValue() == new BigDecimal(0)
					.longValue());
			assertTrue(account.getNumerator().longValue() == new BigDecimal(1)
					.longValue());
			
			
			assertTrue(account.getValue().compareTo(new BigDecimal(0)) == 0);
			assertTrue(account.getNumerator().compareTo(new BigDecimal(1)) == 0);
			// 0 1/8

			// 0 1/8 -0 -16/8
			account = service.add(accountProductFraction, new BigDecimal(0),
					new BigDecimal(-16));
			System.out.println(accountProductFraction.getValue().longValue());
			System.out.println(accountProductFraction.getNumerator()
					.longValue());

			assertTrue(account.getValue().longValue() == new BigDecimal(-1)
					.longValue());
			assertTrue(account.getNumerator().longValue() == new BigDecimal(-7)
					.longValue());
			
			
			assertTrue(account.getValue().compareTo(new BigDecimal(-1)) == 0);
			assertTrue(account.getNumerator().compareTo(new BigDecimal(-7)) == 0);
			// -1 -7/8

			account = service.add(accountProductFraction, new BigDecimal(-400),
					new BigDecimal(-1000));
			account = service.add(accountProductFraction, new BigDecimal(400),
					new BigDecimal(1000));
			assertTrue(account.getValue().longValue() == new BigDecimal(-1)
					.longValue());
			assertTrue(account.getNumerator().longValue() == new BigDecimal(-7)
					.longValue());
			
			
			assertTrue(account.getValue().compareTo(new BigDecimal(-1)) == 0);
			assertTrue(account.getNumerator().compareTo(new BigDecimal(-7)) == 0);

			try {
				account = service.add(accountProductFraction, v10_25,
						new BigDecimal(0));
				assertTrue("Was added not integer value for INTEGER", false);
			} catch (Exception ex) {
				assertTrue(true);
			}

			try {
				account = service.add(accountProductFraction,
						new BigDecimal(0), v10_25);
				assertTrue("Was added not integer value for INTEGER", false);
			} catch (Exception ex) {
				assertTrue(true);
			}

		}
	}

	/**
	 * Test sub accounts.
	 * 
	 * @throws Exception
	 *             the exception
	 */
	@Test
	@Rollback(true)
	public void testSubAccountsFloatLongScale() throws Exception {
		createTestEntitys();

		Account accountFloatOil = service.findAccount(division1,
				productFloatOil, warehouse1);

		assertTrue(accountFloatOil.getSubAccounts().size() == productFloatOil
				.getCountedAttribute().getChildren().size());

		// Duplicate test
		Account accountFloatOilDublicate = service.findAccount(division1,
				productFloatOil, warehouse1);

		assertTrue(accountFloatOilDublicate.getSubAccounts().size() == productFloatOil
				.getCountedAttribute().getChildren().size());
		assertEquals(accountFloatOil, accountFloatOilDublicate);

		// add new count attribute test
		int size1 = accountFloatOilDublicate.getSubAccounts().size();

		{

			CountedAttribute countedAttribute = new CountedAttribute();
			countedAttribute.setKeyName("BROWN");
			countedAttribute.setDescription("BROWN COLOR");
			countedAttribute.setParent(countedAttributeColor);
			countedAttribute = countedAttributeServece
					.persist(countedAttribute);

			countedAttributeColor.getChildren().add(countedAttribute);

		}

		accountFloatOilDublicate = service.findAccount(division1,
				productFloatOil, warehouse1);

		assertTrue(accountFloatOilDublicate.getSubAccounts().size() > size1);
		assertTrue(accountFloatOilDublicate.getSubAccounts().size() - size1 == 1);
		assertEquals(accountFloatOil, accountFloatOilDublicate);

		// Add sub account test

		Account subAccount1 = accountFloatOil.getSubAccounts().get(0);
		Account subAccount2 = accountFloatOil.getSubAccounts().get(1);
		Account subAccount3 = accountFloatOil.getSubAccounts().get(2);

		subAccount1 = service.add(subAccount1, new BigDecimal(
				1.1111111111111111111), null);
		subAccount2 = service.add(subAccount2, new BigDecimal(
				2.2222222222222222222), null);
		subAccount3 = service.add(subAccount3, new BigDecimal(
				3.6666666666666666666), null);

		BigDecimal oldValue = accountFloatOil.getValue();

		try {
			accountFloatOil = service.add(accountFloatOil, new BigDecimal(
					3.121111111111111111111111), null);

			assertTrue("Bad operation", false);
		} catch (Exception ex) {
			assertTrue("Operation is ok!", true);
		}

		assertTrue(oldValue.compareTo(accountFloatOil.getValue()) == 0);

		System.out.println(">>>" + subAccount1.getValue());
		System.out.println(">>>" + subAccount2.getValue());
		System.out.println(">>>" + subAccount3.getValue());

		System.out.println(">>>"
				+ subAccount1.getValue().add(subAccount2.getValue())
						.add(subAccount3.getValue()));

		accountFloatOil = service.add(accountFloatOil, subAccount1.getValue().add(subAccount2.getValue()).add(subAccount3.getValue()), null );

		assertTrue(oldValue.compareTo(accountFloatOil.getValue()) != 0);
	}

	/**
	 * Test sub accounts.
	 * 
	 * @throws Exception
	 *             the exception
	 */
	@Test
	@Rollback(true)
	public void testSubAccounts() throws Exception {
		createTestEntitys();

		{ // INTEGER TEST
			Account accountIntegerShoes = service.findAccount(division1,
					productIntegerShoes, warehouse1);

			assertTrue(accountIntegerShoes.getSubAccounts().size() == productIntegerShoes
					.getCountedAttribute().getChildren().size());

			// Duplicate test
			Account accountIntegerShoesDublicate = service.findAccount(
					division1, productIntegerShoes, warehouse1);

			assertTrue(accountIntegerShoesDublicate.getSubAccounts().size() == productIntegerShoes
					.getCountedAttribute().getChildren().size());
			assertEquals(accountIntegerShoes, accountIntegerShoesDublicate);
			assertEquals(accountIntegerShoes.getId(), accountIntegerShoesDublicate.getId());

			// add new count attribute test
			int size1 = accountIntegerShoesDublicate.getSubAccounts().size();

			{

				CountedAttribute countedAttribute = new CountedAttribute();
				countedAttribute.setKeyName("41");
				countedAttribute.setDescription("41 size");
				countedAttribute.setParent(countedAttributeShoesSize);
				countedAttribute = countedAttributeServece
						.persist(countedAttribute);

				countedAttributeShoesSize.getChildren().add(countedAttribute);

			}

			accountIntegerShoesDublicate = service.findAccount(division1,
					productIntegerShoes, warehouse1);

			assertTrue(accountIntegerShoesDublicate.getSubAccounts().size() > size1);
			assertTrue(accountIntegerShoesDublicate.getSubAccounts().size()
					- size1 == 1);
			assertEquals(accountIntegerShoes, accountIntegerShoesDublicate);

			// Add sub account test

			Account subAccount1 = accountIntegerShoes.getSubAccounts().get(0);
			Account subAccount2 = accountIntegerShoes.getSubAccounts().get(1);
			Account subAccount3 = accountIntegerShoes.getSubAccounts().get(2);

			subAccount1 = service.add(subAccount1, new BigDecimal(1), null);
			subAccount2 = service.add(subAccount2, new BigDecimal(2), null);
			subAccount3 = service.add(subAccount3, new BigDecimal(3), null);

			BigDecimal oldValue = accountIntegerShoes.getValue();

			try {
				accountIntegerShoes = service.add(accountIntegerShoes,
						new BigDecimal(3), null);

				assertTrue("Bad operation", false);
			} catch (Exception ex) {
				assertTrue("Operation is ok!", true);
			}

			assertTrue(oldValue.compareTo(accountIntegerShoes.getValue()) == 0);

			accountIntegerShoes = service.add(accountIntegerShoes,
					new BigDecimal(6), null);

			assertTrue(oldValue.compareTo(accountIntegerShoes.getValue()) != 0);
		}

		{ // FLOAT TEST 1
			Account accountFloatOil = service.findAccount(division1,
					productFloatOil, warehouse1);

			assertTrue(accountFloatOil.getSubAccounts().size() == productFloatOil
					.getCountedAttribute().getChildren().size());

			// Duplicate test
			Account accountFloatOilDublicate = service.findAccount(division1,
					productFloatOil, warehouse1);

			assertTrue(accountFloatOilDublicate.getSubAccounts().size() == productFloatOil
					.getCountedAttribute().getChildren().size());
			assertEquals(accountFloatOil, accountFloatOilDublicate);

			// add new count attribute test
			int size1 = accountFloatOilDublicate.getSubAccounts().size();

			{

				CountedAttribute countedAttribute = new CountedAttribute();
				countedAttribute.setKeyName("BROWN");
				countedAttribute.setDescription("BROWN COLOR");
				countedAttribute.setParent(countedAttributeColor);
				countedAttribute = countedAttributeServece
						.persist(countedAttribute);

				countedAttributeColor.getChildren().add(countedAttribute);

			}

			accountFloatOilDublicate = service.findAccount(division1,
					productFloatOil, warehouse1);

			assertTrue(accountFloatOilDublicate.getSubAccounts().size() > size1);
			assertTrue(accountFloatOilDublicate.getSubAccounts().size() - size1 == 1);
			assertEquals(accountFloatOil, accountFloatOilDublicate);

			// Add sub account test

			Account subAccount1 = accountFloatOil.getSubAccounts().get(0);
			Account subAccount2 = accountFloatOil.getSubAccounts().get(1);
			Account subAccount3 = accountFloatOil.getSubAccounts().get(2);

			subAccount1 = service.add(subAccount1, new BigDecimal(1.11), null);
			subAccount2 = service.add(subAccount2, new BigDecimal(2.22), null);
			subAccount3 = service.add(subAccount3, new BigDecimal(3.33), null);

			BigDecimal oldValue = accountFloatOil.getValue();

			try {
				accountFloatOil = service.add(accountFloatOil, new BigDecimal(
						3.12), null);

				assertTrue("Bad operation", false);
			} catch (Exception ex) {
				assertTrue("Operation is ok!", true);
			}

			assertTrue(oldValue.compareTo(accountFloatOil.getValue()) == 0);

			System.out.println(">>>" + subAccount1.getValue());
			System.out.println(">>>" + subAccount2.getValue());
			System.out.println(">>>" + subAccount3.getValue());

			System.out.println(">>>"
					+ subAccount1.getValue().add(subAccount2.getValue())
							.add(subAccount3.getValue()));

			accountFloatOil = service.add(accountFloatOil, new BigDecimal(6.66)
					.setScale(Account.SCALE, RoundingMode.HALF_UP), null);

			assertTrue(oldValue.compareTo(accountFloatOil.getValue()) != 0);
		}

		{ // FRACTION TEST
			Account accountFractionTablet = service.findAccount(division1,
					productFractionTablet, warehouse1);

			assertTrue(accountFractionTablet.getSubAccounts().size() == productFractionTablet
					.getCountedAttribute().getChildren().size());

			// Duplicate test
			Account accountFractionTabletDublicate = service.findAccount(
					division1, productFractionTablet, warehouse1);

			assertEquals(accountFractionTablet, accountFractionTabletDublicate);
			assertTrue(accountFractionTabletDublicate.getSubAccounts().size() == productFractionTablet
					.getCountedAttribute().getChildren().size());

			// add new count attribute test
			int size1 = accountFractionTabletDublicate.getSubAccounts().size();

			{

				CountedAttribute countedAttribute = new CountedAttribute();
				countedAttribute.setKeyName("BLACK");
				countedAttribute.setDescription("BLACK COLOR");
				countedAttribute.setParent(countedAttributeColor);
				countedAttribute = countedAttributeServece
						.persist(countedAttribute);

				countedAttributeColor.getChildren().add(countedAttribute);

			}

			accountFractionTabletDublicate = service.findAccount(division1,
					productFractionTablet, warehouse1);

			assertTrue(accountFractionTabletDublicate.getSubAccounts().size() > size1);
			assertTrue(accountFractionTabletDublicate.getSubAccounts().size()
					- size1 == 1);
			assertEquals(accountFractionTablet, accountFractionTabletDublicate);

			// Add sub account test

			Account subAccount1 = accountFractionTablet.getSubAccounts().get(0);
			Account subAccount2 = accountFractionTablet.getSubAccounts().get(1);
			Account subAccount3 = accountFractionTablet.getSubAccounts().get(2);

			subAccount1 = service.add(subAccount1, new BigDecimal(1),
					new BigDecimal(1));
			subAccount2 = service.add(subAccount2, new BigDecimal(2),
					new BigDecimal(2));
			subAccount3 = service.add(subAccount3, new BigDecimal(3),
					new BigDecimal(3));

			BigDecimal oldValue = accountFractionTablet.getValue();

			try {
				accountFractionTablet = service.add(accountFractionTablet,
						new BigDecimal(3), null);

				assertTrue("Bad operation", false);
			} catch (Exception ex) {
				assertTrue("Operation is ok!", true);
			}

			try {
				accountFractionTablet = service.add(accountFractionTablet,
						null, new BigDecimal(3));

				assertTrue("Bad operation", false);
			} catch (Exception ex) {
				assertTrue("Operation is ok!", true);
			}

			try {
				accountFractionTablet = service.add(accountFractionTablet,
						new BigDecimal(3), new BigDecimal(0));

				assertTrue("Bad operation", false);
			} catch (Exception ex) {
				assertTrue("Operation is ok!", true);
			}

			try {
				accountFractionTablet = service.add(accountFractionTablet,
						new BigDecimal(0), new BigDecimal(3));

				assertTrue("Bad operation", false);
			} catch (Exception ex) {
				assertTrue("Operation is ok!", true);
			}

			assertTrue(oldValue.compareTo(accountFractionTablet.getValue()) == 0);

			accountFractionTablet = service.add(accountFractionTablet,
					new BigDecimal(6), new BigDecimal(6));

			assertTrue(oldValue.compareTo(accountFractionTablet.getValue()) != 0);

			subAccount1 = service.add(subAccount1, new BigDecimal(1),
					new BigDecimal(1));
			subAccount2 = service.add(subAccount2, new BigDecimal(2),
					new BigDecimal(2));
			subAccount3 = service.add(subAccount3, new BigDecimal(16),
					new BigDecimal(16));

			accountFractionTablet = service.add(accountFractionTablet,
					new BigDecimal(19), new BigDecimal(19));

			subAccount1 = service.add(subAccount1, new BigDecimal(1),
					new BigDecimal(1));
			subAccount2 = service.add(subAccount2, new BigDecimal(2),
					new BigDecimal(2));
			subAccount3 = service.add(subAccount3, new BigDecimal(16),
					new BigDecimal(16));

			accountFractionTablet = service.add(accountFractionTablet,
					new BigDecimal(20), new BigDecimal(3));

			subAccount1 = service.add(subAccount1, new BigDecimal(1),
					new BigDecimal(1));
			subAccount2 = service.add(subAccount2, new BigDecimal(2),
					new BigDecimal(2));
			subAccount3 = service.add(subAccount3, new BigDecimal(16),
					new BigDecimal(16));

			try {
				accountFractionTablet = service.add(accountFractionTablet,
						new BigDecimal(0), new BigDecimal(324));

				assertTrue("Bad operation", false);
			} catch (Exception ex) {
				assertTrue("Operation is ok!", true);
			}

			accountFractionTablet = service.add(accountFractionTablet,
					new BigDecimal(0), new BigDecimal(323));
		}

	}

	/**
	 * Test for not supported operations.
	 * 
	 * @throws Exception
	 *             the exception
	 */
	@Test
	@Rollback(true)
	public void testForNotSupportedOperations() throws Exception {
		createTestEntitys();

		Account accountCash = service.findAccount(division1, productInteger,
				warehouse1);

		try {
			service.persist(accountCash);

			assertTrue("persist operation supported", false);
		} catch (NotSupportedException ex) {
			assertTrue(true);
		}

		try {
			service.merge(accountCash);

			assertTrue("persist operation supported", false);
		} catch (NotSupportedException ex) {
			assertTrue(true);
		}

		try {
			service.remove(accountCash);

			assertTrue("persist operation supported", false);
		} catch (NotSupportedException ex) {
			assertTrue(true);
		}
	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#getAllEntities()}.
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testGetAllEntities() throws Exception {
		createTestEntitys();

		long size = service.count();

		Account accountProductInteger = service.findAccount(division1,
				productInteger, warehouse1);
		++size;
		assertNotNull(accountProductInteger);
		assertTrue(size == service.count());

		Account accountProductFloat = service.findAccount(division1,
				productFloat, warehouse1);
		++size;
		assertNotNull(accountProductFloat);
		assertTrue(size == service.count());

		Account accountProductFraction = service.findAccount(division1,
				productFraction, warehouse1);
		++size;
		assertNotNull(accountProductFraction);
		assertTrue(size == service.count());

		Account accountCurrencyKZ = service.findAccount(division1, currencyKZ,
				cash);
		++size;
		assertEquals(accountCurrencyKZ, accountCurrencyKZ);
		assertTrue(size == service.count());

		List<Account> list = service.getAllEntities();

		assertTrue(list != null);
		assertTrue(list.size() > 0);
		assertTrue(list.size() == size);
	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#getAllEntities(javax.persistence.metamodel.SingularAttribute, pk.home.libs.combine.dao.ABaseDAO.SortOrderType)}
	 * .
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testGetAllEntitiesSingularAttributeOfTQSortOrderType()
			throws Exception {
		createTestEntitys();

		long size = service.count();

		Account accountProductInteger = service.findAccount(division1,
				productInteger, warehouse1);
		++size;
		assertNotNull(accountProductInteger);
		assertTrue(size == service.count());

		Account accountProductFloat = service.findAccount(division1,
				productFloat, warehouse1);
		++size;
		assertNotNull(accountProductFloat);
		assertTrue(size == service.count());

		Account accountProductFraction = service.findAccount(division1,
				productFraction, warehouse1);
		++size;
		assertNotNull(accountProductFraction);
		assertTrue(size == service.count());

		Account accountCurrencyKZ = service.findAccount(division1, currencyKZ,
				cash);
		++size;
		assertEquals(accountCurrencyKZ, accountCurrencyKZ);
		assertTrue(size == service.count());

		List<Account> list = service.getAllEntities(Account_.id,
				SortOrderType.ASC);

		assertTrue(list != null);
		assertTrue(list.size() > 0);
		assertTrue(list.size() == size);

		long lastId = 0;
		for (Account account : list) {
			assertTrue(lastId < account.getId());
			lastId = account.getId();
		}
	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#getAllEntities(int, int)}.
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testGetAllEntitiesIntInt() throws Exception {
		createTestEntitys();

		long size = service.count();

		Account accountProductInteger = service.findAccount(division1,
				productInteger, warehouse1);
		++size;
		assertNotNull(accountProductInteger);
		assertTrue(size == service.count());

		Account accountProductFloat = service.findAccount(division1,
				productFloat, warehouse1);
		++size;
		assertNotNull(accountProductFloat);
		assertTrue(size == service.count());

		Account accountProductFraction = service.findAccount(division1,
				productFraction, warehouse1);
		++size;
		assertNotNull(accountProductFraction);
		assertTrue(size == service.count());

		Account accountCurrencyKZ = service.findAccount(division1, currencyKZ,
				cash);
		++size;
		assertEquals(accountCurrencyKZ, accountCurrencyKZ);
		assertTrue(size == service.count());

		List<Account> list = service.getAllEntities(0, 2);

		assertTrue(list != null);
		assertTrue(list.size() > 0);
		assertTrue(list.size() == 2);
	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#getAllEntities(int, int, javax.persistence.metamodel.SingularAttribute, pk.home.libs.combine.dao.ABaseDAO.SortOrderType)}
	 * .
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testGetAllEntitiesIntIntSingularAttributeOfTQSortOrderType()
			throws Exception {
		createTestEntitys();

		long size = service.count();

		Account accountProductInteger = service.findAccount(division1,
				productInteger, warehouse1);
		++size;
		assertNotNull(accountProductInteger);
		assertTrue(size == service.count());

		Account accountProductFloat = service.findAccount(division1,
				productFloat, warehouse1);
		++size;
		assertNotNull(accountProductFloat);
		assertTrue(size == service.count());

		Account accountProductFraction = service.findAccount(division1,
				productFraction, warehouse1);
		++size;
		assertNotNull(accountProductFraction);
		assertTrue(size == service.count());

		Account accountCurrencyKZ = service.findAccount(division1, currencyKZ,
				cash);
		++size;
		assertEquals(accountCurrencyKZ, accountCurrencyKZ);
		assertTrue(size == service.count());

		List<Account> list = service.getAllEntities(0, 1, Account_.id,
				SortOrderType.ASC);

		assertTrue(list != null);
		assertTrue(list.size() > 0);
		assertTrue(list.size() == 1);

		long lastId = 0;
		for (Account account : list) {
			assertTrue(lastId < account.getId());
			lastId = account.getId();
		}
	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#getAllEntities(boolean, int, int, javax.persistence.metamodel.SingularAttribute, pk.home.libs.combine.dao.ABaseDAO.SortOrderType)}
	 * .
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testGetAllEntitiesBooleanIntIntSingularAttributeOfTQSortOrderType()
			throws Exception {
		createTestEntitys();

		long size = service.count();

		Account accountProductInteger = service.findAccount(division1,
				productInteger, warehouse1);
		++size;
		assertNotNull(accountProductInteger);
		assertTrue(size == service.count());

		Account accountProductFloat = service.findAccount(division1,
				productFloat, warehouse1);
		++size;
		assertNotNull(accountProductFloat);
		assertTrue(size == service.count());

		Account accountProductFraction = service.findAccount(division1,
				productFraction, warehouse1);
		++size;
		assertNotNull(accountProductFraction);
		assertTrue(size == service.count());

		Account accountCurrencyKZ = service.findAccount(division1, currencyKZ,
				cash);
		++size;
		assertEquals(accountCurrencyKZ, accountCurrencyKZ);
		assertTrue(size == service.count());

		// all - TRUE
		List<Account> list = service.getAllEntities(true, 1, 2, Account_.id,
				SortOrderType.ASC);

		assertTrue(list != null);
		assertTrue(list.size() > 0);
		assertTrue(list.size() == size);

		long lastId = 0;
		for (Account account : list) {
			assertTrue(lastId < account.getId());
			lastId = account.getId();
		}
	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#find(java.lang.Object)}.
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testFind() throws Exception {
		createTestEntitys();

		Account accountProductInteger = service.findAccount(division1,
				productInteger, warehouse1);
		assertNotNull(accountProductInteger);

		Account account = service.find(accountProductInteger.getId());

		assertEquals(accountProductInteger, account);
		assertTrue(accountProductInteger.getId() == account.getId());

	}

	/**
	 * Test method for {@link pk.home.libs.combine.dao.ABaseDAO#count()}.
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testCount() throws Exception {
		createTestEntitys();

		long size = service.count();

		Account accountProductInteger = service.findAccount(division1,
				productInteger, warehouse1);
		++size;
		assertNotNull(accountProductInteger);
		assertTrue(size == service.count());

		Account accountProductFloat = service.findAccount(division1,
				productFloat, warehouse1);
		++size;
		assertNotNull(accountProductFloat);
		assertTrue(size == service.count());

		Account accountProductFraction = service.findAccount(division1,
				productFraction, warehouse1);
		++size;
		assertNotNull(accountProductFraction);
		assertTrue(size == service.count());

		Account accountCurrencyKZ = service.findAccount(division1, currencyKZ,
				cash);
		++size;
		assertEquals(accountCurrencyKZ, accountCurrencyKZ);
		assertTrue(size == service.count());

	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#persist(java.lang.Object)}.
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testPersist() throws Exception {
		createTestEntitys();

		Account account = service.findAccount(division1, productInteger,
				warehouse1);

		try {
			service.persist(account);

			assertTrue("persist operation supported", false);
		} catch (NotSupportedException ex) {
			assertTrue(true);
		}

	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#refresh(java.lang.Object)}.
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testRefresh() throws Exception {
		createTestEntitys();

		Account account = service.findAccount(division1, productInteger,
				warehouse1);

		try {
			service.persist(account);

			assertTrue("persist operation supported", false);
		} catch (NotSupportedException ex) {
			assertTrue(true);
		}

	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#merge(java.lang.Object)}.
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testMerge() throws Exception {
		createTestEntitys();

		Account account = service.findAccount(division1, productInteger,
				warehouse1);

		try {
			service.merge(account);

			assertTrue("persist operation supported", false);
		} catch (NotSupportedException ex) {
			assertTrue(true);
		}

	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#remove(java.lang.Object)}.
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testRemove() throws Exception {

		createTestEntitys();

		Account account = service.findAccount(division1, productInteger,
				warehouse1);

		try {
			service.remove(account);

			assertTrue("persist operation supported", false);
		} catch (NotSupportedException ex) {
			assertTrue(true);
		}

	}

}
