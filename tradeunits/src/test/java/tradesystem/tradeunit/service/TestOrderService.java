/*
 * 
 */
package tradesystem.tradeunit.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.transaction.NotSupportedException;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import pk.home.libs.combine.dao.ABaseDAO.SortOrderType;
import tradesystem.tradeunit.basetestutils.BaseTest;
import tradesystem.tradeunit.domain.Account;
import tradesystem.tradeunit.domain.Cash;
import tradesystem.tradeunit.domain.CashPrivileges;
import tradesystem.tradeunit.domain.Customer;
import tradesystem.tradeunit.domain.Division;
import tradesystem.tradeunit.domain.ICountsObject;
import tradesystem.tradeunit.domain.IOwner;
import tradesystem.tradeunit.domain.Item;
import tradesystem.tradeunit.domain.Measure.MeasureType;
import tradesystem.tradeunit.domain.Order;
import tradesystem.tradeunit.domain.OrderType;
import tradesystem.tradeunit.domain.Order_;
import tradesystem.tradeunit.domain.Privileges;
import tradesystem.tradeunit.domain.Product;
import tradesystem.tradeunit.domain.Warehouse;
import tradesystem.tradeunit.domain.WarehousePrivileges;

/**
 * JUnit test service class for entity class: Order
 * Order - ордер
 */
@RunWith(SpringJUnit4ClassRunner.class)
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
		DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class })
@Transactional
@ActiveProfiles({"Dev"})
@ContextConfiguration(locations = { "file:./src/main/resources/applicationContext.xml" })
public class TestOrderService extends BaseTest{

	/**
	 * The DAO being tested, injected by Spring
	 * 
	 */
	private OrderService service;

	/**
	 * Method to allow Spring to inject the DAO that will be tested
	 * 
	 */
	@Autowired
	public void setservice(OrderService service) {
		this.service = service;
	}

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}
	
	
	
	/**
	 * Test product plus.
	 *
	 * @throws Exception the exception
	 */
	@Test
	@Rollback(true)
	public void testProductPlus() throws Exception {
		createTestEntitys();
		
		String descrioption = "Operation description...";
		
		// PLUS (+)
		{
			Account account = accountService.findAccount(division1, productInteger, warehouse1);
			
			BigDecimal oldValue = account.getValue();
			BigDecimal oldNumerator = account.getNumerator();
			
			System.out.println(">>>>>>>>>"+ oldValue);
			
			BigDecimal newValue = new BigDecimal(10);
			BigDecimal newNumerator = new BigDecimal(0);
			
			Item item = new Item(warehouse1, productInteger, newValue, newNumerator, null, "");
			
			List<Item> items = new ArrayList<>();
			items.add(item);

			Order order = orderService.productPlus(userAccount, division1, OrderType.PRODUCT_PLUS, items, descrioption);
			
			Item item2 = order.getItems().toArray(new Item[]{})[0];
			
			
			assertEquals(item,item2);
			assertEquals(account,item.getAccount());
			assertEquals(account.getDivision(),order.getDivision());
			
			assertTrue(account.getValue().compareTo(oldValue.add(newValue)) == 0);
			assertTrue(account.getNumerator().compareTo(oldNumerator.add(newNumerator)) == 0);
		}
		
		{
			Account account = accountService.findAccount(division1, productFloat, warehouse1);
			
			BigDecimal oldValue = account.getValue();
			BigDecimal oldNumerator = account.getNumerator();
			
			System.out.println(">>>>>>>>>"+ oldValue);
			
			BigDecimal newValue = new BigDecimal(10.75);
			BigDecimal newNumerator = new BigDecimal(0);
			
			Item item = new Item(warehouse1, productFloat, newValue, newNumerator, null, "");
			
			List<Item> items = new ArrayList<>();
			items.add(item);

			Order order = orderService.productPlus(userAccount, division1, OrderType.PRODUCT_PLUS, items, descrioption);
			
			Item item2 = order.getItems().toArray(new Item[]{})[0];
			
			
			assertEquals(item,item2);
			assertEquals(account,item.getAccount());
			assertEquals(account.getDivision(),order.getDivision());
			
			assertTrue(account.getValue().compareTo(oldValue.add(newValue)) == 0);
			assertTrue(account.getNumerator().compareTo(oldNumerator.add(newNumerator)) == 0);
		}
		
		{
			Account account = accountService.findAccount(division1, productFraction, warehouse1);
			
			BigDecimal oldValue = account.getValue();
			BigDecimal oldNumerator = account.getNumerator();
			
			System.out.println(">>>>>>>>>"+ oldValue + " " + oldNumerator);
			
			BigDecimal newValue = new BigDecimal(7);
			BigDecimal newNumerator = new BigDecimal(5);
			
			Item item = new Item(warehouse1, productFraction, newValue, newNumerator, null, "");
			
			List<Item> items = new ArrayList<>();
			items.add(item);

			Order order = orderService.productPlus(userAccount, division1, OrderType.PRODUCT_PLUS, items, descrioption);
			
			Item item2 = order.getItems().toArray(new Item[]{})[0];
			
			
			assertEquals(item,item2);
			assertEquals(account,item.getAccount());
			assertEquals(account.getDivision(),order.getDivision());
			
			assertTrue(account.getValue().compareTo(oldValue.add(newValue)) == 0);
			assertTrue(account.getNumerator().compareTo(oldNumerator.add(newNumerator)) == 0);
		}
		
		{
			{
				System.out.println("ET");
				
				Item item = new Item(warehouse1, productInteger, new BigDecimal(-7), new BigDecimal(0), null, "");

				List<Item> items = new ArrayList<>();
				items.add(item);

				try {
					orderService.productPlus(userAccount, division1, OrderType.PRODUCT_PLUS, items,
							descrioption);

					assertTrue("Bad operation", false);
				} catch (Exception ex) {
					assertTrue(true);
				}
			}
			
			{
				System.out.println("ET");
				
				Item item = new Item(warehouse1, productFloat, new BigDecimal(-0.244), new BigDecimal(0), null, "");

				List<Item> items = new ArrayList<>();
				items.add(item);

				try {
					orderService.productPlus(userAccount, division1, OrderType.PRODUCT_PLUS, items,
							descrioption);

					assertTrue("Bad operation", false);
				} catch (Exception ex) {
					assertTrue(true);
				}
			}
			
			
			{
				System.out.println("ET");
				
				Item item = new Item(warehouse1, productFraction, new BigDecimal(-7), new BigDecimal(5), null, "");

				List<Item> items = new ArrayList<>();
				items.add(item);

				try {
					orderService.productPlus(userAccount, division1, OrderType.PRODUCT_PLUS, items,
							descrioption);

					assertTrue("Bad operation", false);
				} catch (Exception ex) {
					assertTrue(true);
				}
			}
			
			{
				System.out.println("ET");
				
				Item item = new Item(warehouse1, productFraction, new BigDecimal(7), new BigDecimal(-5), null, "");

				List<Item> items = new ArrayList<>();
				items.add(item);

				try {
					orderService.productPlus(userAccount, division1, OrderType.PRODUCT_PLUS, items,
							descrioption);

					assertTrue("Bad operation", false);
				} catch (Exception ex) {
					assertTrue(true);
				}
			}
			
			{
				System.out.println("ET");
				
				Item item = new Item(warehouse1, productFraction, new BigDecimal(-7), new BigDecimal(-5), null, "");

				List<Item> items = new ArrayList<>();
				items.add(item);

				try {
					orderService.productPlus(userAccount, division1, OrderType.PRODUCT_PLUS, items,
							descrioption);

					assertTrue("Bad operation", false);
				} catch (Exception ex) {
					assertTrue(true);
				}
			}
		}
		
		
		// MINUS (-)
		
		{
			Account account = accountService.findAccount(division1, productInteger, warehouse1);
			
			BigDecimal oldValue = account.getValue();
			BigDecimal oldNumerator = account.getNumerator();
			
			System.out.println(">>>>>>>>>"+ oldValue);
			
			BigDecimal newValue = new BigDecimal(-10);
			BigDecimal newNumerator = new BigDecimal(0);
			
			Item item = new Item(warehouse1, productInteger, newValue, newNumerator, null, "");
			
			List<Item> items = new ArrayList<>();
			items.add(item);

			Order order = orderService.productPlus(userAccount, division1, OrderType.PRODUCT_MINUS,	items, descrioption);
			
			Item item2 = order.getItems().toArray(new Item[]{})[0];
			
			
			assertEquals(item,item2);
			assertEquals(account,item.getAccount());
			assertEquals(account.getDivision(),order.getDivision());
			
			assertTrue(account.getValue().compareTo(oldValue.add(newValue)) == 0);
			assertTrue(account.getNumerator().compareTo(oldNumerator.add(newNumerator)) == 0);
		}
		
		{
			Account account = accountService.findAccount(division1, productFloat, warehouse1);
			
			BigDecimal oldValue = account.getValue();
			BigDecimal oldNumerator = account.getNumerator();
			
			System.out.println(">>>>>>>>>"+ oldValue);
			
			BigDecimal newValue = new BigDecimal(-10.75);
			BigDecimal newNumerator = new BigDecimal(0);
			
			Item item = new Item(warehouse1, productFloat, newValue, newNumerator, null, "");
			
			List<Item> items = new ArrayList<>();
			items.add(item);

			Order order = orderService.productPlus(userAccount, division1, OrderType.PRODUCT_MINUS,	items, descrioption);
			
			Item item2 = order.getItems().toArray(new Item[]{})[0];
			
			
			assertEquals(item,item2);
			assertEquals(account,item.getAccount());
			assertEquals(account.getDivision(),order.getDivision());
			
			assertTrue(account.getValue().compareTo(oldValue.add(newValue)) == 0);
			assertTrue(account.getNumerator().compareTo(oldNumerator.add(newNumerator)) == 0);
		}
		
		{
			Account account = accountService.findAccount(division1, productFraction, warehouse1);
			
			BigDecimal oldValue = account.getValue();
			BigDecimal oldNumerator = account.getNumerator();
			
			System.out.println(">>>>>>>>>"+ oldValue + " " + oldNumerator);
			
			
			BigDecimal newValue = new BigDecimal(-7);
			BigDecimal newNumerator = new BigDecimal(-5);
			
			Item item = new Item(warehouse1, productFraction, newValue, newNumerator, null, "");
			
			List<Item> items = new ArrayList<>();
			items.add(item);

			Order order = orderService.productPlus(userAccount, division1, OrderType.PRODUCT_MINUS,	items, descrioption);
			
			Item item2 = order.getItems().toArray(new Item[]{})[0];
			
			
			assertEquals(item,item2);
			assertEquals(account,item.getAccount());
			assertEquals(account.getDivision(),order.getDivision());
			
			assertTrue(account.getValue().compareTo(oldValue.add(newValue)) == 0);
			assertTrue(account.getNumerator().compareTo(oldNumerator.add(newNumerator)) == 0);
		}
		
		{
			{
				Item item = new Item(warehouse1, productInteger, new BigDecimal(7), new BigDecimal(0), null, "");

				List<Item> items = new ArrayList<>();
				items.add(item);

				try {
					orderService.productPlus(userAccount, division1, OrderType.PRODUCT_MINUS, items,
							descrioption);

					assertTrue("Bad operation", false);
				} catch (Exception ex) {
					assertTrue(true);
				}
			}
			
			{
				Item item = new Item(warehouse1, productFloat, new BigDecimal(0.244), new BigDecimal(0), null, "");

				List<Item> items = new ArrayList<>();
				items.add(item);

				try {
					orderService.productPlus(userAccount, division1, OrderType.PRODUCT_MINUS, items,
							descrioption);

					assertTrue("Bad operation", false);
				} catch (Exception ex) {
					assertTrue(true);
				}
			}
			
			
			{
				Item item = new Item(warehouse1, productFraction, new BigDecimal(7), new BigDecimal(-5), null, "");

				List<Item> items = new ArrayList<>();
				items.add(item);

				try {
					orderService.productPlus(userAccount, division1, OrderType.PRODUCT_MINUS, items,
							descrioption);

					assertTrue("Bad operation", false);
				} catch (Exception ex) {
					assertTrue(true);
				}
			}
			
			{
				Item item = new Item(warehouse1, productFraction, new BigDecimal(-7), new BigDecimal(5), null, "");

				List<Item> items = new ArrayList<>();
				items.add(item);

				try {
					orderService.productPlus(userAccount, division1, OrderType.PRODUCT_MINUS, items,
							descrioption);

					assertTrue("Bad operation", false);
				} catch (Exception ex) {
					assertTrue(true);
				}
			}
			
			{
				Item item = new Item(warehouse1, productFraction, new BigDecimal(7), new BigDecimal(5), null, "");

				List<Item> items = new ArrayList<>();
				items.add(item);

				try {
					orderService.productPlus(userAccount, division1, OrderType.PRODUCT_MINUS, items,
							descrioption);

					assertTrue("Bad operation", false);
				} catch (Exception ex) {
					assertTrue(true);
				}
			}
		}
		
		
	}
	
		
	
	// ----------------------------------------------------------------------------------------------------------------------------
	
	
	/**
	 * Adds the accounts to test map.
	 *
	 * @param testMap the map
	 * @param o the o
	 * @return the map
	 */
	@Transactional
	public Map<Account, BigDecimal[]> addAccountsToTestMap(Map<Account, BigDecimal[]> testMap, Order o ) {

		for(Item i: o.getItems())
			testMap.put(i.getAccount(), new BigDecimal[] { i.getAccount().getValue(), i.getAccount().getNumerator() });
		
		return testMap;
	}

	
	/**
	 * Pr product plus.
	 *
	 * @throws Exception the exception
	 */
	@Transactional
	public Map<Account, BigDecimal[]> prProductPLUS() throws Exception {
		
		Map<Account, BigDecimal[]> map = new HashMap<>();
		
		
		{	
			BigDecimal newValue = new BigDecimal(10);
			BigDecimal newNumerator = new BigDecimal(0);
			
			Item item = new Item(warehouse1, productInteger, newValue, newNumerator, null, "");
			
			List<Item> items = new ArrayList<>();
			items.add(item);

			Order o = orderService.productPlus(userAccount, division1, OrderType.PRODUCT_PLUS, items, "");
			
			map = addAccountsToTestMap(map, o);
		}
		
		{ //	
			BigDecimal newValue = new BigDecimal(10);
			BigDecimal newNumerator = new BigDecimal(0);
			
			Item item = new Item(warehouse2, productInteger, newValue, newNumerator, null, "");
			
			List<Item> items = new ArrayList<>();
			items.add(item);

			Order o = orderService.productPlus(userAccount, division1, OrderType.PRODUCT_PLUS, items, "");
			
			map = addAccountsToTestMap(map, o);
		}
		
		
		
		
		
		{
			
			BigDecimal newValue = new BigDecimal(20);
			BigDecimal newNumerator = new BigDecimal(0);
			
			Item item = new Item(warehouse1, productFloat, newValue, newNumerator, null, "");
			
			List<Item> items = new ArrayList<>();
			items.add(item);

			Order o = orderService.productPlus(userAccount, division1, OrderType.PRODUCT_PLUS, items, "");
		
			map = addAccountsToTestMap(map, o);
		}
		
		{
			
			BigDecimal newValue = new BigDecimal(30);
			BigDecimal newNumerator = new BigDecimal(3);
			
			Item item = new Item(warehouse1, productFraction, newValue, newNumerator, null, "");
			
			List<Item> items = new ArrayList<>();
			items.add(item);

			Order o = orderService.productPlus(userAccount, division1, OrderType.PRODUCT_PLUS, items, "");
			
			map = addAccountsToTestMap(map, o);
		}
		
		
		{	
			
			List<Item> items = itemService.productItems(warehouse1,productIntegerShoes);
			items.get(0).setValue(new BigDecimal(60));
			items.get(0).setNumerator(new BigDecimal(0));
			
			
			items.get(1).setValue(new BigDecimal(10));
			items.get(1).setNumerator(new BigDecimal(0));
			
			items.get(2).setValue(new BigDecimal(20));
			items.get(2).setNumerator(new BigDecimal(0));
			
			items.get(3).setValue(new BigDecimal(30));
			items.get(3).setNumerator(new BigDecimal(0));
			

			Order o = orderService.productPlus(userAccount, division1, OrderType.PRODUCT_PLUS, items, "");
			
			map = addAccountsToTestMap(map, o);
		}
		
		{	
			List<Item> items = itemService.productItems(warehouse1,productFloatOil);
			items.get(0).setValue(new BigDecimal(600.60));
			items.get(0).setNumerator(new BigDecimal(0));
			
			
			items.get(1).setValue(new BigDecimal(100.1));
			items.get(1).setNumerator(new BigDecimal(0));
			
			items.get(2).setValue(new BigDecimal(200.2));
			items.get(2).setNumerator(new BigDecimal(0));
			
			items.get(3).setValue(new BigDecimal(300.3));
			items.get(3).setNumerator(new BigDecimal(0));

			Order o = orderService.productPlus(userAccount, division1, OrderType.PRODUCT_PLUS, items, "");
			
			map = addAccountsToTestMap(map, o);
		}
		
		{	
			List<Item> items = itemService.productItems(warehouse1,productFractionTablet);
			items.get(0).setValue(new BigDecimal(6000));
			items.get(0).setNumerator(new BigDecimal(6000));
			
			
			items.get(1).setValue(new BigDecimal(1000));
			items.get(1).setNumerator(new BigDecimal(1000));
			
			items.get(2).setValue(new BigDecimal(2000));
			items.get(2).setNumerator(new BigDecimal(2000));
			
			items.get(3).setValue(new BigDecimal(3000));
			items.get(3).setNumerator(new BigDecimal(3000));

			Order o = orderService.productPlus(userAccount, division1, OrderType.PRODUCT_PLUS, items, "");
			
			map = addAccountsToTestMap(map, o);
		}
		
		return map;
	}
	
	/**
	 * New cash item.
	 *
	 * @param division the division
	 * @param countsObject the counts object
	 * @param owner the owner
	 * @param value the value
	 * @param numerator the numerator
	 * @return the item
	 * @throws Exception the exception
	 */
	@Transactional
	public Item newItem(Division division, ICountsObject countsObject,
			IOwner owner, Number value, Number numerator) throws Exception{
	
		Item i = new Item();
		
		value = value != null ? value : 0;
		numerator = numerator != null ? numerator : 0;

		i.setValue(new BigDecimal(value.doubleValue()));
		i.setNumerator(new BigDecimal(numerator.doubleValue()));
		
		i.setTransientOwner(owner);
		i.setTransientCountsObject(countsObject);
		
		return i;
	}
	
	/**
	 * New items.
	 *
	 * @param warehouse the warehouse
	 * @param product the product
	 * @param costValue the cost value
	 * @param toSaleValue the to sale value
	 * @return the list
	 * @throws Exception the exception
	 */
	@Transactional
	public List<Item> newPrItems(Warehouse warehouse, Product product, Number costValue, Number costValueNumerator, Number[][] toSaleValue) throws Exception{
		
		List<Item> ilist = itemService.productItems(warehouse, product); 

		System.out.println("ilist.size() = " + ilist.size());
		
		if (toSaleValue != null)
			for (int i = 0; i < toSaleValue.length; ++i) {
				Number[] c = toSaleValue[i];
				BigDecimal value = new BigDecimal((c.length > 0 && c[0] != null ? c[0] : 0).doubleValue()).setScale(Account.SCALE, RoundingMode.HALF_UP);
				BigDecimal numerator = new BigDecimal((c.length > 1 && c[1] != null ? c[1] : 0).doubleValue()).setScale(Account.SCALE, RoundingMode.HALF_UP);

				System.out.println(value.floatValue() + " " + numerator.floatValue());				
				
				if (i < ilist.size()) {
					ilist.get(i).setValue(value);
					ilist.get(i).setNumerator(numerator);
				}
			}

		for(Item i: ilist){
			i.setTransientOwner(warehouse);
			i.setTransientCountsObject(product);
			
			i.setActualPriceValue(priceServece.findPrice(division1, product).getValue());
			i.setActualPriceValueNumerator(priceServece.findPrice(division1, product).getValueNumrator());
			
			i.setCostValue(new BigDecimal(costValue.doubleValue()).setScale(Account.SCALE, RoundingMode.HALF_UP));
			i.setCostValueNumerator(new BigDecimal(costValueNumerator.doubleValue()).setScale(Account.SCALE, RoundingMode.HALF_UP));
		}
		
		return ilist;
	}
	
	/**
	 * Common order sale check.
	 *
	 * @param o the o
	 * @param o2 the o2
	 * @param testMap the test map
	 * @throws Exception the exception
	 */
	void commonOrderSaleCheck(Order o, Order o2, Map<Account, BigDecimal[]> testMap) throws Exception {
		assertEquals(o, o2);

		assertTrue(o2.getId() != null);
		assertTrue(o2.getId() > 0);
		assertTrue(o2.getOpTime() != null);
		assertTrue(o2.getPreviousOrder() == null);
		assertEquals(o2.getUserAccount(), userAccount);
		assertEquals(o2.getDivision(), division1);

		assertTrue(o2.getItems().size() > 0);

		short warehouseI = 0, cashI = 0, customerI = 0;
		BigDecimal custSum = new BigDecimal(0);
		BigDecimal cashSum = new BigDecimal(0);
		BigDecimal customerSum = new BigDecimal(0);

		for (Item i : o2.getItems()) {

			System.out.println("Owner: " + i.getAccount().getOwner().getKeyName() + "\tCountedObject: " + i.getAccount().getCountsObject().getKeyName()
					+ "\tQuantity: " + i.getValue() + " " + i.getNumerator() + "\t QA:"
					+ (i.getCountedAttribute() != null ? i.getCountedAttribute().getKeyName() : "ROOT") + " VALUE TYPE:"
					+ i.getTransientCountsObject().getValueType());

			assertTrue(i.getId() != null);
			assertTrue(i.getId() > 0);
			assertTrue(i.getOrder() != null);
			assertEquals(i.getOrder(), o2);
			assertTrue(i.getAccount() != null);

			assertTrue(i.getValue() != null);
			assertTrue(i.getNumerator() != null);

			assertEquals(i.getAccount().getDivision(), o2.getDivision());

			if (i.getAccount().getOwner() instanceof Warehouse) {
				++warehouseI;

				assertTrue(i.getMeasure() != null);
				assertEquals(i.getMeasure(), i.getAccount().getProduct().getMeasure());

				assertTrue(i.getAccount().getWarehouse() != null && i.getAccount().getCash() == null && i.getAccount().getCustomer() == null);
				assertEquals(i.getAccount().getWarehouse(), i.getAccount().getOwner());

				assertTrue(i.getValue().compareTo(new BigDecimal(0)) < 0);
				assertTrue(i.getValue().compareTo(new BigDecimal(0)) <= 0);

				assertTrue(i.getActualPriceValue() != null);
				assertTrue(i.getActualPriceValue().compareTo(new BigDecimal(0)) >= 0);
				assertTrue(i.getActualPriceValue().compareTo(priceServece.findPrice(division1, i.getAccount().getProduct()).getValue()) == 0);
				

				assertTrue(i.getCostValue() != null);
				assertTrue(i.getCostValue().compareTo(new BigDecimal(0)) >= 0);

				assertTrue(i.getCostValueNumerator() != null);
				assertTrue(i.getCostValueNumerator().compareTo(new BigDecimal(0)) >= 0);

				if (i.getAccount().getProduct().getMeasure().getMeasureType() == MeasureType.FRACTION) {
					assertTrue(i.getActualPriceValueNumerator() != null);
					assertTrue(i.getActualPriceValueNumerator().compareTo(priceServece.findPrice(division1, i.getAccount().getProduct()).getValueNumrator()) == 0);
					assertTrue(i.getCostValueNumerator().compareTo(new BigDecimal(0)) >= 0);
				} else {
					assertTrue(i.getActualPriceValueNumerator() == null);
				}

				assertTrue(i.getCountedAttribute() == null || i.getCountedAttribute().equals(i.getAccount().getCountedAttribute()));

				if (i.getCountedAttribute() == null || i.getCountedAttribute().getParent() == null) {

					BigDecimal ppSum = i.getValue().multiply(i.getCostValue());

					if (i.getAccount().getProduct().getMeasure().getMeasureType() == MeasureType.FRACTION)
						ppSum = ppSum.add(i.getNumerator().multiply(i.getCostValueNumerator()));

					custSum = custSum.add(ppSum);
				}

			} else if (i.getAccount().getOwner() instanceof Cash) {
				++cashI;

				assertTrue(i.getMeasure() == null);

				assertTrue(i.getValue().compareTo(new BigDecimal(0)) >= 0);
				assertTrue(i.getNumerator().compareTo(new BigDecimal(0)) >= 0);

				assertTrue(i.getAccount().getWarehouse() == null && i.getAccount().getCash() != null && i.getAccount().getCustomer() == null);
				assertEquals(i.getAccount().getCash(), i.getAccount().getOwner());

				assertTrue(i.getActualPriceValue() == null);
				assertTrue(i.getActualPriceValueNumerator() == null);
				assertTrue(i.getCostValueNumerator() == null);
				assertTrue(i.getCostValue() == null);

				cashSum = i.getValue();

			} else if (i.getAccount().getOwner() instanceof Customer) {
				++customerI;

				assertTrue(i.getMeasure() == null);

				assertTrue(i.getValue().compareTo(new BigDecimal(0)) <= 0);
				assertTrue(i.getNumerator().compareTo(new BigDecimal(0)) <= 0);

				assertTrue(i.getAccount().getWarehouse() == null && i.getAccount().getCash() == null && i.getAccount().getCustomer() != null);
				assertEquals(i.getAccount().getCustomer(), i.getAccount().getOwner());

				assertTrue(i.getActualPriceValue() == null);
				assertTrue(i.getActualPriceValueNumerator() == null);
				assertTrue(i.getCostValueNumerator() == null);
				assertTrue(i.getCostValue() == null);

				customerSum = i.getValue();

			} else
				assertTrue("The item owner is not warehouse or cash or customer ", false);
		}

		if (warehouseI == 0)
			assertTrue("The warehouse items is 0", false);
		if (cashI != 1)
			assertTrue("The cash item is no one cashI:" + cashI, false);
		if (customerI > 1)
			assertTrue("The customer item is more then one ", false);

		assertTrue(custSum.abs().compareTo(cashSum.add(customerSum.abs())) == 0);
		
		
		
		for (Item i : o2.getItems()) {

			if (i.getAccount().getOwner() instanceof Warehouse) {

				if (i.getAccount().getProduct().getMeasure().getMeasureType() == MeasureType.FRACTION) {

					BigDecimal v1 = testMap.get(i.getAccount())[0].multiply(i.getAccount().getProduct().getDenominator()).add(testMap.get(i.getAccount())[1]);

					BigDecimal v2 = i.getAccount().getValue().multiply(i.getAccount().getProduct().getDenominator()).add(i.getAccount().getNumerator());

					assertTrue(i.getValue().add(i.getNumerator()).compareTo(new BigDecimal(0)) < 0);
					assertTrue(v1.compareTo(v2) >= 0);

					System.out.println("\t" + v1 + " >= " + v2);

				} else {

					assertTrue(testMap.get(i.getAccount())[0].compareTo(i.getAccount().getValue()) == 1);
					System.out.println(testMap.get(i.getAccount())[0] + " >= " + i.getAccount().getValue());

				}
			}
		}
	}
	
	/**
	 * Cmp account values.
	 *
	 * @param testMap the test map
	 * @param items the items
	 * @param owner the owner
	 * @param countsObject the counts object
	 * @param plus the plus
	 * @return true, if successful
	 * @throws Exception the exception
	 */
	@Transactional
	public boolean cmpAccountValues(Map<Account, BigDecimal[]> testMap, Collection<Item> items,  IOwner owner, ICountsObject countsObject, Number[] plus) throws Exception {
		
		Account a1 = ItemService.findItemForCountsObject(items, owner, countsObject).getAccount();
		
		if (a1.getOwner() instanceof Warehouse && a1.getProduct().getMeasure().getMeasureType() == MeasureType.FRACTION) {

				BigDecimal v1 = testMap.get(a1)[0].multiply(a1.getProduct().getDenominator()).add(testMap.get(a1)[1]);

				BigDecimal v2 = a1.getValue().multiply(a1.getProduct().getDenominator()).add(a1.getNumerator());
				
				BigDecimal plusV = (new BigDecimal(plus[0].doubleValue()).setScale(Account.SCALE, RoundingMode.HALF_UP))
						.multiply(a1.getProduct().getDenominator()).add(new BigDecimal(plus[1].doubleValue()).setScale(Account.SCALE, RoundingMode.HALF_UP));

				return v2.add(plusV).compareTo(v1) == 0;
		
		} else
			
			return a1.getValue().add(new BigDecimal(plus[0].doubleValue()).setScale(Account.SCALE, RoundingMode.HALF_UP))
					.compareTo(testMap.get(a1)[0]) == 0;
	}
	
	
	/**
	 * Find itemand cmp value.
	 *
	 * @param items the items
	 * @param owner the owner
	 * @param cObject the c object
	 * @param value the value
	 * @return the int
	 * @throws Exception the exception
	 */
	int findItemandCmpValue(Collection<Item> items, IOwner owner,  ICountsObject cObject, Number value) throws Exception{
		return ItemService.findItemForCountsObject(items, owner, cObject).getValue()
				.compareTo(new BigDecimal(value.doubleValue()).setScale(Account.SCALE, RoundingMode.HALF_UP));
	}
	
	/**
	 * Find itemand cmp numerator.
	 *
	 * @param items the items
	 * @param owner the owner
	 * @param cObject the c object
	 * @param numerator the numerator
	 * @return the int
	 * @throws Exception the exception
	 */
	int findItemandCmpNumerator(Collection<Item> items, IOwner owner, ICountsObject cObject, Number numerator) throws Exception{
		return ItemService.findItemForCountsObject(items, owner, cObject).getNumerator()
				.compareTo(new BigDecimal(numerator.doubleValue()).setScale(Account.SCALE, RoundingMode.HALF_UP));
	}

	
	/**
	 * Find item and cmp cost value.
	 *
	 * @param items the items
	 * @param owner the owner
	 * @param cObject the c object
	 * @param costValue the cost value
	 * @return the int
	 * @throws Exception the exception
	 */
	int findItemAndCmpCostValue(Collection<Item> items, IOwner owner,  ICountsObject cObject, Number costValue) throws Exception{
		return ItemService.findItemForCountsObject(items, owner, cObject).getCostValue()
				.compareTo(new BigDecimal(costValue.doubleValue()).setScale(Account.SCALE, RoundingMode.HALF_UP));
	}
	
	
	/**
	 * Find item and cmp cost value numerator.
	 *
	 * @param items the items
	 * @param owner the owner
	 * @param cObject the c object
	 * @param costValueNumerator the cost value numerator
	 * @return the int
	 * @throws Exception the exception
	 */
	int findItemAndCmpCostValueNumerator(Collection<Item> items, IOwner owner, ICountsObject cObject, Number costValueNumerator) throws Exception{
		return ItemService.findItemForCountsObject(items, owner, cObject).getCostValueNumerator()
				.compareTo(new BigDecimal(costValueNumerator.doubleValue()).setScale(Account.SCALE, RoundingMode.HALF_UP));
	}
	
	
	
	// ---------------------------------------------------- OP SALE TESTS ---------------------------------------------
	
	/**
	 * Test product plus.
	 *
	 * @throws Exception the exception
	 */
	@Test
	@Rollback(true)
	public void testProductSale1() throws Exception {
		createTestEntitys();
		Map<Account, BigDecimal[]> testMap = prProductPLUS(); 
		
		
		String description = "TEST DESCRIPTION...";
		
		// PRIVILEGES
		privilegesService.persist(new Privileges(division1, userAccount, null, warehouse1, WarehousePrivileges.PRODUCT_SALE, null, null, true, ""));
		privilegesService.persist(new Privileges(division1, userAccount, null, null, null, cash, CashPrivileges.PRODUCT_SALE, true, ""));

		// PREPEARING
		
		List<Item> items = new ArrayList<>();

		items.addAll(newPrItems(warehouse1, productInteger, 100, 0, new Number[][] { { -1 } }));
		items.addAll(newPrItems(warehouse1, productFloat, 200, 0, new Number[][] { { -2.5 } }));
		items.addAll(newPrItems(warehouse1, productFraction, 5, 1, new Number[][] { { -5, -5 } }));

		items.addAll(newPrItems(warehouse1, productIntegerShoes, 4000, 0, new Number[][] { { -1 }, { 0 }, { -1 } }));
		items.addAll(newPrItems(warehouse1, productFloatOil, 150, 0, new Number[][] { { -2.2 }, { -1 }, { -1.2 }, {} }));
		items.addAll(newPrItems(warehouse1, productFractionTablet, 10, 1, new Number[][] { { -5, -5 }, { -1, 0 }, { -2, -3 }, { -2, -2 }, {} }));

		System.out.println("PITEMS: " + items.size());

		Item cashItem = newItem(division1, currencyKZ, cash, 4000, 0);
		Item customerItem = newItem(division1, currencyKZ, customer, -1015, 0);

		// RUN
		
		Order o = orderService.productSale(userAccount, division1, items, cashItem, customerItem, description);

		// CHECKING
		
		Order o2 = orderService.find(o.getId());
		
		commonOrderSaleCheck(o,o2, testMap);
		
		// ---------------------------
		assertEquals(o2.getDescription(), description);
		
		assertTrue(findItemandCmpValue(o2.getItems(), warehouse1, productInteger, -1) == 0);
		assertTrue(findItemandCmpNumerator(o2.getItems(), warehouse1, productInteger, 0) == 0);
		assertTrue(findItemAndCmpCostValue(o2.getItems(), warehouse1, productInteger, 100) == 0);
		assertTrue(findItemAndCmpCostValueNumerator(o2.getItems(), warehouse1, productInteger, 0) == 0);
		assertTrue(cmpAccountValues(testMap, o2.getItems(), warehouse1, productInteger, new Number[]{1}));
		
		assertTrue(findItemandCmpValue(o2.getItems(), warehouse1, productFloat, -2.5) == 0);
		assertTrue(findItemandCmpNumerator(o2.getItems(), warehouse1, productFloat, 0) == 0);
		assertTrue(findItemAndCmpCostValue(o2.getItems(), warehouse1, productFloat, 200) == 0);
		assertTrue(findItemAndCmpCostValueNumerator(o2.getItems(), warehouse1, productFloat, 0) == 0);
		assertTrue(cmpAccountValues(testMap, o2.getItems(), warehouse1, productFloat, new Number[]{2.5}));
		
		assertTrue(findItemandCmpValue(o2.getItems(), warehouse1, productFraction, -5) == 0);
		assertTrue(findItemandCmpNumerator(o2.getItems(), warehouse1, productFraction, -5) == 0);
		assertTrue(findItemAndCmpCostValue(o2.getItems(), warehouse1, productFraction, 5) == 0);
		assertTrue(findItemAndCmpCostValueNumerator(o2.getItems(), warehouse1, productFraction, 1) == 0);
		assertTrue(cmpAccountValues(testMap, o2.getItems(), warehouse1, productFraction, new Number[]{5,5}));
		
		assertTrue(findItemandCmpValue(o2.getItems(), warehouse1, productIntegerShoes, -1) == 0);
		assertTrue(findItemandCmpNumerator(o2.getItems(), warehouse1, productIntegerShoes, 0) == 0);
		assertTrue(findItemAndCmpCostValue(o2.getItems(), warehouse1, productIntegerShoes, 4000) == 0);
		assertTrue(findItemAndCmpCostValueNumerator(o2.getItems(), warehouse1, productIntegerShoes, 0) == 0);
		assertTrue(cmpAccountValues(testMap, o2.getItems(), warehouse1, productIntegerShoes, new Number[]{1}));
		
		assertTrue(findItemandCmpValue(o2.getItems(), warehouse1, productFloatOil, -2.2) == 0);
		assertTrue(findItemandCmpNumerator(o2.getItems(), warehouse1, productFloatOil, 0) == 0);
		assertTrue(findItemAndCmpCostValue(o2.getItems(), warehouse1, productFloatOil, 150) == 0);
		assertTrue(findItemAndCmpCostValueNumerator(o2.getItems(), warehouse1, productFloatOil, 0) == 0);
		assertTrue(cmpAccountValues(testMap, o2.getItems(), warehouse1, productFloatOil, new Number[]{2.2}));
		
		assertTrue(findItemandCmpValue(o2.getItems(), warehouse1, productFractionTablet, -5) == 0);
		assertTrue(findItemandCmpNumerator(o2.getItems(), warehouse1, productFractionTablet, -5) == 0);
		assertTrue(findItemAndCmpCostValue(o2.getItems(), warehouse1, productFractionTablet, 10) == 0);
		assertTrue(findItemAndCmpCostValueNumerator(o2.getItems(), warehouse1, productFractionTablet, 1) == 0);
		assertTrue(cmpAccountValues(testMap, o2.getItems(), warehouse1, productFractionTablet, new Number[]{5,5}));
		
		assertTrue(findItemandCmpValue(o2.getItems(), cash, currencyKZ, 4000) == 0);
		assertTrue(ItemService.findItemForCountsObject(o2.getItems(), cash, currencyKZ).getValue()
				.add(new BigDecimal(-4000)).compareTo(new BigDecimal(0)) == 0);
		
		assertTrue(findItemandCmpValue(o2.getItems(), customer, currencyKZ, -1015) == 0);
		assertTrue(ItemService.findItemForCountsObject(o2.getItems(), customer, currencyKZ).getValue()
				.add(new BigDecimal(1015)).compareTo(new BigDecimal(0)) == 0);
	}
	
	
	/**
	 * Test product plus.
	 *
	 * @throws Exception the exception
	 */
	@Test
	@Rollback(true)
	public void testProductSale2() throws Exception {
		createTestEntitys();
		Map<Account, BigDecimal[]> testMap = prProductPLUS(); 
		
		
		String description = "TEST DESCRIPTION...";
		
		// PRIVILEGES
		privilegesService.persist(new Privileges(division1, userAccount, null, warehouse1, WarehousePrivileges.PRODUCT_SALE, null, null, true, ""));
		privilegesService.persist(new Privileges(division1, userAccount, null, warehouse2, WarehousePrivileges.PRODUCT_SALE, null, null, true, ""));
		privilegesService.persist(new Privileges(division1, userAccount, null, null, null, cash, CashPrivileges.PRODUCT_SALE, true, ""));

		// PREPEARING
		
		List<Item> items = new ArrayList<>();

		items.addAll(newPrItems(warehouse1, productInteger, 100, 0, new Number[][] { { -1 } }));
		items.addAll(newPrItems(warehouse2, productInteger, 100, 0, new Number[][] { { -2 } }));
		items.addAll(newPrItems(warehouse1, productFloat, 200, 0, new Number[][] { { -2.5 } }));
		items.addAll(newPrItems(warehouse1, productFraction, 5, 1, new Number[][] { { -5, -5 } }));

		items.addAll(newPrItems(warehouse1, productIntegerShoes, 4000, 0, new Number[][] { { -1 }, { 0 }, { -1 } }));
		items.addAll(newPrItems(warehouse1, productFloatOil, 150, 0, new Number[][] { { -2.2 }, { -1 }, { -1.2 }, {} }));
		items.addAll(newPrItems(warehouse1, productFractionTablet, 10, 1, new Number[][] { { -5, -5 }, { -1, 0 }, { -2, -3 }, { -2, -2 }, {} }));

		System.out.println("PITEMS: " + items.size());

		Item cashItem = newItem(division1, currencyKZ, cash, 4200, 0);
		Item customerItem = newItem(division1, currencyKZ, customer, -1015, 0);

		// RUN
		
		Order o = orderService.productSale(userAccount, division1, items, cashItem, customerItem, description);

		// CHECKING
		
		Order o2 = orderService.find(o.getId());
		
		commonOrderSaleCheck(o,o2, testMap);
		
		// ---------------------------
		assertEquals(o2.getDescription(), description);
		
		assertTrue(findItemandCmpValue(o2.getItems(), warehouse1, productInteger, -1) == 0);
		assertTrue(findItemandCmpNumerator(o2.getItems(), warehouse1, productInteger, 0) == 0);
		assertTrue(findItemAndCmpCostValue(o2.getItems(), warehouse1, productInteger, 100) == 0);
		assertTrue(findItemAndCmpCostValueNumerator(o2.getItems(), warehouse1, productInteger, 0) == 0);
		assertTrue(cmpAccountValues(testMap, o2.getItems(), warehouse1, productInteger, new Number[]{1}));
		
		
		// !
		assertTrue(findItemandCmpValue(o2.getItems(), warehouse2, productInteger, -2) == 0);
		assertTrue(findItemandCmpNumerator(o2.getItems(), warehouse2, productInteger, 0) == 0);
		assertTrue(findItemAndCmpCostValue(o2.getItems(), warehouse2, productInteger, 100) == 0);
		assertTrue(findItemAndCmpCostValueNumerator(o2.getItems(), warehouse2, productInteger, 0) == 0);
		assertTrue(cmpAccountValues(testMap, o2.getItems(), warehouse2, productInteger, new Number[]{2}));
		
		
		assertTrue(findItemandCmpValue(o2.getItems(), warehouse1, productFloat, -2.5) == 0);
		assertTrue(findItemandCmpNumerator(o2.getItems(), warehouse1, productFloat, 0) == 0);
		assertTrue(findItemAndCmpCostValue(o2.getItems(), warehouse1, productFloat, 200) == 0);
		assertTrue(findItemAndCmpCostValueNumerator(o2.getItems(), warehouse1, productFloat, 0) == 0);
		assertTrue(cmpAccountValues(testMap, o2.getItems(), warehouse1, productFloat, new Number[]{2.5}));
		
		assertTrue(findItemandCmpValue(o2.getItems(), warehouse1, productFraction, -5) == 0);
		assertTrue(findItemandCmpNumerator(o2.getItems(), warehouse1, productFraction, -5) == 0);
		assertTrue(findItemAndCmpCostValue(o2.getItems(), warehouse1, productFraction, 5) == 0);
		assertTrue(findItemAndCmpCostValueNumerator(o2.getItems(), warehouse1, productFraction, 1) == 0);
		assertTrue(cmpAccountValues(testMap, o2.getItems(), warehouse1, productFraction, new Number[]{5,5}));
		
		assertTrue(findItemandCmpValue(o2.getItems(), warehouse1, productIntegerShoes, -1) == 0);
		assertTrue(findItemandCmpNumerator(o2.getItems(), warehouse1, productIntegerShoes, 0) == 0);
		assertTrue(findItemAndCmpCostValue(o2.getItems(), warehouse1, productIntegerShoes, 4000) == 0);
		assertTrue(findItemAndCmpCostValueNumerator(o2.getItems(), warehouse1, productIntegerShoes, 0) == 0);
		assertTrue(cmpAccountValues(testMap, o2.getItems(), warehouse1, productIntegerShoes, new Number[]{1}));
		
		assertTrue(findItemandCmpValue(o2.getItems(), warehouse1, productFloatOil, -2.2) == 0);
		assertTrue(findItemandCmpNumerator(o2.getItems(), warehouse1, productFloatOil, 0) == 0);
		assertTrue(findItemAndCmpCostValue(o2.getItems(), warehouse1, productFloatOil, 150) == 0);
		assertTrue(findItemAndCmpCostValueNumerator(o2.getItems(), warehouse1, productFloatOil, 0) == 0);
		assertTrue(cmpAccountValues(testMap, o2.getItems(), warehouse1, productFloatOil, new Number[]{2.2}));
		
		assertTrue(findItemandCmpValue(o2.getItems(), warehouse1, productFractionTablet, -5) == 0);
		assertTrue(findItemandCmpNumerator(o2.getItems(), warehouse1, productFractionTablet, -5) == 0);
		assertTrue(findItemAndCmpCostValue(o2.getItems(), warehouse1, productFractionTablet, 10) == 0);
		assertTrue(findItemAndCmpCostValueNumerator(o2.getItems(), warehouse1, productFractionTablet, 1) == 0);
		assertTrue(cmpAccountValues(testMap, o2.getItems(), warehouse1, productFractionTablet, new Number[]{5,5}));
		
		assertTrue(findItemandCmpValue(o2.getItems(), cash, currencyKZ, 4200) == 0);
		assertTrue(ItemService.findItemForCountsObject(o2.getItems(), cash, currencyKZ).getValue()
				.add(new BigDecimal(-4200)).compareTo(new BigDecimal(0)) == 0);
		
		assertTrue(findItemandCmpValue(o2.getItems(), customer, currencyKZ, -1015) == 0);
		assertTrue(ItemService.findItemForCountsObject(o2.getItems(), customer, currencyKZ).getValue()
				.add(new BigDecimal(1015)).compareTo(new BigDecimal(0)) == 0);
	}
	
	
	
	/**
	 * Test product plus.
	 *
	 * @throws Exception the exception
	 */
	@Test
	@Rollback(true)
	public void testProductSaleWithoutCustomer() throws Exception {
		createTestEntitys();
		Map<Account, BigDecimal[]> testMap = prProductPLUS(); 
		
		
		String description = "TEST DESCRIPTION...";
		
		// PRIVILEGES
		privilegesService.persist(new Privileges(division1, userAccount, null, warehouse1, WarehousePrivileges.PRODUCT_SALE, null, null, true, ""));
		privilegesService.persist(new Privileges(division1, userAccount, null, null, null, cash, CashPrivileges.PRODUCT_SALE, true, ""));

		// PREPEARING
		
		List<Item> items = new ArrayList<>();

		items.addAll(newPrItems(warehouse1, productInteger, 100, 0, new Number[][] { { -1 } }));
		items.addAll(newPrItems(warehouse1, productFloat, 200, 0, new Number[][] { { -2.5 } }));
		items.addAll(newPrItems(warehouse1, productFraction, 5, 1, new Number[][] { { -5, -5 } }));

		items.addAll(newPrItems(warehouse1, productIntegerShoes, 4000, 0, new Number[][] { { -1 }, { 0 }, { -1 } }));
		items.addAll(newPrItems(warehouse1, productFloatOil, 150, 0, new Number[][] { { -2.2 }, { -1 }, { -1.2 }, {} }));
		items.addAll(newPrItems(warehouse1, productFractionTablet, 10, 1, new Number[][] { { -5, -5 }, { -1, 0 }, { -2, -3 }, { -2, -2 }, {} }));

		System.out.println("PITEMS: " + items.size());

		Item cashItem = newItem(division1, currencyKZ, cash, 5015, 0);

		// RUN
		
		Order o = orderService.productSale(userAccount, division1, items, cashItem, null, description);

		// CHECKING
		
		Order o2 = orderService.find(o.getId());
		
		commonOrderSaleCheck(o,o2, testMap);
		
		// ---------------------------
		assertEquals(o2.getDescription(), description);
		
		assertTrue(findItemandCmpValue(o2.getItems(), warehouse1, productInteger, -1) == 0);
		assertTrue(findItemandCmpNumerator(o2.getItems(), warehouse1, productInteger, 0) == 0);
		assertTrue(findItemAndCmpCostValue(o2.getItems(), warehouse1, productInteger, 100) == 0);
		assertTrue(findItemAndCmpCostValueNumerator(o2.getItems(), warehouse1, productInteger, 0) == 0);
		assertTrue(cmpAccountValues(testMap, o2.getItems(), warehouse1, productInteger, new Number[]{1}));
		
		assertTrue(findItemandCmpValue(o2.getItems(), warehouse1, productFloat, -2.5) == 0);
		assertTrue(findItemandCmpNumerator(o2.getItems(), warehouse1, productFloat, 0) == 0);
		assertTrue(findItemAndCmpCostValue(o2.getItems(), warehouse1, productFloat, 200) == 0);
		assertTrue(findItemAndCmpCostValueNumerator(o2.getItems(), warehouse1, productFloat, 0) == 0);
		assertTrue(cmpAccountValues(testMap, o2.getItems(), warehouse1, productFloat, new Number[]{2.5}));
		
		assertTrue(findItemandCmpValue(o2.getItems(), warehouse1, productFraction, -5) == 0);
		assertTrue(findItemandCmpNumerator(o2.getItems(), warehouse1, productFraction, -5) == 0);
		assertTrue(findItemAndCmpCostValue(o2.getItems(), warehouse1, productFraction, 5) == 0);
		assertTrue(findItemAndCmpCostValueNumerator(o2.getItems(), warehouse1, productFraction, 1) == 0);
		assertTrue(cmpAccountValues(testMap, o2.getItems(), warehouse1, productFraction, new Number[]{5,5}));
		
		assertTrue(findItemandCmpValue(o2.getItems(), warehouse1, productIntegerShoes, -1) == 0);
		assertTrue(findItemandCmpNumerator(o2.getItems(), warehouse1, productIntegerShoes, 0) == 0);
		assertTrue(findItemAndCmpCostValue(o2.getItems(), warehouse1, productIntegerShoes, 4000) == 0);
		assertTrue(findItemAndCmpCostValueNumerator(o2.getItems(), warehouse1, productIntegerShoes, 0) == 0);
		assertTrue(cmpAccountValues(testMap, o2.getItems(), warehouse1, productIntegerShoes, new Number[]{1}));
		
		assertTrue(findItemandCmpValue(o2.getItems(), warehouse1, productFloatOil, -2.2) == 0);
		assertTrue(findItemandCmpNumerator(o2.getItems(), warehouse1, productFloatOil, 0) == 0);
		assertTrue(findItemAndCmpCostValue(o2.getItems(), warehouse1, productFloatOil, 150) == 0);
		assertTrue(findItemAndCmpCostValueNumerator(o2.getItems(), warehouse1, productFloatOil, 0) == 0);
		assertTrue(cmpAccountValues(testMap, o2.getItems(), warehouse1, productFloatOil, new Number[]{2.2}));
		
		assertTrue(findItemandCmpValue(o2.getItems(), warehouse1, productFractionTablet, -5) == 0);
		assertTrue(findItemandCmpNumerator(o2.getItems(), warehouse1, productFractionTablet, -5) == 0);
		assertTrue(findItemAndCmpCostValue(o2.getItems(), warehouse1, productFractionTablet, 10) == 0);
		assertTrue(findItemAndCmpCostValueNumerator(o2.getItems(), warehouse1, productFractionTablet, 1) == 0);
		assertTrue(cmpAccountValues(testMap, o2.getItems(), warehouse1, productFractionTablet, new Number[]{5,5}));
		
		assertTrue(findItemandCmpValue(o2.getItems(), cash, currencyKZ, 5015) == 0);
		assertTrue(ItemService.findItemForCountsObject(o2.getItems(), cash, currencyKZ).getValue()
				.add(new BigDecimal(-5015)).compareTo(new BigDecimal(0)) == 0);
	}
	

	/**
	 * Test product plus.
	 *
	 * @throws Exception the exception
	 */
	@Test
	@Rollback(true)
	public void testProductSale3() throws Exception {
		createTestEntitys();
		Map<Account, BigDecimal[]> testMap = prProductPLUS(); 
		
		
		String description = "TEST DESCRIPTION...";
		
		// PRIVILEGES
		privilegesService.persist(new Privileges(division1, userAccount, null, warehouse1, WarehousePrivileges.PRODUCT_SALE, null, null, true, ""));
		privilegesService.persist(new Privileges(division1, userAccount, null, null, null, cash, CashPrivileges.PRODUCT_SALE, true, ""));

		// PREPEARING
		
		List<Item> items = new ArrayList<>();

		items.addAll(newPrItems(warehouse1, productInteger, 100, 0, new Number[][] { { -1 } }));

		System.out.println("PITEMS: " + items.size());

		Item cashItem = newItem(division1, currencyKZ, cash, 100, 0);
		Item customerItem = newItem(division1, currencyKZ, customer, 0, 0);

		// RUN
		
		Order o = orderService.productSale(userAccount, division1, items, cashItem, customerItem, description);

		// CHECKING
		
		Order o2 = orderService.find(o.getId());
		
		commonOrderSaleCheck(o,o2, testMap);
		
		// ---------------------------
		assertEquals(o2.getDescription(), description);
		
		assertTrue(findItemandCmpValue(o2.getItems(), warehouse1, productInteger, -1) == 0);
		assertTrue(findItemandCmpNumerator(o2.getItems(), warehouse1, productInteger, 0) == 0);
		assertTrue(findItemAndCmpCostValue(o2.getItems(), warehouse1, productInteger, 100) == 0);
		assertTrue(findItemAndCmpCostValueNumerator(o2.getItems(), warehouse1, productInteger, 0) == 0);
		assertTrue(cmpAccountValues(testMap, o2.getItems(), warehouse1, productInteger, new Number[]{1}));
		
		assertTrue(findItemandCmpValue(o2.getItems(), cash, currencyKZ, 100) == 0);
		assertTrue(ItemService.findItemForCountsObject(o2.getItems(), cash, currencyKZ).getValue()
				.add(new BigDecimal(-100)).compareTo(new BigDecimal(0)) == 0);
	}
	
	
	// -----------------------------------------------------------------------------------------------------------------------------------
	
	/**
	 * Test product plus.
	 *
	 * @throws Exception the exception
	 */
	@Test
	@Rollback(true)
	public void testProductSaleSumProductInteger1() throws Exception {
		createTestEntitys();
		prProductPLUS(); 
		
		String description = "TEST DESCRIPTION...";
		
		// PRIVILEGES
		privilegesService.persist(new Privileges(division1, userAccount, null, warehouse1, WarehousePrivileges.PRODUCT_SALE, null, null, true, ""));
		privilegesService.persist(new Privileges(division1, userAccount, null, null, null, cash, CashPrivileges.PRODUCT_SALE, true, ""));

		// PREPEARING
		
		List<Item> items = new ArrayList<>();

		items.addAll(newPrItems(warehouse1, productInteger, 100, 0, new Number[][] { { -2 } })); // MAKE BAD DATA: 1 -> 2 
		items.addAll(newPrItems(warehouse1, productFloat, 200, 0, new Number[][] { { -2.5 } }));
		items.addAll(newPrItems(warehouse1, productFraction, 5, 1, new Number[][] { { -5, -5 } }));

		items.addAll(newPrItems(warehouse1, productIntegerShoes, 4000, 0, new Number[][] { { -1 }, { 0 }, { -1 } }));
		items.addAll(newPrItems(warehouse1, productFloatOil, 150, 0, new Number[][] { { -2.2 }, { -1 }, { -1.2 }, {} }));
		items.addAll(newPrItems(warehouse1, productFractionTablet, 10, 1, new Number[][] { { -5, -5 }, { -1, 0 }, { -2, -3 }, { -2, -2 }, {} }));

		System.out.println("PITEMS: " + items.size());

		Item cashItem = newItem(division1, currencyKZ, cash, 4000, 0);
		Item customerItem = newItem(division1, currencyKZ, customer, -1015, 0);

		// RUN
		
		try{
		
			orderService.productSale(userAccount, division1, items, cashItem, customerItem, description);
		
			assertTrue("Bad sums validation!",false);
		} catch (Exception ex ){
			assertTrue(true);
		}		
	}
	
	/**
	 * Test product plus.
	 *
	 * @throws Exception the exception
	 */
	@Test
	@Rollback(true)
	public void testProductSaleSumProductFloat1() throws Exception {
		createTestEntitys();
		prProductPLUS(); 
		
		String description = "TEST DESCRIPTION...";
		
		// PRIVILEGES
		privilegesService.persist(new Privileges(division1, userAccount, null, warehouse1, WarehousePrivileges.PRODUCT_SALE, null, null, true, ""));
		privilegesService.persist(new Privileges(division1, userAccount, null, null, null, cash, CashPrivileges.PRODUCT_SALE, true, ""));

		// PREPEARING
		
		List<Item> items = new ArrayList<>();

		items.addAll(newPrItems(warehouse1, productInteger, 100, 0, new Number[][] { { -1 } })); 
		items.addAll(newPrItems(warehouse1, productFloat, 200, 0, new Number[][] { { -2.51 } })); // MAKE BAD DATA: 2.5 -> 2.51 
		items.addAll(newPrItems(warehouse1, productFraction, 5, 1, new Number[][] { { -5, -5 } }));

		items.addAll(newPrItems(warehouse1, productIntegerShoes, 4000, 0, new Number[][] { { -1 }, { 0 }, { -1 } }));
		items.addAll(newPrItems(warehouse1, productFloatOil, 150, 0, new Number[][] { { -2.2 }, { -1 }, { -1.2 }, {} }));
		items.addAll(newPrItems(warehouse1, productFractionTablet, 10, 1, new Number[][] { { -5, -5 }, { -1, 0 }, { -2, -3 }, { -2, -2 }, {} }));

		System.out.println("PITEMS: " + items.size());

		Item cashItem = newItem(division1, currencyKZ, cash, 4000, 0);
		Item customerItem = newItem(division1, currencyKZ, customer, -1015, 0);

		// RUN
		
		try{
		
			orderService.productSale(userAccount, division1, items, cashItem, customerItem, description);
		
			assertTrue("Bad sums validation!",false);
		} catch (Exception ex ){
			assertTrue(true);
		}		
	}
	
	/**
	 * Test product plus.
	 *
	 * @throws Exception the exception
	 */
	@Test
	@Rollback(true)
	public void testProductSaleSumProductFraction1() throws Exception {
		createTestEntitys();
		prProductPLUS(); 
		
		String description = "TEST DESCRIPTION...";
		
		// PRIVILEGES
		privilegesService.persist(new Privileges(division1, userAccount, null, warehouse1, WarehousePrivileges.PRODUCT_SALE, null, null, true, ""));
		privilegesService.persist(new Privileges(division1, userAccount, null, null, null, cash, CashPrivileges.PRODUCT_SALE, true, ""));

		// PREPEARING
		
		List<Item> items = new ArrayList<>();

		items.addAll(newPrItems(warehouse1, productInteger, 100, 0, new Number[][] { { -1 } })); 
		items.addAll(newPrItems(warehouse1, productFloat, 200, 0, new Number[][] { { -2.5 } }));  
		items.addAll(newPrItems(warehouse1, productFraction, 5, 1, new Number[][] { { -5, -4 } })); // MAKE BAD DATA: 5 -> 5, 5 -> 4

		items.addAll(newPrItems(warehouse1, productIntegerShoes, 4000, 0, new Number[][] { { -1 }, { 0 }, { -1 } }));
		items.addAll(newPrItems(warehouse1, productFloatOil, 150, 0, new Number[][] { { -2.2 }, { -1 }, { -1.2 }, {} }));
		items.addAll(newPrItems(warehouse1, productFractionTablet, 10, 1, new Number[][] { { -5, -5 }, { -1, 0 }, { -2, -3 }, { -2, -2 }, {} }));

		System.out.println("PITEMS: " + items.size());

		Item cashItem = newItem(division1, currencyKZ, cash, 4000, 0);
		Item customerItem = newItem(division1, currencyKZ, customer, -1015, 0);

		// RUN
		
		try{
		
			orderService.productSale(userAccount, division1, items, cashItem, customerItem, description);
		
			assertTrue("Bad sums validation!",false);
		} catch (Exception ex ){
			assertTrue(true);
		}		
	}
	
	/**
	 * Test product plus.
	 *
	 * @throws Exception the exception
	 */
	@Test
	@Rollback(true)
	public void testProductSaleSumProductFraction2() throws Exception {
		createTestEntitys();
		prProductPLUS(); 
		
		String description = "TEST DESCRIPTION...";
		
		// PRIVILEGES
		privilegesService.persist(new Privileges(division1, userAccount, null, warehouse1, WarehousePrivileges.PRODUCT_SALE, null, null, true, ""));
		privilegesService.persist(new Privileges(division1, userAccount, null, null, null, cash, CashPrivileges.PRODUCT_SALE, true, ""));

		// PREPEARING
		
		List<Item> items = new ArrayList<>();

		items.addAll(newPrItems(warehouse1, productInteger, 100, 0, new Number[][] { { -1 } })); 
		items.addAll(newPrItems(warehouse1, productFloat, 200, 0, new Number[][] { { -2.5 } }));  
		items.addAll(newPrItems(warehouse1, productFraction, 5, 1, new Number[][] { { -6, -5 } })); // MAKE BAD DATA: 5 -> 6, 5 -> 5

		items.addAll(newPrItems(warehouse1, productIntegerShoes, 4000, 0, new Number[][] { { -1 }, { 0 }, { -1 } }));
		items.addAll(newPrItems(warehouse1, productFloatOil, 150, 0, new Number[][] { { -2.2 }, { -1 }, { -1.2 }, {} }));
		items.addAll(newPrItems(warehouse1, productFractionTablet, 10, 1, new Number[][] { { -5, -5 }, { -1, 0 }, { -2, -3 }, { -2, -2 }, {} }));

		System.out.println("PITEMS: " + items.size());

		Item cashItem = newItem(division1, currencyKZ, cash, 4000, 0);
		Item customerItem = newItem(division1, currencyKZ, customer, -1015, 0);

		// RUN
		
		try{
		
			orderService.productSale(userAccount, division1, items, cashItem, customerItem, description);
		
			assertTrue("Bad sums validation!",false);
		} catch (Exception ex ){
			assertTrue(true);
		}		
	}
	
	/**
	 * Test product plus.
	 *
	 * @throws Exception the exception
	 */
	@Test
	@Rollback(true)
	public void testProductSaleSumProductFraction3() throws Exception {
		createTestEntitys();
		prProductPLUS(); 
		
		String description = "TEST DESCRIPTION...";
		
		// PRIVILEGES
		privilegesService.persist(new Privileges(division1, userAccount, null, warehouse1, WarehousePrivileges.PRODUCT_SALE, null, null, true, ""));
		privilegesService.persist(new Privileges(division1, userAccount, null, null, null, cash, CashPrivileges.PRODUCT_SALE, true, ""));

		// PREPEARING
		
		List<Item> items = new ArrayList<>();

		items.addAll(newPrItems(warehouse1, productInteger, 100, 0, new Number[][] { { -1 } })); 
		items.addAll(newPrItems(warehouse1, productFloat, 200, 0, new Number[][] { { -2.5 } }));  
		items.addAll(newPrItems(warehouse1, productFraction, 5, 1, new Number[][] { { -4, -5 } })); // MAKE BAD DATA: 5 -> 4, 5 -> 5

		items.addAll(newPrItems(warehouse1, productIntegerShoes, 4000, 0, new Number[][] { { -1 }, { 0 }, { -1 } }));
		items.addAll(newPrItems(warehouse1, productFloatOil, 150, 0, new Number[][] { { -2.2 }, { -1 }, { -1.2 }, {} }));
		items.addAll(newPrItems(warehouse1, productFractionTablet, 10, 1, new Number[][] { { -5, -5 }, { -1, 0 }, { -2, -3 }, { -2, -2 }, {} }));

		System.out.println("PITEMS: " + items.size());

		Item cashItem = newItem(division1, currencyKZ, cash, 4000, 0);
		Item customerItem = newItem(division1, currencyKZ, customer, -1015, 0);

		// RUN
		
		try{
		
			orderService.productSale(userAccount, division1, items, cashItem, customerItem, description);
		
			assertTrue("Bad sums validation!",false);
		} catch (Exception ex ){
			assertTrue(true);
		}		
	}
	
	/**
	 * Test product plus.
	 *
	 * @throws Exception the exception
	 */
	@Test
	@Rollback(true)
	public void testProductSaleSumProductFraction4() throws Exception {
		createTestEntitys();
		prProductPLUS(); 
		
		String description = "TEST DESCRIPTION...";
		
		// PRIVILEGES
		privilegesService.persist(new Privileges(division1, userAccount, null, warehouse1, WarehousePrivileges.PRODUCT_SALE, null, null, true, ""));
		privilegesService.persist(new Privileges(division1, userAccount, null, null, null, cash, CashPrivileges.PRODUCT_SALE, true, ""));

		// PREPEARING
		
		List<Item> items = new ArrayList<>();

		items.addAll(newPrItems(warehouse1, productInteger, 100, 0, new Number[][] { { -1 } })); 
		items.addAll(newPrItems(warehouse1, productFloat, 200, 0, new Number[][] { { -2.5 } }));  
		items.addAll(newPrItems(warehouse1, productFraction, 5, 1, new Number[][] { { -5, -6 } })); // MAKE BAD DATA: 5 -> 5, 5 -> 6

		items.addAll(newPrItems(warehouse1, productIntegerShoes, 4000, 0, new Number[][] { { -1 }, { 0 }, { -1 } }));
		items.addAll(newPrItems(warehouse1, productFloatOil, 150, 0, new Number[][] { { -2.2 }, { -1 }, { -1.2 }, {} }));
		items.addAll(newPrItems(warehouse1, productFractionTablet, 10, 1, new Number[][] { { -5, -5 }, { -1, 0 }, { -2, -3 }, { -2, -2 }, {} }));

		System.out.println("PITEMS: " + items.size());

		Item cashItem = newItem(division1, currencyKZ, cash, 4000, 0);
		Item customerItem = newItem(division1, currencyKZ, customer, -1015, 0);

		// RUN
		
		try{
		
			orderService.productSale(userAccount, division1, items, cashItem, customerItem, description);
		
			assertTrue("Bad sums validation!",false);
		} catch (Exception ex ){
			assertTrue(true);
		}		
	}
	
	/**
	 * Test product plus.
	 *
	 * @throws Exception the exception
	 */
	@Test
	@Rollback(true)
	public void testProductSaleSumProductIntegerShoes1() throws Exception {
		createTestEntitys();
		prProductPLUS(); 
		
		String description = "TEST DESCRIPTION...";
		
		// PRIVILEGES
		privilegesService.persist(new Privileges(division1, userAccount, null, warehouse1, WarehousePrivileges.PRODUCT_SALE, null, null, true, ""));
		privilegesService.persist(new Privileges(division1, userAccount, null, null, null, cash, CashPrivileges.PRODUCT_SALE, true, ""));

		// PREPEARING
		
		List<Item> items = new ArrayList<>();

		items.addAll(newPrItems(warehouse1, productInteger, 100, 0, new Number[][] { { -1 } })); 
		items.addAll(newPrItems(warehouse1, productFloat, 200, 0, new Number[][] { { -2.5 } }));  
		items.addAll(newPrItems(warehouse1, productFraction, 5, 1, new Number[][] { { -5, -5 } })); 

		items.addAll(newPrItems(warehouse1, productIntegerShoes, 4000, 0, new Number[][] { { -1 }, { 0 }, { -2 } })); // MAKE BAD DATA: 1 -> 1 1 -> 2
		items.addAll(newPrItems(warehouse1, productFloatOil, 150, 0, new Number[][] { { -2.2 }, { -1 }, { -1.2 }, {} }));
		items.addAll(newPrItems(warehouse1, productFractionTablet, 10, 1, new Number[][] { { -5, -5 }, { -1, 0 }, { -2, -3 }, { -2, -2 }, {} }));

		System.out.println("PITEMS: " + items.size());

		Item cashItem = newItem(division1, currencyKZ, cash, 4000, 0);
		Item customerItem = newItem(division1, currencyKZ, customer, -1015, 0);

		// RUN
		
		try{
		
			orderService.productSale(userAccount, division1, items, cashItem, customerItem, description);
		
			assertTrue("Bad account and summ validation!",false);
		} catch (Exception ex ){
			assertTrue(true);
		}		
	}
	
	/**
	 * Test product plus.
	 *
	 * @throws Exception the exception
	 */
	@Test
	@Rollback(true)
	public void testProductSaleSumProductIntegerShoes2() throws Exception {
		createTestEntitys();
		prProductPLUS(); 
		
		String description = "TEST DESCRIPTION...";
		
		// PRIVILEGES
		privilegesService.persist(new Privileges(division1, userAccount, null, warehouse1, WarehousePrivileges.PRODUCT_SALE, null, null, true, ""));
		privilegesService.persist(new Privileges(division1, userAccount, null, null, null, cash, CashPrivileges.PRODUCT_SALE, true, ""));

		// PREPEARING
		
		List<Item> items = new ArrayList<>();

		items.addAll(newPrItems(warehouse1, productInteger, 100, 0, new Number[][] { { -1 } })); 
		items.addAll(newPrItems(warehouse1, productFloat, 200, 0, new Number[][] { { -2.5 } }));  
		items.addAll(newPrItems(warehouse1, productFraction, 5, 1, new Number[][] { { -5, -5 } })); 

		items.addAll(newPrItems(warehouse1, productIntegerShoes, 4000, 0, new Number[][] { { -1 }, { 0 }, { 0 } })); // MAKE BAD DATA: 1 -> 1 1 -> 0
		items.addAll(newPrItems(warehouse1, productFloatOil, 150, 0, new Number[][] { { -2.2 }, { -1 }, { -1.2 }, {} }));
		items.addAll(newPrItems(warehouse1, productFractionTablet, 10, 1, new Number[][] { { -5, -5 }, { -1, 0 }, { -2, -3 }, { -2, -2 }, {} }));

		System.out.println("PITEMS: " + items.size());

		Item cashItem = newItem(division1, currencyKZ, cash, 4000, 0);
		Item customerItem = newItem(division1, currencyKZ, customer, -1015, 0);

		// RUN
		
		try{
		
			orderService.productSale(userAccount, division1, items, cashItem, customerItem, description);
		
			assertTrue("Bad account and summ validation!",false);
		} catch (Exception ex ){
			assertTrue(true);
		}		
	}
	
	/**
	 * Test product plus.
	 *
	 * @throws Exception the exception
	 */
	@Test
	@Rollback(true)
	public void testProductSaleSumProductIntegerShoes3() throws Exception {
		createTestEntitys();
		prProductPLUS(); 
		
		String description = "TEST DESCRIPTION...";
		
		// PRIVILEGES
		privilegesService.persist(new Privileges(division1, userAccount, null, warehouse1, WarehousePrivileges.PRODUCT_SALE, null, null, true, ""));
		privilegesService.persist(new Privileges(division1, userAccount, null, null, null, cash, CashPrivileges.PRODUCT_SALE, true, ""));

		// PREPEARING
		
		List<Item> items = new ArrayList<>();

		items.addAll(newPrItems(warehouse1, productInteger, 100, 0, new Number[][] { { -1 } })); 
		items.addAll(newPrItems(warehouse1, productFloat, 200, 0, new Number[][] { { -2.5 } }));  
		items.addAll(newPrItems(warehouse1, productFraction, 5, 1, new Number[][] { { -5, -5 } })); 

		items.addAll(newPrItems(warehouse1, productIntegerShoes, 4000, 0, new Number[][] { { -2 }, { 0 }, { -1 } })); // MAKE BAD DATA: 1 -> 2 1 -> 1
		items.addAll(newPrItems(warehouse1, productFloatOil, 150, 0, new Number[][] { { -2.2 }, { -1 }, { -1.2 }, {} }));
		items.addAll(newPrItems(warehouse1, productFractionTablet, 10, 1, new Number[][] { { -5, -5 }, { -1, 0 }, { -2, -3 }, { -2, -2 }, {} }));

		System.out.println("PITEMS: " + items.size());

		Item cashItem = newItem(division1, currencyKZ, cash, 4000, 0);
		Item customerItem = newItem(division1, currencyKZ, customer, -1015, 0);

		// RUN
		
		try{
		
			orderService.productSale(userAccount, division1, items, cashItem, customerItem, description);
		
			assertTrue("Bad account and summ validation!",false);
		} catch (Exception ex ){
			assertTrue(true);
		}		
	}
	
	/**
	 * Test product plus.
	 *
	 * @throws Exception the exception
	 */
	@Test
	@Rollback(true)
	public void testProductSaleSumProductIntegerShoes4() throws Exception {
		createTestEntitys();
		prProductPLUS(); 
		
		String description = "TEST DESCRIPTION...";
		
		// PRIVILEGES
		privilegesService.persist(new Privileges(division1, userAccount, null, warehouse1, WarehousePrivileges.PRODUCT_SALE, null, null, true, ""));
		privilegesService.persist(new Privileges(division1, userAccount, null, null, null, cash, CashPrivileges.PRODUCT_SALE, true, ""));

		// PREPEARING
		
		List<Item> items = new ArrayList<>();

		items.addAll(newPrItems(warehouse1, productInteger, 100, 0, new Number[][] { { -1 } })); 
		items.addAll(newPrItems(warehouse1, productFloat, 200, 0, new Number[][] { { -2.5 } }));  
		items.addAll(newPrItems(warehouse1, productFraction, 5, 1, new Number[][] { { -5, -5 } })); 

		items.addAll(newPrItems(warehouse1, productIntegerShoes, 4000, 0, new Number[][] { { -2 }, { 0 }, { -2 } })); // MAKE BAD DATA: 1 -> 2 1 -> 2
		items.addAll(newPrItems(warehouse1, productFloatOil, 150, 0, new Number[][] { { -2.2 }, { -1 }, { -1.2 }, {} }));
		items.addAll(newPrItems(warehouse1, productFractionTablet, 10, 1, new Number[][] { { -5, -5 }, { -1, 0 }, { -2, -3 }, { -2, -2 }, {} }));

		System.out.println("PITEMS: " + items.size());

		Item cashItem = newItem(division1, currencyKZ, cash, 4000, 0);
		Item customerItem = newItem(division1, currencyKZ, customer, -1015, 0);

		// RUN
		
		try{
		
			orderService.productSale(userAccount, division1, items, cashItem, customerItem, description);
		
			assertTrue("Bad account and summ validation!",false);
		} catch (Exception ex ){
			assertTrue(true);
		}		
	}
	
	/**
	 * Test product plus.
	 *
	 * @throws Exception the exception
	 */
	@Test
	@Rollback(true)
	public void testProductSaleSumProductIntegerShoes5() throws Exception {
		createTestEntitys();
		prProductPLUS(); 
		
		String description = "TEST DESCRIPTION...";
		
		// PRIVILEGES
		privilegesService.persist(new Privileges(division1, userAccount, null, warehouse1, WarehousePrivileges.PRODUCT_SALE, null, null, true, ""));
		privilegesService.persist(new Privileges(division1, userAccount, null, null, null, cash, CashPrivileges.PRODUCT_SALE, true, ""));

		// PREPEARING
		
		List<Item> items = new ArrayList<>();

		items.addAll(newPrItems(warehouse1, productInteger, 100, 0, new Number[][] { { -1 } })); 
		items.addAll(newPrItems(warehouse1, productFloat, 200, 0, new Number[][] { { -2.5 } }));  
		items.addAll(newPrItems(warehouse1, productFraction, 5, 1, new Number[][] { { -5, -5 } })); 

		items.addAll(newPrItems(warehouse1, productIntegerShoes, 4000, 0, new Number[][] { { 0 }, { 0 }, { -1 } })); // MAKE BAD DATA: 1 -> 0 1 -> 1
		items.addAll(newPrItems(warehouse1, productFloatOil, 150, 0, new Number[][] { { -2.2 }, { -1 }, { -1.2 }, {} }));
		items.addAll(newPrItems(warehouse1, productFractionTablet, 10, 1, new Number[][] { { -5, -5 }, { -1, 0 }, { -2, -3 }, { -2, -2 }, {} }));

		System.out.println("PITEMS: " + items.size());

		Item cashItem = newItem(division1, currencyKZ, cash, 4000, 0);
		Item customerItem = newItem(division1, currencyKZ, customer, -1015, 0);

		// RUN
		
		try{
		
			orderService.productSale(userAccount, division1, items, cashItem, customerItem, description);
		
			assertTrue("Bad account and summ validation!",false);
		} catch (Exception ex ){
			assertTrue(true);
		}		
	}
	
	/**
	 * Test product plus.
	 *
	 * @throws Exception the exception
	 */
	@Test
	@Rollback(true)
	public void testProductSaleSumProductIntegerShoes6() throws Exception {
		createTestEntitys();
		prProductPLUS(); 
		
		String description = "TEST DESCRIPTION...";
		
		// PRIVILEGES
		privilegesService.persist(new Privileges(division1, userAccount, null, warehouse1, WarehousePrivileges.PRODUCT_SALE, null, null, true, ""));
		privilegesService.persist(new Privileges(division1, userAccount, null, null, null, cash, CashPrivileges.PRODUCT_SALE, true, ""));

		// PREPEARING
		
		List<Item> items = new ArrayList<>();

		items.addAll(newPrItems(warehouse1, productInteger, 100, 0, new Number[][] { { -1 } })); 
		items.addAll(newPrItems(warehouse1, productFloat, 200, 0, new Number[][] { { -2.5 } }));  
		items.addAll(newPrItems(warehouse1, productFraction, 5, 1, new Number[][] { { -5, -5 } })); 

		items.addAll(newPrItems(warehouse1, productIntegerShoes, 4000, 0, new Number[][] { { 1 }, { 0 }, { 1 } })); // MAKE BAD DATA: 1 -> +1 1 -> +1
		items.addAll(newPrItems(warehouse1, productFloatOil, 150, 0, new Number[][] { { -2.2 }, { -1 }, { -1.2 }, {} }));
		items.addAll(newPrItems(warehouse1, productFractionTablet, 10, 1, new Number[][] { { -5, -5 }, { -1, 0 }, { -2, -3 }, { -2, -2 }, {} }));

		System.out.println("PITEMS: " + items.size());

		Item cashItem = newItem(division1, currencyKZ, cash, 4000, 0);
		Item customerItem = newItem(division1, currencyKZ, customer, -1015, 0);

		// RUN
		
		try{
		
			orderService.productSale(userAccount, division1, items, cashItem, customerItem, description);
		
			assertTrue("Bad account and summ validation!",false);
		} catch (Exception ex ){
			assertTrue(true);
		}		
	}
	
	
	/**
	 * Test product plus.
	 *
	 * @throws Exception the exception
	 */
	@Test
	@Rollback(true)
	public void testProductSaleSumProductIntegerShoes7() throws Exception {
		createTestEntitys();
		prProductPLUS(); 
		
		String description = "TEST DESCRIPTION...";
		
		// PRIVILEGES
		privilegesService.persist(new Privileges(division1, userAccount, null, warehouse1, WarehousePrivileges.PRODUCT_SALE, null, null, true, ""));
		privilegesService.persist(new Privileges(division1, userAccount, null, null, null, cash, CashPrivileges.PRODUCT_SALE, true, ""));

		// PREPEARING
		
		List<Item> items = new ArrayList<>();

		items.addAll(newPrItems(warehouse1, productInteger, 100, 0, new Number[][] { { -1 } })); 
		items.addAll(newPrItems(warehouse1, productFloat, 200, 0, new Number[][] { { -2.5 } }));  
		items.addAll(newPrItems(warehouse1, productFraction, 5, 1, new Number[][] { { -5, -5 } })); 

		items.addAll(newPrItems(warehouse1, productIntegerShoes, 4000, 0, new Number[][] { { 1 }, { 0 }, { -1 } })); // MAKE BAD DATA: 1 -> +1 1 -> 1
		items.addAll(newPrItems(warehouse1, productFloatOil, 150, 0, new Number[][] { { -2.2 }, { -1 }, { -1.2 }, {} }));
		items.addAll(newPrItems(warehouse1, productFractionTablet, 10, 1, new Number[][] { { -5, -5 }, { -1, 0 }, { -2, -3 }, { -2, -2 }, {} }));

		System.out.println("PITEMS: " + items.size());

		Item cashItem = newItem(division1, currencyKZ, cash, 4000, 0);
		Item customerItem = newItem(division1, currencyKZ, customer, -1015, 0);

		// RUN
		
		try{
		
			orderService.productSale(userAccount, division1, items, cashItem, customerItem, description);
		
			assertTrue("Bad account and summ validation!",false);
		} catch (Exception ex ){
			assertTrue(true);
		}		
	}
	
	/**
	 * Test product plus.
	 *
	 * @throws Exception the exception
	 */
	@Test
	@Rollback(true)
	public void testProductSaleSumProductIntegerShoes8() throws Exception {
		createTestEntitys();
		prProductPLUS(); 
		
		String description = "TEST DESCRIPTION...";
		
		// PRIVILEGES
		privilegesService.persist(new Privileges(division1, userAccount, null, warehouse1, WarehousePrivileges.PRODUCT_SALE, null, null, true, ""));
		privilegesService.persist(new Privileges(division1, userAccount, null, null, null, cash, CashPrivileges.PRODUCT_SALE, true, ""));

		// PREPEARING
		
		List<Item> items = new ArrayList<>();

		items.addAll(newPrItems(warehouse1, productInteger, 100, 0, new Number[][] { { -1 } })); 
		items.addAll(newPrItems(warehouse1, productFloat, 200, 0, new Number[][] { { -2.5 } }));  
		items.addAll(newPrItems(warehouse1, productFraction, 5, 1, new Number[][] { { -5, -5 } })); 

		items.addAll(newPrItems(warehouse1, productIntegerShoes, 4000, 0, new Number[][] { { -1 }, { -2 }, { 1 } })); // MAKE BAD DATA: 1 -> +1 -2 1 -> 1
		items.addAll(newPrItems(warehouse1, productFloatOil, 150, 0, new Number[][] { { -2.2 }, { -1 }, { -1.2 }, {} }));
		items.addAll(newPrItems(warehouse1, productFractionTablet, 10, 1, new Number[][] { { -5, -5 }, { -1, 0 }, { -2, -3 }, { -2, -2 }, {} }));

		System.out.println("PITEMS: " + items.size());

		Item cashItem = newItem(division1, currencyKZ, cash, 4000, 0);
		Item customerItem = newItem(division1, currencyKZ, customer, -1015, 0);

		// RUN
		
		try{
		
			orderService.productSale(userAccount, division1, items, cashItem, customerItem, description);
		
			assertTrue("Bad account and summ validation!",false);
		} catch (Exception ex ){
			assertTrue(true);
		}		
	}
	
	/**
	 * Test product sale sum product float oil.
	 *
	 * @throws Exception the exception
	 */
	@Test
	@Rollback(true)
	public void testProductSaleSumProductFloatOil1() throws Exception {
		createTestEntitys();
		prProductPLUS(); 
		
		String description = "TEST DESCRIPTION...";
		
		// PRIVILEGES
		privilegesService.persist(new Privileges(division1, userAccount, null, warehouse1, WarehousePrivileges.PRODUCT_SALE, null, null, true, ""));
		privilegesService.persist(new Privileges(division1, userAccount, null, null, null, cash, CashPrivileges.PRODUCT_SALE, true, ""));

		// PREPEARING
		
		List<Item> items = new ArrayList<>();

		items.addAll(newPrItems(warehouse1, productInteger, 100, 0, new Number[][] { { -1 } })); 
		items.addAll(newPrItems(warehouse1, productFloat, 200, 0, new Number[][] { { -2.5 } }));  
		items.addAll(newPrItems(warehouse1, productFraction, 5, 1, new Number[][] { { -5, -5 } })); 

		items.addAll(newPrItems(warehouse1, productIntegerShoes, 4000, 0, new Number[][] { { -1 }, { 0 }, { -1 } })); 
		items.addAll(newPrItems(warehouse1, productFloatOil, 150, 0, new Number[][] { { -2.2 }, { -1 }, { -1.3 }, {} })); // MAKE BAD DATA: -2.2 -> -2.2; -1 -> -1; -1.2 -> -1.3 
		items.addAll(newPrItems(warehouse1, productFractionTablet, 10, 1, new Number[][] { { -5, -5 }, { -1, 0 }, { -2, -3 }, { -2, -2 }, {} }));

		System.out.println("PITEMS: " + items.size());

		Item cashItem = newItem(division1, currencyKZ, cash, 4000, 0);
		Item customerItem = newItem(division1, currencyKZ, customer, -1015, 0);

		// RUN
		
		try{
		
			orderService.productSale(userAccount, division1, items, cashItem, customerItem, description);
		
			assertTrue("Bad account and summ validation!",false);
		} catch (Exception ex ){
			assertTrue(true);
		}		
	}
	
	/**
	 * Test product sale sum product float oil.
	 *
	 * @throws Exception the exception
	 */
	@Test
	@Rollback(true)
	public void testProductSaleSumProductFloatOil2() throws Exception {
		createTestEntitys();
		prProductPLUS(); 
		
		String description = "TEST DESCRIPTION...";
		
		// PRIVILEGES
		privilegesService.persist(new Privileges(division1, userAccount, null, warehouse1, WarehousePrivileges.PRODUCT_SALE, null, null, true, ""));
		privilegesService.persist(new Privileges(division1, userAccount, null, null, null, cash, CashPrivileges.PRODUCT_SALE, true, ""));

		// PREPEARING
		
		List<Item> items = new ArrayList<>();

		items.addAll(newPrItems(warehouse1, productInteger, 100, 0, new Number[][] { { -1 } })); 
		items.addAll(newPrItems(warehouse1, productFloat, 200, 0, new Number[][] { { -2.5 } }));  
		items.addAll(newPrItems(warehouse1, productFraction, 5, 1, new Number[][] { { -5, -5 } })); 

		items.addAll(newPrItems(warehouse1, productIntegerShoes, 4000, 0, new Number[][] { { -1 }, { 0 }, { -1 } })); 
		items.addAll(newPrItems(warehouse1, productFloatOil, 150, 0, new Number[][] { { -2.2 }, { -1 }, { -1.1 }, {} })); // MAKE BAD DATA: -2.2 -> -2.2; -1 -> -1; -1.2 -> -1.1 
		items.addAll(newPrItems(warehouse1, productFractionTablet, 10, 1, new Number[][] { { -5, -5 }, { -1, 0 }, { -2, -3 }, { -2, -2 }, {} }));

		System.out.println("PITEMS: " + items.size());

		Item cashItem = newItem(division1, currencyKZ, cash, 4000, 0);
		Item customerItem = newItem(division1, currencyKZ, customer, -1015, 0);

		// RUN
		
		try{
		
			orderService.productSale(userAccount, division1, items, cashItem, customerItem, description);
		
			assertTrue("Bad account and summ validation!",false);
		} catch (Exception ex ){
			assertTrue(true);
		}		
	}
	
	/**
	 * Test product sale sum product float oil.
	 *
	 * @throws Exception the exception
	 */
	@Test
	@Rollback(true)
	public void testProductSaleSumProductFloatOil3() throws Exception {
		createTestEntitys();
		prProductPLUS(); 
		
		String description = "TEST DESCRIPTION...";
		
		// PRIVILEGES
		privilegesService.persist(new Privileges(division1, userAccount, null, warehouse1, WarehousePrivileges.PRODUCT_SALE, null, null, true, ""));
		privilegesService.persist(new Privileges(division1, userAccount, null, null, null, cash, CashPrivileges.PRODUCT_SALE, true, ""));

		// PREPEARING
		
		List<Item> items = new ArrayList<>();

		items.addAll(newPrItems(warehouse1, productInteger, 100, 0, new Number[][] { { -1 } })); 
		items.addAll(newPrItems(warehouse1, productFloat, 200, 0, new Number[][] { { -2.5 } }));  
		items.addAll(newPrItems(warehouse1, productFraction, 5, 1, new Number[][] { { -5, -5 } })); 

		items.addAll(newPrItems(warehouse1, productIntegerShoes, 4000, 0, new Number[][] { { -1 }, { 0 }, { -1 } })); 
		items.addAll(newPrItems(warehouse1, productFloatOil, 150, 0, new Number[][] { { -2.2 }, { -1 }, { 5 }, {} })); // MAKE BAD DATA: -2.2 -> -2.2; -1 -> -1; -1.2 -> 5 
		items.addAll(newPrItems(warehouse1, productFractionTablet, 10, 1, new Number[][] { { -5, -5 }, { -1, 0 }, { -2, -3 }, { -2, -2 }, {} }));

		System.out.println("PITEMS: " + items.size());

		Item cashItem = newItem(division1, currencyKZ, cash, 4000, 0);
		Item customerItem = newItem(division1, currencyKZ, customer, -1015, 0);

		// RUN
		
		try{
		
			orderService.productSale(userAccount, division1, items, cashItem, customerItem, description);
		
			assertTrue("Bad account and summ validation!",false);
		} catch (Exception ex ){
			assertTrue(true);
		}		
	}
	
	/**
	 * Test product sale sum product float oil.
	 *
	 * @throws Exception the exception
	 */
	@Test
	@Rollback(true)
	public void testProductSaleSumProductFloatOil4() throws Exception {
		createTestEntitys();
		prProductPLUS(); 
		
		String description = "TEST DESCRIPTION...";
		
		// PRIVILEGES
		privilegesService.persist(new Privileges(division1, userAccount, null, warehouse1, WarehousePrivileges.PRODUCT_SALE, null, null, true, ""));
		privilegesService.persist(new Privileges(division1, userAccount, null, null, null, cash, CashPrivileges.PRODUCT_SALE, true, ""));

		// PREPEARING
		
		List<Item> items = new ArrayList<>();

		items.addAll(newPrItems(warehouse1, productInteger, 100, 0, new Number[][] { { -1 } })); 
		items.addAll(newPrItems(warehouse1, productFloat, 200, 0, new Number[][] { { -2.5 } }));  
		items.addAll(newPrItems(warehouse1, productFraction, 5, 1, new Number[][] { { -5, -5 } })); 

		items.addAll(newPrItems(warehouse1, productIntegerShoes, 4000, 0, new Number[][] { { -1 }, { 0 }, { -1 } })); 
		items.addAll(newPrItems(warehouse1, productFloatOil, 150, 0, new Number[][] { { -2.2 }, { -1.01 }, { -1.2 }, {} })); // MAKE BAD DATA: -2.2 -> -2.2; -1 -> -1.01; -1.2 -> -1.2 
		items.addAll(newPrItems(warehouse1, productFractionTablet, 10, 1, new Number[][] { { -5, -5 }, { -1, 0 }, { -2, -3 }, { -2, -2 }, {} }));

		System.out.println("PITEMS: " + items.size());

		Item cashItem = newItem(division1, currencyKZ, cash, 4000, 0);
		Item customerItem = newItem(division1, currencyKZ, customer, -1015, 0);

		// RUN
		
		try{
		
			orderService.productSale(userAccount, division1, items, cashItem, customerItem, description);
		
			assertTrue("Bad account and summ validation!",false);
		} catch (Exception ex ){
			assertTrue(true);
		}		
	}
	
	/**
	 * Test product sale sum product float oil.
	 *
	 * @throws Exception the exception
	 */
	@Test
	@Rollback(true)
	public void testProductSaleSumProductFloatOil5() throws Exception {
		createTestEntitys();
		prProductPLUS(); 
		
		String description = "TEST DESCRIPTION...";
		
		// PRIVILEGES
		privilegesService.persist(new Privileges(division1, userAccount, null, warehouse1, WarehousePrivileges.PRODUCT_SALE, null, null, true, ""));
		privilegesService.persist(new Privileges(division1, userAccount, null, null, null, cash, CashPrivileges.PRODUCT_SALE, true, ""));

		// PREPEARING
		
		List<Item> items = new ArrayList<>();

		items.addAll(newPrItems(warehouse1, productInteger, 100, 0, new Number[][] { { -1 } })); 
		items.addAll(newPrItems(warehouse1, productFloat, 200, 0, new Number[][] { { -2.5 } }));  
		items.addAll(newPrItems(warehouse1, productFraction, 5, 1, new Number[][] { { -5, -5 } })); 

		items.addAll(newPrItems(warehouse1, productIntegerShoes, 4000, 0, new Number[][] { { -1 }, { 0 }, { -1 } })); 
		items.addAll(newPrItems(warehouse1, productFloatOil, 150, 0, new Number[][] { { -2.2 }, { -0.99 }, { -1.2 }, {} })); // MAKE BAD DATA: -2.2 -> -2.2; -1 -> -0.99; -1.2 -> -1.2 
		items.addAll(newPrItems(warehouse1, productFractionTablet, 10, 1, new Number[][] { { -5, -5 }, { -1, 0 }, { -2, -3 }, { -2, -2 }, {} }));

		System.out.println("PITEMS: " + items.size());

		Item cashItem = newItem(division1, currencyKZ, cash, 4000, 0);
		Item customerItem = newItem(division1, currencyKZ, customer, -1015, 0);

		// RUN
		
		try{
		
			orderService.productSale(userAccount, division1, items, cashItem, customerItem, description);
		
			assertTrue("Bad account and summ validation!",false);
		} catch (Exception ex ){
			assertTrue(true);
		}		
	}
	
	
	/**
	 * Test product sale sum product float oil.
	 *
	 * @throws Exception the exception
	 */
	@Test
	@Rollback(true)
	public void testProductSaleSumProductFloatOil6() throws Exception {
		createTestEntitys();
		prProductPLUS(); 
		
		String description = "TEST DESCRIPTION...";
		
		// PRIVILEGES
		privilegesService.persist(new Privileges(division1, userAccount, null, warehouse1, WarehousePrivileges.PRODUCT_SALE, null, null, true, ""));
		privilegesService.persist(new Privileges(division1, userAccount, null, null, null, cash, CashPrivileges.PRODUCT_SALE, true, ""));

		// PREPEARING
		
		List<Item> items = new ArrayList<>();

		items.addAll(newPrItems(warehouse1, productInteger, 100, 0, new Number[][] { { -1 } })); 
		items.addAll(newPrItems(warehouse1, productFloat, 200, 0, new Number[][] { { -2.5 } }));  
		items.addAll(newPrItems(warehouse1, productFraction, 5, 1, new Number[][] { { -5, -5 } })); 

		items.addAll(newPrItems(warehouse1, productIntegerShoes, 4000, 0, new Number[][] { { -1 }, { 0 }, { -1 } })); 
		items.addAll(newPrItems(warehouse1, productFloatOil, 150, 0, new Number[][] { { -2.2 }, { 1 }, { -1.2 }, {} })); // MAKE BAD DATA: -2.2 -> -2.2; -1 -> 1; -1.2 -> -1.2 
		items.addAll(newPrItems(warehouse1, productFractionTablet, 10, 1, new Number[][] { { -5, -5 }, { -1, 0 }, { -2, -3 }, { -2, -2 }, {} }));

		System.out.println("PITEMS: " + items.size());

		Item cashItem = newItem(division1, currencyKZ, cash, 4000, 0);
		Item customerItem = newItem(division1, currencyKZ, customer, -1015, 0);

		// RUN
		
		try{
		
			orderService.productSale(userAccount, division1, items, cashItem, customerItem, description);
		
			assertTrue("Bad account and summ validation!",false);
		} catch (Exception ex ){
			assertTrue(true);
		}		
	}
	
	/**
	 * Test product sale sum product float oil.
	 *
	 * @throws Exception the exception
	 */
	@Test
	@Rollback(true)
	public void testProductSaleSumProductFloatOil7() throws Exception {
		createTestEntitys();
		prProductPLUS(); 
		
		String description = "TEST DESCRIPTION...";
		
		// PRIVILEGES
		privilegesService.persist(new Privileges(division1, userAccount, null, warehouse1, WarehousePrivileges.PRODUCT_SALE, null, null, true, ""));
		privilegesService.persist(new Privileges(division1, userAccount, null, null, null, cash, CashPrivileges.PRODUCT_SALE, true, ""));

		// PREPEARING
		
		List<Item> items = new ArrayList<>();

		items.addAll(newPrItems(warehouse1, productInteger, 100, 0, new Number[][] { { -1 } })); 
		items.addAll(newPrItems(warehouse1, productFloat, 200, 0, new Number[][] { { -2.5 } }));  
		items.addAll(newPrItems(warehouse1, productFraction, 5, 1, new Number[][] { { -5, -5 } })); 

		items.addAll(newPrItems(warehouse1, productIntegerShoes, 4000, 0, new Number[][] { { -1 }, { 0 }, { -1 } })); 
		items.addAll(newPrItems(warehouse1, productFloatOil, 150, 0, new Number[][] { { -2.3 }, { -1 }, { -1.2 }, {} })); // MAKE BAD DATA: -2.2 -> -2.3; -1 -> -1; -1.2 -> -1.2 
		items.addAll(newPrItems(warehouse1, productFractionTablet, 10, 1, new Number[][] { { -5, -5 }, { -1, 0 }, { -2, -3 }, { -2, -2 }, {} }));

		System.out.println("PITEMS: " + items.size());

		Item cashItem = newItem(division1, currencyKZ, cash, 4000, 0);
		Item customerItem = newItem(division1, currencyKZ, customer, -1015, 0);

		// RUN
		
		try{
		
			orderService.productSale(userAccount, division1, items, cashItem, customerItem, description);
		
			assertTrue("Bad account and summ validation!",false);
		} catch (Exception ex ){
			assertTrue(true);
		}		
	}
	
	/**
	 * Test product sale sum product float oil.
	 *
	 * @throws Exception the exception
	 */
	@Test
	@Rollback(true)
	public void testProductSaleSumProductFloatOil8() throws Exception {
		createTestEntitys();
		prProductPLUS(); 
		
		String description = "TEST DESCRIPTION...";
		
		// PRIVILEGES
		privilegesService.persist(new Privileges(division1, userAccount, null, warehouse1, WarehousePrivileges.PRODUCT_SALE, null, null, true, ""));
		privilegesService.persist(new Privileges(division1, userAccount, null, null, null, cash, CashPrivileges.PRODUCT_SALE, true, ""));

		// PREPEARING
		
		List<Item> items = new ArrayList<>();

		items.addAll(newPrItems(warehouse1, productInteger, 100, 0, new Number[][] { { -1 } })); 
		items.addAll(newPrItems(warehouse1, productFloat, 200, 0, new Number[][] { { -2.5 } }));  
		items.addAll(newPrItems(warehouse1, productFraction, 5, 1, new Number[][] { { -5, -5 } })); 

		items.addAll(newPrItems(warehouse1, productIntegerShoes, 4000, 0, new Number[][] { { -1 }, { 0 }, { -1 } })); 
		items.addAll(newPrItems(warehouse1, productFloatOil, 150, 0, new Number[][] { { -2.1 }, { -1 }, { -1.2 }, {} })); // MAKE BAD DATA: -2.2 -> -2.1; -1 -> -1; -1.2 -> -1.2 
		items.addAll(newPrItems(warehouse1, productFractionTablet, 10, 1, new Number[][] { { -5, -5 }, { -1, 0 }, { -2, -3 }, { -2, -2 }, {} }));

		System.out.println("PITEMS: " + items.size());

		Item cashItem = newItem(division1, currencyKZ, cash, 4000, 0);
		Item customerItem = newItem(division1, currencyKZ, customer, -1015, 0);

		// RUN
		
		try{
		
			orderService.productSale(userAccount, division1, items, cashItem, customerItem, description);
		
			assertTrue("Bad account and summ validation!",false);
		} catch (Exception ex ){
			assertTrue(true);
		}		
	}
	
	
	/**
	 * Test product sale sum product float oil.
	 *
	 * @throws Exception the exception
	 */
	@Test
	@Rollback(true)
	public void testProductSaleSumProductFloatOil9() throws Exception {
		createTestEntitys();
		prProductPLUS(); 
		
		String description = "TEST DESCRIPTION...";
		
		// PRIVILEGES
		privilegesService.persist(new Privileges(division1, userAccount, null, warehouse1, WarehousePrivileges.PRODUCT_SALE, null, null, true, ""));
		privilegesService.persist(new Privileges(division1, userAccount, null, null, null, cash, CashPrivileges.PRODUCT_SALE, true, ""));

		// PREPEARING
		
		List<Item> items = new ArrayList<>();

		items.addAll(newPrItems(warehouse1, productInteger, 100, 0, new Number[][] { { -1 } })); 
		items.addAll(newPrItems(warehouse1, productFloat, 200, 0, new Number[][] { { -2.5 } }));  
		items.addAll(newPrItems(warehouse1, productFraction, 5, 1, new Number[][] { { -5, -5 } })); 

		items.addAll(newPrItems(warehouse1, productIntegerShoes, 4000, 0, new Number[][] { { -1 }, { 0 }, { -1 } })); 
		items.addAll(newPrItems(warehouse1, productFloatOil, 150, 0, new Number[][] { { 2.2 }, { -1 }, { -1.2 }, {} })); // MAKE BAD DATA: -2.2 -> 2.2; -1 -> -1; -1.2 -> -1.2 
		items.addAll(newPrItems(warehouse1, productFractionTablet, 10, 1, new Number[][] { { -5, -5 }, { -1, 0 }, { -2, -3 }, { -2, -2 }, {} }));

		System.out.println("PITEMS: " + items.size());

		Item cashItem = newItem(division1, currencyKZ, cash, 4000, 0);
		Item customerItem = newItem(division1, currencyKZ, customer, -1015, 0);

		// RUN
		
		try{
		
			orderService.productSale(userAccount, division1, items, cashItem, customerItem, description);
		
			assertTrue("Bad account and summ validation!",false);
		} catch (Exception ex ){
			assertTrue(true);
		}		
	}
	
	/**
	 * Test product sale sum product float oil.
	 *
	 * @throws Exception the exception
	 */
	@Test
	@Rollback(true)
	public void testProductSaleSumProductFloatOil10() throws Exception {
		createTestEntitys();
		prProductPLUS(); 
		
		String description = "TEST DESCRIPTION...";
		
		// PRIVILEGES
		privilegesService.persist(new Privileges(division1, userAccount, null, warehouse1, WarehousePrivileges.PRODUCT_SALE, null, null, true, ""));
		privilegesService.persist(new Privileges(division1, userAccount, null, null, null, cash, CashPrivileges.PRODUCT_SALE, true, ""));

		// PREPEARING
		
		List<Item> items = new ArrayList<>();

		items.addAll(newPrItems(warehouse1, productInteger, 100, 0, new Number[][] { { -1 } })); 
		items.addAll(newPrItems(warehouse1, productFloat, 200, 0, new Number[][] { { -2.5 } }));  
		items.addAll(newPrItems(warehouse1, productFraction, 5, 1, new Number[][] { { -5, -5 } })); 

		items.addAll(newPrItems(warehouse1, productIntegerShoes, 4000, 0, new Number[][] { { -1 }, { 0 }, { -1 } })); 
		items.addAll(newPrItems(warehouse1, productFloatOil, 150, 0, new Number[][] { { -2.2 }, { 1 }, { -3.2 }, {} })); // MAKE BAD DATA: -2.2 -> -2.2; -1 -> 1; -1.2 -> -3.2 
		items.addAll(newPrItems(warehouse1, productFractionTablet, 10, 1, new Number[][] { { -5, -5 }, { -1, 0 }, { -2, -3 }, { -2, -2 }, {} }));

		System.out.println("PITEMS: " + items.size());

		Item cashItem = newItem(division1, currencyKZ, cash, 4000, 0);
		Item customerItem = newItem(division1, currencyKZ, customer, -1015, 0);

		// RUN
		
		try{
		
			orderService.productSale(userAccount, division1, items, cashItem, customerItem, description);
		
			assertTrue("Bad account and summ validation!",false);
		} catch (Exception ex ){
			assertTrue(true);
		}		
	}
	
	/**
	 * Test product sale sum product float oil.
	 *
	 * @throws Exception the exception
	 */
	@Test
	@Rollback(true)
	public void testProductSaleSumProductFloatOil11() throws Exception {
		createTestEntitys();
		prProductPLUS(); 
		
		String description = "TEST DESCRIPTION...";
		
		// PRIVILEGES
		privilegesService.persist(new Privileges(division1, userAccount, null, warehouse1, WarehousePrivileges.PRODUCT_SALE, null, null, true, ""));
		privilegesService.persist(new Privileges(division1, userAccount, null, null, null, cash, CashPrivileges.PRODUCT_SALE, true, ""));

		// PREPEARING
		
		List<Item> items = new ArrayList<>();

		items.addAll(newPrItems(warehouse1, productInteger, 100, 0, new Number[][] { { -1 } })); 
		items.addAll(newPrItems(warehouse1, productFloat, 200, 0, new Number[][] { { -2.5 } }));  
		items.addAll(newPrItems(warehouse1, productFraction, 5, 1, new Number[][] { { -5, -5 } })); 

		items.addAll(newPrItems(warehouse1, productIntegerShoes, 4000, 0, new Number[][] { { -1 }, { 0 }, { -1 } })); 
		items.addAll(newPrItems(warehouse1, productFloatOil, 150, 0, new Number[][] { { -2.2 } })); // MAKE BAD DATA: -2.2 -> -2.2 ......... 
		items.addAll(newPrItems(warehouse1, productFractionTablet, 10, 1, new Number[][] { { -5, -5 }, { -1, 0 }, { -2, -3 }, { -2, -2 }, {} }));

		System.out.println("PITEMS: " + items.size());

		Item cashItem = newItem(division1, currencyKZ, cash, 4000, 0);
		Item customerItem = newItem(division1, currencyKZ, customer, -1015, 0);

		// RUN
		
		try{
		
			orderService.productSale(userAccount, division1, items, cashItem, customerItem, description);
		
			assertTrue("Bad account and summ validation!",false);
		} catch (Exception ex ){
			assertTrue(true);
		}		
	}
	
	
	/**
	 * Test product sale sum product fraction tablet.
	 *
	 * @throws Exception the exception
	 */
	@Test
	@Rollback(true)
	public void testProductSaleSumProductFractionTablet1() throws Exception {
		createTestEntitys();
		prProductPLUS(); 
		
		String description = "TEST DESCRIPTION...";
		
		// PRIVILEGES
		privilegesService.persist(new Privileges(division1, userAccount, null, warehouse1, WarehousePrivileges.PRODUCT_SALE, null, null, true, ""));
		privilegesService.persist(new Privileges(division1, userAccount, null, null, null, cash, CashPrivileges.PRODUCT_SALE, true, ""));

		// PREPEARING
		
		List<Item> items = new ArrayList<>();

		items.addAll(newPrItems(warehouse1, productInteger, 100, 0, new Number[][] { { -1 } })); 
		items.addAll(newPrItems(warehouse1, productFloat, 200, 0, new Number[][] { { -2.5 } }));  
		items.addAll(newPrItems(warehouse1, productFraction, 5, 1, new Number[][] { { -5, -5 } })); 

		items.addAll(newPrItems(warehouse1, productIntegerShoes, 4000, 0, new Number[][] { { -1 }, { 0 }, { -1 } })); 
		items.addAll(newPrItems(warehouse1, productFloatOil, 150, 0, new Number[][] { { -2.2 }, { -1 }, { -1.2 }, {} }));  
		items.addAll(newPrItems(warehouse1, productFractionTablet, 10, 1, new Number[][] { { -5, -5 }, { -1, 0 }, { -2, -3 }, { -2, -3 }, {} })); // MAKE BAD DATA: -2;-2 -> -2 -3

		System.out.println("PITEMS: " + items.size());

		Item cashItem = newItem(division1, currencyKZ, cash, 4000, 0);
		Item customerItem = newItem(division1, currencyKZ, customer, -1015, 0);

		// RUN
		
		try{
		
			orderService.productSale(userAccount, division1, items, cashItem, customerItem, description);
		
			assertTrue("Bad account and summ validation!",false);
		} catch (Exception ex ){
			assertTrue(true);
		}		
	}
	
	/**
	 * Test product sale sum product fraction tablet5.
	 *
	 * @throws Exception the exception
	 */
	@Test
	@Rollback(true)
	public void testProductSaleSumProductFractionTablet() throws Exception {
		createTestEntitys();
		prProductPLUS(); 
		
		String description = "TEST DESCRIPTION...";
		
		// PRIVILEGES
		privilegesService.persist(new Privileges(division1, userAccount, null, warehouse1, WarehousePrivileges.PRODUCT_SALE, null, null, true, ""));
		privilegesService.persist(new Privileges(division1, userAccount, null, null, null, cash, CashPrivileges.PRODUCT_SALE, true, ""));

		// PREPEARING
		
		List<Item> items = new ArrayList<>();

		items.addAll(newPrItems(warehouse1, productInteger, 100, 0, new Number[][] { { -1 } })); 
		items.addAll(newPrItems(warehouse1, productFloat, 200, 0, new Number[][] { { -2.5 } }));  
		items.addAll(newPrItems(warehouse1, productFraction, 5, 1, new Number[][] { { -5, -5 } })); 

		items.addAll(newPrItems(warehouse1, productIntegerShoes, 4000, 0, new Number[][] { { -1 }, { 0 }, { -1 } })); 
		items.addAll(newPrItems(warehouse1, productFloatOil, 150, 0, new Number[][] { { -2.2 }, { -1 }, { -1.2 }, {} }));  
		items.addAll(newPrItems(warehouse1, productFractionTablet, 10, 1, new Number[][] { { -5, -5 }, { -1, 0 }, { -2, -3 }, { -2, -1 }, {} })); // MAKE BAD DATA: -2;-2 -> -2 -3

		System.out.println("PITEMS: " + items.size());

		Item cashItem = newItem(division1, currencyKZ, cash, 4000, 0);
		Item customerItem = newItem(division1, currencyKZ, customer, -1015, 0);

		// RUN
		
		try{
		
			orderService.productSale(userAccount, division1, items, cashItem, customerItem, description);
		
			assertTrue("Bad account and summ validation!",false);
		} catch (Exception ex ){
			assertTrue(true);
		}		
	}
	
	/**
	 * Test product sale sum product fraction tablet.
	 *
	 * @throws Exception the exception
	 */
	@Test
	@Rollback(true)
	public void testProductSaleSumProductFractionTablet3() throws Exception {
		createTestEntitys();
		prProductPLUS(); 
		
		String description = "TEST DESCRIPTION...";
		
		// PRIVILEGES
		privilegesService.persist(new Privileges(division1, userAccount, null, warehouse1, WarehousePrivileges.PRODUCT_SALE, null, null, true, ""));
		privilegesService.persist(new Privileges(division1, userAccount, null, null, null, cash, CashPrivileges.PRODUCT_SALE, true, ""));

		// PREPEARING
		
		List<Item> items = new ArrayList<>();

		items.addAll(newPrItems(warehouse1, productInteger, 100, 0, new Number[][] { { -1 } })); 
		items.addAll(newPrItems(warehouse1, productFloat, 200, 0, new Number[][] { { -2.5 } }));  
		items.addAll(newPrItems(warehouse1, productFraction, 5, 1, new Number[][] { { -5, -5 } })); 

		items.addAll(newPrItems(warehouse1, productIntegerShoes, 4000, 0, new Number[][] { { -1 }, { 0 }, { -1 } })); 
		items.addAll(newPrItems(warehouse1, productFloatOil, 150, 0, new Number[][] { { -2.2 }, { -1 }, { -1.2 }, {} }));  
		items.addAll(newPrItems(warehouse1, productFractionTablet, 10, 1, new Number[][] { { -5, -5 }, { -1, 0 }, { -2, -3 }, { 2, 2 }, {} })); // MAKE BAD DATA: -2;-2 -> 2;2

		System.out.println("PITEMS: " + items.size());

		Item cashItem = newItem(division1, currencyKZ, cash, 4000, 0);
		Item customerItem = newItem(division1, currencyKZ, customer, -1015, 0);

		// RUN
		
		try{
		
			orderService.productSale(userAccount, division1, items, cashItem, customerItem, description);
		
			assertTrue("Bad account and summ validation!",false);
		} catch (Exception ex ){
			assertTrue(true);
		}		
	}
	
	/**
	 * Test product sale sum product fraction tablet.
	 *
	 * @throws Exception the exception
	 */
	@Test
	@Rollback(true)
	public void testProductSaleSumProductFractionTablet4() throws Exception {
		createTestEntitys();
		prProductPLUS(); 
		
		String description = "TEST DESCRIPTION...";
		
		// PRIVILEGES
		privilegesService.persist(new Privileges(division1, userAccount, null, warehouse1, WarehousePrivileges.PRODUCT_SALE, null, null, true, ""));
		privilegesService.persist(new Privileges(division1, userAccount, null, null, null, cash, CashPrivileges.PRODUCT_SALE, true, ""));

		// PREPEARING
		
		List<Item> items = new ArrayList<>();

		items.addAll(newPrItems(warehouse1, productInteger, 100, 0, new Number[][] { { -1 } })); 
		items.addAll(newPrItems(warehouse1, productFloat, 200, 0, new Number[][] { { -2.5 } }));  
		items.addAll(newPrItems(warehouse1, productFraction, 5, 1, new Number[][] { { -5, -5 } })); 

		items.addAll(newPrItems(warehouse1, productIntegerShoes, 4000, 0, new Number[][] { { -1 }, { 0 }, { -1 } })); 
		items.addAll(newPrItems(warehouse1, productFloatOil, 150, 0, new Number[][] { { -2.2 }, { -1 }, { -1.2 }, {} }));  
		items.addAll(newPrItems(warehouse1, productFractionTablet, 10, 1, new Number[][] { { -5, -7 }, { -1, 0 }, { -2, -3 }, { -2, -2 }, {} })); // MAKE BAD DATA: -5;-5 -> -5;-7

		System.out.println("PITEMS: " + items.size());

		Item cashItem = newItem(division1, currencyKZ, cash, 4000, 0);
		Item customerItem = newItem(division1, currencyKZ, customer, -1015, 0);

		// RUN
		
		try{
		
			orderService.productSale(userAccount, division1, items, cashItem, customerItem, description);
		
			assertTrue("Bad account and summ validation!",false);
		} catch (Exception ex ){
			assertTrue(true);
		}		
	}
	
	/**
	 * Test product sale sum product fraction tablet.
	 *
	 * @throws Exception the exception
	 */
	@Test
	@Rollback(true)
	public void testProductSaleSumProductFractionTablet5() throws Exception {
		createTestEntitys();
		prProductPLUS(); 
		
		String description = "TEST DESCRIPTION...";
		
		// PRIVILEGES
		privilegesService.persist(new Privileges(division1, userAccount, null, warehouse1, WarehousePrivileges.PRODUCT_SALE, null, null, true, ""));
		privilegesService.persist(new Privileges(division1, userAccount, null, null, null, cash, CashPrivileges.PRODUCT_SALE, true, ""));

		// PREPEARING
		
		List<Item> items = new ArrayList<>();

		items.addAll(newPrItems(warehouse1, productInteger, 100, 0, new Number[][] { { -1 } })); 
		items.addAll(newPrItems(warehouse1, productFloat, 200, 0, new Number[][] { { -2.5 } }));  
		items.addAll(newPrItems(warehouse1, productFraction, 5, 1, new Number[][] { { -5, -5 } })); 

		items.addAll(newPrItems(warehouse1, productIntegerShoes, 4000, 0, new Number[][] { { -1 }, { 0 }, { -1 } })); 
		items.addAll(newPrItems(warehouse1, productFloatOil, 150, 0, new Number[][] { { -2.2 }, { -1 }, { -1.2 }, {} }));  
		items.addAll(newPrItems(warehouse1, productFractionTablet, 10, 1, new Number[][] { { -5, -4 }, { -1, 0 }, { -2, -3 }, { -2, -2 }, {} })); // MAKE BAD DATA: -5;-5 -> -5;-4

		System.out.println("PITEMS: " + items.size());

		Item cashItem = newItem(division1, currencyKZ, cash, 4000, 0);
		Item customerItem = newItem(division1, currencyKZ, customer, -1015, 0);

		// RUN
		
		try{
		
			orderService.productSale(userAccount, division1, items, cashItem, customerItem, description);
		
			assertTrue("Bad account and summ validation!",false);
		} catch (Exception ex ){
			assertTrue(true);
		}		
	}
	

	/**
	 * Test product sale sum product fraction tablet.
	 *
	 * @throws Exception the exception
	 */
	@Test
	@Rollback(true)
	public void testProductSaleSumProductFractionTablet6() throws Exception {
		createTestEntitys();
		prProductPLUS(); 
		
		String description = "TEST DESCRIPTION...";
		
		// PRIVILEGES
		privilegesService.persist(new Privileges(division1, userAccount, null, warehouse1, WarehousePrivileges.PRODUCT_SALE, null, null, true, ""));
		privilegesService.persist(new Privileges(division1, userAccount, null, null, null, cash, CashPrivileges.PRODUCT_SALE, true, ""));

		// PREPEARING
		
		List<Item> items = new ArrayList<>();

		items.addAll(newPrItems(warehouse1, productInteger, 100, 0, new Number[][] { { -1 } })); 
		items.addAll(newPrItems(warehouse1, productFloat, 200, 0, new Number[][] { { -2.5 } }));  
		items.addAll(newPrItems(warehouse1, productFraction, 5, 1, new Number[][] { { -5, -5 } })); 

		items.addAll(newPrItems(warehouse1, productIntegerShoes, 4000, 0, new Number[][] { { -1 }, { 0 }, { -1 } })); 
		items.addAll(newPrItems(warehouse1, productFloatOil, 150, 0, new Number[][] { { -2.2 }, { -1 }, { -1.2 }, {} }));  
		items.addAll(newPrItems(warehouse1, productFractionTablet, 10, 1, new Number[][] { { -4, -5 }, { -1, 0 }, { -2, -3 }, { -2, -2 }, {} })); // MAKE BAD DATA: -5;-5 -> -4;-5

		System.out.println("PITEMS: " + items.size());

		Item cashItem = newItem(division1, currencyKZ, cash, 4000, 0);
		Item customerItem = newItem(division1, currencyKZ, customer, -1015, 0);

		// RUN
		
		try{
		
			orderService.productSale(userAccount, division1, items, cashItem, customerItem, description);
		
			assertTrue("Bad account and summ validation!",false);
		} catch (Exception ex ){
			assertTrue(true);
		}		
	}
	
	/**
	 * Test product sale sum product fraction tablet.
	 *
	 * @throws Exception the exception
	 */
	@Test
	@Rollback(true)
	public void testProductSaleSumProductFractionTablet7() throws Exception {
		createTestEntitys();
		prProductPLUS(); 
		
		String description = "TEST DESCRIPTION...";
		
		// PRIVILEGES
		privilegesService.persist(new Privileges(division1, userAccount, null, warehouse1, WarehousePrivileges.PRODUCT_SALE, null, null, true, ""));
		privilegesService.persist(new Privileges(division1, userAccount, null, null, null, cash, CashPrivileges.PRODUCT_SALE, true, ""));

		// PREPEARING
		
		List<Item> items = new ArrayList<>();

		items.addAll(newPrItems(warehouse1, productInteger, 100, 0, new Number[][] { { -1 } })); 
		items.addAll(newPrItems(warehouse1, productFloat, 200, 0, new Number[][] { { -2.5 } }));  
		items.addAll(newPrItems(warehouse1, productFraction, 5, 1, new Number[][] { { -5, -5 } })); 

		items.addAll(newPrItems(warehouse1, productIntegerShoes, 4000, 0, new Number[][] { { -1 }, { 0 }, { -1 } })); 
		items.addAll(newPrItems(warehouse1, productFloatOil, 150, 0, new Number[][] { { -2.2 }, { -1 }, { -1.2 }, {} }));  
		items.addAll(newPrItems(warehouse1, productFractionTablet, 10, 1, new Number[][] { { -6, -5 }, { -1, 0 }, { -2, -3 }, { -2, -2 }, {} })); // MAKE BAD DATA: -5;-5 -> -6;-5

		System.out.println("PITEMS: " + items.size());

		Item cashItem = newItem(division1, currencyKZ, cash, 4000, 0);
		Item customerItem = newItem(division1, currencyKZ, customer, -1015, 0);

		// RUN
		
		try{
		
			orderService.productSale(userAccount, division1, items, cashItem, customerItem, description);
		
			assertTrue("Bad account and summ validation!",false);
		} catch (Exception ex ){
			assertTrue(true);
		}		
	}
	
	/**
	 * Test product sale sum product fraction tablet.
	 *
	 * @throws Exception the exception
	 */
	@Test
	@Rollback(true)
	public void testProductSaleSumProductFractionTablet8() throws Exception {
		createTestEntitys();
		prProductPLUS(); 
		
		String description = "TEST DESCRIPTION...";
		
		// PRIVILEGES
		privilegesService.persist(new Privileges(division1, userAccount, null, warehouse1, WarehousePrivileges.PRODUCT_SALE, null, null, true, ""));
		privilegesService.persist(new Privileges(division1, userAccount, null, null, null, cash, CashPrivileges.PRODUCT_SALE, true, ""));

		// PREPEARING
		
		List<Item> items = new ArrayList<>();

		items.addAll(newPrItems(warehouse1, productInteger, 100, 0, new Number[][] { { -1 } })); 
		items.addAll(newPrItems(warehouse1, productFloat, 200, 0, new Number[][] { { -2.5 } }));  
		items.addAll(newPrItems(warehouse1, productFraction, 5, 1, new Number[][] { { -5, -5 } })); 

		items.addAll(newPrItems(warehouse1, productIntegerShoes, 4000, 0, new Number[][] { { -1 }, { 0 }, { -1 } })); 
		items.addAll(newPrItems(warehouse1, productFloatOil, 150, 0, new Number[][] { { -2.2 }, { -1 }, { -1.2 }, {} }));  
		items.addAll(newPrItems(warehouse1, productFractionTablet, 10, 1, new Number[][] { { 5, 5 }, { -1, 0 }, { -2, -3 }, { -2, -2 }, {} })); // MAKE BAD DATA: -5;-5 -> 5;5

		System.out.println("PITEMS: " + items.size());

		Item cashItem = newItem(division1, currencyKZ, cash, 4000, 0);
		Item customerItem = newItem(division1, currencyKZ, customer, -1015, 0);

		// RUN
		
		try{
		
			orderService.productSale(userAccount, division1, items, cashItem, customerItem, description);
		
			assertTrue("Bad account and summ validation!",false);
		} catch (Exception ex ){
			assertTrue(true);
		}		
	}
	
	/**
	 * Test product sale sum product fraction tablet.
	 *
	 * @throws Exception the exception
	 */
	@Test
	@Rollback(true)
	public void testProductSaleSumProductFractionTablet9() throws Exception {
		createTestEntitys();
		prProductPLUS(); 
		
		String description = "TEST DESCRIPTION...";
		
		// PRIVILEGES
		privilegesService.persist(new Privileges(division1, userAccount, null, warehouse1, WarehousePrivileges.PRODUCT_SALE, null, null, true, ""));
		privilegesService.persist(new Privileges(division1, userAccount, null, null, null, cash, CashPrivileges.PRODUCT_SALE, true, ""));

		// PREPEARING
		
		List<Item> items = new ArrayList<>();

		items.addAll(newPrItems(warehouse1, productInteger, 100, 0, new Number[][] { { -1 } })); 
		items.addAll(newPrItems(warehouse1, productFloat, 200, 0, new Number[][] { { -2.5 } }));  
		items.addAll(newPrItems(warehouse1, productFraction, 5, 1, new Number[][] { { -5, -5 } })); 

		items.addAll(newPrItems(warehouse1, productIntegerShoes, 4000, 0, new Number[][] { { -1 }, { 0 }, { -1 } })); 
		items.addAll(newPrItems(warehouse1, productFloatOil, 150, 0, new Number[][] { { -2.2 }, { -1 }, { -1.2 }, {} }));  
		items.addAll(newPrItems(warehouse1, productFractionTablet, 10, 1, new Number[][] { { -5, 5 }, { -1, 0 }, { -2, -3 }, { -2, -2 }, {} })); // MAKE BAD DATA: -5;-5 -> -5;5

		System.out.println("PITEMS: " + items.size());

		Item cashItem = newItem(division1, currencyKZ, cash, 4000, 0);
		Item customerItem = newItem(division1, currencyKZ, customer, -1015, 0);

		// RUN
		
		try{
		
			orderService.productSale(userAccount, division1, items, cashItem, customerItem, description);
		
			assertTrue("Bad account and summ validation!",false);
		} catch (Exception ex ){
			assertTrue(true);
		}		
	}
	
	/**
	 * Test product sale sum product fraction tablet.
	 *
	 * @throws Exception the exception
	 */
	@Test
	@Rollback(true)
	public void testProductSaleSumProductFractionTablet10() throws Exception {
		createTestEntitys();
		prProductPLUS(); 
		
		String description = "TEST DESCRIPTION...";
		
		// PRIVILEGES
		privilegesService.persist(new Privileges(division1, userAccount, null, warehouse1, WarehousePrivileges.PRODUCT_SALE, null, null, true, ""));
		privilegesService.persist(new Privileges(division1, userAccount, null, null, null, cash, CashPrivileges.PRODUCT_SALE, true, ""));

		// PREPEARING
		
		List<Item> items = new ArrayList<>();

		items.addAll(newPrItems(warehouse1, productInteger, 100, 0, new Number[][] { { -1 } })); 
		items.addAll(newPrItems(warehouse1, productFloat, 200, 0, new Number[][] { { -2.5 } }));  
		items.addAll(newPrItems(warehouse1, productFraction, 5, 1, new Number[][] { { -5, -5 } })); 

		items.addAll(newPrItems(warehouse1, productIntegerShoes, 4000, 0, new Number[][] { { -1 }, { 0 }, { -1 } })); 
		items.addAll(newPrItems(warehouse1, productFloatOil, 150, 0, new Number[][] { { -2.2 }, { -1 }, { -1.2 }, {} }));  
		items.addAll(newPrItems(warehouse1, productFractionTablet, 10, 1, new Number[][] { { -5, -5 }, { 0, 0 }, { -7, -7 }, { 2, 2 }, {} })); // MAKE BAD DATA: -1;0 -> 0;0 , -2;-3 -> -7;-7, -2;-2 -> 2;2

		System.out.println("PITEMS: " + items.size());

		Item cashItem = newItem(division1, currencyKZ, cash, 4000, 0);
		Item customerItem = newItem(division1, currencyKZ, customer, -1015, 0);

		// RUN
		
		try{
		
			orderService.productSale(userAccount, division1, items, cashItem, customerItem, description);
		
			assertTrue("Bad account and summ validation!",false);
		} catch (Exception ex ){
			assertTrue(true);
		}		
	}
	
	
	/**
	 * Test product sale sum product fraction tablet.
	 *
	 * @throws Exception the exception
	 */
	@Test
	@Rollback(true)
	public void testProductSaleSumProductFractionTablet11() throws Exception {
		createTestEntitys();
		prProductPLUS(); 
		
		String description = "TEST DESCRIPTION...";
		
		// PRIVILEGES
		privilegesService.persist(new Privileges(division1, userAccount, null, warehouse1, WarehousePrivileges.PRODUCT_SALE, null, null, true, ""));
		privilegesService.persist(new Privileges(division1, userAccount, null, null, null, cash, CashPrivileges.PRODUCT_SALE, true, ""));

		// PREPEARING
		
		List<Item> items = new ArrayList<>();

		items.addAll(newPrItems(warehouse1, productInteger, 100, 0, new Number[][] { { -1 } })); 
		items.addAll(newPrItems(warehouse1, productFloat, 200, 0, new Number[][] { { -2.5 } }));  
		items.addAll(newPrItems(warehouse1, productFraction, 5, 1, new Number[][] { { -5, -5 } })); 

		items.addAll(newPrItems(warehouse1, productIntegerShoes, 4000, 0, new Number[][] { { -1 }, { 0 }, { -1 } })); 
		items.addAll(newPrItems(warehouse1, productFloatOil, 150, 0, new Number[][] { { -2.2 }, { -1 }, { -1.2 }, {} }));  
		items.addAll(newPrItems(warehouse1, productFractionTablet, 10, 1, new Number[][] { { -5, -5 }, { 0, 0 }, { -7, -7 }, { 2, 2 }, {} })); // MAKE BAD DATA: -1;0 -> 0;0 , -2;-3 -> -7;-7, -2;-2 -> 2;2

		System.out.println("PITEMS: " + items.size());

		Item cashItem = newItem(division1, currencyKZ, cash, 4000, 0);
		Item customerItem = newItem(division1, currencyKZ, customer, -1015, 0);

		// RUN
		
		try{
		
			orderService.productSale(userAccount, division1, items, cashItem, customerItem, description);
		
			assertTrue("Bad account and summ validation!",false);
		} catch (Exception ex ){
			assertTrue(true);
		}		
	}
	
	
	/**
	 * Test product sale sum cash.
	 *
	 * @throws Exception the exception
	 */
	@Test
	@Rollback(true)
	public void testProductSaleSumCash1() throws Exception {
		createTestEntitys();
		prProductPLUS(); 
		
		String description = "TEST DESCRIPTION...";
		
		// PRIVILEGES
		privilegesService.persist(new Privileges(division1, userAccount, null, warehouse1, WarehousePrivileges.PRODUCT_SALE, null, null, true, ""));
		privilegesService.persist(new Privileges(division1, userAccount, null, null, null, cash, CashPrivileges.PRODUCT_SALE, true, ""));

		// PREPEARING
		
		List<Item> items = new ArrayList<>();

		items.addAll(newPrItems(warehouse1, productInteger, 100, 0, new Number[][] { { -1 } })); 
		items.addAll(newPrItems(warehouse1, productFloat, 200, 0, new Number[][] { { -2.5 } }));  
		items.addAll(newPrItems(warehouse1, productFraction, 5, 1, new Number[][] { { -5, -5 } })); 

		items.addAll(newPrItems(warehouse1, productIntegerShoes, 4000, 0, new Number[][] { { -1 }, { 0 }, { -1 } })); 
		items.addAll(newPrItems(warehouse1, productFloatOil, 150, 0, new Number[][] { { -2.2 }, { -1 }, { -1.2 }, {} }));  
		items.addAll(newPrItems(warehouse1, productFractionTablet, 10, 1, new Number[][] { { -5, -5 }, { -1, 0 }, { -2, -3 }, { -2, -2 }, {} })); 

		System.out.println("PITEMS: " + items.size());

		Item cashItem = newItem(division1, currencyKZ, cash, 4001, 0);  // MAKE BAD DATA: 4000 -> 4001
		Item customerItem = newItem(division1, currencyKZ, customer, -1015, 0);

		// RUN
		
		try{
		
			orderService.productSale(userAccount, division1, items, cashItem, customerItem, description);
		
			assertTrue("Bad account and summ validation!",false);
		} catch (Exception ex ){
			assertTrue(true);
		}		
	}
	
	/**
	 * Test product sale sum cash.
	 *
	 * @throws Exception the exception
	 */
	@Test
	@Rollback(true)
	public void testProductSaleSumCash2() throws Exception {
		createTestEntitys();
		prProductPLUS(); 
		
		String description = "TEST DESCRIPTION...";
		
		// PRIVILEGES
		privilegesService.persist(new Privileges(division1, userAccount, null, warehouse1, WarehousePrivileges.PRODUCT_SALE, null, null, true, ""));
		privilegesService.persist(new Privileges(division1, userAccount, null, null, null, cash, CashPrivileges.PRODUCT_SALE, true, ""));

		// PREPEARING
		
		List<Item> items = new ArrayList<>();

		items.addAll(newPrItems(warehouse1, productInteger, 100, 0, new Number[][] { { -1 } })); 
		items.addAll(newPrItems(warehouse1, productFloat, 200, 0, new Number[][] { { -2.5 } }));  
		items.addAll(newPrItems(warehouse1, productFraction, 5, 1, new Number[][] { { -5, -5 } })); 

		items.addAll(newPrItems(warehouse1, productIntegerShoes, 4000, 0, new Number[][] { { -1 }, { 0 }, { -1 } })); 
		items.addAll(newPrItems(warehouse1, productFloatOil, 150, 0, new Number[][] { { -2.2 }, { -1 }, { -1.2 }, {} }));  
		items.addAll(newPrItems(warehouse1, productFractionTablet, 10, 1, new Number[][] { { -5, -5 }, { -1, 0 }, { -2, -3 }, { -2, -2 }, {} })); 

		System.out.println("PITEMS: " + items.size());

		Item cashItem = newItem(division1, currencyKZ, cash, 3999.99, 0);  // MAKE BAD DATA: 4000 -> 3999.99
		Item customerItem = newItem(division1, currencyKZ, customer, -1015, 0);

		// RUN
		
		try{
		
			orderService.productSale(userAccount, division1, items, cashItem, customerItem, description);
		
			assertTrue("Bad account and summ validation!",false);
		} catch (Exception ex ){
			assertTrue(true);
		}		
	}
	
	/**
	 * Test product sale sum cash.
	 *
	 * @throws Exception the exception
	 */
	@Test
	@Rollback(true)
	public void testProductSaleSumCash3() throws Exception {
		createTestEntitys();
		prProductPLUS(); 
		
		String description = "TEST DESCRIPTION...";
		
		// PRIVILEGES
		privilegesService.persist(new Privileges(division1, userAccount, null, warehouse1, WarehousePrivileges.PRODUCT_SALE, null, null, true, ""));
		privilegesService.persist(new Privileges(division1, userAccount, null, null, null, cash, CashPrivileges.PRODUCT_SALE, true, ""));

		// PREPEARING
		
		List<Item> items = new ArrayList<>();

		items.addAll(newPrItems(warehouse1, productInteger, 100, 0, new Number[][] { { -1 } })); 
		items.addAll(newPrItems(warehouse1, productFloat, 200, 0, new Number[][] { { -2.5 } }));  
		items.addAll(newPrItems(warehouse1, productFraction, 5, 1, new Number[][] { { -5, -5 } })); 

		items.addAll(newPrItems(warehouse1, productIntegerShoes, 4000, 0, new Number[][] { { -1 }, { 0 }, { -1 } })); 
		items.addAll(newPrItems(warehouse1, productFloatOil, 150, 0, new Number[][] { { -2.2 }, { -1 }, { -1.2 }, {} }));  
		items.addAll(newPrItems(warehouse1, productFractionTablet, 10, 1, new Number[][] { { -5, -5 }, { -1, 0 }, { -2, -3 }, { -2, -2 }, {} })); 

		System.out.println("PITEMS: " + items.size());

		Item cashItem = newItem(division1, currencyKZ, cash, -4000, 0);  // MAKE BAD DATA: 4000 -> -4000
		Item customerItem = newItem(division1, currencyKZ, customer, -1015, 0);

		// RUN
		
		try{
		
			orderService.productSale(userAccount, division1, items, cashItem, customerItem, description);
		
			assertTrue("Bad account and summ validation!",false);
		} catch (Exception ex ){
			assertTrue(true);
		}		
	}
	
	/**
	 * Test product sale sum cash.
	 *
	 * @throws Exception the exception
	 */
	@Test
	@Rollback(true)
	public void testProductSaleSumCash4() throws Exception {
		createTestEntitys();
		prProductPLUS(); 
		
		String description = "TEST DESCRIPTION...";
		
		// PRIVILEGES
		privilegesService.persist(new Privileges(division1, userAccount, null, warehouse1, WarehousePrivileges.PRODUCT_SALE, null, null, true, ""));
		privilegesService.persist(new Privileges(division1, userAccount, null, null, null, cash, CashPrivileges.PRODUCT_SALE, true, ""));

		// PREPEARING
		
		List<Item> items = new ArrayList<>();

		items.addAll(newPrItems(warehouse1, productInteger, 100, 0, new Number[][] { { -1 } })); 
		items.addAll(newPrItems(warehouse1, productFloat, 200, 0, new Number[][] { { -2.5 } }));  
		items.addAll(newPrItems(warehouse1, productFraction, 5, 1, new Number[][] { { -5, -5 } })); 

		items.addAll(newPrItems(warehouse1, productIntegerShoes, 4000, 0, new Number[][] { { -1 }, { 0 }, { -1 } })); 
		items.addAll(newPrItems(warehouse1, productFloatOil, 150, 0, new Number[][] { { -2.2 }, { -1 }, { -1.2 }, {} }));  
		items.addAll(newPrItems(warehouse1, productFractionTablet, 10, 1, new Number[][] { { -5, -5 }, { -1, 0 }, { -2, -3 }, { -2, -2 }, {} })); 

		System.out.println("PITEMS: " + items.size());

		// Commented for current test
		//Item cashItem = newItem(division1, currencyKZ, cash, 4000, 0);  
		Item cashItem = null;
		
		
		Item customerItem = newItem(division1, currencyKZ, customer, -1015, 0);

		// RUN
		
		try{
			
			orderService.productSale(userAccount, division1, items, cashItem, customerItem, description);
		
			assertTrue("Bad account and summ validation!",false);
		} catch (Exception ex ){
			assertTrue(true);
		}		
	}
	
	
	
	/**
	 * Test product sale privilrgrs1.
	 *
	 * @throws Exception the exception
	 */
	@Test
	@Rollback(true)
	public void testProductSalePrivilrgrs1() throws Exception {
		createTestEntitys();
		prProductPLUS(); 
		
		String description = "TEST DESCRIPTION...";
		
		// PRIVILEGES
		// Commentedt for current test
		//privilegesService.persist(new Privileges(division1, userAccount, null, warehouse1, WarehousePrivileges.PRODUCT_SALE, null, null, true, ""));
		privilegesService.persist(new Privileges(division1, userAccount, null, null, null, cash, CashPrivileges.PRODUCT_SALE, true, ""));

		// PREPEARING
		
		List<Item> items = new ArrayList<>();

		items.addAll(newPrItems(warehouse1, productInteger, 100, 0, new Number[][] { { -1 } })); 
		items.addAll(newPrItems(warehouse1, productFloat, 200, 0, new Number[][] { { -2.5 } }));  
		items.addAll(newPrItems(warehouse1, productFraction, 5, 1, new Number[][] { { -5, -5 } })); 

		items.addAll(newPrItems(warehouse1, productIntegerShoes, 4000, 0, new Number[][] { { -1 }, { 0 }, { -1 } })); 
		items.addAll(newPrItems(warehouse1, productFloatOil, 150, 0, new Number[][] { { -2.2 }, { -1 }, { -1.2 }, {} }));  
		items.addAll(newPrItems(warehouse1, productFractionTablet, 10, 1, new Number[][] { { -5, -5 }, { -1, 0 }, { -2, -3 }, { -2, -2 }, {} })); 

		System.out.println("PITEMS: " + items.size());
		
		Item cashItem = newItem(division1, currencyKZ, cash, 4000, 0);
		
		Item customerItem = newItem(division1, currencyKZ, customer, -1015, 0);

		// RUN
		
		try{
		
			orderService.productSale(userAccount, division1, items, cashItem, customerItem, description);
		
			assertTrue("Bad account and summ validation!",false);
		} catch (Exception ex ){
			assertTrue(true);
		}		
	}
	
	/**
	 * Test product sale privilrgrs1.
	 *
	 * @throws Exception the exception
	 */
	@Test
	@Rollback(true)
	public void testProductSalePrivilrgrs2() throws Exception {
		createTestEntitys();
		prProductPLUS(); 
		
		String description = "TEST DESCRIPTION...";
		
		// PRIVILEGES
		privilegesService.persist(new Privileges(division1, userAccount, null, warehouse1, WarehousePrivileges.PRODUCT_SALE, null, null, false, "")); // true -> false
		privilegesService.persist(new Privileges(division1, userAccount, null, null, null, cash, CashPrivileges.PRODUCT_SALE, true, ""));

		// PREPEARING
		
		List<Item> items = new ArrayList<>();

		items.addAll(newPrItems(warehouse1, productInteger, 100, 0, new Number[][] { { -1 } })); 
		items.addAll(newPrItems(warehouse1, productFloat, 200, 0, new Number[][] { { -2.5 } }));  
		items.addAll(newPrItems(warehouse1, productFraction, 5, 1, new Number[][] { { -5, -5 } })); 

		items.addAll(newPrItems(warehouse1, productIntegerShoes, 4000, 0, new Number[][] { { -1 }, { 0 }, { -1 } })); 
		items.addAll(newPrItems(warehouse1, productFloatOil, 150, 0, new Number[][] { { -2.2 }, { -1 }, { -1.2 }, {} }));  
		items.addAll(newPrItems(warehouse1, productFractionTablet, 10, 1, new Number[][] { { -5, -5 }, { -1, 0 }, { -2, -3 }, { -2, -2 }, {} })); 

		System.out.println("PITEMS: " + items.size());
		
		Item cashItem = newItem(division1, currencyKZ, cash, 4000, 0);
		
		Item customerItem = newItem(division1, currencyKZ, customer, -1015, 0);

		// RUN
		
		try{
		
			orderService.productSale(userAccount, division1, items, cashItem, customerItem, description);
		
			assertTrue("Bad account and summ validation!",false);
		} catch (Exception ex ){
			assertTrue(true);
		}		
	}
	
	/**
	 * Test product sale privilrgrs1.
	 *
	 * @throws Exception the exception
	 */
	@Test
	@Rollback(true)
	public void testProductSalePrivilrgrs3() throws Exception {
		createTestEntitys();
		prProductPLUS(); 
		
		String description = "TEST DESCRIPTION...";
		
		// PRIVILEGES
		privilegesService.persist(new Privileges(division1, userAccount, null, warehouse1, WarehousePrivileges.PRODUCT_PLUS, null, null, true, "")); // WarehousePrivileges.PRODUCT_SALE -> WarehousePrivileges.PRODUCT_PLUS
		privilegesService.persist(new Privileges(division1, userAccount, null, null, null, cash, CashPrivileges.PRODUCT_SALE, true, ""));

		// PREPEARING
		
		List<Item> items = new ArrayList<>();

		items.addAll(newPrItems(warehouse1, productInteger, 100, 0, new Number[][] { { -1 } })); 
		items.addAll(newPrItems(warehouse1, productFloat, 200, 0, new Number[][] { { -2.5 } }));  
		items.addAll(newPrItems(warehouse1, productFraction, 5, 1, new Number[][] { { -5, -5 } })); 

		items.addAll(newPrItems(warehouse1, productIntegerShoes, 4000, 0, new Number[][] { { -1 }, { 0 }, { -1 } })); 
		items.addAll(newPrItems(warehouse1, productFloatOil, 150, 0, new Number[][] { { -2.2 }, { -1 }, { -1.2 }, {} }));  
		items.addAll(newPrItems(warehouse1, productFractionTablet, 10, 1, new Number[][] { { -5, -5 }, { -1, 0 }, { -2, -3 }, { -2, -2 }, {} })); 

		System.out.println("PITEMS: " + items.size());
		
		Item cashItem = newItem(division1, currencyKZ, cash, 4000, 0);
		
		Item customerItem = newItem(division1, currencyKZ, customer, -1015, 0);

		// RUN
		
		try{
		
			orderService.productSale(userAccount, division1, items, cashItem, customerItem, description);
		
			assertTrue("Bad account and summ validation!",false);
		} catch (Exception ex ){
			assertTrue(true);
		}		
	}
	
	/**
	 * Test product sale privilrgrs1.
	 *
	 * @throws Exception the exception
	 */
	@Test
	@Rollback(true)
	public void testProductSalePrivilrgrs4() throws Exception {
		createTestEntitys();
		prProductPLUS(); 
		
		String description = "TEST DESCRIPTION...";
		
		// PRIVILEGES
		privilegesService.persist(new Privileges(division1, userAccount, null, warehouse1, WarehousePrivileges.PRODUCT_SALE, null, null, true, ""));
		
		// Commented for current tests
		//privilegesService.persist(new Privileges(division1, userAccount, null, null, null, cash, CashPrivileges.PRODUCT_SALE, true, ""));

		// PREPEARING
		
		List<Item> items = new ArrayList<>();

		items.addAll(newPrItems(warehouse1, productInteger, 100, 0, new Number[][] { { -1 } })); 
		items.addAll(newPrItems(warehouse1, productFloat, 200, 0, new Number[][] { { -2.5 } }));  
		items.addAll(newPrItems(warehouse1, productFraction, 5, 1, new Number[][] { { -5, -5 } })); 

		items.addAll(newPrItems(warehouse1, productIntegerShoes, 4000, 0, new Number[][] { { -1 }, { 0 }, { -1 } })); 
		items.addAll(newPrItems(warehouse1, productFloatOil, 150, 0, new Number[][] { { -2.2 }, { -1 }, { -1.2 }, {} }));  
		items.addAll(newPrItems(warehouse1, productFractionTablet, 10, 1, new Number[][] { { -5, -5 }, { -1, 0 }, { -2, -3 }, { -2, -2 }, {} })); 

		System.out.println("PITEMS: " + items.size());
		
		Item cashItem = newItem(division1, currencyKZ, cash, 4000, 0);
		
		Item customerItem = newItem(division1, currencyKZ, customer, -1015, 0);

		// RUN
		
		try{
		
			orderService.productSale(userAccount, division1, items, cashItem, customerItem, description);
		
			assertTrue("Bad account and summ validation!",false);
		} catch (Exception ex ){
			assertTrue(true);
		}		
	}
	
	
	/**
	 * Test product sale privilrgrs1.
	 *
	 * @throws Exception the exception
	 */
	@Test
	@Rollback(true)
	public void testProductSalePrivilrgrs5() throws Exception {
		createTestEntitys();
		prProductPLUS(); 
		
		String description = "TEST DESCRIPTION...";
		
		// PRIVILEGES
		privilegesService.persist(new Privileges(division1, userAccount, null, warehouse1, WarehousePrivileges.PRODUCT_SALE, null, null, true, "")); 
		privilegesService.persist(new Privileges(division1, userAccount, null, null, null, cash, CashPrivileges.PRODUCT_SALE, false, "")); // true -> false

		// PREPEARING
		
		List<Item> items = new ArrayList<>();

		items.addAll(newPrItems(warehouse1, productInteger, 100, 0, new Number[][] { { -1 } })); 
		items.addAll(newPrItems(warehouse1, productFloat, 200, 0, new Number[][] { { -2.5 } }));  
		items.addAll(newPrItems(warehouse1, productFraction, 5, 1, new Number[][] { { -5, -5 } })); 

		items.addAll(newPrItems(warehouse1, productIntegerShoes, 4000, 0, new Number[][] { { -1 }, { 0 }, { -1 } })); 
		items.addAll(newPrItems(warehouse1, productFloatOil, 150, 0, new Number[][] { { -2.2 }, { -1 }, { -1.2 }, {} }));  
		items.addAll(newPrItems(warehouse1, productFractionTablet, 10, 1, new Number[][] { { -5, -5 }, { -1, 0 }, { -2, -3 }, { -2, -2 }, {} })); 

		System.out.println("PITEMS: " + items.size());
		
		Item cashItem = newItem(division1, currencyKZ, cash, 4000, 0);
		
		Item customerItem = newItem(division1, currencyKZ, customer, -1015, 0);

		// RUN
		
		try{
		
			orderService.productSale(userAccount, division1, items, cashItem, customerItem, description);
		
			assertTrue("Bad account and summ validation!",false);
		} catch (Exception ex ){
			assertTrue(true);
		}		
	}
	
	/**
	 * Test product sale privilrgrs1.
	 *
	 * @throws Exception the exception
	 */
	@Test
	@Rollback(true)
	public void testProductSalePrivilrgrs6() throws Exception {
		createTestEntitys();
		prProductPLUS(); 
		
		String description = "TEST DESCRIPTION...";
		
		// PRIVILEGES
		privilegesService.persist(new Privileges(division1, userAccount, null, warehouse1, WarehousePrivileges.PRODUCT_SALE, null, null, true, "")); 
		privilegesService.persist(new Privileges(division1, userAccount, null, null, null, cash, CashPrivileges.CASH_PLUS, true, "")); // CashPrivileges.PRODUCT_SALE -> CashPrivileges.CASH_PLUS
		

		// PREPEARING
		
		List<Item> items = new ArrayList<>();

		items.addAll(newPrItems(warehouse1, productInteger, 100, 0, new Number[][] { { -1 } })); 
		items.addAll(newPrItems(warehouse1, productFloat, 200, 0, new Number[][] { { -2.5 } }));  
		items.addAll(newPrItems(warehouse1, productFraction, 5, 1, new Number[][] { { -5, -5 } })); 

		items.addAll(newPrItems(warehouse1, productIntegerShoes, 4000, 0, new Number[][] { { -1 }, { 0 }, { -1 } })); 
		items.addAll(newPrItems(warehouse1, productFloatOil, 150, 0, new Number[][] { { -2.2 }, { -1 }, { -1.2 }, {} }));  
		items.addAll(newPrItems(warehouse1, productFractionTablet, 10, 1, new Number[][] { { -5, -5 }, { -1, 0 }, { -2, -3 }, { -2, -2 }, {} })); 

		System.out.println("PITEMS: " + items.size());
		
		Item cashItem = newItem(division1, currencyKZ, cash, 4000, 0);
		
		Item customerItem = newItem(division1, currencyKZ, customer, -1015, 0);

		// RUN
		
		try{
		
			orderService.productSale(userAccount, division1, items, cashItem, customerItem, description);
		
			assertTrue("Bad account and summ validation!",false);
		} catch (Exception ex ){
			assertTrue(true);
		}		
	}
	
	
	// ------------------------------------------------------------------------------------------------------------------------------------------
	
	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#getAllEntities()}.
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testGetAllEntities() throws Exception {
		createTestEntitys();
		

		long index = orderDAO.count();
		for (int i = 0; i < 100; i++) {
			Order order = new Order();
			order.setOrderType(OrderType.PRODUCT_PLUS);
			order.setOpTime(new Date());
			order.setDivision(division1);
			order.setDescription("Order description");
			order.setUserAccount(userAccount);
			
			orderDAO.persist(order);
			index++;
		}

		List<Order> list = service.getAllEntities();

		assertTrue(list != null);
		assertTrue(list.size() > 0);
		assertTrue(list.size() == index);
	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#getAllEntities(javax.persistence.metamodel.SingularAttribute, pk.home.libs.combine.dao.ABaseDAO.SortOrderType)}
	 * .
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testGetAllEntitiesSingularAttributeOfTQSortOrderType()
			throws Exception {
		createTestEntitys();
		
		long index = orderDAO.count();
		for (int i = 0; i < 100; i++) {
			Order order = new Order();
			order.setOrderType(OrderType.PRODUCT_PLUS);
			order.setOpTime(new Date());
			order.setDivision(division1);
			order.setDescription("Order description");
			order.setUserAccount(userAccount);
			orderDAO.persist(order);
			index++;
		}

		List<Order> list = service.getAllEntities(Order_.id,
				SortOrderType.ASC);

		assertTrue(list != null);
		assertTrue(list.size() > 0);
		assertTrue(list.size() == index);

		long lastId = 0;
		for (Order order : list) {
			assertTrue(lastId < order.getId());
			lastId = order.getId();
		}
	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#getAllEntities(int, int)}.
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testGetAllEntitiesIntInt() throws Exception {
		createTestEntitys();

		// int index = 0;
		for (int i = 0; i < 100; i++) {
			Order order = new Order();
			order.setOrderType(OrderType.PRODUCT_PLUS);
			order.setOpTime(new Date());
			order.setDivision(division1);
			order.setDescription("Order description");
			order.setUserAccount(userAccount);
			orderDAO.persist(order);
			// index++;
		}

		List<Order> list = service.getAllEntities(10, 10);

		assertTrue(list != null);
		assertTrue(list.size() > 0);
		assertTrue(list.size() == 10);
	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#getAllEntities(int, int, javax.persistence.metamodel.SingularAttribute, pk.home.libs.combine.dao.ABaseDAO.SortOrderType)}
	 * .
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testGetAllEntitiesIntIntSingularAttributeOfTQSortOrderType()
			throws Exception {
		createTestEntitys();
		
		// long index = orderDAO.count();
		for (int i = 0; i < 100; i++) {
			Order order = new Order();
			order.setOrderType(OrderType.PRODUCT_PLUS);
			order.setOpTime(new Date());
			order.setDivision(division1);
			order.setDescription("Order description");
			order.setUserAccount(userAccount);
			orderDAO.persist(order);
			// index++;
		}

		List<Order> list = service.getAllEntities(10, 10, Order_.id,
				SortOrderType.ASC);

		assertTrue(list != null);
		assertTrue(list.size() > 0);
		assertTrue(list.size() == 10);

		long lastId = 0;
		for (Order order : list) {
			assertTrue(lastId < order.getId());
			lastId = order.getId();
		}
	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#getAllEntities(boolean, int, int, javax.persistence.metamodel.SingularAttribute, pk.home.libs.combine.dao.ABaseDAO.SortOrderType)}
	 * .
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testGetAllEntitiesBooleanIntIntSingularAttributeOfTQSortOrderType()
			throws Exception {
		createTestEntitys();
		
		long index = orderDAO.count();
		for (int i = 0; i < 100; i++) {
			Order order = new Order();
			order.setOrderType(OrderType.PRODUCT_PLUS);
			order.setOpTime(new Date());
			order.setDivision(division1);
			order.setDescription("Order description");
			order.setUserAccount(userAccount);
			orderDAO.persist(order);
			index++;
		}

		// all - FALSE
		List<Order> list = service.getAllEntities(false, 10, 10, Order_.id,
				SortOrderType.ASC);

		assertTrue(list != null);
		assertTrue(list.size() > 0);
		assertTrue(list.size() == 10);

		long lastId = 0;
		for (Order order : list) {
			assertTrue(lastId < order.getId());
			lastId = order.getId();
		}

		// all - TRUE
		list = service.getAllEntities(true, 10, 10, Order_.id,
				SortOrderType.ASC);

		assertTrue(list != null);
		assertTrue(list.size() > 0);
		assertTrue(list.size() == index);

		lastId = 0;
		for (Order order : list) {
			assertTrue(lastId < order.getId());
			lastId = order.getId();
		}
	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#find(java.lang.Object)}.
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testFind() throws Exception {
		createTestEntitys();

		Order order = new Order();
		order.setOrderType(OrderType.PRODUCT_PLUS);
		order.setOpTime(new Date());
		order.setDivision(division1);
		order.setDescription("Order description");
		order.setUserAccount(userAccount);
		order = orderDAO.persist(order);

		long id = order.getId();

		Order order2 = service.find(id);

		assertEquals(order, order2);
		assertTrue(order.getId() == order2.getId());
		assertEquals(order.getOrderType(), order2.getOrderType());

	}

	/**
	 * Test method for {@link pk.home.libs.combine.dao.ABaseDAO#count()}.
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testCount() throws Exception {
		createTestEntitys();
		
		long index = orderDAO.count();
		for (int i = 0; i < 100; i++) {
			Order order = new Order();
			order.setOrderType(OrderType.PRODUCT_PLUS);
			order.setOpTime(new Date());
			order.setDivision(division1);
			order.setDescription("Order description");
			order.setUserAccount(userAccount);
			orderDAO.persist(order);
			index++;
		}

		long count = service.count();

		assertTrue(count == index);
	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#persist(java.lang.Object)}.
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testPersist() throws Exception {
		createTestEntitys();
		
		Order order = new Order();
		order.setOrderType(OrderType.PRODUCT_PLUS);
		order.setOpTime(new Date());
		order.setDivision(division1);
		order.setDescription("Order description");
		order.setUserAccount(userAccount);
		try{
			order = service.persist(order);
			
			assertTrue("Operation persist is supported!",false);
			
		}catch(NotSupportedException ex){
			assertTrue(true);
		}
	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#refresh(java.lang.Object)}.
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testRefresh() throws Exception {
		createTestEntitys();
		
		Order order = new Order();
		order.setOrderType(OrderType.PRODUCT_PLUS);
		order.setOpTime(new Date());
		order.setDivision(division1);
		order.setDescription("Order description");
		order.setUserAccount(userAccount);
		order = orderDAO.persist(order);

		long id = order.getId();

		Order order2 = service.find(id);

		assertEquals(order, order2);
		assertTrue(order.getId() == order2.getId());
		assertEquals(order.getOrderType(), order2.getOrderType());

		order2.setOrderType(OrderType.PRODUCT_PLUS);
		order2.setOpTime(new Date());
		order2.setDivision(division1);
		order2.setDescription("Order description");
		order2.setUserAccount(userAccount);
		order2 = service.refresh(order2);

		assertEquals(order, order2);
		assertTrue(order.getId() == order2.getId());
		assertEquals(order.getOrderType(), order2.getOrderType());

	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#merge(java.lang.Object)}.
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testMerge() throws Exception {
		createTestEntitys();
		
		Order order = new Order();
		order.setOrderType(OrderType.PRODUCT_PLUS);
		order.setOpTime(new Date());
		order.setDivision(division1);
		order.setDescription("Order description");
		order.setUserAccount(userAccount);
		

		try{
			order = service.merge(order);
			
			assertTrue("Operation merge is supported!",false);
			
		}catch(NotSupportedException ex){
			assertTrue(true);
		}
	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#remove(java.lang.Object)}.
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testRemove() throws Exception {
		createTestEntitys();
		
		Order order = new Order();
		order.setOrderType(OrderType.PRODUCT_PLUS);
		order.setOpTime(new Date());
		order.setDivision(division1);
		order.setDescription("Order description");
		order.setUserAccount(userAccount);
		
		try{
			service.remove(order);
			
			assertTrue("Operation remove is supported!",false);
			
		}catch(NotSupportedException ex){
			assertTrue(true);
		}

	}

	// -----------------------------------------------------------------------------------------------------------------

	@Test
	@Rollback(true)
	public void insertEntities() throws Exception {
		createTestEntitys();

		long index = orderDAO.count();
		for (int i = 200; i < 210; i++) {
			Order order = new Order();
			order.setOrderType(OrderType.PRODUCT_PLUS);
			order.setOpTime(new Date());
			order.setDivision(division1);
			order.setDescription("Order description");
			order.setUserAccount(userAccount);
			orderDAO.persist(order);
			index++;
		}

		List<Order> list = service.getAllEntities();

		assertTrue(list != null);
		assertTrue(list.size() > 0);
		assertTrue(list.size() == index);
	}
	

}
