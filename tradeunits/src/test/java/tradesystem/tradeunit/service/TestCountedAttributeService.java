package tradesystem.tradeunit.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import pk.home.libs.combine.dao.ABaseDAO.SortOrderType;
import tradesystem.tradeunit.basetestutils.BaseTest;
import tradesystem.tradeunit.domain.CountedAttribute;
import tradesystem.tradeunit.domain.CountedAttribute_;

/**
 * JUnit test service class for entity class: CountedAttribute
 * СountedAttribute - подсчитываемый атрибут
 */
@RunWith(SpringJUnit4ClassRunner.class)
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
		DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class })
@Transactional
@ActiveProfiles({"Dev"})
@ContextConfiguration(locations = { "file:./src/main/resources/applicationContext.xml" })
public class TestCountedAttributeService extends BaseTest{

	/**
	 * The DAO being tested, injected by Spring
	 * 
	 */
	private CountedAttributeService service;

	/**
	 * Method to allow Spring to inject the DAO that will be tested
	 * 
	 */
	@Autowired
	public void setDataStore(CountedAttributeService service) {
		this.service = service;
	}

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test sort accounts.
	 *
	 * @throws Exception the exception
	 */
	@Test
	@Rollback(true)
	public void testAttributesAsSortedCollection() throws Exception {
		createTestEntitys();
		
		List<CountedAttribute> coll = service.attributesAsSortedCollection(countedAttributeColor, false);
		
		assertNotNull(coll);
		
		System.out.println(coll.size());
		assertTrue(coll.size() == 4);
		assertNull(coll.get(0).getParent());
		
		for(CountedAttribute ca: coll)
			System.out.println(ca.getId() + "  " + (ca.getParent() != null ? ca.getParent().getId() : "null")  + "  " + ca.getKeyName());
		
		
		coll = service.attributesAsSortedCollection(countedAttributeColor, true);
		
		assertNotNull(coll);
		
		System.out.println(coll.size());
		assertNotNull(coll.get(0).getParent());
		assertTrue(coll.size() == 4);
		
		for(CountedAttribute ca: coll)
			System.out.println(ca.getId() + "  " + (ca.getParent() != null ? ca.getParent().getId() : "null")  + "  " + ca.getKeyName());
		
	}
	
	
	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#getAllEntities()}.
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testGetAllEntities() throws Exception {

		long index = service.count();
		for (int i = 0; i < 100; i++) {
			CountedAttribute countedAttribute = new CountedAttribute();
			countedAttribute.setKeyName("key " + i);
			service.persist(countedAttribute);
			index++;
		}

		List<CountedAttribute> list = service.getAllEntities();

		assertTrue(list != null);
		assertTrue(list.size() > 0);
		assertTrue(list.size() == index);
	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#getAllEntities(javax.persistence.metamodel.SingularAttribute, pk.home.libs.combine.dao.ABaseDAO.SortOrderType)}
	 * .
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testGetAllEntitiesSingularAttributeOfTQSortOrderType()
			throws Exception {
		long index = service.count();
		for (int i = 0; i < 100; i++) {
			CountedAttribute countedAttribute = new CountedAttribute();
			countedAttribute.setKeyName("key " + i);
			service.persist(countedAttribute);
			index++;
		}

		List<CountedAttribute> list = service.getAllEntities(CountedAttribute_.id, SortOrderType.ASC);

		assertTrue(list != null);
		assertTrue(list.size() > 0);
		assertTrue(list.size() == index);

		long lastId = 0;
		for (CountedAttribute countedAttribute : list) {
			assertTrue(lastId < countedAttribute.getId());
			lastId = countedAttribute.getId();
		}
	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#getAllEntities(int, int)}.
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testGetAllEntitiesIntInt() throws Exception {

		// int index = 0;
		for (int i = 0; i < 100; i++) {
			CountedAttribute countedAttribute = new CountedAttribute();
			countedAttribute.setKeyName("key " + i);
			service.persist(countedAttribute);
			// index++;
		}

		List<CountedAttribute> list = service.getAllEntities(10, 10);

		assertTrue(list != null);
		assertTrue(list.size() > 0);
		assertTrue(list.size() == 10);
	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#getAllEntities(int, int, javax.persistence.metamodel.SingularAttribute, pk.home.libs.combine.dao.ABaseDAO.SortOrderType)}
	 * .
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testGetAllEntitiesIntIntSingularAttributeOfTQSortOrderType()
			throws Exception {
		// long index = service.count();
		for (int i = 0; i < 100; i++) {
			CountedAttribute countedAttribute = new CountedAttribute();
			countedAttribute.setKeyName("key " + i);
			service.persist(countedAttribute);
			// index++;
		}

		List<CountedAttribute> list = service.getAllEntities(10, 10, CountedAttribute_.id,
				SortOrderType.ASC);

		assertTrue(list != null);
		assertTrue(list.size() > 0);
		assertTrue(list.size() == 10);

		long lastId = 0;
		for (CountedAttribute countedAttribute : list) {
			assertTrue(lastId < countedAttribute.getId());
			lastId = countedAttribute.getId();
		}
	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#getAllEntities(boolean, int, int, javax.persistence.metamodel.SingularAttribute, pk.home.libs.combine.dao.ABaseDAO.SortOrderType)}
	 * .
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testGetAllEntitiesBooleanIntIntSingularAttributeOfTQSortOrderType()
			throws Exception {
		long index = service.count();
		for (int i = 0; i < 100; i++) {
			CountedAttribute countedAttribute = new CountedAttribute();
			countedAttribute.setKeyName("key " + i);
			service.persist(countedAttribute);
			index++;
		}

		// all - FALSE
		List<CountedAttribute> list = service.getAllEntities(false, 10, 10, CountedAttribute_.id,
				SortOrderType.ASC);

		assertTrue(list != null);
		assertTrue(list.size() > 0);
		assertTrue(list.size() == 10);

		long lastId = 0;
		for (CountedAttribute countedAttribute : list) {
			assertTrue(lastId < countedAttribute.getId());
			lastId = countedAttribute.getId();
		}

		// all - TRUE
		list = service.getAllEntities(true, 10, 10, CountedAttribute_.id,
				SortOrderType.ASC);

		assertTrue(list != null);
		assertTrue(list.size() > 0);
		assertTrue(list.size() == index);

		lastId = 0;
		for (CountedAttribute countedAttribute : list) {
			assertTrue(lastId < countedAttribute.getId());
			lastId = countedAttribute.getId();
		}
	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#find(java.lang.Object)}.
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testFind() throws Exception {

		CountedAttribute countedAttribute = new CountedAttribute();
		countedAttribute.setKeyName("key " + 999);
		countedAttribute = service.persist(countedAttribute);

		long id = countedAttribute.getId();

		CountedAttribute countedAttribute2 = service.find(id);

		assertEquals(countedAttribute, countedAttribute2);
		assertTrue(countedAttribute.getId() == countedAttribute2.getId());
		assertEquals(countedAttribute.getKeyName(), countedAttribute2.getKeyName());

	}

	/**
	 * Test method for {@link pk.home.libs.combine.dao.ABaseDAO#count()}.
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testCount() throws Exception {
		long index = service.count();
		for (int i = 0; i < 100; i++) {
			CountedAttribute countedAttribute = new CountedAttribute();
			countedAttribute.setKeyName("key " + i);
			service.persist(countedAttribute);
			index++;
		}

		long count = service.count();

		assertTrue(count == index);
	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#persist(java.lang.Object)}.
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testPersist() throws Exception {
		CountedAttribute countedAttribute = new CountedAttribute();
		countedAttribute.setKeyName("key " + 999);
		countedAttribute = service.persist(countedAttribute);

		long id = countedAttribute.getId();

		CountedAttribute countedAttribute2 = service.find(id);

		assertEquals(countedAttribute, countedAttribute2);
		assertTrue(countedAttribute.getId() == countedAttribute2.getId());
		assertEquals(countedAttribute.getKeyName(), countedAttribute2.getKeyName());
	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#refresh(java.lang.Object)}.
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testRefresh() throws Exception {
		CountedAttribute countedAttribute = new CountedAttribute();
		countedAttribute.setKeyName("key " + 999);
		countedAttribute = service.persist(countedAttribute);

		long id = countedAttribute.getId();

		CountedAttribute countedAttribute2 = service.find(id);

		assertEquals(countedAttribute, countedAttribute2);
		assertTrue(countedAttribute.getId() == countedAttribute2.getId());
		assertEquals(countedAttribute.getKeyName(), countedAttribute2.getKeyName());

		countedAttribute2.setKeyName("key 65535");
		countedAttribute2 = service.refresh(countedAttribute2);

		assertEquals(countedAttribute, countedAttribute2);
		assertTrue(countedAttribute.getId() == countedAttribute2.getId());
		assertEquals(countedAttribute.getKeyName(), countedAttribute2.getKeyName());

	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#merge(java.lang.Object)}.
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testMerge() throws Exception {
		CountedAttribute countedAttribute = new CountedAttribute();
		countedAttribute.setKeyName("key " + 999);
		countedAttribute = service.persist(countedAttribute);

		long id = countedAttribute.getId();

		CountedAttribute countedAttribute2 = service.find(id);

		assertEquals(countedAttribute, countedAttribute2);
		assertTrue(countedAttribute.getId() == countedAttribute2.getId());
		assertEquals(countedAttribute.getKeyName(), countedAttribute2.getKeyName());

		countedAttribute2.setKeyName("key 65535");
		countedAttribute2 = service.merge(countedAttribute2);

		countedAttribute = service.refresh(countedAttribute);

		assertEquals(countedAttribute, countedAttribute2);
		assertTrue(countedAttribute.getId() == countedAttribute2.getId());
		assertEquals(countedAttribute.getKeyName(), countedAttribute2.getKeyName());
	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#remove(java.lang.Object)}.
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testRemove() throws Exception {
		CountedAttribute countedAttribute = new CountedAttribute();
		countedAttribute.setKeyName("key " + 999);
		countedAttribute = service.persist(countedAttribute);

		long id = countedAttribute.getId();

		CountedAttribute countedAttribute2 = service.find(id);

		assertEquals(countedAttribute, countedAttribute2);
		assertTrue(countedAttribute.getId() == countedAttribute2.getId());
		assertEquals(countedAttribute.getKeyName(), countedAttribute2.getKeyName());

		service.remove(countedAttribute);

		CountedAttribute countedAttribute3 = service.find(id);
		assertTrue(countedAttribute3 == null);

	}
	
	
	
	// -----------------------------------------------------------------------------------------------------------------
	
	@Test
	@Rollback(true)
	public void insertEntities() throws Exception {

		long index = service.count();
		for (int i = 200; i < 210; i++) {
			CountedAttribute countedAttribute = new CountedAttribute();
			countedAttribute.setKeyName("key " + i);
			service.persist(countedAttribute);
			index++;
		}

		List<CountedAttribute> list = service.getAllEntities();

		assertTrue(list != null);
		assertTrue(list.size() > 0);
		assertTrue(list.size() == index);
	}
	

}
