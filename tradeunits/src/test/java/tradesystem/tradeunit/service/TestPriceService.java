package tradesystem.tradeunit.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import pk.home.libs.combine.dao.ABaseDAO.SortOrderType;
import tradesystem.tradeunit.basetestutils.BaseTest;
import tradesystem.tradeunit.domain.Price;
import tradesystem.tradeunit.domain.Price_;
import tradesystem.tradeunit.domain.Product;

/**
 * JUnit test service class for entity class: Price
 * Price - цена.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
		DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class })
@Transactional
@ActiveProfiles({"Dev"})
@ContextConfiguration(locations = { "file:./src/main/resources/applicationContext.xml" })
public class TestPriceService extends BaseTest{

	/** The DAO being tested, injected by Spring. */
	private PriceService service;

	/**
	 * Method to allow Spring to inject the DAO that will be tested.
	 *
	 * @param service the new service
	 */
	@Autowired
	public void setservice(PriceService service) {
		this.service = service;
	}

	/**
	 * Sets the up before class.
	 *
	 * @throws Exception the exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * Tear down after class.
	 *
	 * @throws Exception the exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * Sets the up.
	 *
	 * @throws Exception the exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * Tear down.
	 *
	 * @throws Exception the exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	
	/**
	 * Test find price validation.
	 *
	 * @throws Exception the exception
	 */
	@Test
	@Rollback(true)
	public void testFindPriceValidation() throws Exception {
		createTestEntitys();

		try{
			service.findPrice(null,productInteger);
			assertTrue("Bad validation",false);
		} catch (Exception ex) {
			assertTrue(true);
		}
		
		try{
			service.findPrice(division1,null);
			assertTrue("Bad validation",false);
		} catch (Exception ex) {
			assertTrue(true);
		}
	}
	
	
	/**
	 * Test find price.
	 *
	 * @throws Exception the exception
	 */
	@Test
	@Rollback(true)
	public void testFindPrice() throws Exception {
		createTestEntitys();

		Price price = service.findPrice(division1,productInteger);
		Price price2 = service.findPrice(division1,productInteger);

		assertEquals(price, price2);
		assertTrue(price.getId() == price2.getId());
		assertEquals(price, price2);

	}
	
	
	
	/**
	 * Test method for.
	 *
	 * @throws Exception the exception
	 * {@link pk.home.libs.combine.dao.ABaseDAO#getAllEntities()}.
	 */
	@Test
	@Rollback(true)
	public void testGetAllEntities() throws Exception {
		createTestEntitys();

		long index = service.count();
		{
			Product p1 = new Product();
			p1.setKeyName("TEST PRICE 1");
			p1.setBarcode("TEST PRICE 1");
			p1.setMeasure(measureInteger);
			
			p1 = productService.persist(p1);
			
			service.findPrice(division1,p1);
			index+=1;
		}

		List<Price> list = service.getAllEntities();

		assertTrue(list != null);
		assertTrue(list.size() > 0);
		assertTrue(list.size() == index);
	}

	/**
	 * Test method for.
	 *
	 * @throws Exception the exception
	 * {@link pk.home.libs.combine.dao.ABaseDAO#getAllEntities(javax.persistence.metamodel.SingularAttribute, pk.home.libs.combine.dao.ABaseDAO.SortOrderType)}
	 * .
	 */
	@Test
	@Rollback(true)
	public void testGetAllEntitiesSingularAttributeOfTQSortOrderType()
			throws Exception {
		createTestEntitys();
		
		long index = service.count();
		
		Product p1 = new Product();
		p1.setKeyName("TEST PRICE 1");
		p1.setBarcode("TEST PRICE 1");
		p1.setMeasure(measureInteger);
		
		p1 = productService.persist(p1);
		
		service.findPrice(division1,p1);

		List<Price> list = service.getAllEntities(Price_.id, SortOrderType.ASC);

		
		assertTrue(list != null);
		assertTrue(list.size() > 0);
		assertTrue(list.size() == index + 1);

		long lastId = 0;
		for (Price price : list) {
			assertTrue(lastId < price.getId());
			lastId = price.getId();
		}
	}

	/**
	 * Test method for.
	 *
	 * @throws Exception the exception
	 * {@link pk.home.libs.combine.dao.ABaseDAO#getAllEntities(int, int)}.
	 */
	@Test
	@Rollback(true)
	public void testGetAllEntitiesIntInt() throws Exception {
		createTestEntitys();

		{
			service.findPrice(division1,productInteger);
			service.findPrice(division1,productFloat);
			service.findPrice(division1,productFraction);
			service.findPrice(division1,productIntegerShoes);
			service.findPrice(division1,productFractionTablet);
		}

		List<Price> list = service.getAllEntities(0, 5);

		assertTrue(list != null);
		assertTrue(list.size() > 0);
		assertTrue(list.size() == 5);
	}

	/**
	 * Test method for.
	 *
	 * @throws Exception the exception
	 * {@link pk.home.libs.combine.dao.ABaseDAO#getAllEntities(int, int, javax.persistence.metamodel.SingularAttribute, pk.home.libs.combine.dao.ABaseDAO.SortOrderType)}
	 * .
	 */
	@Test
	@Rollback(true)
	public void testGetAllEntitiesIntIntSingularAttributeOfTQSortOrderType()
			throws Exception {
		createTestEntitys();
		
		// long index = service.count();
		{
			service.findPrice(division1,productInteger);
			service.findPrice(division1,productInteger);
			service.findPrice(division1,productFloat);
			service.findPrice(division1,productFraction);
			service.findPrice(division1,productIntegerShoes);
			service.findPrice(division1,productFractionTablet);
			// index++;
		}

		List<Price> list = service.getAllEntities(0, 5, Price_.id,
				SortOrderType.ASC);

		assertTrue(list != null);
		assertTrue(list.size() > 0);
		assertTrue(list.size() == 5);

		long lastId = 0;
		for (Price price : list) {
			assertTrue(lastId < price.getId());
			lastId = price.getId();
		}
	}

	/**
	 * Test method for.
	 *
	 * @throws Exception the exception
	 * {@link pk.home.libs.combine.dao.ABaseDAO#getAllEntities(boolean, int, int, javax.persistence.metamodel.SingularAttribute, pk.home.libs.combine.dao.ABaseDAO.SortOrderType)}
	 * .
	 */
	@Test
	@Rollback(true)
	public void testGetAllEntitiesBooleanIntIntSingularAttributeOfTQSortOrderType()
			throws Exception {
		createTestEntitys();
		
		long index = service.count();
		for(int i=0; i< 5; i++)
		{
			Product p1 = new Product();
			p1.setKeyName("TEST PRICE " + i);
			p1.setBarcode("TEST PRICE " + i);
			p1.setMeasure(measureInteger);
			
			p1 = productService.persist(p1);
			
			service.findPrice(division1,p1);
			
			index += 1;
		}

		// all - FALSE
		List<Price> list = service.getAllEntities(false, 0, 5, Price_.id,
				SortOrderType.ASC);

		assertTrue(list != null);
		assertTrue(list.size() > 0);
		assertTrue(list.size() == 5);

		long lastId = 0;
		for (Price price : list) {
			assertTrue(lastId < price.getId());
			lastId = price.getId();
		}

		// all - TRUE
		list = service.getAllEntities(true, 0, 10, Price_.id,
				SortOrderType.ASC);

		assertTrue(list != null);
		assertTrue(list.size() > 0);
		assertTrue(list.size() == index);

		lastId = 0;
		for (Price price : list) {
			assertTrue(lastId < price.getId());
			lastId = price.getId();
		}
	}

	/**
	 * Test method for.
	 *
	 * @throws Exception the exception
	 * {@link pk.home.libs.combine.dao.ABaseDAO#find(java.lang.Object)}.
	 */
	@Test
	@Rollback(true)
	public void testFind() throws Exception {
		createTestEntitys();

		Price price = service.findPrice(division1,productInteger);

		long id = price.getId();

		Price price2 = service.find(id);

		assertEquals(price, price2);
		assertTrue(price.getId() == price2.getId());
		assertEquals(price, price2);

	}

	/**
	 * Test method for {@link pk.home.libs.combine.dao.ABaseDAO#count()}.
	 *
	 * @throws Exception the exception
	 */
	@Test
	@Rollback(true)
	public void testCount() throws Exception {
		createTestEntitys();
		
		long index = service.count();

		Product p1 = new Product();
		p1.setKeyName("TEST PRICE 1");
		p1.setBarcode("TEST PRICE 1");
		p1.setMeasure(measureInteger);
		
		p1 = productService.persist(p1);
		
		service.findPrice(division1,p1);

		long count = service.count();

		assertTrue(count == index + 1);
	}

	/**
	 * Test method for.
	 *
	 * @throws Exception the exception
	 * {@link pk.home.libs.combine.dao.ABaseDAO#persist(java.lang.Object)}.
	 */
	@Test
	@Rollback(true)
	public void testPersist() throws Exception {
		createTestEntitys();
		
		/*
		Price price = new Price();
		price.setDivision(division1);
		price.setProduct(productInteger);
		price = service.find(division1,productInteger)(price);

		long id = price.getId();

		Price price2 = service.find(id);

		assertEquals(price, price2);
		assertTrue(price.getId() == price2.getId());
		assertEquals(price, price2);
		*/
		
		 try{
			Price price = new Price();
			price.setDivision(division1);
			price.setProduct(productInteger);
			price = service.persist(price);
			
			assertTrue("Was executed not suported operation" ,false);
		 } catch (Exception ex) {
			assertTrue(true); 
		 }
	}

	/**
	 * Test method for.
	 *
	 * @throws Exception the exception
	 * {@link pk.home.libs.combine.dao.ABaseDAO#refresh(java.lang.Object)}.
	 */
	@Test
	@Rollback(true)
	public void testRefresh() throws Exception {
		createTestEntitys();
		
		Price price = service.findPrice(division1,productInteger);

		long id = price.getId();

		Price price2 = service.find(id);

		assertEquals(price, price2);
		assertTrue(price.getId() == price2.getId());
		assertEquals(price, price2);

		price.setDivision(division1);
		price.setProduct(productFloat);
		price2 = service.refresh(price2);

		assertEquals(price, price2);
		assertTrue(price.getId() == price2.getId());
		assertEquals(price, price2);

	}

	/**
	 * Test method for.
	 *
	 * @throws Exception the exception
	 * {@link pk.home.libs.combine.dao.ABaseDAO#merge(java.lang.Object)}.
	 */
	@Test
	@Rollback(true)
	public void testMerge() throws Exception {
		createTestEntitys();
		
		Price price = service.findPrice(division1,productInteger);

		long id = price.getId();

		Price price2 = service.find(id);

		assertEquals(price, price2);
		assertTrue(price.getId() == price2.getId());
		assertEquals(price, price2);

		price.setDivision(division1);
		price.setProduct(productFloat);
		price2 = service.merge(price2);

		price = service.refresh(price);

		assertEquals(price, price2);
		assertTrue(price.getId() == price2.getId());
		assertEquals(price, price2);
		assertEquals(price.getProduct(), price2.getProduct());
	}

	/**
	 * Test method for.
	 *
	 * @throws Exception the exception
	 * {@link pk.home.libs.combine.dao.ABaseDAO#remove(java.lang.Object)}.
	 */
	@Test
	@Rollback(true)
	public void testRemove() throws Exception {
		createTestEntitys();
		
		Price price = service.findPrice(division1,productInteger);

		long id = price.getId();

		Price price2 = service.find(id);

		assertEquals(price, price2);
		assertTrue(price.getId() == price2.getId());
		assertEquals(price, price2);

		service.remove(price);

		Price price3 = service.find(id);
		assertTrue(price3 == null);

	}
	
	
	
	// -----------------------------------------------------------------------------------------------------------------
	
	/**
	 * Insert entities.
	 *
	 * @throws Exception the exception
	 */
	@Test
	@Rollback(true)
	public void insertEntities() throws Exception {
		createTestEntitys();

		long index = service.count();
		
		Product p1 = new Product();
		p1.setKeyName("TEST PRICE 1");
		p1.setBarcode("TEST PRICE 1");
		p1.setMeasure(measureInteger);
		
		p1 = productService.persist(p1);
		
		service.findPrice(division1,p1);
		

		List<Price> list = service.getAllEntities();

		assertTrue(list != null);
		assertTrue(list.size() > 0);
		assertTrue(list.size() == index + 1);
	}
	

}
