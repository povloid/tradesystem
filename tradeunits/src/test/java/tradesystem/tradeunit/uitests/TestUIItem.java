package tradesystem.tradeunit.uitests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import tradesystem.tradeunit.basetestutils.BaseTest;
import tradesystem.tradeunit.domain.Account;
import tradesystem.tradeunit.domain.Item;
import tradesystem.tradeunit.domain.Order;
import tradesystem.tradeunit.domain.OrderType;
import tradesystem.tradeunit.web.jsf.webflow.utils.UIItem;

/**
 * JUnit test service class for entity class: Order Order - ордер
 */
@RunWith(SpringJUnit4ClassRunner.class)
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
		DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class })
@Transactional
@ActiveProfiles({ "Dev" })
@ContextConfiguration(locations = { "file:./src/main/resources/applicationContext.xml" })
public class TestUIItem extends BaseTest {

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test product plus.
	 * 
	 * @throws Exception
	 *             the exception
	 */
	@Test
	@Rollback(true)
	public void testProductPlus() throws Exception {
		createTestEntitys();

		String descrioption = "Operation description...";

		// PLUS (+)
		{
			Account account = accountService.findAccount(division1,
					productInteger, warehouse1);

			BigDecimal oldValue = account.getValue();
			BigDecimal oldNumerator = account.getNumerator();

			System.out.println(">>>>>>>>>" + oldValue);

			BigDecimal newValue = new BigDecimal(10);
			BigDecimal newNumerator = new BigDecimal(0);

			UIItem item = new UIItem(new Item(account),false); // default sign +
			item.setUiValue(newValue);
			item.setUiNumerator(newNumerator);

			List<UIItem> uiItems = new ArrayList<>();
			uiItems.add(item);

			Order order = orderService.productPlus(userAccount, division1, OrderType.PRODUCT_PLUS,
					UIItem.createItemsList(uiItems), descrioption);

			Item item2 = order.getItems().toArray(new Item[] {})[0];

			assertEquals(item.getItem(), item2);
			assertEquals(account, item2.getAccount());
			assertEquals(account.getDivision(), order.getDivision());

			assertTrue(account.getValue().compareTo(oldValue.add(newValue)) == 0);
			assertTrue(account.getNumerator().compareTo(
					oldNumerator.add(newNumerator)) == 0);
		}

		{
			Account account = accountService.findAccount(division1,
					productFloat, warehouse1);

			BigDecimal oldValue = account.getValue();
			BigDecimal oldNumerator = account.getNumerator();

			System.out.println(">>>>>>>>>" + oldValue);

			BigDecimal newValue = new BigDecimal(10.75);
			BigDecimal newNumerator = new BigDecimal(0);

			UIItem item = new UIItem(new Item(account),false); // default sign +
			item.setUiValue(newValue);
			item.setUiNumerator(newNumerator);

			List<UIItem> uiItems = new ArrayList<>();
			uiItems.add(item);

			Order order = orderService.productPlus(userAccount, division1, OrderType.PRODUCT_PLUS,
					UIItem.createItemsList(uiItems), descrioption);

			Item item2 = order.getItems().toArray(new Item[] {})[0];

			assertEquals(item.getItem(), item2);
			assertEquals(account, item2.getAccount());
			assertEquals(account.getDivision(), order.getDivision());

			assertTrue(account.getValue().compareTo(oldValue.add(newValue)) == 0);
			assertTrue(account.getNumerator().compareTo(
					oldNumerator.add(newNumerator)) == 0);
		}

		{
			Account account = accountService.findAccount(division1,
					productFraction, warehouse1);

			BigDecimal oldValue = account.getValue();
			BigDecimal oldNumerator = account.getNumerator();

			System.out.println(">>>>>>>>>" + oldValue + " " + oldNumerator);

			BigDecimal newValue = new BigDecimal(7);
			BigDecimal newNumerator = new BigDecimal(5);

			UIItem item = new UIItem(new Item(account),false); // default sign +
			item.setUiValue(newValue);
			item.setUiNumerator(newNumerator);

			List<UIItem> uiItems = new ArrayList<>();
			uiItems.add(item);

			Order order = orderService.productPlus(userAccount, division1, OrderType.PRODUCT_PLUS,
					UIItem.createItemsList(uiItems), descrioption);

			Item item2 = order.getItems().toArray(new Item[] {})[0];

			assertEquals(item.getItem(), item2);
			assertEquals(account, item2.getAccount());
			assertEquals(account.getDivision(), order.getDivision());

			assertTrue(account.getValue().compareTo(oldValue.add(newValue)) == 0);
			assertTrue(account.getNumerator().compareTo(
					oldNumerator.add(newNumerator)) == 0);
		}

		// MINUS (-)

		{
			Account account = accountService.findAccount(division1,
					productInteger, warehouse1);

			BigDecimal oldValue = account.getValue();
			BigDecimal oldNumerator = account.getNumerator();

			System.out.println(">>>>>>>>>" + oldValue);

			BigDecimal newValue = new BigDecimal(10);
			BigDecimal newNumerator = new BigDecimal(0);

			UIItem item = new UIItem(new Item(account),true);
			item.setUiValue(newValue);
			item.setUiNumerator(newNumerator);

			List<UIItem> uiItems = new ArrayList<>();
			uiItems.add(item);

			Order order = orderService.productPlus(userAccount, division1, OrderType.PRODUCT_MINUS,
					UIItem.createItemsList(uiItems), descrioption);

			Item item2 = order.getItems().toArray(new Item[] {})[0];

			assertEquals(item.getItem(), item2);
			assertEquals(account, item2.getAccount());
			assertEquals(account.getDivision(), order.getDivision());

			assertTrue(account.getValue().compareTo(oldValue.subtract(newValue)) == 0);
			assertTrue(account.getNumerator().compareTo(
					oldNumerator.add(newNumerator)) == 0);
		}

		{
			Account account = accountService.findAccount(division1,
					productFloat, warehouse1);

			BigDecimal oldValue = account.getValue();
			BigDecimal oldNumerator = account.getNumerator();

			System.out.println(">>>>>>>>>" + oldValue);

			BigDecimal newValue = new BigDecimal(10.75);
			BigDecimal newNumerator = new BigDecimal(0);

			UIItem item = new UIItem(new Item(account),true);
			item.setUiValue(newValue);
			item.setUiNumerator(newNumerator);

			List<UIItem> uiItems = new ArrayList<>();
			uiItems.add(item);

			Order order = orderService.productPlus(userAccount, division1, OrderType.PRODUCT_MINUS,
					UIItem.createItemsList(uiItems), descrioption);

			Item item2 = order.getItems().toArray(new Item[] {})[0];

			assertEquals(item.getItem(), item2);
			assertEquals(account, item2.getAccount());
			assertEquals(account.getDivision(), order.getDivision());

			assertTrue(account.getValue().compareTo(oldValue.subtract(newValue)) == 0);
			assertTrue(account.getNumerator().compareTo(
					oldNumerator.add(newNumerator)) == 0);
		}

		{
			Account account = accountService.findAccount(division1,
					productFraction, warehouse1);

			BigDecimal oldValue = account.getValue();
			BigDecimal oldNumerator = account.getNumerator();

			System.out.println(">>>>>>>>>" + oldValue + " " + oldNumerator);

			BigDecimal newValue = new BigDecimal(7);
			BigDecimal newNumerator = new BigDecimal(5);

			UIItem item = new UIItem(new Item(account),true);
			item.setUiValue(newValue);
			item.setUiNumerator(newNumerator);

			List<UIItem> uiItems = new ArrayList<>();
			uiItems.add(item);

			Order order = orderService.productPlus(userAccount, division1, OrderType.PRODUCT_MINUS,
					UIItem.createItemsList(uiItems), descrioption);

			Item item2 = order.getItems().toArray(new Item[] {})[0];

			assertEquals(item.getItem(), item2);
			assertEquals(account, item2.getAccount());
			assertEquals(account.getDivision(), order.getDivision());

			assertTrue(account.getValue().compareTo(oldValue.subtract(newValue)) == 0);
			assertTrue(account.getNumerator().compareTo(
					oldNumerator.subtract(newNumerator)) == 0);
		}

	}

}
