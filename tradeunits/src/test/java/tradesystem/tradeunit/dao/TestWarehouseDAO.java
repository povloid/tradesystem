package tradesystem.tradeunit.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import pk.home.libs.combine.dao.ABaseDAO.SortOrderType;
import tradesystem.tradeunit.basetestutils.BaseTest;
import tradesystem.tradeunit.domain.Warehouse;
import tradesystem.tradeunit.domain.Warehouse_;

/**
 * JUnit test DAO class for entity class: Warehouse
 * Warehouse - склад
 */
@RunWith(SpringJUnit4ClassRunner.class)
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
		DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class })
@Transactional
@ActiveProfiles({"Dev"})
@ContextConfiguration(locations = { "file:./src/main/resources/applicationContext.xml" })
public class TestWarehouseDAO extends BaseTest{

	/**
	 * The DAO being tested, injected by Spring
	 * 
	 */
	private WarehouseDAO dataStore;

	/**
	 * Method to allow Spring to inject the DAO that will be tested
	 * 
	 */
	@Autowired
	public void setDataStore(WarehouseDAO dataStore) {
		this.dataStore = dataStore;
	}

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#getAllEntities()}.
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testGetAllEntities() throws Exception {
		createTestEntitys();

		long index = dataStore.count();
		for (int i = 0; i < 100; i++) {
			Warehouse warehouse = new Warehouse(); warehouse.setDivision(division1);
			warehouse.setKeyName("key " + i);
			dataStore.persist(warehouse);
			index++;
		}

		List<Warehouse> list = dataStore.getAllEntities();

		assertTrue(list != null);
		assertTrue(list.size() > 0);
		assertTrue(list.size() == index);
	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#getAllEntities(javax.persistence.metamodel.SingularAttribute, pk.home.libs.combine.dao.ABaseDAO.SortOrderType)}
	 * .
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testGetAllEntitiesSingularAttributeOfTQSortOrderType()
			throws Exception {
		createTestEntitys();
		
		long index = dataStore.count();
		for (int i = 0; i < 100; i++) {
			Warehouse warehouse = new Warehouse(); warehouse.setDivision(division1);
			warehouse.setKeyName("key " + i);
			dataStore.persist(warehouse);
			index++;
		}

		List<Warehouse> list = dataStore.getAllEntities(Warehouse_.id, SortOrderType.ASC);

		assertTrue(list != null);
		assertTrue(list.size() > 0);
		assertTrue(list.size() == index);

		long lastId = 0;
		for (Warehouse warehouse : list) {
			assertTrue(lastId < warehouse.getId());
			lastId = warehouse.getId();
		}
	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#getAllEntities(int, int)}.
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testGetAllEntitiesIntInt() throws Exception {
		createTestEntitys();

		// int index = 0;
		for (int i = 0; i < 100; i++) {
			Warehouse warehouse = new Warehouse(); warehouse.setDivision(division1);
			warehouse.setKeyName("key " + i);
			dataStore.persist(warehouse);
			// index++;
		}

		List<Warehouse> list = dataStore.getAllEntities(10, 10);

		assertTrue(list != null);
		assertTrue(list.size() > 0);
		assertTrue(list.size() == 10);
	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#getAllEntities(int, int, javax.persistence.metamodel.SingularAttribute, pk.home.libs.combine.dao.ABaseDAO.SortOrderType)}
	 * .
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testGetAllEntitiesIntIntSingularAttributeOfTQSortOrderType()
			throws Exception {
		createTestEntitys();
		
		// long index = dataStore.count();
		for (int i = 0; i < 100; i++) {
			Warehouse warehouse = new Warehouse(); warehouse.setDivision(division1);
			warehouse.setKeyName("key " + i);
			dataStore.persist(warehouse);
			// index++;
		}

		List<Warehouse> list = dataStore.getAllEntities(10, 10, Warehouse_.id,
				SortOrderType.ASC);

		assertTrue(list != null);
		assertTrue(list.size() > 0);
		assertTrue(list.size() == 10);

		long lastId = 0;
		for (Warehouse warehouse : list) {
			assertTrue(lastId < warehouse.getId());
			lastId = warehouse.getId();
		}
	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#getAllEntities(boolean, int, int, javax.persistence.metamodel.SingularAttribute, pk.home.libs.combine.dao.ABaseDAO.SortOrderType)}
	 * .
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testGetAllEntitiesBooleanIntIntSingularAttributeOfTQSortOrderType()
			throws Exception {
		createTestEntitys();
		
		long index = dataStore.count();
		for (int i = 0; i < 100; i++) {
			Warehouse warehouse = new Warehouse(); warehouse.setDivision(division1);
			warehouse.setKeyName("key " + i);
			dataStore.persist(warehouse);
			index++;
		}

		// all - FALSE
		List<Warehouse> list = dataStore.getAllEntities(false, 10, 10, Warehouse_.id,
				SortOrderType.ASC);

		assertTrue(list != null);
		assertTrue(list.size() > 0);
		assertTrue(list.size() == 10);

		long lastId = 0;
		for (Warehouse warehouse : list) {
			assertTrue(lastId < warehouse.getId());
			lastId = warehouse.getId();
		}

		// all - TRUE
		list = dataStore.getAllEntities(true, 10, 10, Warehouse_.id,
				SortOrderType.ASC);

		assertTrue(list != null);
		assertTrue(list.size() > 0);
		assertTrue(list.size() == index);

		lastId = 0;
		for (Warehouse warehouse : list) {
			assertTrue(lastId < warehouse.getId());
			lastId = warehouse.getId();
		}
	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#find(java.lang.Object)}.
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testFind() throws Exception {
		createTestEntitys();

		Warehouse warehouse = new Warehouse(); warehouse.setDivision(division1);
		warehouse.setKeyName("key " + 999);
		warehouse = dataStore.persist(warehouse);

		long id = warehouse.getId();

		Warehouse warehouse2 = dataStore.find(id);

		assertEquals(warehouse, warehouse2);
		assertTrue(warehouse.getId() == warehouse2.getId());
		assertEquals(warehouse.getKeyName(), warehouse2.getKeyName());

	}

	/**
	 * Test method for {@link pk.home.libs.combine.dao.ABaseDAO#count()}.
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testCount() throws Exception {
		createTestEntitys();
		
		long index = dataStore.count();
		for (int i = 0; i < 100; i++) {
			Warehouse warehouse = new Warehouse(); warehouse.setDivision(division1);
			warehouse.setKeyName("key " + i);
			dataStore.persist(warehouse);
			index++;
		}

		long count = dataStore.count();

		assertTrue(count == index);
	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#persist(java.lang.Object)}.
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testPersist() throws Exception {
		createTestEntitys();
		
		Warehouse warehouse = new Warehouse(); warehouse.setDivision(division1);
		warehouse.setKeyName("key " + 999);
		warehouse = dataStore.persist(warehouse);

		long id = warehouse.getId();

		Warehouse warehouse2 = dataStore.find(id);

		assertEquals(warehouse, warehouse2);
		assertTrue(warehouse.getId() == warehouse2.getId());
		assertEquals(warehouse.getKeyName(), warehouse2.getKeyName());
	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#refresh(java.lang.Object)}.
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testRefresh() throws Exception {
		createTestEntitys();
		
		Warehouse warehouse = new Warehouse(); warehouse.setDivision(division1);
		warehouse.setKeyName("key " + 999);
		warehouse = dataStore.persist(warehouse);

		long id = warehouse.getId();

		Warehouse warehouse2 = dataStore.find(id);

		assertEquals(warehouse, warehouse2);
		assertTrue(warehouse.getId() == warehouse2.getId());
		assertEquals(warehouse.getKeyName(), warehouse2.getKeyName());

		warehouse2.setKeyName("key 65535");
		warehouse2 = dataStore.refresh(warehouse2);

		assertEquals(warehouse, warehouse2);
		assertTrue(warehouse.getId() == warehouse2.getId());
		assertEquals(warehouse.getKeyName(), warehouse2.getKeyName());

	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#merge(java.lang.Object)}.
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testMerge() throws Exception {
		createTestEntitys();
		
		Warehouse warehouse = new Warehouse(); warehouse.setDivision(division1);
		warehouse.setKeyName("key " + 999);
		warehouse = dataStore.persist(warehouse);

		long id = warehouse.getId();

		Warehouse warehouse2 = dataStore.find(id);

		assertEquals(warehouse, warehouse2);
		assertTrue(warehouse.getId() == warehouse2.getId());
		assertEquals(warehouse.getKeyName(), warehouse2.getKeyName());

		warehouse2.setKeyName("key 65535");
		warehouse2 = dataStore.merge(warehouse2);

		warehouse = dataStore.refresh(warehouse);

		assertEquals(warehouse, warehouse2);
		assertTrue(warehouse.getId() == warehouse2.getId());
		assertEquals(warehouse.getKeyName(), warehouse2.getKeyName());
	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#remove(java.lang.Object)}.
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testRemove() throws Exception {
		createTestEntitys();
		
		Warehouse warehouse = new Warehouse(); warehouse.setDivision(division1);
		warehouse.setKeyName("key " + 999);
		warehouse = dataStore.persist(warehouse);

		long id = warehouse.getId();

		Warehouse warehouse2 = dataStore.find(id);

		assertEquals(warehouse, warehouse2);
		assertTrue(warehouse.getId() == warehouse2.getId());
		assertEquals(warehouse.getKeyName(), warehouse2.getKeyName());

		dataStore.remove(warehouse);

		Warehouse warehouse3 = dataStore.find(id);
		assertTrue(warehouse3 == null);

	}
	
	
	
	// -----------------------------------------------------------------------------------------------------------------
	
	@Test
	@Rollback(true)
	public void insertEntities() throws Exception {
		createTestEntitys();

		long index = dataStore.count();
		for (int i = 200; i < 210; i++) {
			Warehouse warehouse = new Warehouse(); warehouse.setDivision(division1);
			warehouse.setKeyName("key " + i);
			dataStore.persist(warehouse);
			index++;
		}

		List<Warehouse> list = dataStore.getAllEntities();

		assertTrue(list != null);
		assertTrue(list.size() > 0);
		assertTrue(list.size() == index);
	}
	

}
