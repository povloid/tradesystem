package tradesystem.tradeunit.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import pk.home.libs.combine.dao.ABaseDAO.SortOrderType;
import tradesystem.tradeunit.domain.Provider;
import tradesystem.tradeunit.domain.Provider_;

/**
 * JUnit test DAO class for entity class: Provider
 * Provider - продукт
 */
@RunWith(SpringJUnit4ClassRunner.class)
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
		DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class })
@Transactional
@ActiveProfiles({"Dev"})
@ContextConfiguration(locations = { "file:./src/main/resources/applicationContext.xml" })
public class TestProviderDAO {

	/**
	 * The DAO being tested, injected by Spring
	 * 
	 */
	private ProviderDAO dataStore;

	/**
	 * Method to allow Spring to inject the DAO that will be tested
	 * 
	 */
	@Autowired
	public void setDataStore(ProviderDAO dataStore) {
		this.dataStore = dataStore;
	}

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#getAllEntities()}.
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testGetAllEntities() throws Exception {

		long index = dataStore.count();
		for (int i = 0; i < 100; i++) {
			Provider provider = new Provider();
			provider.setKeyName("key " + i);
			dataStore.persist(provider);
			index++;
		}

		List<Provider> list = dataStore.getAllEntities();

		assertTrue(list != null);
		assertTrue(list.size() > 0);
		assertTrue(list.size() == index);
	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#getAllEntities(javax.persistence.metamodel.SingularAttribute, pk.home.libs.combine.dao.ABaseDAO.SortOrderType)}
	 * .
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testGetAllEntitiesSingularAttributeOfTQSortOrderType()
			throws Exception {
		long index = dataStore.count();
		for (int i = 0; i < 100; i++) {
			Provider provider = new Provider();
			provider.setKeyName("key " + i);
			dataStore.persist(provider);
			index++;
		}

		List<Provider> list = dataStore.getAllEntities(Provider_.id, SortOrderType.ASC);

		assertTrue(list != null);
		assertTrue(list.size() > 0);
		assertTrue(list.size() == index);

		long lastId = 0;
		for (Provider provider : list) {
			assertTrue(lastId < provider.getId());
			lastId = provider.getId();
		}
	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#getAllEntities(int, int)}.
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testGetAllEntitiesIntInt() throws Exception {

		// int index = 0;
		for (int i = 0; i < 100; i++) {
			Provider provider = new Provider();
			provider.setKeyName("key " + i);
			dataStore.persist(provider);
			// index++;
		}

		List<Provider> list = dataStore.getAllEntities(10, 10);

		assertTrue(list != null);
		assertTrue(list.size() > 0);
		assertTrue(list.size() == 10);
	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#getAllEntities(int, int, javax.persistence.metamodel.SingularAttribute, pk.home.libs.combine.dao.ABaseDAO.SortOrderType)}
	 * .
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testGetAllEntitiesIntIntSingularAttributeOfTQSortOrderType()
			throws Exception {
		// long index = dataStore.count();
		for (int i = 0; i < 100; i++) {
			Provider provider = new Provider();
			provider.setKeyName("key " + i);
			dataStore.persist(provider);
			// index++;
		}

		List<Provider> list = dataStore.getAllEntities(10, 10, Provider_.id,
				SortOrderType.ASC);

		assertTrue(list != null);
		assertTrue(list.size() > 0);
		assertTrue(list.size() == 10);

		long lastId = 0;
		for (Provider provider : list) {
			assertTrue(lastId < provider.getId());
			lastId = provider.getId();
		}
	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#getAllEntities(boolean, int, int, javax.persistence.metamodel.SingularAttribute, pk.home.libs.combine.dao.ABaseDAO.SortOrderType)}
	 * .
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testGetAllEntitiesBooleanIntIntSingularAttributeOfTQSortOrderType()
			throws Exception {
		long index = dataStore.count();
		for (int i = 0; i < 100; i++) {
			Provider provider = new Provider();
			provider.setKeyName("key " + i);
			dataStore.persist(provider);
			index++;
		}

		// all - FALSE
		List<Provider> list = dataStore.getAllEntities(false, 10, 10, Provider_.id,
				SortOrderType.ASC);

		assertTrue(list != null);
		assertTrue(list.size() > 0);
		assertTrue(list.size() == 10);

		long lastId = 0;
		for (Provider provider : list) {
			assertTrue(lastId < provider.getId());
			lastId = provider.getId();
		}

		// all - TRUE
		list = dataStore.getAllEntities(true, 10, 10, Provider_.id,
				SortOrderType.ASC);

		assertTrue(list != null);
		assertTrue(list.size() > 0);
		assertTrue(list.size() == index);

		lastId = 0;
		for (Provider provider : list) {
			assertTrue(lastId < provider.getId());
			lastId = provider.getId();
		}
	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#find(java.lang.Object)}.
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testFind() throws Exception {

		Provider provider = new Provider();
		provider.setKeyName("key " + 999);
		provider = dataStore.persist(provider);

		long id = provider.getId();

		Provider provider2 = dataStore.find(id);

		assertEquals(provider, provider2);
		assertTrue(provider.getId() == provider2.getId());
		assertEquals(provider.getKeyName(), provider2.getKeyName());

	}

	/**
	 * Test method for {@link pk.home.libs.combine.dao.ABaseDAO#count()}.
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testCount() throws Exception {
		long index = dataStore.count();
		for (int i = 0; i < 100; i++) {
			Provider provider = new Provider();
			provider.setKeyName("key " + i);
			dataStore.persist(provider);
			index++;
		}

		long count = dataStore.count();

		assertTrue(count == index);
	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#persist(java.lang.Object)}.
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testPersist() throws Exception {
		Provider provider = new Provider();
		provider.setKeyName("key " + 999);
		provider = dataStore.persist(provider);

		long id = provider.getId();

		Provider provider2 = dataStore.find(id);

		assertEquals(provider, provider2);
		assertTrue(provider.getId() == provider2.getId());
		assertEquals(provider.getKeyName(), provider2.getKeyName());
	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#refresh(java.lang.Object)}.
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testRefresh() throws Exception {
		Provider provider = new Provider();
		provider.setKeyName("key " + 999);
		provider = dataStore.persist(provider);

		long id = provider.getId();

		Provider provider2 = dataStore.find(id);

		assertEquals(provider, provider2);
		assertTrue(provider.getId() == provider2.getId());
		assertEquals(provider.getKeyName(), provider2.getKeyName());

		provider2.setKeyName("key 65535");
		provider2 = dataStore.refresh(provider2);

		assertEquals(provider, provider2);
		assertTrue(provider.getId() == provider2.getId());
		assertEquals(provider.getKeyName(), provider2.getKeyName());

	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#merge(java.lang.Object)}.
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testMerge() throws Exception {
		Provider provider = new Provider();
		provider.setKeyName("key " + 999);
		provider = dataStore.persist(provider);

		long id = provider.getId();

		Provider provider2 = dataStore.find(id);

		assertEquals(provider, provider2);
		assertTrue(provider.getId() == provider2.getId());
		assertEquals(provider.getKeyName(), provider2.getKeyName());

		provider2.setKeyName("key 65535");
		provider2 = dataStore.merge(provider2);

		provider = dataStore.refresh(provider);

		assertEquals(provider, provider2);
		assertTrue(provider.getId() == provider2.getId());
		assertEquals(provider.getKeyName(), provider2.getKeyName());
	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#remove(java.lang.Object)}.
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testRemove() throws Exception {
		Provider provider = new Provider();
		provider.setKeyName("key " + 999);
		provider = dataStore.persist(provider);

		long id = provider.getId();

		Provider provider2 = dataStore.find(id);

		assertEquals(provider, provider2);
		assertTrue(provider.getId() == provider2.getId());
		assertEquals(provider.getKeyName(), provider2.getKeyName());

		dataStore.remove(provider);

		Provider provider3 = dataStore.find(id);
		assertTrue(provider3 == null);

	}
	
	
	
	// -----------------------------------------------------------------------------------------------------------------
	
	@Test
	@Rollback(true)
	public void insertEntities() throws Exception {

		long index = dataStore.count();
		for (int i = 200; i < 210; i++) {
			Provider provider = new Provider();
			provider.setKeyName("key " + i);
			dataStore.persist(provider);
			index++;
		}

		List<Provider> list = dataStore.getAllEntities();

		assertTrue(list != null);
		assertTrue(list.size() > 0);
		assertTrue(list.size() == index);
	}
	

}
