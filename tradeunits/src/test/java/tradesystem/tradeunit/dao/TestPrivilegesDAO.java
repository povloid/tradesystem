package tradesystem.tradeunit.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import pk.home.libs.combine.dao.ABaseDAO.SortOrderType;
import tradesystem.tradeunit.basetestutils.BaseTest;
import tradesystem.tradeunit.domain.Privileges;
import tradesystem.tradeunit.domain.Privileges_;

/**
 * JUnit test DAO class for entity class: Privileges
 * Privileges - привелегии
 */
@RunWith(SpringJUnit4ClassRunner.class)
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
		DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class })
@Transactional
@ActiveProfiles({"Dev"})
@ContextConfiguration(locations = { "file:./src/main/resources/applicationContext.xml" })
public class TestPrivilegesDAO extends BaseTest{

	/**
	 * The DAO being tested, injected by Spring
	 * 
	 */
	private PrivilegesDAO dataStore;

	/**
	 * Method to allow Spring to inject the DAO that will be tested
	 * 
	 */
	@Autowired
	public void setDataStore(PrivilegesDAO dataStore) {
		this.dataStore = dataStore;
	}

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#getAllEntities()}.
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testGetAllEntities() throws Exception {
		createTestEntitys();

		long index = dataStore.count();
		for (int i = 0; i < 100; i++) {
			Privileges privileges = new Privileges();
			privileges.setDivision(division1);
			privileges.setUserAccount(userAccount);
			
			dataStore.persist(privileges);
			index++;
		}

		List<Privileges> list = dataStore.getAllEntities();

		assertTrue(list != null);
		assertTrue(list.size() > 0);
		assertTrue(list.size() == index);
	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#getAllEntities(javax.persistence.metamodel.SingularAttribute, pk.home.libs.combine.dao.ABaseDAO.SortOrderType)}
	 * .
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testGetAllEntitiesSingularAttributeOfTQSortOrderType()
			throws Exception {
		createTestEntitys();
		
		long index = dataStore.count();
		for (int i = 0; i < 100; i++) {
			Privileges privileges = new Privileges();
			privileges.setDivision(division1);
			privileges.setUserAccount(userAccount);
			dataStore.persist(privileges);
			index++;
		}

		List<Privileges> list = dataStore.getAllEntities(Privileges_.id, SortOrderType.ASC);

		assertTrue(list != null);
		assertTrue(list.size() > 0);
		assertTrue(list.size() == index);

		long lastId = 0;
		for (Privileges privileges : list) {
			assertTrue(lastId < privileges.getId());
			lastId = privileges.getId();
		}
	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#getAllEntities(int, int)}.
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testGetAllEntitiesIntInt() throws Exception {
		createTestEntitys();

		// int index = 0;
		for (int i = 0; i < 100; i++) {
			Privileges privileges = new Privileges();
			privileges.setDivision(division1);
			privileges.setUserAccount(userAccount);
			dataStore.persist(privileges);
			// index++;
		}

		List<Privileges> list = dataStore.getAllEntities(10, 10);

		assertTrue(list != null);
		assertTrue(list.size() > 0);
		assertTrue(list.size() == 10);
	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#getAllEntities(int, int, javax.persistence.metamodel.SingularAttribute, pk.home.libs.combine.dao.ABaseDAO.SortOrderType)}
	 * .
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testGetAllEntitiesIntIntSingularAttributeOfTQSortOrderType()
			throws Exception {
		createTestEntitys();
		
		// long index = dataStore.count();
		for (int i = 0; i < 100; i++) {
			Privileges privileges = new Privileges();
			privileges.setDivision(division1);
			privileges.setUserAccount(userAccount);
			dataStore.persist(privileges);
			// index++;
		}

		List<Privileges> list = dataStore.getAllEntities(10, 10, Privileges_.id,
				SortOrderType.ASC);

		assertTrue(list != null);
		assertTrue(list.size() > 0);
		assertTrue(list.size() == 10);

		long lastId = 0;
		for (Privileges privileges : list) {
			assertTrue(lastId < privileges.getId());
			lastId = privileges.getId();
		}
	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#getAllEntities(boolean, int, int, javax.persistence.metamodel.SingularAttribute, pk.home.libs.combine.dao.ABaseDAO.SortOrderType)}
	 * .
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testGetAllEntitiesBooleanIntIntSingularAttributeOfTQSortOrderType()
			throws Exception {
		createTestEntitys();
		
		long index = dataStore.count();
		for (int i = 0; i < 100; i++) {
			Privileges privileges = new Privileges();
			privileges.setDivision(division1);
			privileges.setUserAccount(userAccount);
			dataStore.persist(privileges);
			index++;
		}

		// all - FALSE
		List<Privileges> list = dataStore.getAllEntities(false, 10, 10, Privileges_.id,
				SortOrderType.ASC);

		assertTrue(list != null);
		assertTrue(list.size() > 0);
		assertTrue(list.size() == 10);

		long lastId = 0;
		for (Privileges privileges : list) {
			assertTrue(lastId < privileges.getId());
			lastId = privileges.getId();
		}

		// all - TRUE
		list = dataStore.getAllEntities(true, 10, 10, Privileges_.id,
				SortOrderType.ASC);

		assertTrue(list != null);
		assertTrue(list.size() > 0);
		assertTrue(list.size() == index);

		lastId = 0;
		for (Privileges privileges : list) {
			assertTrue(lastId < privileges.getId());
			lastId = privileges.getId();
		}
	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#find(java.lang.Object)}.
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testFind() throws Exception {
		createTestEntitys();

		Privileges privileges = new Privileges();
		privileges.setDivision(division1);
		privileges.setUserAccount(userAccount);
		privileges = dataStore.persist(privileges);

		long id = privileges.getId();

		Privileges privileges2 = dataStore.find(id);

		assertEquals(privileges, privileges2);
		assertTrue(privileges.getId() == privileges2.getId());

	}

	/**
	 * Test method for {@link pk.home.libs.combine.dao.ABaseDAO#count()}.
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testCount() throws Exception {
		createTestEntitys();
		
		long index = dataStore.count();
		for (int i = 0; i < 100; i++) {
			Privileges privileges = new Privileges();
			privileges.setDivision(division1);
			privileges.setUserAccount(userAccount);
			dataStore.persist(privileges);
			index++;
		}

		long count = dataStore.count();

		assertTrue(count == index);
	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#persist(java.lang.Object)}.
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testPersist() throws Exception {
		createTestEntitys();
		
		Privileges privileges = new Privileges();
		privileges.setDivision(division1);
		privileges.setUserAccount(userAccount);
		privileges = dataStore.persist(privileges);

		long id = privileges.getId();

		Privileges privileges2 = dataStore.find(id);

		assertEquals(privileges, privileges2);
		assertTrue(privileges.getId() == privileges2.getId());
	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#refresh(java.lang.Object)}.
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testRefresh() throws Exception {
		createTestEntitys();
		
		Privileges privileges = new Privileges();
		privileges.setDivision(division1);
		privileges.setUserAccount(userAccount);
		privileges = dataStore.persist(privileges);

		long id = privileges.getId();

		Privileges privileges2 = dataStore.find(id);

		assertEquals(privileges, privileges2);
		assertTrue(privileges.getId() == privileges2.getId());

		privileges2 = dataStore.refresh(privileges2);

		assertEquals(privileges, privileges2);
		assertTrue(privileges.getId() == privileges2.getId());

	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#merge(java.lang.Object)}.
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testMerge() throws Exception {
		createTestEntitys();
		
		Privileges privileges = new Privileges();
		privileges.setDivision(division1);
		privileges.setUserAccount(userAccount);
		privileges = dataStore.persist(privileges);

		long id = privileges.getId();

		Privileges privileges2 = dataStore.find(id);

		assertEquals(privileges, privileges2);
		assertTrue(privileges.getId() == privileges2.getId());

		privileges2 = dataStore.merge(privileges2);

		privileges = dataStore.refresh(privileges);

		assertEquals(privileges, privileges2);
		assertTrue(privileges.getId() == privileges2.getId());
	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#remove(java.lang.Object)}.
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testRemove() throws Exception {
		createTestEntitys();
		
		Privileges privileges = new Privileges();
		privileges.setDivision(division1);
		privileges.setUserAccount(userAccount);
		privileges = dataStore.persist(privileges);

		long id = privileges.getId();

		Privileges privileges2 = dataStore.find(id);

		assertEquals(privileges, privileges2);
		assertTrue(privileges.getId() == privileges2.getId());

		dataStore.remove(privileges);

		Privileges privileges3 = dataStore.find(id);
		assertTrue(privileges3 == null);

	}
	
	
	
	// -----------------------------------------------------------------------------------------------------------------
	
	@Test
	@Rollback(true)
	public void insertEntities() throws Exception {
		createTestEntitys();

		long index = dataStore.count();
		for (int i = 200; i < 210; i++) {
			Privileges privileges = new Privileges();
			privileges.setDivision(division1);
			privileges.setUserAccount(userAccount);
			dataStore.persist(privileges);
			index++;
		}

		List<Privileges> list = dataStore.getAllEntities();

		assertTrue(list != null);
		assertTrue(list.size() > 0);
		assertTrue(list.size() == index);
	}
	

}
