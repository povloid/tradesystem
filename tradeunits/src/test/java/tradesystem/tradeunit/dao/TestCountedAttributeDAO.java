package tradesystem.tradeunit.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import pk.home.libs.combine.dao.ABaseDAO.SortOrderType;
import tradesystem.tradeunit.domain.CountedAttribute;
import tradesystem.tradeunit.domain.CountedAttribute_;

/**
 * JUnit test DAO class for entity class: CountedAttribute
 * СountedAttribute - подсчитываемый атрибут
 */
@RunWith(SpringJUnit4ClassRunner.class)
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
		DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class })
@Transactional
@ActiveProfiles({"Dev"})
@ContextConfiguration(locations = { "file:./src/main/resources/applicationContext.xml" })
public class TestCountedAttributeDAO {

	/**
	 * The DAO being tested, injected by Spring
	 * 
	 */
	private CountedAttributeDAO dataStore;

	/**
	 * Method to allow Spring to inject the DAO that will be tested
	 * 
	 */
	@Autowired
	public void setDataStore(CountedAttributeDAO dataStore) {
		this.dataStore = dataStore;
	}

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#getAllEntities()}.
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testGetAllEntities() throws Exception {

		long index = dataStore.count();
		for (int i = 0; i < 100; i++) {
			CountedAttribute countedAttribute = new CountedAttribute();
			countedAttribute.setKeyName("key " + i);
			dataStore.persist(countedAttribute);
			index++;
		}

		List<CountedAttribute> list = dataStore.getAllEntities();

		assertTrue(list != null);
		assertTrue(list.size() > 0);
		assertTrue(list.size() == index);
	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#getAllEntities(javax.persistence.metamodel.SingularAttribute, pk.home.libs.combine.dao.ABaseDAO.SortOrderType)}
	 * .
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testGetAllEntitiesSingularAttributeOfTQSortOrderType()
			throws Exception {
		long index = dataStore.count();
		for (int i = 0; i < 100; i++) {
			CountedAttribute countedAttribute = new CountedAttribute();
			countedAttribute.setKeyName("key " + i);
			dataStore.persist(countedAttribute);
			index++;
		}

		List<CountedAttribute> list = dataStore.getAllEntities(CountedAttribute_.id, SortOrderType.ASC);

		assertTrue(list != null);
		assertTrue(list.size() > 0);
		assertTrue(list.size() == index);

		long lastId = 0;
		for (CountedAttribute countedAttribute : list) {
			assertTrue(lastId < countedAttribute.getId());
			lastId = countedAttribute.getId();
		}
	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#getAllEntities(int, int)}.
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testGetAllEntitiesIntInt() throws Exception {

		// int index = 0;
		for (int i = 0; i < 100; i++) {
			CountedAttribute countedAttribute = new CountedAttribute();
			countedAttribute.setKeyName("key " + i);
			dataStore.persist(countedAttribute);
			// index++;
		}

		List<CountedAttribute> list = dataStore.getAllEntities(10, 10);

		assertTrue(list != null);
		assertTrue(list.size() > 0);
		assertTrue(list.size() == 10);
	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#getAllEntities(int, int, javax.persistence.metamodel.SingularAttribute, pk.home.libs.combine.dao.ABaseDAO.SortOrderType)}
	 * .
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testGetAllEntitiesIntIntSingularAttributeOfTQSortOrderType()
			throws Exception {
		// long index = dataStore.count();
		for (int i = 0; i < 100; i++) {
			CountedAttribute countedAttribute = new CountedAttribute();
			countedAttribute.setKeyName("key " + i);
			dataStore.persist(countedAttribute);
			// index++;
		}

		List<CountedAttribute> list = dataStore.getAllEntities(10, 10, CountedAttribute_.id,
				SortOrderType.ASC);

		assertTrue(list != null);
		assertTrue(list.size() > 0);
		assertTrue(list.size() == 10);

		long lastId = 0;
		for (CountedAttribute countedAttribute : list) {
			assertTrue(lastId < countedAttribute.getId());
			lastId = countedAttribute.getId();
		}
	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#getAllEntities(boolean, int, int, javax.persistence.metamodel.SingularAttribute, pk.home.libs.combine.dao.ABaseDAO.SortOrderType)}
	 * .
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testGetAllEntitiesBooleanIntIntSingularAttributeOfTQSortOrderType()
			throws Exception {
		long index = dataStore.count();
		for (int i = 0; i < 100; i++) {
			CountedAttribute countedAttribute = new CountedAttribute();
			countedAttribute.setKeyName("key " + i);
			dataStore.persist(countedAttribute);
			index++;
		}

		// all - FALSE
		List<CountedAttribute> list = dataStore.getAllEntities(false, 10, 10, CountedAttribute_.id,
				SortOrderType.ASC);

		assertTrue(list != null);
		assertTrue(list.size() > 0);
		assertTrue(list.size() == 10);

		long lastId = 0;
		for (CountedAttribute countedAttribute : list) {
			assertTrue(lastId < countedAttribute.getId());
			lastId = countedAttribute.getId();
		}

		// all - TRUE
		list = dataStore.getAllEntities(true, 10, 10, CountedAttribute_.id,
				SortOrderType.ASC);

		assertTrue(list != null);
		assertTrue(list.size() > 0);
		assertTrue(list.size() == index);

		lastId = 0;
		for (CountedAttribute countedAttribute : list) {
			assertTrue(lastId < countedAttribute.getId());
			lastId = countedAttribute.getId();
		}
	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#find(java.lang.Object)}.
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testFind() throws Exception {

		CountedAttribute countedAttribute = new CountedAttribute();
		countedAttribute.setKeyName("key " + 999);
		countedAttribute = dataStore.persist(countedAttribute);

		long id = countedAttribute.getId();

		CountedAttribute countedAttribute2 = dataStore.find(id);

		assertEquals(countedAttribute, countedAttribute2);
		assertTrue(countedAttribute.getId() == countedAttribute2.getId());
		assertEquals(countedAttribute.getKeyName(), countedAttribute2.getKeyName());

	}

	/**
	 * Test method for {@link pk.home.libs.combine.dao.ABaseDAO#count()}.
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testCount() throws Exception {
		long index = dataStore.count();
		for (int i = 0; i < 100; i++) {
			CountedAttribute countedAttribute = new CountedAttribute();
			countedAttribute.setKeyName("key " + i);
			dataStore.persist(countedAttribute);
			index++;
		}

		long count = dataStore.count();

		assertTrue(count == index);
	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#persist(java.lang.Object)}.
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testPersist() throws Exception {
		CountedAttribute countedAttribute = new CountedAttribute();
		countedAttribute.setKeyName("key " + 999);
		countedAttribute = dataStore.persist(countedAttribute);

		long id = countedAttribute.getId();

		CountedAttribute countedAttribute2 = dataStore.find(id);

		assertEquals(countedAttribute, countedAttribute2);
		assertTrue(countedAttribute.getId() == countedAttribute2.getId());
		assertEquals(countedAttribute.getKeyName(), countedAttribute2.getKeyName());
	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#refresh(java.lang.Object)}.
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testRefresh() throws Exception {
		CountedAttribute countedAttribute = new CountedAttribute();
		countedAttribute.setKeyName("key " + 999);
		countedAttribute = dataStore.persist(countedAttribute);

		long id = countedAttribute.getId();

		CountedAttribute countedAttribute2 = dataStore.find(id);

		assertEquals(countedAttribute, countedAttribute2);
		assertTrue(countedAttribute.getId() == countedAttribute2.getId());
		assertEquals(countedAttribute.getKeyName(), countedAttribute2.getKeyName());

		countedAttribute2.setKeyName("key 65535");
		countedAttribute2 = dataStore.refresh(countedAttribute2);

		assertEquals(countedAttribute, countedAttribute2);
		assertTrue(countedAttribute.getId() == countedAttribute2.getId());
		assertEquals(countedAttribute.getKeyName(), countedAttribute2.getKeyName());

	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#merge(java.lang.Object)}.
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testMerge() throws Exception {
		CountedAttribute countedAttribute = new CountedAttribute();
		countedAttribute.setKeyName("key " + 999);
		countedAttribute = dataStore.persist(countedAttribute);

		long id = countedAttribute.getId();

		CountedAttribute countedAttribute2 = dataStore.find(id);

		assertEquals(countedAttribute, countedAttribute2);
		assertTrue(countedAttribute.getId() == countedAttribute2.getId());
		assertEquals(countedAttribute.getKeyName(), countedAttribute2.getKeyName());

		countedAttribute2.setKeyName("key 65535");
		countedAttribute2 = dataStore.merge(countedAttribute2);

		countedAttribute = dataStore.refresh(countedAttribute);

		assertEquals(countedAttribute, countedAttribute2);
		assertTrue(countedAttribute.getId() == countedAttribute2.getId());
		assertEquals(countedAttribute.getKeyName(), countedAttribute2.getKeyName());
	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#remove(java.lang.Object)}.
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testRemove() throws Exception {
		CountedAttribute countedAttribute = new CountedAttribute();
		countedAttribute.setKeyName("key " + 999);
		countedAttribute = dataStore.persist(countedAttribute);

		long id = countedAttribute.getId();

		CountedAttribute countedAttribute2 = dataStore.find(id);

		assertEquals(countedAttribute, countedAttribute2);
		assertTrue(countedAttribute.getId() == countedAttribute2.getId());
		assertEquals(countedAttribute.getKeyName(), countedAttribute2.getKeyName());

		dataStore.remove(countedAttribute);

		CountedAttribute countedAttribute3 = dataStore.find(id);
		assertTrue(countedAttribute3 == null);

	}
	
	
	
	// -----------------------------------------------------------------------------------------------------------------
	
	@Test
	@Rollback(true)
	public void insertEntities() throws Exception {

		long index = dataStore.count();
		for (int i = 200; i < 210; i++) {
			CountedAttribute countedAttribute = new CountedAttribute();
			countedAttribute.setKeyName("key " + i);
			dataStore.persist(countedAttribute);
			index++;
		}

		List<CountedAttribute> list = dataStore.getAllEntities();

		assertTrue(list != null);
		assertTrue(list.size() > 0);
		assertTrue(list.size() == index);
	}
	

}
