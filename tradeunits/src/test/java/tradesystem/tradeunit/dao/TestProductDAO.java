package tradesystem.tradeunit.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import pk.home.libs.combine.dao.ABaseDAO.SortOrderType;
import tradesystem.tradeunit.domain.Measure;
import tradesystem.tradeunit.domain.Product;
import tradesystem.tradeunit.domain.Product_;

/**
 * JUnit test DAO class for entity class: Product Product - продукт
 */
@RunWith(SpringJUnit4ClassRunner.class)
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
		DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class })
@Transactional
@ActiveProfiles({ "Dev" })
@ContextConfiguration(locations = { "file:./src/main/resources/applicationContext.xml" })
public class TestProductDAO {

	/**
	 * The DAO being tested, injected by Spring
	 * 
	 */
	private ProductDAO dataStore;

	/**
	 * 
	 */
	private MeasureDAO measureDAO;

	/**
	 * Method to allow Spring to inject the DAO that will be tested
	 * 
	 */
	@Autowired
	public void setDataStore(ProductDAO dataStore) {
		this.dataStore = dataStore;
	}

	/**
	 * Method to allow Spring to inject the DAO that will be tested
	 * 
	 */
	@Autowired
	public void setDataStore(MeasureDAO measureDAO) {
		this.measureDAO = measureDAO;
	}

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#getAllEntities()}.
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testGetAllEntities() throws Exception {

		Measure measure = new Measure();
		measure.setKeyName("measyre test");
		measure.setMeasureType(Measure.MeasureType.INTEGER);
		measure = measureDAO.persist(measure);

		long index = dataStore.count();
		for (int i = 0; i < 100; i++) {
			Product product = new Product();
			product.setKeyName("key " + i);
			product.setBarcode("testcode" + i);
			product.setMeasure(measure);
			product.setDenominator(new BigDecimal(0));

			dataStore.persist(product);
			index++;
		}

		List<Product> list = dataStore.getAllEntities();

		assertTrue(list != null);
		assertTrue(list.size() > 0);
		assertTrue(list.size() == index);
	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#getAllEntities(javax.persistence.metamodel.SingularAttribute, pk.home.libs.combine.dao.ABaseDAO.SortOrderType)}
	 * .
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testGetAllEntitiesSingularAttributeOfTQSortOrderType()
			throws Exception {

		Measure measure = new Measure();
		measure.setKeyName("measyre test");
		measure.setMeasureType(Measure.MeasureType.INTEGER);
		measure = measureDAO.persist(measure);

		long index = dataStore.count();
		for (int i = 0; i < 100; i++) {
			Product product = new Product();
			product.setKeyName("key " + i);
			product.setBarcode("testcode" + i);
			product.setMeasure(measure);
			product.setDenominator(new BigDecimal(0));
			dataStore.persist(product);
			index++;
		}

		List<Product> list = dataStore.getAllEntities(Product_.id,
				SortOrderType.ASC);

		assertTrue(list != null);
		assertTrue(list.size() > 0);
		assertTrue(list.size() == index);

		long lastId = 0;
		for (Product product : list) {
			assertTrue(lastId < product.getId());
			lastId = product.getId();
		}
	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#getAllEntities(int, int)}.
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testGetAllEntitiesIntInt() throws Exception {

		Measure measure = new Measure();
		measure.setKeyName("measyre test");
		measure.setMeasureType(Measure.MeasureType.INTEGER);
		measure = measureDAO.persist(measure);

		// int index = 0;
		for (int i = 0; i < 100; i++) {
			Product product = new Product();
			product.setKeyName("key " + i);
			product.setBarcode("testcode" + i);
			product.setMeasure(measure);
			product.setDenominator(new BigDecimal(0));
			dataStore.persist(product);
			// index++;
		}

		List<Product> list = dataStore.getAllEntities(10, 10);

		assertTrue(list != null);
		assertTrue(list.size() > 0);
		assertTrue(list.size() == 10);
	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#getAllEntities(int, int, javax.persistence.metamodel.SingularAttribute, pk.home.libs.combine.dao.ABaseDAO.SortOrderType)}
	 * .
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testGetAllEntitiesIntIntSingularAttributeOfTQSortOrderType()
			throws Exception {

		Measure measure = new Measure();
		measure.setKeyName("measyre test");
		measure.setMeasureType(Measure.MeasureType.INTEGER);
		measure = measureDAO.persist(measure);

		// long index = dataStore.count();
		for (int i = 0; i < 100; i++) {
			Product product = new Product();
			product.setKeyName("key " + i);
			product.setBarcode("testcode" + i);
			product.setMeasure(measure);
			product.setDenominator(new BigDecimal(0));
			dataStore.persist(product);
			// index++;
		}

		List<Product> list = dataStore.getAllEntities(10, 10, Product_.id,
				SortOrderType.ASC);

		assertTrue(list != null);
		assertTrue(list.size() > 0);
		assertTrue(list.size() == 10);

		long lastId = 0;
		for (Product product : list) {
			assertTrue(lastId < product.getId());
			lastId = product.getId();
		}
	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#getAllEntities(boolean, int, int, javax.persistence.metamodel.SingularAttribute, pk.home.libs.combine.dao.ABaseDAO.SortOrderType)}
	 * .
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testGetAllEntitiesBooleanIntIntSingularAttributeOfTQSortOrderType()
			throws Exception {

		Measure measure = new Measure();
		measure.setKeyName("measyre test");
		measure.setMeasureType(Measure.MeasureType.INTEGER);
		measure = measureDAO.persist(measure);

		long index = dataStore.count();
		for (int i = 0; i < 100; i++) {
			Product product = new Product();
			product.setKeyName("key " + i);
			product.setBarcode("testcode" + i);
			product.setMeasure(measure);
			product.setDenominator(new BigDecimal(0));
			dataStore.persist(product);
			index++;
		}

		// all - FALSE
		List<Product> list = dataStore.getAllEntities(false, 10, 10,
				Product_.id, SortOrderType.ASC);

		assertTrue(list != null);
		assertTrue(list.size() > 0);
		assertTrue(list.size() == 10);

		long lastId = 0;
		for (Product product : list) {
			assertTrue(lastId < product.getId());
			lastId = product.getId();
		}

		// all - TRUE
		list = dataStore.getAllEntities(true, 10, 10, Product_.id,
				SortOrderType.ASC);

		assertTrue(list != null);
		assertTrue(list.size() > 0);
		assertTrue(list.size() == index);

		lastId = 0;
		for (Product product : list) {
			assertTrue(lastId < product.getId());
			lastId = product.getId();
		}
	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#find(java.lang.Object)}.
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testFind() throws Exception {
		Measure measure = new Measure();
		measure.setKeyName("measyre test");
		measure.setMeasureType(Measure.MeasureType.INTEGER);
		measure = measureDAO.persist(measure);

		Product product = new Product();
		product.setKeyName("key " + 999);
		product.setBarcode("999");
		product.setMeasure(measure);
		product.setDenominator(new BigDecimal(0));
		product = dataStore.persist(product);

		long id = product.getId();

		Product product2 = dataStore.find(id);

		assertEquals(product, product2);
		assertTrue(product.getId() == product2.getId());
		assertEquals(product.getKeyName(), product2.getKeyName());

	}

	/**
	 * Test method for {@link pk.home.libs.combine.dao.ABaseDAO#count()}.
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testCount() throws Exception {
		Measure measure = new Measure();
		measure.setKeyName("measyre test");
		measure.setMeasureType(Measure.MeasureType.INTEGER);
		measure = measureDAO.persist(measure);

		long index = dataStore.count();
		for (int i = 0; i < 100; i++) {
			Product product = new Product();
			product.setKeyName("key " + i);
			product.setBarcode("testcode" + i);
			product.setMeasure(measure);
			product.setDenominator(new BigDecimal(0));
			dataStore.persist(product);
			index++;
		}

		long count = dataStore.count();

		assertTrue(count == index);
	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#persist(java.lang.Object)}.
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testPersist() throws Exception {

		Measure measure = new Measure();
		measure.setKeyName("measyre test");
		measure.setMeasureType(Measure.MeasureType.INTEGER);
		measure = measureDAO.persist(measure);

		Product product = new Product();
		product.setKeyName("key " + 999);
		product.setBarcode("999");
		product.setMeasure(measure);
		product.setDenominator(new BigDecimal(0));
		product = dataStore.persist(product);

		long id = product.getId();

		Product product2 = dataStore.find(id);

		assertEquals(product, product2);
		assertTrue(product.getId() == product2.getId());
		assertEquals(product.getKeyName(), product2.getKeyName());
	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#refresh(java.lang.Object)}.
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testRefresh() throws Exception {
		Measure measure = new Measure();
		measure.setKeyName("measyre test");
		measure.setMeasureType(Measure.MeasureType.INTEGER);
		measure = measureDAO.persist(measure);

		Product product = new Product();
		product.setKeyName("key " + 999);
		product.setBarcode("999");
		product.setMeasure(measure);
		product.setDenominator(new BigDecimal(0));
		product = dataStore.persist(product);

		long id = product.getId();

		Product product2 = dataStore.find(id);

		assertEquals(product, product2);
		assertTrue(product.getId() == product2.getId());
		assertEquals(product.getKeyName(), product2.getKeyName());

		product2.setKeyName("key 65535");
		product2 = dataStore.refresh(product2);

		assertEquals(product, product2);
		assertTrue(product.getId() == product2.getId());
		assertEquals(product.getKeyName(), product2.getKeyName());

	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#merge(java.lang.Object)}.
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testMerge() throws Exception {
		Measure measure = new Measure();
		measure.setKeyName("measyre test");
		measure.setMeasureType(Measure.MeasureType.INTEGER);
		measure = measureDAO.persist(measure);

		Product product = new Product();
		product.setKeyName("key " + 999);
		product.setBarcode("999");
		product.setMeasure(measure);
		product.setDenominator(new BigDecimal(0));
		product = dataStore.persist(product);

		long id = product.getId();

		Product product2 = dataStore.find(id);

		assertEquals(product, product2);
		assertTrue(product.getId() == product2.getId());
		assertEquals(product.getKeyName(), product2.getKeyName());

		product2.setKeyName("key 65535");
		product2 = dataStore.merge(product2);

		product = dataStore.refresh(product);

		assertEquals(product, product2);
		assertTrue(product.getId() == product2.getId());
		assertEquals(product.getKeyName(), product2.getKeyName());
	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#remove(java.lang.Object)}.
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testRemove() throws Exception {
		Measure measure = new Measure();
		measure.setKeyName("measyre test");
		measure.setMeasureType(Measure.MeasureType.INTEGER);
		measure = measureDAO.persist(measure);

		Product product = new Product();
		product.setKeyName("key " + 999);
		product.setBarcode("999");
		product.setMeasure(measure);
		product.setDenominator(new BigDecimal(0));
		product = dataStore.persist(product);

		long id = product.getId();

		Product product2 = dataStore.find(id);

		assertEquals(product, product2);
		assertTrue(product.getId() == product2.getId());
		assertEquals(product.getKeyName(), product2.getKeyName());

		dataStore.remove(product);

		Product product3 = dataStore.find(id);
		assertTrue(product3 == null);

	}

	// -----------------------------------------------------------------------------------------------------------------

	@Test
	@Rollback(true)
	public void insertEntities() throws Exception {
		Measure measure = new Measure();
		measure.setKeyName("measyre test");
		measure.setMeasureType(Measure.MeasureType.INTEGER);
		measure = measureDAO.persist(measure);

		long index = dataStore.count();
		for (int i = 200; i < 210; i++) {
			Product product = new Product();
			product.setKeyName("key " + i);
			product.setBarcode("999" + i);
			product.setMeasure(measure);
			product.setDenominator(new BigDecimal(0));
			dataStore.persist(product);
			index++;
		}

		List<Product> list = dataStore.getAllEntities();

		assertTrue(list != null);
		assertTrue(list.size() > 0);
		assertTrue(list.size() == index);
	}

}
