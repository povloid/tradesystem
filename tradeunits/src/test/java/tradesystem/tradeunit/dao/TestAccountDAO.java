package tradesystem.tradeunit.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import pk.home.libs.combine.dao.ABaseDAO.SortOrderType;
import tradesystem.tradeunit.basetestutils.BaseTest;
import tradesystem.tradeunit.domain.Account;
import tradesystem.tradeunit.domain.Account_;

/**
 * JUnit test DAO class for entity class: Account Account - счет
 */
@RunWith(SpringJUnit4ClassRunner.class)
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
		DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class })
@Transactional
@ActiveProfiles({ "Dev" })
@ContextConfiguration(locations = { "file:./src/main/resources/applicationContext.xml" })
public class TestAccountDAO extends BaseTest {

	/**
	 * The DAO being tested, injected by Spring
	 * 
	 */
	private AccountDAO dataStore;

	/**
	 * Method to allow Spring to inject the DAO that will be tested
	 * 
	 */
	@Autowired
	public void setDataStore(AccountDAO dataStore) {
		this.dataStore = dataStore;
	}

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test checks.
	 * 
	 * @throws Exception
	 *             the exception
	 */
	@Test
	@Rollback(true)
	public void testChecks() throws Exception {
		createTestEntitys();

		Object key = null;

		{
			Account account = new Account();
			account.setDivision(division1);
			account.setOwner(warehouse1);
			account.setCountsObject(productInteger);
			account.setValue(new BigDecimal(0));
			account.setNumerator(new BigDecimal(0));

			key = dataStore.persist(account).getId();
		}

		{
			Account account = dataStore.find(key);
			assertNotNull(account);
		}

		{
			Account account = new Account();
			account.setDivision(division1);

			account.setProduct(productInteger);

			account.setValue(new BigDecimal(0));
			account.setNumerator(new BigDecimal(0));

			try {
				key = dataStore.persist(account).getId();
				assertTrue("Injected zero owners to account!", false);
			} catch (Exception ex) {

			}
		}
		
		{
			Account account = new Account();
			account.setDivision(division1);
			account.setCash(cash);
			account.setCustomer(customer);
			account.setWarehouse(warehouse1);

			account.setProduct(productInteger);

			account.setValue(new BigDecimal(0));
			account.setNumerator(new BigDecimal(0));

			try {
				key = dataStore.persist(account).getId();
				assertTrue("Injected several owners to account!", false);
			} catch (Exception ex) {

			}
		}
		
		{
			Account account = new Account();
			account.setDivision(division1);
			account.setOwner(warehouse1);

			account.setProduct(productInteger);
			account.setCurrency(currencyKZ);

			account.setValue(new BigDecimal(0));
			account.setNumerator(new BigDecimal(0));

			try {
				key = dataStore.persist(account).getId();
				assertTrue("Injected two counters to account!", false);
			} catch (Exception ex) {

			}
		}

		{
			Account account = new Account();
			account.setDivision(division1);
			account.setOwner(warehouse1);

			account.setValue(new BigDecimal(0));
			account.setNumerator(new BigDecimal(0));

			try {
				key = dataStore.persist(account).getId();
				assertTrue("Injected null as owner to account!", false);
			} catch (Exception ex) {

			}
		}

		{
			Account account = new Account();
			account.setDivision(division1);
			account.setOwner(warehouse1);

			account.setCurrency(currencyKZ);

			account.setValue(null);
			account.setNumerator(new BigDecimal(0));

			try {
				key = dataStore.persist(account).getId();
				assertTrue("Injected null as value to account!", false);
			} catch (Exception ex) {

			}
		}

		{
			Account account = new Account();
			account.setDivision(division1);
			account.setOwner(warehouse1);

			account.setCurrency(currencyKZ);

			account.setValue(new BigDecimal(0));
			account.setNumerator(null);

			try {
				key = dataStore.persist(account).getId();
				assertTrue("Injected null as numerator to account!", false);
			} catch (Exception ex) {

			}
		}

		{
			Account account = new Account();
			account.setDivision(division1);
			account.setOwner(warehouse1);

			account.setCurrency(currencyKZ);

			account.setValue(null);
			account.setNumerator(null);

			try {
				key = dataStore.persist(account).getId();
				assertTrue("Injected null as value and numerator to account!",
						false);
			} catch (Exception ex) {

			}
		}

		{
			Account account = new Account();
			account.setDivision(division1);
			account.setOwner(warehouse1);

			account.setCurrency(currencyKZ);
			account.setCredit(false);

			account.setValue(new BigDecimal(-1));
			account.setNumerator(new BigDecimal(-1));

			assertTrue(true);

		}

		{
			Account account = new Account();
			account.setDivision(division1);
			account.setOwner(warehouse1);

			account.setCurrency(currencyKZ);
			account.setCredit(true);

			account.setValue(new BigDecimal(-1));
			account.setNumerator(new BigDecimal(0));

			try {
				key = dataStore.persist(account).getId();
				assertTrue(
						"Injected -1 as value to account when acoount is credit!",
						false);
			} catch (Exception ex) {

			}
		}

		{
			Account account = new Account();
			account.setDivision(division1);
			account.setOwner(warehouse1);

			account.setCurrency(currencyKZ);
			account.setCredit(true);

			account.setValue(new BigDecimal(0));
			account.setNumerator(new BigDecimal(-1));

			try {
				key = dataStore.persist(account).getId();
				assertTrue(
						"Injected -1 as numerator to account when acoount is credit!",
						false);
			} catch (Exception ex) {

			}
		}

		{
			Account account = new Account();
			account.setDivision(division1);
			account.setOwner(warehouse1);

			account.setCurrency(currencyKZ);
			account.setCredit(true);

			account.setValue(new BigDecimal(-1));
			account.setNumerator(new BigDecimal(-1));

			try {
				key = dataStore.persist(account).getId();
				assertTrue(
						"Injected -1 as value and numerator to account when acoount is credit!",
						false);
			} catch (Exception ex) {

			}
		}

	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#getAllEntities()}.
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testGetAllEntities() throws Exception {
		createTestEntitys();

		long index = dataStore.count();
		for (int i = 0; i < 100; i++) {
			Account account = new Account();
			account.setDivision(division1);
			account.setOwner(warehouse1);
			account.setCountsObject(productInteger);
			account.setValue(new BigDecimal(0));
			account.setNumerator(new BigDecimal(0));

			dataStore.persist(account);
			index++;
		}

		List<Account> list = dataStore.getAllEntities();

		assertTrue(list != null);
		assertTrue(list.size() > 0);
		assertTrue(list.size() == index);
	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#getAllEntities(javax.persistence.metamodel.SingularAttribute, pk.home.libs.combine.dao.ABaseDAO.SortOrderType)}
	 * .
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testGetAllEntitiesSingularAttributeOfTQSortOrderType()
			throws Exception {
		createTestEntitys();

		long index = dataStore.count();
		for (int i = 0; i < 100; i++) {
			Account account = new Account();
			account.setDivision(division1);
			account.setOwner(warehouse1);
			account.setCountsObject(productInteger);
			account.setValue(new BigDecimal(0));
			account.setNumerator(new BigDecimal(0));

			dataStore.persist(account);
			index++;
		}

		List<Account> list = dataStore.getAllEntities(Account_.id,
				SortOrderType.ASC);

		assertTrue(list != null);
		assertTrue(list.size() > 0);
		assertTrue(list.size() == index);

		long lastId = 0;
		for (Account account : list) {
			assertTrue(lastId < account.getId());
			lastId = account.getId();
		}
	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#getAllEntities(int, int)}.
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testGetAllEntitiesIntInt() throws Exception {
		createTestEntitys();

		// int index = 0;
		for (int i = 0; i < 100; i++) {
			Account account = new Account();
			account.setDivision(division1);
			account.setOwner(warehouse1);
			account.setCountsObject(productInteger);
			account.setValue(new BigDecimal(0));
			account.setNumerator(new BigDecimal(0));

			dataStore.persist(account);
			// index++;
		}

		List<Account> list = dataStore.getAllEntities(10, 10);

		assertTrue(list != null);
		assertTrue(list.size() > 0);
		assertTrue(list.size() == 10);
	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#getAllEntities(int, int, javax.persistence.metamodel.SingularAttribute, pk.home.libs.combine.dao.ABaseDAO.SortOrderType)}
	 * .
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testGetAllEntitiesIntIntSingularAttributeOfTQSortOrderType()
			throws Exception {
		createTestEntitys();

		// long index = dataStore.count();
		for (int i = 0; i < 100; i++) {
			Account account = new Account();
			account.setDivision(division1);
			account.setOwner(warehouse1);
			account.setCountsObject(productInteger);
			account.setValue(new BigDecimal(0));
			account.setNumerator(new BigDecimal(0));

			dataStore.persist(account);
			// index++;
		}

		List<Account> list = dataStore.getAllEntities(10, 10, Account_.id,
				SortOrderType.ASC);

		assertTrue(list != null);
		assertTrue(list.size() > 0);
		assertTrue(list.size() == 10);

		long lastId = 0;
		for (Account account : list) {
			assertTrue(lastId < account.getId());
			lastId = account.getId();
		}
	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#getAllEntities(boolean, int, int, javax.persistence.metamodel.SingularAttribute, pk.home.libs.combine.dao.ABaseDAO.SortOrderType)}
	 * .
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testGetAllEntitiesBooleanIntIntSingularAttributeOfTQSortOrderType()
			throws Exception {
		createTestEntitys();

		long index = dataStore.count();
		for (int i = 0; i < 100; i++) {
			Account account = new Account();
			account.setDivision(division1);
			account.setOwner(warehouse1);
			account.setCountsObject(productInteger);
			account.setValue(new BigDecimal(0));
			account.setNumerator(new BigDecimal(0));

			dataStore.persist(account);
			index++;
		}

		// all - FALSE
		List<Account> list = dataStore.getAllEntities(false, 10, 10,
				Account_.id, SortOrderType.ASC);

		assertTrue(list != null);
		assertTrue(list.size() > 0);
		assertTrue(list.size() == 10);

		long lastId = 0;
		for (Account account : list) {
			assertTrue(lastId < account.getId());
			lastId = account.getId();
		}

		// all - TRUE
		list = dataStore.getAllEntities(true, 10, 10, Account_.id,
				SortOrderType.ASC);

		assertTrue(list != null);
		assertTrue(list.size() > 0);
		assertTrue(list.size() == index);

		lastId = 0;
		for (Account account : list) {
			assertTrue(lastId < account.getId());
			lastId = account.getId();
		}
	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#find(java.lang.Object)}.
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testFind() throws Exception {
		createTestEntitys();

		Account account = new Account();
		account.setDivision(division1);
		account.setOwner(warehouse1);
		account.setCountsObject(productInteger);
		account.setValue(new BigDecimal(0));
		account.setNumerator(new BigDecimal(0));

		account = dataStore.persist(account);

		long id = account.getId();

		Account account2 = dataStore.find(id);

		assertEquals(account, account2);
		assertTrue(account.getId() == account2.getId());

	}

	/**
	 * Test method for {@link pk.home.libs.combine.dao.ABaseDAO#count()}.
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testCount() throws Exception {
		createTestEntitys();

		long index = dataStore.count();
		for (int i = 0; i < 100; i++) {
			Account account = new Account();
			account.setDivision(division1);
			account.setOwner(warehouse1);
			account.setCountsObject(productInteger);
			account.setValue(new BigDecimal(0));
			account.setNumerator(new BigDecimal(0));

			dataStore.persist(account);
			index++;
		}

		long count = dataStore.count();

		assertTrue(count == index);
	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#persist(java.lang.Object)}.
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testPersist() throws Exception {
		createTestEntitys();

		Account account = new Account();
		account.setDivision(division1);
		account.setOwner(warehouse1);
		account.setCountsObject(productInteger);
		account.setValue(new BigDecimal(0));
		account.setNumerator(new BigDecimal(0));

		account = dataStore.persist(account);

		long id = account.getId();

		Account account2 = dataStore.find(id);

		assertEquals(account, account2);
		assertTrue(account.getId() == account2.getId());
	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#refresh(java.lang.Object)}.
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testRefresh() throws Exception {
		createTestEntitys();

		Account account = new Account();
		account.setDivision(division1);
		account.setOwner(warehouse1);
		account.setCountsObject(productInteger);
		account.setValue(new BigDecimal(0));
		account.setNumerator(new BigDecimal(0));

		account = dataStore.persist(account);

		long id = account.getId();

		Account account2 = dataStore.find(id);

		assertEquals(account, account2);
		assertTrue(account.getId() == account2.getId());

		account2 = dataStore.refresh(account2);

		assertEquals(account, account2);
		assertTrue(account.getId() == account2.getId());

	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#merge(java.lang.Object)}.
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testMerge() throws Exception {
		createTestEntitys();

		Account account = new Account();
		account.setDivision(division1);
		account.setOwner(warehouse1);
		account.setCountsObject(productInteger);
		account.setValue(new BigDecimal(0));
		account.setNumerator(new BigDecimal(0));

		account = dataStore.persist(account);

		long id = account.getId();

		Account account2 = dataStore.find(id);

		assertEquals(account, account2);
		assertTrue(account.getId() == account2.getId());

		account2 = dataStore.merge(account2);

		account = dataStore.refresh(account);

		assertEquals(account, account2);
		assertTrue(account.getId() == account2.getId());
	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#remove(java.lang.Object)}.
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testRemove() throws Exception {
		createTestEntitys();

		Account account = new Account();

		account.setDivision(division1);
		account.setOwner(warehouse1);
		account.setCountsObject(productInteger);
		account.setValue(new BigDecimal(0));
		account.setNumerator(new BigDecimal(0));
		account = dataStore.persist(account);

		long id = account.getId();

		Account account2 = dataStore.find(id);

		assertEquals(account, account2);
		assertTrue(account.getId() == account2.getId());

		dataStore.remove(account);

		Account account3 = dataStore.find(id);
		assertTrue(account3 == null);

	}

	// -----------------------------------------------------------------------------------------------------------------

	@Test
	@Rollback(true)
	public void insertEntities() throws Exception {
		createTestEntitys();

		long index = dataStore.count();
		for (int i = 200; i < 210; i++) {
			Account account = new Account();
			account.setDivision(division1);
			account.setOwner(warehouse1);
			account.setCountsObject(productInteger);
			account.setValue(new BigDecimal(0));
			account.setNumerator(new BigDecimal(0));

			dataStore.persist(account);
			index++;
		}

		List<Account> list = dataStore.getAllEntities();

		assertTrue(list != null);
		assertTrue(list.size() > 0);
		assertTrue(list.size() == index);
	}

}
