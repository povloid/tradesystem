package tradesystem.tradeunit.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Date;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import pk.home.libs.combine.dao.ABaseDAO.SortOrderType;
import tradesystem.tradeunit.basetestutils.BaseTest;
import tradesystem.tradeunit.domain.Order;
import tradesystem.tradeunit.domain.OrderType;
import tradesystem.tradeunit.domain.Order_;

/**
 * JUnit test DAO class for entity class: Order Order - ордер
 */
@RunWith(SpringJUnit4ClassRunner.class)
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
		DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class })
@Transactional
@ActiveProfiles({ "Dev" })
@ContextConfiguration(locations = { "file:./src/main/resources/applicationContext.xml" })
public class TestOrderDAO extends BaseTest{

	/**
	 * The DAO being tested, injected by Spring
	 * 
	 */
	private OrderDAO dataStore;

	/**
	 * Method to allow Spring to inject the DAO that will be tested
	 * 
	 */
	@Autowired
	public void setDataStore(OrderDAO dataStore) {
		this.dataStore = dataStore;
	}

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#getAllEntities()}.
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testGetAllEntities() throws Exception {
		createTestEntitys();
		

		long index = dataStore.count();
		for (int i = 0; i < 100; i++) {
			Order order = new Order();
			order.setOrderType(OrderType.PRODUCT_PLUS);
			order.setOpTime(new Date());
			order.setDivision(division1);
			order.setDescription("Order description");
			order.setUserAccount(userAccount);
			
			dataStore.persist(order);
			index++;
		}

		List<Order> list = dataStore.getAllEntities();

		assertTrue(list != null);
		assertTrue(list.size() > 0);
		assertTrue(list.size() == index);
	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#getAllEntities(javax.persistence.metamodel.SingularAttribute, pk.home.libs.combine.dao.ABaseDAO.SortOrderType)}
	 * .
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testGetAllEntitiesSingularAttributeOfTQSortOrderType()
			throws Exception {
		createTestEntitys();
		
		long index = dataStore.count();
		for (int i = 0; i < 100; i++) {
			Order order = new Order();
			order.setOrderType(OrderType.PRODUCT_PLUS);
			order.setOpTime(new Date());
			order.setDivision(division1);
			order.setDescription("Order description");
			order.setUserAccount(userAccount);
			dataStore.persist(order);
			index++;
		}

		List<Order> list = dataStore.getAllEntities(Order_.id,
				SortOrderType.ASC);

		assertTrue(list != null);
		assertTrue(list.size() > 0);
		assertTrue(list.size() == index);

		long lastId = 0;
		for (Order order : list) {
			assertTrue(lastId < order.getId());
			lastId = order.getId();
		}
	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#getAllEntities(int, int)}.
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testGetAllEntitiesIntInt() throws Exception {
		createTestEntitys();

		// int index = 0;
		for (int i = 0; i < 100; i++) {
			Order order = new Order();
			order.setOrderType(OrderType.PRODUCT_PLUS);
			order.setOpTime(new Date());
			order.setDivision(division1);
			order.setDescription("Order description");
			order.setUserAccount(userAccount);
			dataStore.persist(order);
			// index++;
		}

		List<Order> list = dataStore.getAllEntities(10, 10);

		assertTrue(list != null);
		assertTrue(list.size() > 0);
		assertTrue(list.size() == 10);
	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#getAllEntities(int, int, javax.persistence.metamodel.SingularAttribute, pk.home.libs.combine.dao.ABaseDAO.SortOrderType)}
	 * .
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testGetAllEntitiesIntIntSingularAttributeOfTQSortOrderType()
			throws Exception {
		createTestEntitys();
		
		// long index = dataStore.count();
		for (int i = 0; i < 100; i++) {
			Order order = new Order();
			order.setOrderType(OrderType.PRODUCT_PLUS);
			order.setOpTime(new Date());
			order.setDivision(division1);
			order.setDescription("Order description");
			order.setUserAccount(userAccount);
			dataStore.persist(order);
			// index++;
		}

		List<Order> list = dataStore.getAllEntities(10, 10, Order_.id,
				SortOrderType.ASC);

		assertTrue(list != null);
		assertTrue(list.size() > 0);
		assertTrue(list.size() == 10);

		long lastId = 0;
		for (Order order : list) {
			assertTrue(lastId < order.getId());
			lastId = order.getId();
		}
	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#getAllEntities(boolean, int, int, javax.persistence.metamodel.SingularAttribute, pk.home.libs.combine.dao.ABaseDAO.SortOrderType)}
	 * .
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testGetAllEntitiesBooleanIntIntSingularAttributeOfTQSortOrderType()
			throws Exception {
		createTestEntitys();
		
		long index = dataStore.count();
		for (int i = 0; i < 100; i++) {
			Order order = new Order();
			order.setOrderType(OrderType.PRODUCT_PLUS);
			order.setOpTime(new Date());
			order.setDivision(division1);
			order.setDescription("Order description");
			order.setUserAccount(userAccount);
			dataStore.persist(order);
			index++;
		}

		// all - FALSE
		List<Order> list = dataStore.getAllEntities(false, 10, 10, Order_.id,
				SortOrderType.ASC);

		assertTrue(list != null);
		assertTrue(list.size() > 0);
		assertTrue(list.size() == 10);

		long lastId = 0;
		for (Order order : list) {
			assertTrue(lastId < order.getId());
			lastId = order.getId();
		}

		// all - TRUE
		list = dataStore.getAllEntities(true, 10, 10, Order_.id,
				SortOrderType.ASC);

		assertTrue(list != null);
		assertTrue(list.size() > 0);
		assertTrue(list.size() == index);

		lastId = 0;
		for (Order order : list) {
			assertTrue(lastId < order.getId());
			lastId = order.getId();
		}
	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#find(java.lang.Object)}.
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testFind() throws Exception {
		createTestEntitys();

		Order order = new Order();
		order.setOrderType(OrderType.PRODUCT_PLUS);
		order.setOpTime(new Date());
		order.setDivision(division1);
		order.setDescription("Order description");
		order.setUserAccount(userAccount);
		order = dataStore.persist(order);

		long id = order.getId();

		Order order2 = dataStore.find(id);

		assertEquals(order, order2);
		assertTrue(order.getId() == order2.getId());
		assertEquals(order.getOrderType(), order2.getOrderType());

	}

	/**
	 * Test method for {@link pk.home.libs.combine.dao.ABaseDAO#count()}.
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testCount() throws Exception {
		createTestEntitys();
		
		long index = dataStore.count();
		for (int i = 0; i < 100; i++) {
			Order order = new Order();
			order.setOrderType(OrderType.PRODUCT_PLUS);
			order.setOpTime(new Date());
			order.setDivision(division1);
			order.setDescription("Order description");
			order.setUserAccount(userAccount);
			dataStore.persist(order);
			index++;
		}

		long count = dataStore.count();

		assertTrue(count == index);
	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#persist(java.lang.Object)}.
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testPersist() throws Exception {
		createTestEntitys();
		
		Order order = new Order();
		order.setOrderType(OrderType.PRODUCT_PLUS);
		order.setOpTime(new Date());
		order.setDivision(division1);
		order.setDescription("Order description");
		order.setUserAccount(userAccount);
		order = dataStore.persist(order);

		long id = order.getId();

		Order order2 = dataStore.find(id);

		assertEquals(order, order2);
		assertTrue(order.getId() == order2.getId());
		assertEquals(order.getOrderType(), order2.getOrderType());
	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#refresh(java.lang.Object)}.
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testRefresh() throws Exception {
		createTestEntitys();
		
		Order order = new Order();
		order.setOrderType(OrderType.PRODUCT_PLUS);
		order.setOpTime(new Date());
		order.setDivision(division1);
		order.setDescription("Order description");
		order.setUserAccount(userAccount);
		order = dataStore.persist(order);

		long id = order.getId();

		Order order2 = dataStore.find(id);

		assertEquals(order, order2);
		assertTrue(order.getId() == order2.getId());
		assertEquals(order.getOrderType(), order2.getOrderType());

		order2.setOrderType(OrderType.PRODUCT_PLUS);
		order2.setOpTime(new Date());
		order2.setDivision(division1);
		order2.setDescription("Order description");
		order2.setUserAccount(userAccount);
		order2 = dataStore.refresh(order2);

		assertEquals(order, order2);
		assertTrue(order.getId() == order2.getId());
		assertEquals(order.getOrderType(), order2.getOrderType());

	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#merge(java.lang.Object)}.
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testMerge() throws Exception {
		createTestEntitys();
		
		Order order = new Order();
		order.setOrderType(OrderType.PRODUCT_PLUS);
		order.setOpTime(new Date());
		order.setDivision(division1);
		order.setDescription("Order description");
		order.setUserAccount(userAccount);
		order = dataStore.persist(order);

		long id = order.getId();

		Order order2 = dataStore.find(id);

		assertEquals(order, order2);
		assertTrue(order.getId() == order2.getId());
		assertEquals(order.getOrderType(), order2.getOrderType());

		order2.setOrderType(OrderType.PRODUCT_PLUS);
		order2.setOpTime(new Date());
		order2.setDivision(division1);
		order2.setDescription("Order description");
		order2.setUserAccount(userAccount);
		order2 = dataStore.merge(order2);

		order = dataStore.refresh(order);

		assertEquals(order, order2);
		assertTrue(order.getId() == order2.getId());
		assertEquals(order.getOrderType(), order2.getOrderType());
	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#remove(java.lang.Object)}.
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testRemove() throws Exception {
		createTestEntitys();
		
		Order order = new Order();
		order.setOrderType(OrderType.PRODUCT_PLUS);
		order.setOpTime(new Date());
		order.setDivision(division1);
		order.setDescription("Order description");
		order.setUserAccount(userAccount);
		order = dataStore.persist(order);

		long id = order.getId();

		Order order2 = dataStore.find(id);

		assertEquals(order, order2);
		assertTrue(order.getId() == order2.getId());
		assertEquals(order.getOrderType(), order2.getOrderType());

		dataStore.remove(order);

		Order order3 = dataStore.find(id);
		assertTrue(order3 == null);

	}

	// -----------------------------------------------------------------------------------------------------------------

	@Test
	@Rollback(true)
	public void insertEntities() throws Exception {
		createTestEntitys();

		long index = dataStore.count();
		for (int i = 200; i < 210; i++) {
			Order order = new Order();
			order.setOrderType(OrderType.PRODUCT_PLUS);
			order.setOpTime(new Date());
			order.setDivision(division1);
			order.setDescription("Order description");
			order.setUserAccount(userAccount);
			dataStore.persist(order);
			index++;
		}

		List<Order> list = dataStore.getAllEntities();

		assertTrue(list != null);
		assertTrue(list.size() > 0);
		assertTrue(list.size() == index);
	}

}
