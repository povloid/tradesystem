package tradesystem.tradeunit.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import pk.home.libs.combine.dao.ABaseDAO.SortOrderType;
import tradesystem.tradeunit.basetestutils.BaseTest;
import tradesystem.tradeunit.domain.Price;
import tradesystem.tradeunit.domain.Price_;
import tradesystem.tradeunit.domain.Product;

/**
 * JUnit test DAO class for entity class: Price
 * Price - цена.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
		DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class })
@Transactional
@ActiveProfiles({"Dev"})
@ContextConfiguration(locations = { "file:./src/main/resources/applicationContext.xml" })
public class TestPriceDAO extends BaseTest{

	/** The DAO being tested, injected by Spring. */
	private PriceDAO dataStore;

	/**
	 * Method to allow Spring to inject the DAO that will be tested.
	 *
	 * @param dataStore the new data store
	 */
	@Autowired
	public void setDataStore(PriceDAO dataStore) {
		this.dataStore = dataStore;
	}

	/**
	 * Sets the up before class.
	 *
	 * @throws Exception the exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * Tear down after class.
	 *
	 * @throws Exception the exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * Sets the up.
	 *
	 * @throws Exception the exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * Tear down.
	 *
	 * @throws Exception the exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for.
	 *
	 * @throws Exception the exception
	 * {@link pk.home.libs.combine.dao.ABaseDAO#getAllEntities()}.
	 */
	@Test
	@Rollback(true)
	public void testGetAllEntities() throws Exception {
		createTestEntitys();

		long index = dataStore.count();
		{
			Product p1 = new Product();
			p1.setKeyName("TEST PRICE 1");
			p1.setBarcode("TEST PRICE 1");
			p1.setMeasure(measureInteger);
			
			p1 = productService.persist(p1);
			
			Price price = new Price();
			price.setDivision(division1);
			price.setProduct(p1);
			price = dataStore.persist(price);
			index++;
		}

		List<Price> list = dataStore.getAllEntities();

		assertTrue(list != null);
		assertTrue(list.size() > 0);
		assertTrue(list.size() == index);
	}

	/**
	 * Test method for.
	 *
	 * @throws Exception the exception
	 * {@link pk.home.libs.combine.dao.ABaseDAO#getAllEntities(javax.persistence.metamodel.SingularAttribute, pk.home.libs.combine.dao.ABaseDAO.SortOrderType)}
	 * .
	 */
	@Test
	@Rollback(true)
	public void testGetAllEntitiesSingularAttributeOfTQSortOrderType()
			throws Exception {
		createTestEntitys();
		
		long index = dataStore.count();
		{
			Product p1 = new Product();
			p1.setKeyName("TEST PRICE 1");
			p1.setBarcode("TEST PRICE 1");
			p1.setMeasure(measureInteger);
			
			p1 = productService.persist(p1);
			
			Price price = new Price();
			price.setDivision(division1);
			price.setProduct(p1);
			price = dataStore.persist(price);
			index++;
		}

		List<Price> list = dataStore.getAllEntities(Price_.id, SortOrderType.ASC);

		assertTrue(list != null);
		assertTrue(list.size() > 0);
		assertTrue(list.size() == index);

		long lastId = 0;
		for (Price price : list) {
			assertTrue(lastId < price.getId());
			lastId = price.getId();
		}
	}

	/**
	 * Test method for.
	 *
	 * @throws Exception the exception
	 * {@link pk.home.libs.combine.dao.ABaseDAO#getAllEntities(int, int)}.
	 */
	@Test
	@Rollback(true)
	public void testGetAllEntitiesIntInt() throws Exception {
		createTestEntitys();

		// int index = 0;
		{
			Product p1 = new Product();
			p1.setKeyName("TEST PRICE 1");
			p1.setBarcode("TEST PRICE 1");
			p1.setMeasure(measureInteger);
			
			p1 = productService.persist(p1);
			
			Price price = new Price();
			price.setDivision(division1);
			price.setProduct(p1);
			price = dataStore.persist(price);
			// index++;
		}

		List<Price> list = dataStore.getAllEntities(0, 1);

		assertTrue(list != null);
		assertTrue(list.size() > 0);
		assertTrue(list.size() == 1);
	}

	/**
	 * Test method for.
	 *
	 * @throws Exception the exception
	 * {@link pk.home.libs.combine.dao.ABaseDAO#getAllEntities(int, int, javax.persistence.metamodel.SingularAttribute, pk.home.libs.combine.dao.ABaseDAO.SortOrderType)}
	 * .
	 */
	@Test
	@Rollback(true)
	public void testGetAllEntitiesIntIntSingularAttributeOfTQSortOrderType()
			throws Exception {
		createTestEntitys();
		
		// long index = dataStore.count();
		{
			Product p1 = new Product();
			p1.setKeyName("TEST PRICE 1");
			p1.setBarcode("TEST PRICE 1");
			p1.setMeasure(measureInteger);
			
			p1 = productService.persist(p1);
			
			Price price = new Price();
			price.setDivision(division1);
			price.setProduct(p1);
			price = dataStore.persist(price);
			// index++;
		}

		List<Price> list = dataStore.getAllEntities(0, 1, Price_.id,
				SortOrderType.ASC);

		assertTrue(list != null);
		assertTrue(list.size() > 0);
		assertTrue(list.size() == 1);

		long lastId = 0;
		for (Price price : list) {
			assertTrue(lastId < price.getId());
			lastId = price.getId();
		}
	}

	/**
	 * Test method for.
	 *
	 * @throws Exception the exception
	 * {@link pk.home.libs.combine.dao.ABaseDAO#getAllEntities(boolean, int, int, javax.persistence.metamodel.SingularAttribute, pk.home.libs.combine.dao.ABaseDAO.SortOrderType)}
	 * .
	 */
	@Test
	@Rollback(true)
	public void testGetAllEntitiesBooleanIntIntSingularAttributeOfTQSortOrderType()
			throws Exception {
		createTestEntitys();
		
		long index = dataStore.count();
		{
			Product p1 = new Product();
			p1.setKeyName("TEST PRICE 1");
			p1.setBarcode("TEST PRICE 1");
			p1.setMeasure(measureInteger);
			
			p1 = productService.persist(p1);
			
			Price price = new Price();
			price.setDivision(division1);
			price.setProduct(p1);
			price = dataStore.persist(price);
			index++;
		}

		// all - FALSE
		List<Price> list = dataStore.getAllEntities(false, 0, 1, Price_.id,
				SortOrderType.ASC);

		assertTrue(list != null);
		assertTrue(list.size() > 0);
		assertTrue(list.size() == 1);

		long lastId = 0;
		for (Price price : list) {
			assertTrue(lastId < price.getId());
			lastId = price.getId();
		}

		// all - TRUE
		list = dataStore.getAllEntities(true, 0, 1, Price_.id,
				SortOrderType.ASC);

		assertTrue(list != null);
		assertTrue(list.size() > 0);
		assertTrue(list.size() == index);

		lastId = 0;
		for (Price price : list) {
			assertTrue(lastId < price.getId());
			lastId = price.getId();
		}
	}

	/**
	 * Test method for.
	 *
	 * @throws Exception the exception
	 * {@link pk.home.libs.combine.dao.ABaseDAO#find(java.lang.Object)}.
	 */
	@Test
	@Rollback(true)
	public void testFind() throws Exception {
		createTestEntitys();

		Product p1 = new Product();
		p1.setKeyName("TEST PRICE 1");
		p1.setBarcode("TEST PRICE 1");
		p1.setMeasure(measureInteger);
		
		p1 = productService.persist(p1);
		
		Price price = new Price();
		price.setDivision(division1);
		price.setProduct(p1);
		price = dataStore.persist(price);

		long id = price.getId();

		Price price2 = dataStore.find(id);

		assertEquals(price, price2);
		assertTrue(price.getId() == price2.getId());
		assertEquals(price, price2);

	}

	/**
	 * Test method for {@link pk.home.libs.combine.dao.ABaseDAO#count()}.
	 *
	 * @throws Exception the exception
	 */
	@Test
	@Rollback(true)
	public void testCount() throws Exception {
		createTestEntitys();
		
		long index = dataStore.count();

		Product p1 = new Product();
		p1.setKeyName("TEST PRICE 1");
		p1.setBarcode("TEST PRICE 1");
		p1.setMeasure(measureInteger);
		
		p1 = productService.persist(p1);
		
		Price price = new Price();
		price.setDivision(division1);
		price.setProduct(p1);
		price = dataStore.persist(price);
		
		dataStore.persist(price);
		index++;


		long count = dataStore.count();

		assertTrue(count == index);
	}

	/**
	 * Test method for.
	 *
	 * @throws Exception the exception
	 * {@link pk.home.libs.combine.dao.ABaseDAO#persist(java.lang.Object)}.
	 */
	@Test
	@Rollback(true)
	public void testPersist() throws Exception {
		createTestEntitys();
		
		Product p1 = new Product();
		p1.setKeyName("TEST PRICE 1");
		p1.setBarcode("TEST PRICE 1");
		p1.setMeasure(measureInteger);
		
		p1 = productService.persist(p1);
		
		Price price = new Price();
		price.setDivision(division1);
		price.setProduct(p1);
		price = dataStore.persist(price);

		long id = price.getId();

		Price price2 = dataStore.find(id);

		assertEquals(price, price2);
		assertTrue(price.getId() == price2.getId());
		assertEquals(price, price2);
	}

	/**
	 * Test method for.
	 *
	 * @throws Exception the exception
	 * {@link pk.home.libs.combine.dao.ABaseDAO#refresh(java.lang.Object)}.
	 */
	@Test
	@Rollback(true)
	public void testRefresh() throws Exception {
		createTestEntitys();
		
		Product p1 = new Product();
		p1.setKeyName("TEST PRICE 1");
		p1.setBarcode("TEST PRICE 1");
		p1.setMeasure(measureInteger);
		
		p1 = productService.persist(p1);
		
		Price price = new Price();
		price.setDivision(division1);
		price.setProduct(p1);
		price = dataStore.persist(price);

		long id = price.getId();

		Price price2 = dataStore.find(id);

		assertEquals(price, price2);
		assertTrue(price.getId() == price2.getId());
		assertEquals(price, price2);

		price.setDivision(division1);
		price.setProduct(productFloat);
		price2 = dataStore.refresh(price2);

		assertEquals(price, price2);
		assertTrue(price.getId() == price2.getId());
		assertEquals(price, price2);

	}

	/**
	 * Test method for.
	 *
	 * @throws Exception the exception
	 * {@link pk.home.libs.combine.dao.ABaseDAO#merge(java.lang.Object)}.
	 */
	@Test
	@Rollback(true)
	public void testMerge() throws Exception {
		createTestEntitys();
		
		Product p1 = new Product();
		p1.setKeyName("TEST PRICE 1");
		p1.setBarcode("TEST PRICE 1");
		p1.setMeasure(measureInteger);
		
		p1 = productService.persist(p1);
		
		Price price = new Price();
		price.setDivision(division1);
		price.setProduct(p1);
		price = dataStore.persist(price);

		long id = price.getId();

		Price price2 = dataStore.find(id);

		assertEquals(price, price2);
		assertTrue(price.getId() == price2.getId());
		assertEquals(price, price2);

		price.setDivision(division1);
		price.setProduct(productFloat);
		price2 = dataStore.merge(price2);

		price = dataStore.refresh(price);

		assertEquals(price, price2);
		assertTrue(price.getId() == price2.getId());
		assertEquals(price, price2);
		assertEquals(price.getProduct(), price2.getProduct());
	}

	/**
	 * Test method for.
	 *
	 * @throws Exception the exception
	 * {@link pk.home.libs.combine.dao.ABaseDAO#remove(java.lang.Object)}.
	 */
	@Test
	@Rollback(true)
	public void testRemove() throws Exception {
		createTestEntitys();
		
		Product p1 = new Product();
		p1.setKeyName("TEST PRICE 1");
		p1.setBarcode("TEST PRICE 1");
		p1.setMeasure(measureInteger);
		
		p1 = productService.persist(p1);
		
		Price price = new Price();
		price.setDivision(division1);
		price.setProduct(p1);
		price = dataStore.persist(price);

		long id = price.getId();

		Price price2 = dataStore.find(id);

		assertEquals(price, price2);
		assertTrue(price.getId() == price2.getId());
		assertEquals(price, price2);

		dataStore.remove(price);

		Price price3 = dataStore.find(id);
		assertTrue(price3 == null);

	}
	
	
	
	// -----------------------------------------------------------------------------------------------------------------
	
	/**
	 * Insert entities.
	 *
	 * @throws Exception the exception
	 */
	@Test
	@Rollback(true)
	public void insertEntities() throws Exception {
		createTestEntitys();

		long index = dataStore.count();
		
		Product p1 = new Product();
		p1.setKeyName("TEST PRICE 1");
		p1.setBarcode("TEST PRICE 1");
		p1.setMeasure(measureInteger);
		
		p1 = productService.persist(p1);
		
		Price price = new Price();
		price.setDivision(division1);
		price.setProduct(p1);
		price = dataStore.persist(price);
		index++;
		

		List<Price> list = dataStore.getAllEntities();

		assertTrue(list != null);
		assertTrue(list.size() > 0);
		assertTrue(list.size() == index);
	}
	

}
