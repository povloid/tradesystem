package tradesystem.tradeunit.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import pk.home.libs.combine.dao.ABaseDAO.SortOrderType;
import tradesystem.tradeunit.domain.Measure;
import tradesystem.tradeunit.domain.Measure_;

/**
 * JUnit test DAO class for entity class: Measure
 * Measure - мера
 */
@RunWith(SpringJUnit4ClassRunner.class)
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
		DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class })
@Transactional
@ActiveProfiles({"Dev"})
@ContextConfiguration(locations = { "file:./src/main/resources/applicationContext.xml" })
public class TestMeasureDAO {

	/**
	 * The DAO being tested, injected by Spring
	 * 
	 */
	private MeasureDAO dataStore;

	/**
	 * Method to allow Spring to inject the DAO that will be tested
	 * 
	 */
	@Autowired
	public void setDataStore(MeasureDAO dataStore) {
		this.dataStore = dataStore;
	}

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#getAllEntities()}.
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testGetAllEntities() throws Exception {

		long index = dataStore.count();
		for (int i = 0; i < 100; i++) {
			Measure measure = new Measure();
			measure.setKeyName("key " + i);
			measure.setMeasureType(Measure.MeasureType.INTEGER);
			dataStore.persist(measure);
			index++;
		}

		List<Measure> list = dataStore.getAllEntities();

		assertTrue(list != null);
		assertTrue(list.size() > 0);
		assertTrue(list.size() == index);
	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#getAllEntities(javax.persistence.metamodel.SingularAttribute, pk.home.libs.combine.dao.ABaseDAO.SortOrderType)}
	 * .
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testGetAllEntitiesSingularAttributeOfTQSortOrderType()
			throws Exception {
		long index = dataStore.count();
		for (int i = 0; i < 100; i++) {
			Measure measure = new Measure();
			measure.setKeyName("key " + i);
			measure.setMeasureType(Measure.MeasureType.INTEGER);
			dataStore.persist(measure);
			index++;
		}

		List<Measure> list = dataStore.getAllEntities(Measure_.id, SortOrderType.ASC);

		assertTrue(list != null);
		assertTrue(list.size() > 0);
		assertTrue(list.size() == index);

		long lastId = 0;
		for (Measure measure : list) {
			assertTrue(lastId < measure.getId());
			lastId = measure.getId();
		}
	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#getAllEntities(int, int)}.
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testGetAllEntitiesIntInt() throws Exception {

		// int index = 0;
		for (int i = 0; i < 100; i++) {
			Measure measure = new Measure();
			measure.setKeyName("key " + i);
			measure.setMeasureType(Measure.MeasureType.INTEGER);
			dataStore.persist(measure);
			// index++;
		}

		List<Measure> list = dataStore.getAllEntities(10, 10);

		assertTrue(list != null);
		assertTrue(list.size() > 0);
		assertTrue(list.size() == 10);
	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#getAllEntities(int, int, javax.persistence.metamodel.SingularAttribute, pk.home.libs.combine.dao.ABaseDAO.SortOrderType)}
	 * .
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testGetAllEntitiesIntIntSingularAttributeOfTQSortOrderType()
			throws Exception {
		// long index = dataStore.count();
		for (int i = 0; i < 100; i++) {
			Measure measure = new Measure();
			measure.setKeyName("key " + i);
			measure.setMeasureType(Measure.MeasureType.INTEGER);
			dataStore.persist(measure);
			// index++;
		}

		List<Measure> list = dataStore.getAllEntities(10, 10, Measure_.id,
				SortOrderType.ASC);

		assertTrue(list != null);
		assertTrue(list.size() > 0);
		assertTrue(list.size() == 10);

		long lastId = 0;
		for (Measure measure : list) {
			assertTrue(lastId < measure.getId());
			lastId = measure.getId();
		}
	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#getAllEntities(boolean, int, int, javax.persistence.metamodel.SingularAttribute, pk.home.libs.combine.dao.ABaseDAO.SortOrderType)}
	 * .
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testGetAllEntitiesBooleanIntIntSingularAttributeOfTQSortOrderType()
			throws Exception {
		long index = dataStore.count();
		for (int i = 0; i < 100; i++) {
			Measure measure = new Measure();
			measure.setKeyName("key " + i);
			measure.setMeasureType(Measure.MeasureType.INTEGER);
			dataStore.persist(measure);
			index++;
		}

		// all - FALSE
		List<Measure> list = dataStore.getAllEntities(false, 10, 10, Measure_.id,
				SortOrderType.ASC);

		assertTrue(list != null);
		assertTrue(list.size() > 0);
		assertTrue(list.size() == 10);

		long lastId = 0;
		for (Measure measure : list) {
			assertTrue(lastId < measure.getId());
			lastId = measure.getId();
		}

		// all - TRUE
		list = dataStore.getAllEntities(true, 10, 10, Measure_.id,
				SortOrderType.ASC);

		assertTrue(list != null);
		assertTrue(list.size() > 0);
		assertTrue(list.size() == index);

		lastId = 0;
		for (Measure measure : list) {
			assertTrue(lastId < measure.getId());
			lastId = measure.getId();
		}
	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#find(java.lang.Object)}.
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testFind() throws Exception {

		Measure measure = new Measure();
		measure.setKeyName("key " + 999);
			measure.setMeasureType(Measure.MeasureType.INTEGER);
		measure = dataStore.persist(measure);

		long id = measure.getId();

		Measure measure2 = dataStore.find(id);

		assertEquals(measure, measure2);
		assertTrue(measure.getId() == measure2.getId());
		assertEquals(measure.getKeyName(), measure2.getKeyName());

	}

	/**
	 * Test method for {@link pk.home.libs.combine.dao.ABaseDAO#count()}.
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testCount() throws Exception {
		long index = dataStore.count();
		for (int i = 0; i < 100; i++) {
			Measure measure = new Measure();
			measure.setKeyName("key " + i);
			measure.setMeasureType(Measure.MeasureType.INTEGER);
			dataStore.persist(measure);
			index++;
		}

		long count = dataStore.count();

		assertTrue(count == index);
	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#persist(java.lang.Object)}.
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testPersist() throws Exception {
		Measure measure = new Measure();
		measure.setKeyName("key " + 999);
			measure.setMeasureType(Measure.MeasureType.INTEGER);
		measure = dataStore.persist(measure);

		long id = measure.getId();

		Measure measure2 = dataStore.find(id);

		assertEquals(measure, measure2);
		assertTrue(measure.getId() == measure2.getId());
		assertEquals(measure.getKeyName(), measure2.getKeyName());
	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#refresh(java.lang.Object)}.
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testRefresh() throws Exception {
		Measure measure = new Measure();
		measure.setKeyName("key " + 999);
			measure.setMeasureType(Measure.MeasureType.INTEGER);
		measure = dataStore.persist(measure);

		long id = measure.getId();

		Measure measure2 = dataStore.find(id);

		assertEquals(measure, measure2);
		assertTrue(measure.getId() == measure2.getId());
		assertEquals(measure.getKeyName(), measure2.getKeyName());

		measure2.setKeyName("key 65535");
		measure2 = dataStore.refresh(measure2);

		assertEquals(measure, measure2);
		assertTrue(measure.getId() == measure2.getId());
		assertEquals(measure.getKeyName(), measure2.getKeyName());

	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#merge(java.lang.Object)}.
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testMerge() throws Exception {
		Measure measure = new Measure();
		measure.setKeyName("key " + 999);
			measure.setMeasureType(Measure.MeasureType.INTEGER);
		measure = dataStore.persist(measure);

		long id = measure.getId();

		Measure measure2 = dataStore.find(id);

		assertEquals(measure, measure2);
		assertTrue(measure.getId() == measure2.getId());
		assertEquals(measure.getKeyName(), measure2.getKeyName());

		measure2.setKeyName("key 65535");
		measure2 = dataStore.merge(measure2);

		measure = dataStore.refresh(measure);

		assertEquals(measure, measure2);
		assertTrue(measure.getId() == measure2.getId());
		assertEquals(measure.getKeyName(), measure2.getKeyName());
	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#remove(java.lang.Object)}.
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testRemove() throws Exception {
		Measure measure = new Measure();
		measure.setKeyName("key " + 999);
			measure.setMeasureType(Measure.MeasureType.INTEGER);
		measure = dataStore.persist(measure);

		long id = measure.getId();

		Measure measure2 = dataStore.find(id);

		assertEquals(measure, measure2);
		assertTrue(measure.getId() == measure2.getId());
		assertEquals(measure.getKeyName(), measure2.getKeyName());

		dataStore.remove(measure);

		Measure measure3 = dataStore.find(id);
		assertTrue(measure3 == null);

	}
	
	
	
	// -----------------------------------------------------------------------------------------------------------------
	
	@Test
	@Rollback(true)
	public void insertEntities() throws Exception {

		long index = dataStore.count();
		for (int i = 200; i < 210; i++) {
			Measure measure = new Measure();
			measure.setKeyName("key " + i);
			measure.setMeasureType(Measure.MeasureType.INTEGER);
			dataStore.persist(measure);
			index++;
		}

		List<Measure> list = dataStore.getAllEntities();

		assertTrue(list != null);
		assertTrue(list.size() > 0);
		assertTrue(list.size() == index);
	}
	

}
