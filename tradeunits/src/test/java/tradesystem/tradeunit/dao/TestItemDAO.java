package tradesystem.tradeunit.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import pk.home.libs.combine.dao.ABaseDAO.SortOrderType;
import tradesystem.tradeunit.basetestutils.BaseTest;
import tradesystem.tradeunit.domain.Account;
import tradesystem.tradeunit.domain.Item;
import tradesystem.tradeunit.domain.Item_;
import tradesystem.tradeunit.domain.Order;
import tradesystem.tradeunit.domain.OrderType;

/**
 * JUnit test DAO class for entity class: Item
 * Item - запись
 */
@RunWith(SpringJUnit4ClassRunner.class)
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
		DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class })
@Transactional
@ActiveProfiles({"Dev"})
@ContextConfiguration(locations = { "file:./src/main/resources/applicationContext.xml" })
public class TestItemDAO extends BaseTest{

	/**
	 * The DAO being tested, injected by Spring
	 * 
	 */
	private ItemDAO dataStore;

	/**
	 * Method to allow Spring to inject the DAO that will be tested
	 * 
	 */
	@Autowired
	public void setDataStore(ItemDAO dataStore) {
		this.dataStore = dataStore;
	}

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#getAllEntities()}.
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testGetAllEntities() throws Exception {
		createTestEntitys();

		long index = dataStore.count();
		for (int i = 0; i < 100; i++) {
			
			BigDecimal value = new BigDecimal(1);
			BigDecimal numerator = new BigDecimal(0);
		
			Order order = orderDAO.persist(new Order(new Date(), OrderType.PRODUCT_PLUS, division1, userAccount, "Order description"));
			Account account = accountService.findAccount(division1, productInteger, warehouse1);
			account = accountService.add(account, value, numerator);
			
			Item item = new Item();
			item.setAccount(account);
			item.setOrder(order);
			item.setValue(value);
			item.setNumerator(numerator);
			item.setDescription("key " + i);
			item.setMeasure(account.getProduct().getMeasure());
			
			dataStore.persist(item);
			index++;
		}

		List<Item> list = dataStore.getAllEntities();

		assertTrue(list != null);
		assertTrue(list.size() > 0);
		assertTrue(list.size() == index);
	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#getAllEntities(javax.persistence.metamodel.SingularAttribute, pk.home.libs.combine.dao.ABaseDAO.SortOrderType)}
	 * .
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testGetAllEntitiesSingularAttributeOfTQSortOrderType()
			throws Exception {
		createTestEntitys();
		
		long index = dataStore.count();
		for (int i = 0; i < 100; i++) {
			BigDecimal value = new BigDecimal(1);
			BigDecimal numerator = new BigDecimal(0);
		
			Order order = orderDAO.persist(new Order(new Date(), OrderType.PRODUCT_PLUS, division1, userAccount, "Order description"));
			Account account = accountService.findAccount(division1, productInteger, warehouse1);
			account = accountService.add(account, value, numerator);
			
			Item item = new Item();
			item.setAccount(account);
			item.setOrder(order);
			item.setValue(value);
			item.setNumerator(numerator);
			item.setDescription("key " + i);
			item.setMeasure(account.getProduct().getMeasure());
			
			dataStore.persist(item);
			
			index++;
		}

		List<Item> list = dataStore.getAllEntities(Item_.id, SortOrderType.ASC);

		assertTrue(list != null);
		assertTrue(list.size() > 0);
		assertTrue(list.size() == index);

		long lastId = 0;
		for (Item item : list) {
			assertTrue(lastId < item.getId());
			lastId = item.getId();
		}
	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#getAllEntities(int, int)}.
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testGetAllEntitiesIntInt() throws Exception {
		createTestEntitys();

		// int index = 0;
		for (int i = 0; i < 100; i++) {
			BigDecimal value = new BigDecimal(1);
			BigDecimal numerator = new BigDecimal(0);
		
			Order order = orderDAO.persist(new Order(new Date(), OrderType.PRODUCT_PLUS, division1, userAccount, "Order description"));
			Account account = accountService.findAccount(division1, productInteger, warehouse1);
			account = accountService.add(account, value, numerator);
			
			Item item = new Item();
			item.setAccount(account);
			item.setOrder(order);
			item.setValue(value);
			item.setNumerator(numerator);
			item.setDescription("key " + i);
			item.setMeasure(account.getProduct().getMeasure());
			
			dataStore.persist(item);
			// index++;
		}

		List<Item> list = dataStore.getAllEntities(10, 10);

		assertTrue(list != null);
		assertTrue(list.size() > 0);
		assertTrue(list.size() == 10);
	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#getAllEntities(int, int, javax.persistence.metamodel.SingularAttribute, pk.home.libs.combine.dao.ABaseDAO.SortOrderType)}
	 * .
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testGetAllEntitiesIntIntSingularAttributeOfTQSortOrderType()
			throws Exception {
		createTestEntitys();
		
		// long index = dataStore.count();
		for (int i = 0; i < 100; i++) {
			BigDecimal value = new BigDecimal(1);
			BigDecimal numerator = new BigDecimal(0);
		
			Order order = orderDAO.persist(new Order(new Date(), OrderType.PRODUCT_PLUS, division1, userAccount, "Order description"));
			Account account = accountService.findAccount(division1, productInteger, warehouse1);
			account = accountService.add(account, value, numerator);
			
			Item item = new Item();
			item.setAccount(account);
			item.setOrder(order);
			item.setValue(value);
			item.setNumerator(numerator);
			item.setDescription("key " + i);
			item.setMeasure(account.getProduct().getMeasure());
			
			dataStore.persist(item);
			// index++;
		}

		List<Item> list = dataStore.getAllEntities(10, 10, Item_.id,
				SortOrderType.ASC);

		assertTrue(list != null);
		assertTrue(list.size() > 0);
		assertTrue(list.size() == 10);

		long lastId = 0;
		for (Item item : list) {
			assertTrue(lastId < item.getId());
			lastId = item.getId();
		}
	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#getAllEntities(boolean, int, int, javax.persistence.metamodel.SingularAttribute, pk.home.libs.combine.dao.ABaseDAO.SortOrderType)}
	 * .
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testGetAllEntitiesBooleanIntIntSingularAttributeOfTQSortOrderType()
			throws Exception {
		createTestEntitys();
		
		long index = dataStore.count();
		for (int i = 0; i < 100; i++) {
			BigDecimal value = new BigDecimal(1);
			BigDecimal numerator = new BigDecimal(0);
		
			Order order = orderDAO.persist(new Order(new Date(), OrderType.PRODUCT_PLUS, division1, userAccount, "Order description"));
			Account account = accountService.findAccount(division1, productInteger, warehouse1);
			account = accountService.add(account, value, numerator);
			
			Item item = new Item();
			item.setAccount(account);
			item.setOrder(order);
			item.setValue(value);
			item.setNumerator(numerator);
			item.setDescription("key " + i);
			item.setMeasure(account.getProduct().getMeasure());
			
			dataStore.persist(item);
			index++;
		}

		// all - FALSE
		List<Item> list = dataStore.getAllEntities(false, 10, 10, Item_.id,
				SortOrderType.ASC);

		assertTrue(list != null);
		assertTrue(list.size() > 0);
		assertTrue(list.size() == 10);

		long lastId = 0;
		for (Item item : list) {
			assertTrue(lastId < item.getId());
			lastId = item.getId();
		}

		// all - TRUE
		list = dataStore.getAllEntities(true, 10, 10, Item_.id,
				SortOrderType.ASC);

		assertTrue(list != null);
		assertTrue(list.size() > 0);
		assertTrue(list.size() == index);

		lastId = 0;
		for (Item item : list) {
			assertTrue(lastId < item.getId());
			lastId = item.getId();
		}
	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#find(java.lang.Object)}.
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testFind() throws Exception {
		createTestEntitys();

		BigDecimal value = new BigDecimal(1);
		BigDecimal numerator = new BigDecimal(0);
	
		Order order = orderDAO.persist(new Order(new Date(), OrderType.PRODUCT_PLUS, division1, userAccount, "Order description"));
		Account account = accountService.findAccount(division1, productInteger, warehouse1);
		account = accountService.add(account, value, numerator);
		
		Item item = new Item();
		item.setAccount(account);
		item.setOrder(order);
		item.setValue(value);
		item.setNumerator(numerator);
		item.setDescription("key ");
		item.setMeasure(account.getProduct().getMeasure());
		
		item = dataStore.persist(item);

		long id = item.getId();

		Item item2 = dataStore.find(id);

		assertEquals(item, item2);
		assertTrue(item.getId() == item2.getId());
		assertEquals(item.getDescription(), item2.getDescription());

	}

	/**
	 * Test method for {@link pk.home.libs.combine.dao.ABaseDAO#count()}.
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testCount() throws Exception {
		createTestEntitys();
		
		long index = dataStore.count();
		for (int i = 0; i < 100; i++) {
			BigDecimal value = new BigDecimal(1);
			BigDecimal numerator = new BigDecimal(0);
		
			Order order = orderDAO.persist(new Order(new Date(), OrderType.PRODUCT_PLUS, division1, userAccount, "Order description"));
			Account account = accountService.findAccount(division1, productInteger, warehouse1);
			account = accountService.add(account, value, numerator);
			
			Item item = new Item();
			item.setAccount(account);
			item.setOrder(order);
			item.setValue(value);
			item.setNumerator(numerator);
			item.setDescription("key " + i);
			item.setMeasure(account.getProduct().getMeasure());
			
			dataStore.persist(item);
			index++;
		}

		long count = dataStore.count();

		assertTrue(count == index);
	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#persist(java.lang.Object)}.
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testPersist() throws Exception {
		createTestEntitys();
		
		BigDecimal value = new BigDecimal(1);
		BigDecimal numerator = new BigDecimal(0);
	
		Order order = orderDAO.persist(new Order(new Date(), OrderType.PRODUCT_PLUS, division1, userAccount, "Order description"));
		Account account = accountService.findAccount(division1, productInteger, warehouse1);
		account = accountService.add(account, value, numerator);
		
		Item item = new Item();
		item.setAccount(account);
		item.setOrder(order);
		item.setValue(value);
		item.setNumerator(numerator);
		item.setDescription("key ");
		item.setMeasure(account.getProduct().getMeasure());
		
		item = dataStore.persist(item);

		long id = item.getId();

		Item item2 = dataStore.find(id);

		assertEquals(item, item2);
		assertTrue(item.getId() == item2.getId());
		assertEquals(item.getDescription(), item2.getDescription());
	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#refresh(java.lang.Object)}.
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testRefresh() throws Exception {
		createTestEntitys();
		
		BigDecimal value = new BigDecimal(1);
		BigDecimal numerator = new BigDecimal(0);
	
		Order order = orderDAO.persist(new Order(new Date(), OrderType.PRODUCT_PLUS, division1, userAccount, "Order description"));
		Account account = accountService.findAccount(division1, productInteger, warehouse1);
		account = accountService.add(account, value, numerator);
		
		Item item = new Item();
		item.setAccount(account);
		item.setOrder(order);
		item.setValue(value);
		item.setNumerator(numerator);
		item.setDescription("key ");
		item.setMeasure(account.getProduct().getMeasure());
		
		item = dataStore.persist(item);

		long id = item.getId();

		Item item2 = dataStore.find(id);

		assertEquals(item, item2);
		assertTrue(item.getId() == item2.getId());
		assertEquals(item.getDescription(), item2.getDescription());

		item2.setDescription("key 65535");
		item2 = dataStore.refresh(item2);

		assertEquals(item, item2);
		assertTrue(item.getId() == item2.getId());
		assertEquals(item.getDescription(), item2.getDescription());

	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#merge(java.lang.Object)}.
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testMerge() throws Exception {
		createTestEntitys();
		
		BigDecimal value = new BigDecimal(1);
		BigDecimal numerator = new BigDecimal(0);
	
		Order order = orderDAO.persist(new Order(new Date(), OrderType.PRODUCT_PLUS, division1, userAccount, "Order description"));
		Account account = accountService.findAccount(division1, productInteger, warehouse1);
		account = accountService.add(account, value, numerator);
		
		Item item = new Item();
		item.setAccount(account);
		item.setOrder(order);
		item.setValue(value);
		item.setNumerator(numerator);
		item.setDescription("key ");
		item.setMeasure(account.getProduct().getMeasure());
		
		item = dataStore.persist(item);

		long id = item.getId();

		Item item2 = dataStore.find(id);

		assertEquals(item, item2);
		assertTrue(item.getId() == item2.getId());
		assertEquals(item.getDescription(), item2.getDescription());

		item2.setDescription("key 65535");
		item2 = dataStore.merge(item2);

		item = dataStore.refresh(item);

		assertEquals(item, item2);
		assertTrue(item.getId() == item2.getId());
		assertEquals(item.getDescription(), item2.getDescription());
	}

	/**
	 * Test method for
	 * {@link pk.home.libs.combine.dao.ABaseDAO#remove(java.lang.Object)}.
	 * 
	 * @throws Exception
	 */
	@Test
	@Rollback(true)
	public void testRemove() throws Exception {
		createTestEntitys();
		
		BigDecimal value = new BigDecimal(1);
		BigDecimal numerator = new BigDecimal(0);
	
		Order order = orderDAO.persist(new Order(new Date(), OrderType.PRODUCT_PLUS, division1, userAccount, "Order description"));
		Account account = accountService.findAccount(division1, productInteger, warehouse1);
		account = accountService.add(account, value, numerator);
		
		Item item = new Item();
		item.setAccount(account);
		item.setOrder(order);
		item.setValue(value);
		item.setNumerator(numerator);
		item.setDescription("key ");
		item.setMeasure(account.getProduct().getMeasure());
		
		item = dataStore.persist(item);

		long id = item.getId();

		Item item2 = dataStore.find(id);

		assertEquals(item, item2);
		assertTrue(item.getId() == item2.getId());
		assertEquals(item.getDescription(), item2.getDescription());

		dataStore.remove(item);

		Item item3 = dataStore.find(id);
		assertTrue(item3 == null);

	}
	
	
	
	// -----------------------------------------------------------------------------------------------------------------
	
	@Test
	@Rollback(true)
	public void insertEntities() throws Exception {
		createTestEntitys();
		

		long index = dataStore.count();
		for (int i = 200; i < 210; i++) {
			BigDecimal value = new BigDecimal(1);
			BigDecimal numerator = new BigDecimal(0);
		
			Order order = orderDAO.persist(new Order(new Date(), OrderType.PRODUCT_PLUS, division1, userAccount, "Order description"));
			Account account = accountService.findAccount(division1, productInteger, warehouse1);
			account = accountService.add(account, value, numerator);
			
			Item item = new Item();
			item.setAccount(account);
			item.setOrder(order);
			item.setValue(value);
			item.setNumerator(numerator);
			item.setDescription("key ");
			item.setMeasure(account.getProduct().getMeasure());
			
			dataStore.persist(item);
			index++;
		}

		List<Item> list = dataStore.getAllEntities();

		assertTrue(list != null);
		assertTrue(list.size() > 0);
		assertTrue(list.size() == index);
	}
	

}
