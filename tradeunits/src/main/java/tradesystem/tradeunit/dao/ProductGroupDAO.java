package tradesystem.tradeunit.dao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import pk.home.libs.combine.dao.ABaseTreeDAO;
import tradesystem.tradeunit.domain.ProductGroup;


/**
 * DAO class for entity class: ProductGroup
 * ProductGroup - группа продуктов
 */
@Repository
@Transactional
public class ProductGroupDAO extends ABaseTreeDAO<ProductGroup> {

	@Override
	protected Class<ProductGroup> getTClass() {
		return ProductGroup.class;
	}

	/**
	 * EntityManager injected by Spring for persistence unit
	 * 
	 */
	@PersistenceContext(unitName = "")
	private EntityManager entityManager;

	@Override
	public EntityManager getEntityManager() {
		return entityManager;
	}

	@Override
	public Object getPrimaryKey(ProductGroup o) {
		return o.getId();
	}

	@Override
	public void setParent(ProductGroup object, ProductGroup parent)
			throws Exception {
		object.setParent(parent);
	}

}
