package tradesystem.tradeunit.dao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import pk.home.libs.combine.dao.ABaseDAO;
import tradesystem.tradeunit.domain.Product;


/**
 * DAO class for entity class: Product
 * Product - продукт
 */
@Repository
@Transactional
public class ProductDAO extends ABaseDAO<Product> {

	@Override
	protected Class<Product> getTClass() {
		return Product.class;
	}

	/**
	 * EntityManager injected by Spring for persistence unit
	 * 
	 */
	@PersistenceContext(unitName = "")
	private EntityManager entityManager;

	@Override
	public EntityManager getEntityManager() {
		return entityManager;
	}

	@Override
	public Object getPrimaryKey(Product o) {
		return o.getId();
	}

}
