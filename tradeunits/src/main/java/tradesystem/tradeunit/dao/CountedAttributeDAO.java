package tradesystem.tradeunit.dao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import pk.home.libs.combine.dao.ABaseTreeDAO;
import tradesystem.tradeunit.domain.CountedAttribute;


/**
 * DAO class for entity class: CountedAttribute
 * СountedAttribute - подсчитываемый атрибут
 */
@Repository
@Transactional
public class CountedAttributeDAO extends ABaseTreeDAO<CountedAttribute> {

	@Override
	protected Class<CountedAttribute> getTClass() {
		return CountedAttribute.class;
	}

	/**
	 * EntityManager injected by Spring for persistence unit
	 * 
	 */
	@PersistenceContext(unitName = "")
	private EntityManager entityManager;

	@Override
	public EntityManager getEntityManager() {
		return entityManager;
	}

	@Override
	public Object getPrimaryKey(CountedAttribute o) {
		return o.getId();
	}

	@Override
	public void setParent(CountedAttribute object, CountedAttribute parent)
			throws Exception {
		object.setParent(parent);
	}

}
