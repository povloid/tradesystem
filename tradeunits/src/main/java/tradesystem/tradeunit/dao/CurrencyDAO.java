package tradesystem.tradeunit.dao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import pk.home.libs.combine.dao.ABaseDAO;
import tradesystem.tradeunit.domain.Currency;


/**
 * DAO class for entity class: Currency
 * Currency - валюта
 */
@Repository
@Transactional
public class CurrencyDAO extends ABaseDAO<Currency> {

	@Override
	protected Class<Currency> getTClass() {
		return Currency.class;
	}

	/**
	 * EntityManager injected by Spring for persistence unit
	 * 
	 */
	@PersistenceContext(unitName = "")
	private EntityManager entityManager;

	@Override
	public EntityManager getEntityManager() {
		return entityManager;
	}

	@Override
	public Object getPrimaryKey(Currency o) {
		return o.getId();
	}

}
