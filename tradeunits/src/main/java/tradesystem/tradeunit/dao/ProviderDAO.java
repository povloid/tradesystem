package tradesystem.tradeunit.dao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import pk.home.libs.combine.dao.ABaseDAO;
import tradesystem.tradeunit.domain.Provider;


/**
 * DAO class for entity class: Provider
 * Provider - продукт
 */
@Repository
@Transactional
public class ProviderDAO extends ABaseDAO<Provider> {

	@Override
	protected Class<Provider> getTClass() {
		return Provider.class;
	}

	/**
	 * EntityManager injected by Spring for persistence unit
	 * 
	 */
	@PersistenceContext(unitName = "")
	private EntityManager entityManager;

	@Override
	public EntityManager getEntityManager() {
		return entityManager;
	}

	@Override
	public Object getPrimaryKey(Provider o) {
		return o.getId();
	}

}
