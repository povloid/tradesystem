package tradesystem.tradeunit.dao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import pk.home.libs.combine.dao.ABaseDAO;
import tradesystem.tradeunit.domain.Cash;


/**
 * DAO class for entity class: Cash
 * Cash - касса
 */
@Repository
@Transactional
public class CashDAO extends ABaseDAO<Cash> {

	@Override
	protected Class<Cash> getTClass() {
		return Cash.class;
	}

	/**
	 * EntityManager injected by Spring for persistence unit
	 * 
	 */
	@PersistenceContext(unitName = "")
	private EntityManager entityManager;

	@Override
	public EntityManager getEntityManager() {
		return entityManager;
	}

	@Override
	public Object getPrimaryKey(Cash o) {
		return o.getId();
	}

}
