package tradesystem.tradeunit.dao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import pk.home.libs.combine.dao.ABaseDAO;
import tradesystem.tradeunit.domain.Account;


/**
 * DAO class for entity class: Account
 * Account - счет
 */
@Repository
@Transactional
public class AccountDAO extends ABaseDAO<Account> {

	@Override
	protected Class<Account> getTClass() {
		return Account.class;
	}

	/**
	 * EntityManager injected by Spring for persistence unit
	 * 
	 */
	@PersistenceContext(unitName = "")
	private EntityManager entityManager;

	@Override
	public EntityManager getEntityManager() {
		return entityManager;
	}

	@Override
	public Object getPrimaryKey(Account o) {
		return o.getId();
	}

}
