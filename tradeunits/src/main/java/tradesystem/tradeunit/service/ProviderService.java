package tradesystem.tradeunit.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pk.home.libs.combine.dao.ABaseDAO;
import pk.home.libs.combine.service.ABaseService;
import tradesystem.tradeunit.dao.ProviderDAO;
import tradesystem.tradeunit.domain.Provider;

/**
 * Service class for entity class: Provider
 * Provider - продукт
 */
@Service
@Transactional
public class ProviderService extends ABaseService<Provider> {

	@Autowired
	private ProviderDAO providerDAO;

	@Override
	public ABaseDAO<Provider> getAbstractBasicDAO() {
		return providerDAO;
	}

}
