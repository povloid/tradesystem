package tradesystem.tradeunit.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.transaction.NotSupportedException;

import org.apache.log4j.Logger;
import org.jfree.util.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.ExceptionHandler;

import pk.home.libs.combine.dao.ABaseDAO;
import pk.home.libs.combine.service.ABaseService;
import tradesystem.tradeunit.dao.OrderDAO;
import tradesystem.tradeunit.domain.Account;
import tradesystem.tradeunit.domain.Cash;
import tradesystem.tradeunit.domain.CashPrivileges;
import tradesystem.tradeunit.domain.Currency;
import tradesystem.tradeunit.domain.Customer;
import tradesystem.tradeunit.domain.Division;
import tradesystem.tradeunit.domain.ICountsObject;
import tradesystem.tradeunit.domain.IOwner;
import tradesystem.tradeunit.domain.Item;
import tradesystem.tradeunit.domain.Measure.MeasureType;
import tradesystem.tradeunit.domain.Order;
import tradesystem.tradeunit.domain.OrderType;
import tradesystem.tradeunit.domain.Product;
import tradesystem.tradeunit.domain.Provider;
import tradesystem.tradeunit.domain.Warehouse;
import tradesystem.tradeunit.domain.WarehousePrivileges;
import tradesystem.tradeunit.domain.security.UserAccount;

/**
 * Service class for entity class: Order Order - ордер
 */
@Service
@Transactional
public class OrderService extends ABaseService<Order> {

	private static final Logger LOG = Logger.getLogger(OrderService.class);
	
	@Autowired
	private OrderDAO orderDAO;

	@Autowired
	private AccountService accountService;

	@Autowired
	private ItemService itemService;

	@Autowired
	private PrivilegesService privilegesService;

	@Override
	public ABaseDAO<Order> getAbstractBasicDAO() {
		return orderDAO;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see pk.home.libs.combine.service.ABaseService#persist(java.lang.Object)
	 */
	@Override
	public Order persist(Order o) throws Exception {
		throw new NotSupportedException();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see pk.home.libs.combine.service.ABaseService#merge(java.lang.Object)
	 */
	@Override
	public Order merge(Order o) throws Exception {
		throw new NotSupportedException();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see pk.home.libs.combine.service.ABaseService#remove(java.lang.Object)
	 */
	@Override
	public void remove(Order object) throws Exception {
		throw new NotSupportedException();
	}

	/**
	 * Edits the.
	 * 
	 * @param order
	 *            the order
	 * @param description
	 *            the description
	 * @return the order
	 * @throws Exception
	 *             the exception
	 */
	@ExceptionHandler(Exception.class)
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public Order edit(Order order, String description) throws Exception {

		order = orderDAO.find(order.getId());

		order.setDescription(description);

		return orderDAO.merge(order);
	}
	
	/**
	 * Find and set account for transient fields.
	 *
	 * @param division the division
	 * @param item the item
	 * @return the item
	 * @throws Exception the exception
	 */
	@ExceptionHandler(Exception.class)
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public Item findAndSetAccountForTransientFields(Division division, Item item) throws Exception {
		
		if(item.getTransientOwner() == null )
			throw new Exception("Transient owner is null!");
		
		if(item.getTransientCountsObject() == null)
			throw new Exception("Transient counts object is null!");
			
		item.setAccount(accountService.findAccount(division, item.getTransientCountsObject(), item.getTransientOwner()));
		
		if(item.getAccount() == null 
				|| !item.getAccount().getOwner().equals(item.getTransientOwner())
				|| !item.getAccount().getCountsObject().equals(item.getTransientCountsObject()))
				throw new Exception("Account is not found!");
				
		
		return item;
	}
	
	
	/**
	 * Fill items.
	 *
	 * @param itemsSorted the items sorted
	 * @param division the division
	 * @return the collection
	 * @throws Exception the exception
	 */
	@ExceptionHandler(Exception.class)
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public List<Item> fillItems(List<Item> itemsSorted, Division division) throws Exception {
		
		List<Account> accounts = new ArrayList<>();
		
		for (Item i : itemsSorted)
			if (isRootAttrItem(i))
				accounts.addAll(accountService.findAccounts(division, i.getTransientCountsObject(), i.getTransientOwner(), true));

		mergeItemsAndAccountsForCA(itemsSorted, accounts);

		return itemsSorted;
	}
	
	
	/**
	 * Common do items.
	 *
	 * @param itemsSorted the items sorted
	 * @param order the order
	 * @return the order
	 * @throws Exception the exception
	 */
	@ExceptionHandler(Exception.class)
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public Order commonDoItems(Collection<Item> itemsSorted, Order order)  throws Exception {
		
		short addedItems = 0;
		for (Item i : itemsSorted)
			if (!isEmptyValue(i)) {
				
				System.out.println(">>>> Product: " + i.getTransientCountsObject().getKeyName() 
						+ "\tQuantity: " + i.getValue() 
						+ " " + i.getNumerator() 
						+ "/"+ i.getTransientCountsObject().getDenominator() 
						+ "\t QA: " + (i.getCountedAttribute() != null ? i.getCountedAttribute().getKeyName() : "ROOT")
						+ " VALUE TYPE:" + i.getTransientCountsObject().getValueType()
						+ " OWNER:" + i.getTransientOwner()
				);
				
				
				i.setOrder(order);
				
				// FORCE VALIDATION
				//i.prePersistOrUpdate();
				
				i = itemService.persist(i);

				accountService.add(i.getAccount(), i.getValue(),i.getNumerator());
				
				order.getItems().add(i);
				
				++addedItems;
			}
		
		// TODO: Написать тест
		if(addedItems == 0)
			throw new Exception("Attempt to sell an empty list of products!");
		

		// END
		return orderDAO.merge(order);	
	}
	
	

	/**
	 * Product plus.
	 *
	 * @param userAccount the user account
	 * @param division the division
	 * @param orderType the order type
	 * @param items the items
	 * @param description the description
	 * @return the order
	 * @throws Exception the exception
	 */
	@ExceptionHandler(Exception.class)
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public Order productPlus(UserAccount userAccount, Division division,
			OrderType orderType, Collection<Item> items, String description) throws Exception {

		return productPlus(userAccount, division, orderType, items, description, null);
	}

	
	

	/**
	 * Product plus.
	 *
	 * @param userAccount the user account
	 * @param division the division
	 * @param orderType the order type
	 * @param productItems the items
	 * @param description the description
	 * @param provider the provider
	 * @return the order
	 * @throws Exception the exception
	 */
	@ExceptionHandler(Exception.class)
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public Order productPlus(UserAccount userAccount, Division division, OrderType orderType,
			Collection<Item> productItems, String description, Provider provider)
			throws Exception {

		// INPUT VALIDATION BEGIN
		// TODO: Написать тесты для валидации

		// TODO: Написать валидацию по праву пользователя на операцию и тест
		if (userAccount == null)
			throw new Exception("UserAccount can not be null");

		if (division == null)
			throw new Exception("Division can not be null");
		
		// TODO: Написать тест
		if (CollectionUtils.isEmpty(productItems))
			throw new Exception("Items can not be empty");

		// TODO: Написать тест
		if (orderType != OrderType.PRODUCT_PLUS && orderType != OrderType.PRODUCT_MINUS)
			throw new Exception("Suport only PRODUCT_PLUS or PRODUCT_MINUS");
			
		// INPUT VALIDATION END

		Order order = orderDAO.persist(new Order(new Date(), orderType,
				division, userAccount, description, provider));
		order = orderDAO.refresh(order);

		// Create new collection
		// Sort it for accounts
		List<Item> itemsSorted = itemService.sortForCountedAttributes(
				new ArrayList<>(productItems), true);		

		// DO ALL ITEMS
		return commonDoItems(fillItems(itemsSorted, division), order);
	}

	/**
	 * Product sale.
	 *
	 * @param userAccount the user account
	 * @param division the division
	 * @param warehouseProductsItems the warehouse products items
	 * @param cacheItem the cache item
	 * @param customerItem the customer item
	 * @param description the description
	 * @return the order
	 * @throws Exception the exception
	 */
	@ExceptionHandler(Exception.class)
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public Order productSale(UserAccount userAccount, Division division,
			Collection<Item> warehouseProductsItems, Item cashItem, Item customerItem, String description) throws Exception {

		// INPUT VALIDATION BEGIN
		// ------------------------------------------------------------------------------

		// TODO: Написать валидацию по праву пользователя на операцию и тест
		if (userAccount == null)
			throw new Exception("UserAccount can not be null");

		// TODO: Написать тест
		if (division == null)
			throw new Exception("Division can not be null");

		// BEGIN WAREHOUSE-PRODUCT ITEMS VALIDATION *************************************
		{
			// TODO: Написать тест
			if (CollectionUtils.isEmpty(warehouseProductsItems))
				throw new Exception("warehouseProductsItems is empty!");

			// TODO: Написать тест
			for (Warehouse w : new SelectFromCollection<Warehouse, Item>() {
				
				@Override
				Warehouse getElement(Item i)  throws Exception {
					return checkAndGetWarehouse(i.getTransientOwner());
				}
				
			}.select(warehouseProductsItems))
				privilegesService.checkAccess(userAccount, division, w, WarehousePrivileges.PRODUCT_SALE);
			
			
			for(Item i: warehouseProductsItems){
				// TODO: Написать тест
				if(i.getTransientOwner() == null || !(i.getTransientOwner() instanceof Warehouse))
					throw new Exception("Transient owner is null or not Warehouse type!");
				
				// TODO: Написать тест
				if(i.getTransientCountsObject() == null || !(i.getTransientCountsObject() instanceof Product))
					throw new Exception("Transient counts object is null or not Product type!");
			}
			

		}

		// END PRODUCT ITEMS VALIDATION ***************************************

		// BEGIN CASH VALIDATION
		{
			// TODO: Написать тест
			Cash cash = checkAndGetCash(cashItem.getTransientOwner());
			
			// TODO: Написать тест
			privilegesService.checkAccess(userAccount, division, cash, CashPrivileges.PRODUCT_SALE);
		}
		// END CASH VALIDATION

		// BEGIN CUSTOMER VALIDATION ******************************************
		if (customerItem != null){
			
			// TODO: Написать тест
			//Customer customer = checkAndGetCustomer(customerItem.getTransientOwner());
			checkAndGetCustomer(customerItem.getTransientOwner());
		}
		// END CUSTOMER VALIDATION

		// INPUT VALIDATION END
		// -------------------------------------------------------------------------------
		
		
		// BEGIN TRADE LOGIC VALIDATION
		{
			BigDecimal pSum = productsAllSum(warehouseProductsItems);
			
			Log.debug("pSum = " + pSum);
			
			BigDecimal mSum = cashItem.getValue().add(customerItem != null ? customerItem.getValue().abs() : new BigDecimal(0));
			
			Log.debug("mSum = " + mSum);
			
			// DEBUG
			System.out.println(" warehouseProductsItems.size() = " + warehouseProductsItems.size()); 
			System.out.println(pSum + " <=> " + mSum); 
			
			// TODO: Написать тест
			if(pSum.multiply(new BigDecimal(-1)).compareTo(mSum) != 0)
				throw new Exception("Sum of product is not eq with sum of maney!");
			
		}

		// END TRADE LOGIC VALIDATION

		// Create new order
		Order order = orderDAO.persist(new Order(new Date(),OrderType.PRODUCT_SALE, division, userAccount, description));
		order = orderDAO.refresh(order);

		// Create new collection
		// Sort it for accounts
		List<Item> itemsSorted = itemService.sortForCountedAttributes(new ArrayList<>(warehouseProductsItems), true);
		
		for(Item i: itemsSorted)
			System.out.println("Product: " + i.getTransientCountsObject().getKeyName() 
					+ "\tQuantity: " + i.getValue() 
					+ " " + i.getNumerator() 
					+ "/"+ i.getTransientCountsObject().getDenominator() 
					+ "\t QA: " + (i.getCountedAttribute() != null ? i.getCountedAttribute().getKeyName() : "ROOT")
					+ " VALUE TYPE:" + i.getTransientCountsObject().getValueType()
					+ " OWNER:" + i.getTransientOwner()
					+ " ACCOUNT:" + i.getAccount()
			);
		
		
		// Sorted accounts
		itemsSorted = fillItems(itemsSorted, division);

		// DEBUG
		
		for(Item i: itemsSorted)
			System.out.println("Product: " + i.getTransientCountsObject().getKeyName() 
					+ "\tQuantity: " + i.getValue() 
					+ " " + i.getNumerator() 
					+ "/"+ i.getTransientCountsObject().getDenominator() 
					+ "\t QA: " + (i.getCountedAttribute() != null ? i.getCountedAttribute().getKeyName() : "ROOT")
					+ " VALUE TYPE:" + i.getTransientCountsObject().getValueType()
					+ " OWNER:" + i.getTransientOwner()
					+ " ACCOUNT:" + i.getAccount()
			);
		
		
		// CASH
		itemsSorted.add(findAndSetAccountForTransientFields(division, cashItem));
		
		// CUSTOMER
		if (customerItem != null)
			itemsSorted.add(findAndSetAccountForTransientFields(division, customerItem));		
		
		// DO ALL ITEMS
		return commonDoItems(itemsSorted, order);

	}
	
	
	/**
	 * Product return
	 * 
	 * @param userAccount
	 * @param division
	 * @param parentOrder
	 * @param warehouseProductsItems
	 * @param cashItem
	 * @param customerItem
	 * @param description
	 * @return
	 * @throws Exception
	 */
	@ExceptionHandler(Exception.class)
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public Order productReturn(UserAccount userAccount, Division division, Order parentOrder,
			Collection<Item> warehouseProductsItems, Item cashItem, Item customerItem, String description) throws Exception {
		
		// INPUT VALIDATION BEGIN
		// ------------------------------------------------------------------------------

		// TODO: Написать валидацию по праву пользователя на операцию и тест
		if (userAccount == null)
			throw new Exception("UserAccount can not be null");

		// TODO: Написать тест
		if (division == null)
			throw new Exception("Division can not be null");
		
		// TODO: Написать тест
		if (parentOrder == null)
			throw new Exception("parentOrder can not be null");

		// BEGIN WAREHOUSE-PRODUCT ITEMS VALIDATION *************************************
		{
			// TODO: Написать тест
			if (CollectionUtils.isEmpty(warehouseProductsItems))
				throw new Exception("warehouseProductsItems is empty!");

			// TODO: Написать тест
			for (Warehouse w : new SelectFromCollection<Warehouse, Item>() {
				
				@Override
				Warehouse getElement(Item i)  throws Exception {
					return checkAndGetWarehouse(i.getTransientOwner());
				}
				
			}.select(warehouseProductsItems))
				privilegesService.checkAccess(userAccount, division, w, WarehousePrivileges.PRODUCT_RETURN);
			
			
			for(Item i: warehouseProductsItems){
				// TODO: Написать тест
				if(i.getTransientOwner() == null || !(i.getTransientOwner() instanceof Warehouse))
					throw new Exception("Transient owner is null or not Warehouse type!");
				
				// TODO: Написать тест
				if(i.getTransientCountsObject() == null || !(i.getTransientCountsObject() instanceof Product))
					throw new Exception("Transient counts object is null or not Product type!");
			}
			

		}
		
		
		return null;
		
	}
	
	

	// Common static methods
	// -----------------------------------------------------------------------------------------------------------------------

	// Common predicates -----------------------------------------------------------------------------------
	
	
	/**
	 * Checks if is root counted attribute item.
	 *
	 * @param i the i
	 * @return true, if is root counted attribute item
	 */
	public static boolean isRootAttrItem(Item i) {
		return i.getCountedAttribute() == null || i.getCountedAttribute().getParent() == null;
	}
	
	
	/**
	 * Checks if is empty value.
	 *
	 * @param item the item
	 * @return true, if is empty value
	 * @throws Exception the exception
	 */
	private static boolean isEmptyValue(Item item) throws Exception {
		return ((item.getValue() == null || item.getValue().compareTo(new BigDecimal(0)) == 0)	
				&& (item.getNumerator() == null || item.getNumerator().compareTo(new BigDecimal(0)) == 0));
	}
	
	
	/**
	 * Check empty value.
	 *
	 * @param item the item
	 * @throws Exception the exception
	 */
	private static void checkEmptyValue(Item item) throws Exception {
		if (isEmptyValue(item))
			throw new Exception("Detected item with empty value");
	}
	
	// -----------------------------------------------------------------------------------------------------
	
	
	/**
	 * Merge items and accounts for ca.
	 * 
	 * @param items
	 *            the items
	 * @param accounts
	 *            the accounts
	 * @throws Exception
	 *             the exception
	 */
	private static void mergeItemsAndAccountsForCA(Collection<Item> items, Collection<Account> accounts) throws Exception {

		for (Item ii : items)
			for (Account aa : accounts){
				
				if(ii.getTransientOwner() == null)
					throw new Exception("transientOwner is null on item");
				
				if(ii.getTransientCountsObject() == null)
					throw new Exception("transientCountsObject is null on item");
				
				
				if (ii.getTransientOwner().equals(aa.getOwner()) && ii.getTransientCountsObject().equals(aa.getCountsObject())
					&& 
						(ii.getCountedAttribute() == null && aa.getCountedAttribute() == null 
							|| ii.getCountedAttribute() != null	&& aa.getCountedAttribute() != null && ii.getCountedAttribute().equals(aa.getCountedAttribute()))) {
					
					ii.setAccount(aa);
					break;
				}
			}
	}
	
	
	/**
	 * The Class SelectFromCollection.
	 *
	 * @param <T> the generic type
	 * @param <I> the generic type
	 */
	abstract class SelectFromCollection<T,I> {
		public Set<T> select(Collection<I> items) throws Exception {

			Set<T> set = new HashSet<>();
			
			for(I i: items)
				if(predicat())
					set.add(getElement(i));

			return set;
		}
		
		boolean predicat(){
			return true;
		}
		
		abstract T getElement(I i) throws Exception;
	}
	
	
	/**
	 * Check and get cash.
	 *
	 * @param owner the owner
	 * @return the cash
	 * @throws Exception the exception
	 */
	private static Cash checkAndGetCash(IOwner owner) throws Exception {
		if(owner instanceof Cash)
			return (Cash) owner;
		else 
			throw new Exception("The owner is not Cash type!");
	}
	
	/**
	 * Check and get warehouse.
	 *
	 * @param owner the owner
	 * @return the warehoouse
	 * @throws Exception the exception
	 */
	private static Warehouse checkAndGetWarehouse(IOwner owner) throws Exception {
		if(owner instanceof Warehouse)
			return (Warehouse) owner;
		else 
			throw new Exception("The owner is not Warehouse type!");
	}
	
	/**
	 * Check and get customer.
	 *
	 * @param owner the owner
	 * @return the customer
	 * @throws Exception the exception
	 */
	private static Customer checkAndGetCustomer(IOwner owner) throws Exception {
		if(owner instanceof Customer)
			return (Customer) owner;
		else 
			throw new Exception("The owner is not Customer type!");
	}
	
	
	
	/**
	 * Check and get product.
	 *
	 * @param countsObject the counts object
	 * @return the product
	 * @throws Exception the exception
	 */
	private static Product checkAndGetProduct(ICountsObject countsObject) throws Exception {
		if(countsObject instanceof Product)
			return (Product) countsObject;
		else 
			throw new Exception("The iCountsObject is not Product type!");
	}
	
	
	/**
	 * Check and get currency.
	 *
	 * @param countsObject the counts object
	 * @return the currency
	 * @throws Exception the exception
	 */
	private static Currency checkAndGetCurrency(ICountsObject countsObject) throws Exception {
		if(countsObject instanceof Currency)
			return (Currency) countsObject;
		else 
			throw new Exception("The iCountsObject is not Currency type!");
	}

	
	/**
	 * get products sale sum
	 * @param items
	 * @return
	 * @throws Exception
	 */
	public static BigDecimal productsAllSum(Collection<Item> items) throws Exception {
		BigDecimal sum = new BigDecimal(0);
		
		for(Item i: items){
			
			Product product = checkAndGetProduct(i.getTransientCountsObject());
			
			if (isRootAttrItem(i))
				if (product.getMeasure().getMeasureType() == MeasureType.INTEGER || product.getMeasure().getMeasureType() == MeasureType.FLOAT) {
					BigDecimal s = i.getValue().multiply(i.getCostValue());

					System.out.println("INTEGER/FLOAT " + i.getValue() + " * " + i.getCostValue() + " = " + s);
					LOG.debug("INTEGER/FLOAT " + i.getValue() + " * " + i.getCostValue() + " = " + s);
					
					sum = sum.add(s);

				} else if (product.getMeasure().getMeasureType() == MeasureType.FRACTION) {
					BigDecimal s = i.getValue().multiply(i.getCostValue()).add(i.getNumerator().multiply(i.getCostValueNumerator()));

					System.out.println("FRACTION " + i.getValue() + " * " + i.getCostValue() + " + " + i.getNumerator() + " + " + i.getCostValueNumerator()	+ " = " + s);
					LOG.debug("FRACTION " + i.getValue() + " * " + i.getCostValue() + " + " + i.getNumerator() + " + " + i.getCostValueNumerator()	+ " = " + s);

					sum = sum.add(s);
				}
		}
			
		return sum;
	}
	
	
	
	
	//enum Only {
	//	CASH_ITEMS, WAREHOUSE_ITEMS, CUSTOMER_ITEMS,
	//}


	//private static int selectCountFrom(Collection<Item> items, Only only)
	//		throws Exception {
	//	return selectFrom(items, only).size();
	//}


	//private static List<Item> selectFrom(Collection<Item> items, Only only)
	//		throws Exception {
	//
	//	List<Item> resultList = new ArrayList<>();
	//
	//	for (Item i : items)
	//		if (only == Only.CASH_ITEMS && i.getAccount().getCash() != null
	//			|| only == Only.WAREHOUSE_ITEMS	&& i.getAccount().getWarehouse() != null
	//			|| only == Only.CUSTOMER_ITEMS && i.getAccount().getCustomer() != null)
	//			resultList.add(i);
	//
	//	return resultList;
	//}
	

}
