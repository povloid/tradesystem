package tradesystem.tradeunit.service;

import javax.transaction.NotSupportedException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import pk.home.libs.combine.dao.ABaseDAO;
import pk.home.libs.combine.service.ABaseService;
import tradesystem.tradeunit.dao.PriceDAO;
import tradesystem.tradeunit.domain.Division;
import tradesystem.tradeunit.domain.Price;
import tradesystem.tradeunit.domain.Price_;
import tradesystem.tradeunit.domain.Product;

/**
 * Service class for entity class: Price
 * Price - цена
 */
@Service
@Transactional
public class PriceService extends ABaseService<Price> {

	@Autowired
	private PriceDAO priceDAO;
	
	@Autowired
	private ProductService productService;
	
	@Autowired
	private DivisionService divisionService; 

	@Override
	public ABaseDAO<Price> getAbstractBasicDAO() {
		return priceDAO;
	}

	

	/**
	 * Find price.
	 * 
	 * @param division
	 *            the division
	 * @param product
	 *            the product
	 * @return the price
	 * @throws Exception
	 *             the exception
	 */
	@Transactional(propagation = Propagation.REQUIRED)
	public Price findPrice(Division division, Product product) throws Exception {

		if (division == null)
			throw new Exception("Operation can not be run. One operand division is null.");
		
		if (product == null)
			throw new Exception("Operation can not be run. One operand product is null.");

		if (priceDAO.countAdvanced(Price_.division, division, Price_.product, product) > 0)
			return priceDAO.findAdvanced(Price_.division, division, Price_.product, product);
		else {
			System.out.println("Creating new price");

			Price price = new Price();
			price.setDivision(division);
			price.setProduct(product);

			return priceDAO.persist(price);
		}
	}

	/* (non-Javadoc)
	 * @see pk.home.libs.combine.service.ABaseService#persist(java.lang.Object)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public Price persist(Price o) throws Exception {
		throw new NotSupportedException();
	}
	
}
