package tradesystem.tradeunit.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pk.home.libs.combine.dao.ABaseDAO;
import pk.home.libs.combine.dao.ABaseDAO.SortOrderType;
import pk.home.libs.combine.service.ABaseService;
import tradesystem.tradeunit.dao.MeasureDAO;
import tradesystem.tradeunit.domain.Measure;
import tradesystem.tradeunit.domain.Measure.MeasureType;
import tradesystem.tradeunit.domain.Measure_;

/**
 * Service class for entity class: Measure
 * Measure - мера
 */
@Service
@Transactional
public class MeasureService extends ABaseService<Measure> {

	@Autowired
	private MeasureDAO measureDAO;

	@Override
	public ABaseDAO<Measure> getAbstractBasicDAO() {
		return measureDAO;
	}
	
	/**
	 * Measure types.
	 *
	 * @return the list
	 */
	public MeasureType[] measureTypes() {
		return Measure.MeasureType.values();
	}
	
	
	/**
	 * Measures list.
	 *
	 * @return the list
	 * @throws Exception the exception
	 */
	@Transactional(readOnly=true)
	public List<Measure> measuresList() throws Exception{
		return measureDAO.getAllEntities(Measure_.keyName, SortOrderType.ASC);
	}

}
