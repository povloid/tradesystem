package tradesystem.tradeunit.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pk.home.libs.combine.dao.ABaseDAO;
import pk.home.libs.combine.dao.ABaseTreeDAO;
import pk.home.libs.combine.service.ABaseTreeService;
import pk.home.libs.combine.service.utils.SortTreeAsCollection;
import tradesystem.tradeunit.dao.CountedAttributeDAO;
import tradesystem.tradeunit.domain.CountedAttribute;

/**
 * Service class for entity class: CountedAttribute
 * СountedAttribute - подсчитываемый атрибут
 */
@Service
@Transactional
public class CountedAttributeService extends ABaseTreeService<CountedAttribute> {

	@Autowired
	private CountedAttributeDAO countedAttributeDAO;

	@Override
	public ABaseDAO<CountedAttribute> getAbstractBasicDAO() {
		return countedAttributeDAO;
	}
	
	
	/**
	 * Find object with lazy initialization.
	 *
	 * @param id the id
	 * @return the counted attribute
	 * @throws Exception the exception
	 */
	@Transactional(readOnly= true)
	public CountedAttribute findZ(Long id) throws Exception {
		CountedAttribute countedAttribute = find(id);
		countedAttribute.getChildren().size();
		return countedAttribute;
	}


	/* (non-Javadoc)
	 * @see pk.home.libs.combine.basic.TreeFunctional#setParent(java.lang.Object, java.lang.Object)
	 */
	@Override
	public void setParent(CountedAttribute object, CountedAttribute parent)
			throws Exception {
		object.setParent(parent);
	}

	/* (non-Javadoc)
	 * @see pk.home.libs.combine.service.ABaseTreeService#getAbstractBasicTreeDAO()
	 */
	@Override
	public ABaseTreeDAO<CountedAttribute> getAbstractBasicTreeDAO() {
		return countedAttributeDAO;
	}
	
	
	/**
	 * Attributes as sorted collection.
	 *
	 * @param countedAttribute the counted attribute
	 * @param desc the desc
	 * @return the collection
	 * @throws Exception the exception
	 */
	@Transactional(readOnly= true)
	public List<CountedAttribute> attributesAsSortedCollection(
			CountedAttribute countedAttribute, boolean desc) throws Exception {

		return new SortTreeAsCollection<CountedAttribute, CountedAttribute>() {

			@Override
			protected boolean isRootElement(CountedAttribute e) {
				return e.getParent() == null;
			}

			@Override
			protected boolean isParentElement(CountedAttribute e, CountedAttribute parent) {
				return e.getParent() != null && e.getParent().equals(parent);
			}
		}.sort(attributesAsCollection(countedAttribute,	new ArrayList<CountedAttribute>()), desc);
	}

	/**
	 * Attributes as collection.
	 *
	 * @param countedAttribute the counted attribute
	 * @param collection the collection
	 * @return the collection
	 * @throws Exception the exception
	 */
	@Transactional(readOnly= true)
	public List<CountedAttribute> attributesAsCollection(CountedAttribute countedAttribute, List<CountedAttribute> collection) 
			throws Exception {
		countedAttribute = countedAttributeDAO.find(countedAttribute.getId());
		collection.add(countedAttribute);
		
		if(countedAttribute.getChildren() != null)
			for(CountedAttribute a: countedAttribute.getChildren())
				collection = attributesAsCollection(a, collection);
			
		return collection;
	}

}
