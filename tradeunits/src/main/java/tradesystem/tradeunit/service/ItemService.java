package tradesystem.tradeunit.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.transaction.NotSupportedException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pk.home.libs.combine.dao.ABaseDAO;
import pk.home.libs.combine.service.ABaseService;
import pk.home.libs.combine.service.utils.SortTreeAsCollection;
import tradesystem.tradeunit.dao.ItemDAO;
import tradesystem.tradeunit.domain.CountedAttribute;
import tradesystem.tradeunit.domain.ICountsObject;
import tradesystem.tradeunit.domain.IOwner;
import tradesystem.tradeunit.domain.Item;
import tradesystem.tradeunit.domain.Product;
import tradesystem.tradeunit.domain.Warehouse;

/**
 * Service class for entity class: Item
 * Item - запись
 */
@Service
@Transactional
public class ItemService extends ABaseService<Item> {

	@Autowired
	private ItemDAO itemDAO;
	
	@Autowired
	private ProductService productService;
	
	@Autowired
	private CountedAttributeService countedAttributeService;

	@Override
	public ABaseDAO<Item> getAbstractBasicDAO() {
		return itemDAO;
	}


	/**
	 * Sort for counted attributes.
	 *
	 * @param inList the in list
	 * @param desc the desc
	 * @return the collection
	 * @throws Exception the exception
	 */
	public List<Item> sortForCountedAttributes(Collection<Item> inList,
			boolean desc) throws Exception {
		if (inList.size() == 1)
			return new ArrayList<>(inList);
		else
			return new SortTreeAsCollection<Item, CountedAttribute>() {

				@Override
				protected boolean isRootElement(Item e)  throws Exception{
					return e.getCountedAttribute() == null || e.getCountedAttribute().getParent() == null;
				}

				@Override
				protected boolean isParentElement(Item e, Item parent) throws Exception{
					
					if(e.getTransientOwner() == null || parent.getTransientOwner() == null)
						throw new Exception("transientOwner is null!");
					
					if(e.getTransientCountsObject() == null || parent.getTransientCountsObject() == null)
						throw new Exception("transientCountsObject is null!");
					
					return e.getCountedAttribute() != null && e.getCountedAttribute().getParent() != null
							&& e.getCountedAttribute().getParent().equals(parent.getCountedAttribute())
							&& e.getTransientOwner().equals(parent.getTransientOwner()) 
							&& e.getTransientCountsObject().equals(parent.getTransientCountsObject());
				}

				
			}.sort(inList, desc);
	}	
	
	/**
	 * Product items.
	 *
	 * @param warehouse the warehouse
	 * @param product the product
	 * @return the collection
	 * @throws Exception the exception
	 */
	public List<Item> productItems(Warehouse warehouse, Product product) throws Exception {
		
		List<Item> list = new ArrayList<>();
		
		if (product.getCountedAttribute() == null)
			list.add(new Item(warehouse, product, new BigDecimal(0), new BigDecimal(0), null, ""));
		else
			for (CountedAttribute ca : countedAttributeService.attributesAsSortedCollection(product.getCountedAttribute(), false)) {
				Item item = new Item(warehouse, product, new BigDecimal(0), new BigDecimal(0), ca, ca.getKeyName());
				list.add(item);
			}
		
		return list;
	}
	
	
	
	
	/* (non-Javadoc)
	 * @see pk.home.libs.combine.service.ABaseService#merge(java.lang.Object)
	 */
	public Item merge(Item o) throws Exception {
		throw new NotSupportedException();
	}

	/* (non-Javadoc)
	 * @see pk.home.libs.combine.service.ABaseService#remove(java.lang.Object)
	 */
	public void remove(Item object) throws Exception {
		throw new NotSupportedException();
	}
	
	/**
	 * Find the item for counts object.
	 *
	 * @param items the items
	 * @param owner the owner
	 * @param io the CountsObject
	 * @return the item for counts object
	 * @throws Exception the exception
	 */
	public static Item findItemForCountsObject(Collection<Item> items, IOwner owner, ICountsObject io) throws Exception {
		
		for(Item i: items)
			if(i.getAccount().getOwner().equals(owner) && i.getAccount().getCountsObject().equals(io) 
					&& (i.getAccount().getCountedAttribute() == null || i.getAccount().getCountedAttribute().getParent() == null))
				return i;
		
		return null;
	}
	
	
	

}
