package tradesystem.tradeunit.service;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Query;
import javax.persistence.Tuple;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.SetJoin;
import javax.persistence.metamodel.SingularAttribute;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import pk.home.libs.combine.dao.ABaseDAO.SortOrderType;
import pk.home.libs.combine.service.tuple.TupleS;
import tradesystem.tradeunit.dao.AccountDAO;
import tradesystem.tradeunit.dao.PriceDAO;
import tradesystem.tradeunit.dao.ProductDAO;
import tradesystem.tradeunit.domain.Account;
import tradesystem.tradeunit.domain.Account_;
import tradesystem.tradeunit.domain.Division;
import tradesystem.tradeunit.domain.Price;
import tradesystem.tradeunit.domain.Price_;
import tradesystem.tradeunit.domain.Product;
import tradesystem.tradeunit.domain.ProductGroup;
import tradesystem.tradeunit.domain.ProductGroup_;
import tradesystem.tradeunit.domain.Product_;

/**
 * The Class CommonInfoService.
 * 
 * @author povloid
 * 
 * @date 7/19/2013
 */
@Service
@Transactional
public class CommonInfoService {
	
	@Autowired
	private AccountDAO accountDAO;

	@Autowired
	private PriceDAO priceDAO;

	@Autowired
	private ProductDAO productDAO;

	/**
	 * Products in division.
	 *
	 * @param division the division
	 * @param productGroup the product group
	 * @param enableForProductGroup the enable for product group
	 * @param all the all
	 * @param firstResult the first result
	 * @param maxResults the max results
	 * @param orderByAttribute the order by attribute
	 * @param sortOrder the sort order
	 * @return the list
	 * @throws Exception the exception
	 */
	@Transactional(readOnly = true)
	public List<TupleS> productsInDivision(Division division, ProductGroup productGroup, boolean enableForProductGroup, boolean all,
			int firstResult, int maxResults,
			SingularAttribute<?, ?> orderByAttribute, SortOrderType sortOrder)
			throws Exception {

		CriteriaBuilder cb = productDAO.getEntityManager().getCriteriaBuilder();

		CriteriaQuery<Tuple> cq = cb.createTupleQuery();

		// FROM MAIN TABLE
		Root<Product> tProduct = cq.from(Product.class);

		// JOINS
		
		SetJoin<Product, Price> joinPrice = tProduct.join(Product_.prices,
				JoinType.LEFT);

		cq.multiselect(
				// Product
				tProduct.get(Product_.id).alias(
						"product." + Product_.id.getName()),
				tProduct.get(Product_.barcode).alias(
						"product." + Product_.barcode.getName()),
				tProduct.get(Product_.keyName).alias(
						"product." + Product_.keyName.getName()),
				tProduct.get(Product_.denominator).alias(
						"product." + Product_.denominator.getName()),
				tProduct.get(Product_.description).alias(
						"product." + Product_.description.getName()),
				tProduct.get(Product_.mainImage).alias(
						"product." + Product_.mainImage.getName()),
				tProduct.get(Product_.productGroup).get(ProductGroup_.path).alias(
								"product." + ProductGroup_.path.getName()),
				// Price
				joinPrice.get(Price_.id).alias("price." + Price_.id.getName()),
				joinPrice.get(Price_.value).alias(
						"price." + Price_.value.getName()),
				joinPrice.get(Price_.description).alias(
						"price." + Price_.description.getName())						
						);

		
		
		// WHERE
		Predicate main = cb.and(cb.or(
				cb.equal(joinPrice.get(Price_.division), division),
				cb.isNull(joinPrice.get(Price_.division))));
		
		main = enableForProductGroup ?  
				productGroup != null ? cb.and(main,cb.equal(tProduct.get(Product_.productGroup), productGroup)) 
						:  cb.and(main,cb.isNull(tProduct.get(Product_.productGroup)))
				: main;
						
		cq.where(main);

		
		
		
		// ORDER BY
		Expression<?> orderBy = null;

		Class<?> cl = orderByAttribute.getDeclaringType().getJavaType();

		if (cl.equals(Product.class)) {
			orderBy = tProduct.get(orderByAttribute.getName());
		} else if (cl.equals(Price.class)) {
			orderBy = joinPrice.get(orderByAttribute.getName());
		}		
		
		return addAccountsSums(getAllEntities(all, firstResult, maxResults, orderBy, sortOrder,
				cb, cq, tProduct), "product." + Product_.id.getName(), division);

	}

	/**
	 * Products in division count.
	 *
	 * @param division the division
	 * @param productGroup the product group
	 * @param enableForProductGroup the enable for product group
	 * @return the long
	 * @throws Exception the exception
	 */
	public long productsInDivisionCount(Division division, ProductGroup productGroup, boolean enableForProductGroup) throws Exception {

		CriteriaBuilder cb = productDAO.getEntityManager().getCriteriaBuilder();

		CriteriaQuery<Tuple> cq = cb.createTupleQuery();

		// FROM MAIN TABLE
		Root<Product> tProduct = cq.from(Product.class);

		// JOINS
		SetJoin<Product, Price> joinPrice = tProduct.join(Product_.prices,
				JoinType.LEFT);
		//SetJoin<Product, Account> joinAccount = tProduct.join(
		//		Product_.accounts, JoinType.LEFT);

		// WHERE
		Predicate main = cb.and(cb.or(
				cb.equal(joinPrice.get(Price_.division), division),
				cb.isNull(joinPrice.get(Price_.division))));

		
		main = enableForProductGroup ?  
				productGroup != null ? cb.and(main,cb.equal(tProduct.get(Product_.productGroup), productGroup)) 
						:  cb.and(main,cb.isNull(tProduct.get(Product_.productGroup)))
				: main;
		
		cq.where(main);

		return count(tProduct, cq);
	}
	
	
	/**
	 * Add accounts sums.
	 *
	 * @param list the list
	 * @param field the field
	 * @param division the division
	 * @return the list
	 * @throws Exception the exception
	 */
	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public List<TupleS> addAccountsSums(List<TupleS> list, String field, Division division) throws Exception {
		
		// Validation
		if(list.size() == 0)
			return list;
		
		
		// Code
		CriteriaBuilder cb = productDAO.getEntityManager().getCriteriaBuilder();
		
		CriteriaQuery<Tuple> cq = cb.createTupleQuery();
		
		Root<Account> tAccount = cq.from(Account.class);
		
		cq.multiselect(tAccount.get(Account_.product).get(Product_.id).alias(field),
			cb.sum(tAccount.get(Account_.value)).alias(Account_.value.getName() + ".sum"),
			cb.sum(tAccount.get(Account_.numerator)).alias(Account_.numerator.getName() + ".sum"));
		
		cq.groupBy(tAccount.get(Account_.product).get(Product_.id)
				);
		
		// Predicates
		
		List<Object> values = new ArrayList<>();
		for(TupleS ts : list)
			values.add(ts.get(field));
		
		cq.where(cb.isNull(tAccount.get(Account_.parentAccount)),
				cb.equal(tAccount.get(Account_.division), division),
				tAccount.get(Account_.product).get(Product_.id).in(values));
		
		return TupleS.merge(list, getAllEntities(true, -1, -1 , cq) , field);
	}
	

	/**
	 * Gets the all entities.
	 * 
	 * @param all
	 *            the all
	 * @param firstResult
	 *            the first result
	 * @param maxResults
	 *            the max results
	 * @param orderBy
	 *            the order by
	 * @param sortOrder
	 *            the sort order
	 * @param cb
	 *            the cb
	 * @param cq
	 *            the cq
	 * @param t
	 *            the t
	 * @return the all entities
	 * @throws Exception
	 *             the exception
	 */
	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public List<TupleS> getAllEntities(boolean all, int firstResult,
			int maxResults, Expression<?> orderBy, SortOrderType sortOrder,
			CriteriaBuilder cb, CriteriaQuery<Tuple> cq, Root<?> t)
			throws Exception {

		// order by ------------------------------------------
		if (orderBy != null) {
			switch (sortOrder) {
			case DESC:
				cq.orderBy(cb.desc(orderBy));
				break;
			case ASC:
				cq.orderBy(cb.asc(orderBy));
				break;
			default:
				cq.orderBy(cb.asc(orderBy));
				break;
			}
		}
		return getAllEntities(all, firstResult, maxResults, cq);
	}

	/**
	 * Gets the all entities.
	 * 
	 * @param all
	 *            the all
	 * @param firstResult
	 *            the first result
	 * @param maxResults
	 *            the max results
	 * @param cq
	 *            the cq
	 * @return the all entities
	 * @throws Exception
	 *             the exception
	 */
	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public List<TupleS> getAllEntities(boolean all, int firstResult,
			int maxResults, CriteriaQuery<Tuple> cq) throws Exception {

		// create query
		// ----------------------------------------------------------------------------
		TypedQuery<Tuple> q = productDAO.getEntityManager().createQuery(cq);

		if (!all) {
			q.setMaxResults(maxResults);
			q.setFirstResult(firstResult >= 0 ? firstResult : 0);
		}

		return TupleS.convert(q.getResultList());
	}

	/**
	 * Count.
	 * 
	 * @param rt
	 *            the rt
	 * @param cq
	 *            the cq
	 * @return the long
	 * @throws Exception
	 *             the exception
	 */
	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public long count(Root<?> rt, CriteriaQuery<Tuple> cq) throws Exception {
		cq.multiselect(productDAO.getEntityManager().getCriteriaBuilder()
				.count(rt));
		Query q = productDAO.getEntityManager().createQuery(cq);
		return ((Long) ((Tuple) q.getSingleResult()).get(0)).longValue();
	}

}
