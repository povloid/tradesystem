package tradesystem.tradeunit.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pk.home.libs.combine.dao.ABaseDAO;
import pk.home.libs.combine.service.ABaseService;
import tradesystem.tradeunit.dao.WarehouseDAO;
import tradesystem.tradeunit.domain.Warehouse;

/**
 * Service class for entity class: Warehouse
 * Warehouse - склад
 */
@Service
@Transactional
public class WarehouseService extends ABaseService<Warehouse> {

	@Autowired
	private WarehouseDAO warehouseDAO;

	@Override
	public ABaseDAO<Warehouse> getAbstractBasicDAO() {
		return warehouseDAO;
	}

}
