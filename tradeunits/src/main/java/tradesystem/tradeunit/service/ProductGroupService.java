package tradesystem.tradeunit.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pk.home.libs.combine.dao.ABaseDAO;
import pk.home.libs.combine.dao.ABaseTreeDAO;
import pk.home.libs.combine.service.ABaseTreeService;
import tradesystem.tradeunit.dao.ProductGroupDAO;
import tradesystem.tradeunit.domain.ProductGroup;

/**
 * Service class for entity class: ProductGroup ProductGroup - группа продуктов
 */
@Service
@Transactional
public class ProductGroupService extends ABaseTreeService<ProductGroup> {

	@Autowired
	private ProductGroupDAO productGroupDAO;

	@Override
	public ABaseDAO<ProductGroup> getAbstractBasicDAO() {
		return productGroupDAO;
	}

	@Override
	public void setParent(ProductGroup object, ProductGroup parent)
			throws Exception {
		object.setParent(parent);
	}

	@Override
	public ABaseTreeDAO<ProductGroup> getAbstractBasicTreeDAO() {
		return productGroupDAO;
	}

}
