package tradesystem.tradeunit.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.ExceptionHandler;

import pk.home.libs.combine.dao.ABaseDAO;
import pk.home.libs.combine.dao.ABaseDAO.SortOrderType;
import pk.home.libs.combine.dao.PredicatePair;
import pk.home.libs.combine.service.ABaseService;
import tradesystem.tradeunit.dao.PrivilegesDAO;
import tradesystem.tradeunit.domain.Cash;
import tradesystem.tradeunit.domain.CashPrivileges;
import tradesystem.tradeunit.domain.CommonPrivilegesTypes;
import tradesystem.tradeunit.domain.Division;
import tradesystem.tradeunit.domain.Privileges;
import tradesystem.tradeunit.domain.Privileges_;
import tradesystem.tradeunit.domain.Warehouse;
import tradesystem.tradeunit.domain.WarehousePrivileges;
import tradesystem.tradeunit.domain.security.UserAccount;

/**
 * Service class for entity class: Privileges Privileges - привелегии
 */
@Service
@Transactional
public class PrivilegesService extends ABaseService<Privileges> {

	@Autowired
	private PrivilegesDAO privilegesDAO;

	@Override
	public ABaseDAO<Privileges> getAbstractBasicDAO() {
		return privilegesDAO;
	}

	/**
	 * Get common function
	 * @param userAccount
	 * @param division
	 * @param predicatePair
	 * @return
	 * @throws Exception
	 */
	@Transactional(readOnly = true)
	public List<Privileges> getPrivileges(UserAccount userAccount,
			Division division, PredicatePair<Privileges> predicatePair)
			throws Exception {

		List<PredicatePair<Privileges>> pplist = new ArrayList<>();
		pplist.add(predicatePair);

		return privilegesDAO.getAllEntitiesAdvanced(pplist, true, -1, -1,
				Privileges_.commonPrivilegesTypes, SortOrderType.ASC);

	}

	
	/**
	 * Get warehouse privileges
	 * @param userAccount
	 * @param division
	 * @param warehouse
	 * @return
	 * @throws Exception
	 */
	@Transactional(readOnly = true)
	public List<Privileges> getType(UserAccount userAccount, Division division,
			Warehouse warehouse) throws Exception {

		PredicatePair<Privileges> predicatePair = new PredicatePair<>(
				Privileges_.warehouse, warehouse);

		return getPrivileges(userAccount, division, predicatePair);
	}

	/**
	 * Get cash privileges
	 * @param userAccount
	 * @param division
	 * @param cash
	 * @return
	 * @throws Exception
	 */
	@Transactional(readOnly = true)
	public List<Privileges> getType(UserAccount userAccount, Division division,
			Cash cash) throws Exception {

		PredicatePair<Privileges> predicatePair = new PredicatePair<>(
				Privileges_.cash, cash);

		return getPrivileges(userAccount, division, predicatePair);
	}

	/**
	 * Get common privileges
	 * @param userAccount
	 * @param division
	 * @param price
	 * @return
	 * @throws Exception
	 */
	@Transactional(readOnly = true)
	public List<Privileges> getType(UserAccount userAccount, Division division,
			CommonPrivilegesTypes commonPrivilegesTypes) throws Exception {

		PredicatePair<Privileges> predicatePair = new PredicatePair<>(
				Privileges_.commonPrivilegesTypes, commonPrivilegesTypes);

		return getPrivileges(userAccount, division, predicatePair);
	}

	
	
	/**
	 * Save.
	 *
	 * @param list the list
	 * @return the list
	 * @throws Exception the exception
	 */
	@ExceptionHandler(Exception.class)
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public void save(List<Privileges> list) throws Exception {
		for (Privileges p : list)
			if (p.getId() != null)
				if (!p.isAllow()) {

					p = privilegesDAO.find(p.getId());

					if (p != null)
						privilegesDAO.remove(p);
				} else
					p = privilegesDAO.merge(p);

			else if (p.isAllow())
				p = privilegesDAO.persist(p);
	}
	
	
	
	/**
	 * Gets the privileges for warehouse type.
	 *
	 * @param userAccount the user account
	 * @param division the division
	 * @param warehousePrivileges the warehouse privileges
	 * @return the privileges for warehouse type
	 * @throws Exception the exception
	 */
	@Transactional(readOnly = true)
	public List<Privileges> getPrivilegesForWarehouseType(
			UserAccount userAccount, Division division,
			WarehousePrivileges warehousePrivileges) throws Exception {

		List<PredicatePair<Privileges>> pplist = new ArrayList<>();

		pplist.add(new PredicatePair<>(Privileges_.userAccount , userAccount));
		pplist.add(new PredicatePair<>(Privileges_.division, division));

		pplist.add(new PredicatePair<>(Privileges_.warehousePrivileges,
				warehousePrivileges));

		return privilegesDAO.getAllEntitiesAdvanced(pplist, true, -1, -1,
				Privileges_.id, SortOrderType.ASC);
	}
	
	// -----------------------------------------------------------------------------------------------------
	
	/**
	 * Checks if is accessibly.
	 *
	 * @param userAccount the user account
	 * @param division the division
	 * @param pplist the pplist
	 * @return true, if is accessibly
	 * @throws Exception the exception
	 */
	@Transactional(readOnly = true)
	public boolean isAccessibly(UserAccount userAccount, Division division, 
			List<PredicatePair<Privileges>> pplist
			) throws Exception {
		
		pplist.add(new PredicatePair<>(Privileges_.userAccount , userAccount));
		pplist.add(new PredicatePair<>(Privileges_.division, division));
		
		List<Privileges> list =  privilegesDAO.getAllEntitiesAdvanced(pplist, true, -1, -1,
				Privileges_.id, SortOrderType.ASC);
		
		// Validation !
		if(list != null && list.size() > 1)
			throw new Exception("Returned rows more then one");
		
		return list != null && list.size() == 1 && list.get(0).isAllow() ? true : false;
	
	}
	
	
	/**
	 * As accessibly.
	 *
	 * @param userAccount the user account
	 * @param division the division
	 * @param commonPrivilegesTypes the common privileges types
	 * @return true, if successful
	 * @throws Exception the exception
	 */
	@Transactional(readOnly = true)
	public boolean isAccessibly(UserAccount userAccount, Division division,
			CommonPrivilegesTypes  commonPrivilegesTypes
			) throws Exception {
		
		List<PredicatePair<Privileges>> pplist = new ArrayList<>();

		pplist.add(new PredicatePair<>(Privileges_.commonPrivilegesTypes,
				commonPrivilegesTypes));
		
		return isAccessibly(userAccount, division, pplist);
	}
	
	
	/**
	 * As accessibly.
	 *
	 * @param userAccount the user account
	 * @param division the division
	 * @param cash the cash
	 * @param cashPrivileges the cash privileges
	 * @return true, if successful
	 * @throws Exception the exception
	 */
	@Transactional(readOnly = true)
	public boolean isAccessibly(UserAccount userAccount, Division division,
			Cash cash, CashPrivileges cashPrivileges 
			) throws Exception {
		
		List<PredicatePair<Privileges>> pplist = new ArrayList<>();


		pplist.add(new PredicatePair<>(Privileges_.cash, cash));
		pplist.add(new PredicatePair<>(Privileges_.cashPrivileges,
				cashPrivileges));
		
		return isAccessibly(userAccount, division, pplist);
	}
	
	/**
	 * As accessibly.
	 *
	 * @param userAccount the user account
	 * @param division the division
	 * @param warehouse the warehouse
	 * @param warehousePrivileges the warehouse privileges
	 * @return true, if successful
	 * @throws Exception the exception
	 */
	@Transactional(readOnly = true)
	public boolean isAccessibly(UserAccount userAccount, Division division,
			Warehouse warehouse, WarehousePrivileges warehousePrivileges
			) throws Exception {
		
		List<PredicatePair<Privileges>> pplist = new ArrayList<>();

		pplist.add(new PredicatePair<>(Privileges_.warehouse, warehouse));
		pplist.add(new PredicatePair<>(Privileges_.warehousePrivileges,
				warehousePrivileges));
		
		return isAccessibly(userAccount, division, pplist);
	}
	
	// -----------------------------------------------------------------------------------------------------
	
	/**
	 * Check access.
	 *
	 * @param userAccount the user account
	 * @param division the division
	 * @param commonPrivilegesTypes the common privileges types
	 * @throws Exception the exception
	 */
	@Transactional
	public void checkAccess(UserAccount userAccount, Division division,
			CommonPrivilegesTypes  commonPrivilegesTypes) throws Exception {
		if(!isAccessibly(userAccount, division, commonPrivilegesTypes))
			throw new Exception("access denied!");
	}
	
	/**
	 * Check access.
	 *
	 * @param userAccount the user account
	 * @param division the division
	 * @param cash the cash
	 * @param cashPrivileges the cash privileges
	 * @throws Exception the exception
	 */
	@Transactional
	public void checkAccess(UserAccount userAccount, Division division,
			Cash cash, CashPrivileges cashPrivileges) throws Exception {
		if(!isAccessibly(userAccount, division, cash, cashPrivileges))
			throw new Exception("access denied!");
	}
	
	/**
	 * Check access.
	 *
	 * @param userAccount the user account
	 * @param division the division
	 * @param commonPrivilegesTypes the common privileges types
	 * @throws Exception the exception
	 */
	@Transactional
	public void checkAccess(UserAccount userAccount, Division division,
			Warehouse warehouse, WarehousePrivileges warehousePrivileges) throws Exception {
		if(!isAccessibly(userAccount, division, warehouse, warehousePrivileges))
			throw new Exception("access denied!");
	}
	
	// common static methods ---------------------------------------------------------------------------
	
	
	
	
	/**
	 * Warehouses from privileges.
	 *
	 * @param privileges the privileges
	 * @return the list
	 */
	public static List<Warehouse> warehousesFromPrivileges(List<Privileges> privileges){
		
		List<Warehouse> wList = new ArrayList<>();

		for(Privileges p: privileges)
			if(!wList.contains(p.getWarehouse()))
				wList.add(p.getWarehouse());
		
		return wList;
	}
	
	/**
	 * Cashes from privileges.
	 *
	 * @param privileges the privileges
	 * @return the list
	 */
	public static List<Cash> cashesFromPrivileges(List<Privileges> privileges){
		
		List<Cash> cList = new ArrayList<>();

		for(Privileges p: privileges)
			if(!cList.contains(p.getCash()))
				cList.add(p.getCash());
		
		return cList;
	}
	
	
	
	
}
