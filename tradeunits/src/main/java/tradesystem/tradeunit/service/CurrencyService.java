package tradesystem.tradeunit.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pk.home.libs.combine.dao.ABaseDAO;
import pk.home.libs.combine.service.ABaseService;
import tradesystem.tradeunit.dao.CurrencyDAO;
import tradesystem.tradeunit.domain.Currency;
import tradesystem.tradeunit.domain.Currency_;

/**
 * Service class for entity class: Currency
 * Currency - валюта
 */
@Service
@Transactional
public class CurrencyService extends ABaseService<Currency> {

	@Autowired
	private CurrencyDAO currencyDAO;

	@Override
	public ABaseDAO<Currency> getAbstractBasicDAO() {
		return currencyDAO;
	}
	
	
	/**
	 * Main currency.
	 *
	 * @return the currency
	 * @throws Exception the exception
	 */
	@Transactional(readOnly=true)
	public Currency mainCurrency() throws Exception {
		return findAdvanced(Currency_.main, true);
	}

}
