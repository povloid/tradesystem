package tradesystem.tradeunit.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pk.home.libs.combine.dao.ABaseDAO;
import pk.home.libs.combine.service.ABaseService;
import pk.home.libs.combine.service.DefaultPathDateFileService;
import tradesystem.tradeunit.dao.ImageDAO;
import tradesystem.tradeunit.domain.Image;


/**
 * The Class ImageService.
 */
@Service
@Transactional
public class ImageService extends ABaseService<Image> {

	@Autowired
	private DefaultPathDateFileService defaultPathDateFileService;
	
	@Autowired
	private ImageDAO imageDAO;

	@Override
	public ABaseDAO<Image> getAbstractBasicDAO() {
		return imageDAO;
	}

}
