package tradesystem.tradeunit.service;

import java.io.InputStream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.ExceptionHandler;

import pk.home.libs.combine.dao.ABaseDAO;
import pk.home.libs.combine.service.ABaseService;
import pk.home.libs.combine.service.DefaultPathDateFileService;
import tradesystem.tradeunit.dao.ProductDAO;
import tradesystem.tradeunit.domain.Image;
import tradesystem.tradeunit.domain.Product;

/**
 * Service class for entity class: Product
 * Product - продукт
 */
@Service
@Transactional
public class ProductService extends ABaseService<Product> {

	@Autowired
	private DefaultPathDateFileService defaultPathDateFileService;
	
	@Autowired
	private ProductDAO productDAO;
	
	@Autowired ImageService imageService;

	@Override
	public ABaseDAO<Product> getAbstractBasicDAO() {
		return productDAO;
	}
	
	
	/**
	 * Find product with lazy object
	 * 
	 * @param key
	 * @return
	 * @throws Exception
	 */
	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public Product findLazy(Object key) throws Exception {
		Product product = super.find(key);
		product.getImages().size();
		return product;
	}



	/**
	 * Save image.
	 *
	 * @param product the product
	 * @param fileName the file name
	 * @param inputStream the input stream
	 * @throws Exception the exception
	 */
	@ExceptionHandler(Exception.class)
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public Product saveImage(Product product, String fileName,
			InputStream inputStream) throws Exception {
		
		String filepath = defaultPathDateFileService.saveToFile(inputStream, 
				Product.IMAGESTORE_PREFIX, fileName);

		product = productDAO.find(product.getId());
		product.getImages().add(imageService.persist(new Image(filepath)));
		return productDAO.merge(product);
	}
	
	
	/**
	 * Save image.
	 * 
	 * @param product
	 *            the product
	 * @param fileName
	 *            the file name
	 * @param inputStream
	 *            the input stream
	 * @return the product
	 * @throws Exception
	 *             the exception
	 */
	@ExceptionHandler(Exception.class)
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public Product removeImage(Product product, Image image) throws Exception {

		product.getImages().remove(image);
		product = productDAO.merge(product); 
				
		imageService.remove(image);
		
		return product;
	}

}
