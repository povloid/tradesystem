package tradesystem.tradeunit.service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import javax.transaction.NotSupportedException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.ExceptionHandler;

import pk.home.libs.combine.dao.ABaseDAO;
import pk.home.libs.combine.dao.PredicatePair;
import pk.home.libs.combine.service.ABaseService;
import pk.home.libs.combine.service.utils.SortTreeAsCollection;
import tradesystem.tradeunit.dao.AccountDAO;
import tradesystem.tradeunit.domain.Account;
import tradesystem.tradeunit.domain.Account_;
import tradesystem.tradeunit.domain.Cash;
import tradesystem.tradeunit.domain.CountedAttribute;
import tradesystem.tradeunit.domain.Currency;
import tradesystem.tradeunit.domain.Customer;
import tradesystem.tradeunit.domain.Division;
import tradesystem.tradeunit.domain.ICountsObject;
import tradesystem.tradeunit.domain.IOwner;
import tradesystem.tradeunit.domain.Measure.MeasureType;
import tradesystem.tradeunit.domain.Product;
import tradesystem.tradeunit.domain.Warehouse;

/**
 * Service class for entity class: Account Account - счет.
 */
@Service
@Transactional
public class AccountService extends ABaseService<Account> {

	@Autowired
	private AccountDAO accountDAO;
	
	@Autowired
	private CountedAttributeService countedAttributeService;

	@Override
	public ABaseDAO<Account> getAbstractBasicDAO() {
		return accountDAO;
	}

	
	/**
	 * Sort accounts.
	 *
	 * @param inList the in list
	 * @param desc the desc
	 * @return the list
	 * @throws Exception the exception
	 */
	public List<Account> sortAccounts(Collection<Account> inList,
			boolean desc) throws Exception {
		
		return new SortTreeAsCollection<Account,Account>() {

			@Override
			protected boolean isRootElement(Account e) {
				return e.getParentAccount() == null;
			}

			@Override
			protected boolean isParentElement(Account e, Account parent) {
				return e.getParentAccount() != null && e.getParentAccount().equals(parent);
			}

		}.sort(inList, desc);
	}
	
	
	/**
	 * Find accounts.
	 *
	 * @param division the division
	 * @param countsObject the counts object
	 * @param owner the owner
	 * @param desc the desc
	 * @return the list
	 * @throws Exception the exception
	 */
	public List<Account> findAccounts(Division division, ICountsObject countsObject,
			IOwner owner, boolean desc) throws Exception {
		
		List<Account> list = treeToList(findAccount(division, countsObject, owner));
		
		if(desc)
			Collections.reverse(list);
		
		 return list;
	}
	
	/**
	 * Tree to list.
	 *
	 * @param account the account
	 * @return the list
	 * @throws Exception the exception
	 */
	private List<Account> treeToList(Account account) throws Exception {
		List<Account> accounts = new ArrayList<>();
		
		accounts.add(account);
		if(!CollectionUtils.isEmpty(account.getSubAccounts()))
			for(Account a: account.getSubAccounts())
				accounts.addAll(treeToList(a));
		
		return accounts;
	}
	
	
	
	/**
	 * Find account.
	 *
	 * @param division the division
	 * @param countsObject the counts object
	 * @param owner the owner
	 * @return the account
	 * @throws Exception the exception
	 */
	@Transactional(propagation = Propagation.REQUIRED)
	@ExceptionHandler(Exception.class)
	public Account findAccount(Division division, ICountsObject countsObject,
			IOwner owner) throws Exception {
		return findAccount(division, countsObject, owner, null, null);
	}
	
	
	/**
	 * Find account.
	 *
	 * @param division the division
	 * @param countsObject the owner
	 * @param owner the owner
	 * @param countedAttribute the counted attribute
	 * @param parentAccount the parent account
	 * @return the account
	 * @throws Exception the exception
	 */
	@Transactional(propagation = Propagation.REQUIRED)
	@ExceptionHandler(Exception.class)
	public Account findAccount(Division division, ICountsObject countsObject,
			IOwner owner, CountedAttribute countedAttribute, Account parentAccount) throws Exception {

		System.out.println("find account");
		
		Account account = null;
		
		// first - find the account
		

		List<PredicatePair<Account>> pplist = new ArrayList<>();

		// Division
		pplist.add(new PredicatePair<>(Account_.division, division));
		
		// For counts object
		if (countsObject instanceof Product)
			pplist.add(new PredicatePair<>(Account_.product, countsObject));
		else if (countsObject instanceof Currency)
			pplist.add(new PredicatePair<>(Account_.currency, countsObject));

		// For owner
		if (owner instanceof Warehouse)
			pplist.add(new PredicatePair<>(Account_.warehouse, owner));
		else if (owner instanceof Cash) 
			pplist.add(new PredicatePair<>(Account_.cash, owner));
		else if (owner instanceof Customer)
			pplist.add(new PredicatePair<>(Account_.customer, owner));

		pplist.add(new PredicatePair<>(Account_.countedAttribute,
				parentAccount == null ? countsObject.getCountedAttribute()
						: countedAttribute));

		pplist.add(new PredicatePair<>(Account_.parentAccount, parentAccount));

		if (accountDAO.countAdvanced(pplist) > 0){	
			account = accountDAO.findAdvanced(pplist);
		} else {
			System.out
					.println(">>>>> Account not found. Well be create new account.");
			
			
			System.out.println(" division: " + division.getKeyName() + " -> " + division
							+ "\n countsObject: " + countsObject.getKeyName() + " -> " + countsObject
							+ "\n owner: "	+ owner.getKeyName() +  " -> " + owner  
							+ "\n coutedAttribute: " + (countedAttribute != null? countedAttribute.getKeyName() : null)
							+ "\n parentAccount: " + (parentAccount != null ? parentAccount.getId() : null));
			
			// second - if account not found
			account = new Account();
			account.setDivision(division);
			account.setCountsObject(countsObject);
			account.setValue(new BigDecimal(0));
			account.setNumerator(new BigDecimal(0));
			account.setOwner(owner);
			account.setParentAccount(parentAccount);
			
			account.setCountedAttribute(parentAccount == null ? countsObject
					.getCountedAttribute() : countedAttribute);
			
			account = accountDAO.persist(account);
			// 
		}

		// sub accounts
		if (countsObject.getCountedAttribute() != null )
			for (CountedAttribute ca : parentAccount == null ? countedAttributeService.find(countsObject.getCountedAttribute().getId()).getChildren()  
					: countedAttributeService.find(countedAttribute.getId()).getChildren()) {
				Account subAccount = findAccount(division, countsObject, owner,
						ca, account); // recursive call =>

				if (!account.getSubAccounts().contains(subAccount))
					account.getSubAccounts().add(subAccount);
			}
		

		return account;
	}

	/**
	 * addition account + value and numerator
	 * 
	 * @param account
	 * @param value
	 * @param numerator
	 * @return
	 * @throws Exception
	 */
	@Transactional(propagation = Propagation.SUPPORTS)
	@ExceptionHandler(Exception.class)
	public Account add(Account account, BigDecimal value, BigDecimal numerator)
			throws Exception {

		// Input validation 
		if (account == null)
			throw new Exception("account is null");
		
		// Find account for checking with lazy elements 
		account = accountDAO.find(account.getId());

		// Remember old values
		BigDecimal oldValue = account.getValue();
		BigDecimal oldNumerator = account.getNumerator();
		
		// Add values to account -> (+) 
		account = addCommonOperation(account, value, numerator);
		
		// sub accounts sum checking
		if (!account.getSubAccounts().isEmpty()) {
			// surrogate account
			Account summAcount = new Account();
			summAcount.setCountsObject(account.getCountsObject());	
			summAcount.setValue(account.getCountsObject().getValueType() == MeasureType.FLOAT ?
					new BigDecimal(0).setScale(Account.SCALE, RoundingMode.HALF_UP) : new BigDecimal(0));
			summAcount.setNumerator(new BigDecimal(0));

			for (Account sa : account.getSubAccounts())
				summAcount = addCommonOperation(summAcount, sa.getValue(),
						sa.getNumerator());

			if (!(account.getValue().compareTo(summAcount.getValue()) == 0 && account
					.getNumerator().compareTo(summAcount.getNumerator()) == 0)){
				
				String errorString = "Account value is not equal sum of subaccoutns values!"
						+ "\n\tvalue: " + account.getValue() + " <-> " + summAcount.getValue()
						+ "\n\tnumerator: " + account.getNumerator() +  " <-> " + summAcount.getNumerator() + "\n";
				
				// If error then set old values
				account.setValue(oldValue);
				account.setNumerator(oldNumerator);
				
				throw new Exception(errorString);
			}
		}
		

		return accountDAO.merge(account);
	}

	

	
	/**
	 * Adds the common operation.
	 *
	 * @param account the account
	 * @param value the value
	 * @param numerator the numerator
	 * @return the account
	 * @throws Exception the exception
	 */
	private static Account addCommonOperation(Account account,
			BigDecimal value, BigDecimal numerator) throws Exception {
		
		if (account == null)
			throw new Exception("account is null");
		
		// if null then 0
		value = value != null ? value : new BigDecimal(0);
		numerator = numerator != null ? numerator : new BigDecimal(0);

		switch (account.getCountsObject().getValueType()) {
		case INTEGER:
			if (value.remainder(new BigDecimal(1)).compareTo(new BigDecimal(0)) != 0)
				throw new Exception("Value is not integer!");
			account.setValue(account.getValue().add(value));
			break;
		case FLOAT:
			account.setValue(account.getValue().add(value.setScale(Account.SCALE, RoundingMode.HALF_UP)));
			break;
		case FRACTION:
			if (value.remainder(new BigDecimal(1)).compareTo(new BigDecimal(0)) != 0)
				throw new Exception("Value for fraction is not integer!");
			if (numerator.remainder(new BigDecimal(1)).compareTo(
					new BigDecimal(0)) != 0)
				throw new Exception("Numerator for fraction is not integer!");

			addFrac(account, value, numerator);
			break;

		default:
			throw new Exception("No defined operation for "
					+ account.getProduct().getMeasure().getMeasureType() + ".");

		}

		return account;
	}
	
	/**
	 * Adds the fraction.
	 * 
	 * @param account
	 *            the account
	 * @param value
	 *            the value
	 * @param numerator
	 *            the numerator
	 * @return the account
	 * @throws Exception
	 *             the exception
	 */
	private static Account addFrac(Account account, BigDecimal value,
			BigDecimal numerator) throws Exception {

		if (account == null)
			throw new Exception("account is null");

		if (value == null)
			throw new Exception("value is null");

		if (numerator == null)
			throw new Exception("numerator is null");

		account.setValue(account.getValue().add(value));

		BigDecimal a = account.getNumerator().add(numerator);
		BigDecimal v = a.divideToIntegralValue(account.getCountsObject()
				.getDenominator());
		BigDecimal n = a.remainder(account.getCountsObject().getDenominator());

		account.setValue(account.getValue().add(v));
		account.setNumerator(n);

		System.out.println("account " + account.getValue() + " "
				+ account.getNumerator() + "/"
				+ account.getCountsObject().getDenominator());

		if (account.getValue().signum() == 0
				|| account.getNumerator().signum() == 0
				|| account.getValue().signum() == account.getNumerator()
						.signum())
			return account;
		else {
			return addFrac(account, new BigDecimal(1 * account.getNumerator()
					.signum()), account.getCountsObject().getDenominator()
					.multiply(new BigDecimal(1 * account.getValue().signum())));
		}
	}

	@Override
	public Account persist(Account o) throws Exception {
		throw new NotSupportedException("Operation not supported");
	}

	@Override
	public Account merge(Account o) throws Exception {
		throw new NotSupportedException("Operation not supported");
	}

	@Override
	public void remove(Account object) throws Exception {
		throw new NotSupportedException("Operation not supported");
	}

}
