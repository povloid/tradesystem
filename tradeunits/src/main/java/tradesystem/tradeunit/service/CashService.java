package tradesystem.tradeunit.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pk.home.libs.combine.dao.ABaseDAO;
import pk.home.libs.combine.service.ABaseService;
import tradesystem.tradeunit.dao.CashDAO;
import tradesystem.tradeunit.domain.Cash;

/**
 * Service class for entity class: Cash
 * Cash - касса
 */
@Service
@Transactional
public class CashService extends ABaseService<Cash> {

	@Autowired
	private CashDAO cashDAO;

	@Override
	public ABaseDAO<Cash> getAbstractBasicDAO() {
		return cashDAO;
	}

}
