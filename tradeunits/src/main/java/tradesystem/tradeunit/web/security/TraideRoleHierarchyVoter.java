package tradesystem.tradeunit.web.security;

import java.util.Collection;

import org.apache.log4j.Logger;
import org.springframework.security.access.ConfigAttribute;
import org.springframework.security.access.hierarchicalroles.RoleHierarchy;
import org.springframework.security.access.vote.RoleHierarchyVoter;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.webflow.engine.Flow;

import tradesystem.tradeunit.domain.security.UserAccount;
import tradesystem.tradeunit.service.PrivilegesService;
import tradesystem.tradeunit.service.security.UserAccountService;

/**
 * 
 * Class TraideRoleHierarchyVoter
 *
 * @author Kopychenko Pavel
 *
 * @date 26 окт. 2013 г.
 *
 */
public class TraideRoleHierarchyVoter extends RoleHierarchyVoter {


	private static final Logger LOG = Logger.getLogger(TraideRoleHierarchyVoter.class);
	
	private UserAccountService UserAccountService;
	
	private PrivilegesService privilegesService;
	
	public TraideRoleHierarchyVoter(RoleHierarchy roleHierarchy) {
		super(roleHierarchy);
	}

	@Override
	public int vote(Authentication authentication, Object object,
			Collection<ConfigAttribute> attributes) {
		
		
//		System.out.println("-----------------------------------------------");
//		System.out.println("VOTE>>> " + authentication);
//		System.out.println("VOTE>>> " + authentication.getName());
//		System.out.println("VOTE>>> " + authentication.getDetails());
//		
//		for(GrantedAuthority ga: authentication.getAuthorities())
//			System.out.println("VOTE ga>>>\t" + ga);
//		
//		for(ConfigAttribute ca: attributes)
//			System.out.println("VOTE ca>>>\t" + ca);
//		
//		
//		System.out.println("VOTE object>>>" + object);
//		
//		System.out.println("-----------------------------------------------");
		
		
		if(object instanceof Flow 
				&& authentication.getPrincipal() instanceof UserAccount
				&& isROLE_USER(attributes)){
			
			LOG.debug("-----------------------------------------------");
			
			Flow flow = (Flow) object;
			LOG.debug("VOTE flow			>>>" + flow.getId());
			
			UserAccount userAccount = (UserAccount) authentication.getPrincipal(); 
			
			LOG.debug("VOTE name			>>>" + authentication.getName());
			LOG.debug("VOTE userAccount	>>>" + userAccount);
			
			for(GrantedAuthority ga: authentication.getAuthorities())
				LOG.debug("VOTE ga					*>>>\t" + ga);
		
			for(ConfigAttribute ca: attributes)
				LOG.debug("VOTE ca					*>>>\t" + ca);
			
			
			LOG.debug("-----------------------------------------------");
			
			
			// TODO: Insert privileges check code here.....
			//if("op/product/plus".equals(flow.getId()))
			//	return ACCESS_DENIED;
			
			
		}
		
		
		return super.vote(authentication, object, attributes);
	}
	
	
	
	private boolean isROLE_USER(Collection<ConfigAttribute> attributes){
		for(ConfigAttribute ca: attributes)
			if(ca.getAttribute().equals("ROLE_USER"))
				return true;
		
		return false;
	}

	public UserAccountService getUserAccountService() {
		return UserAccountService;
	}

	public void setUserAccountService(UserAccountService userAccountService) {
		UserAccountService = userAccountService;
	}

	public PrivilegesService getPrivilegesService() {
		return privilegesService;
	}

	public void setPrivilegesService(PrivilegesService privilegesService) {
		this.privilegesService = privilegesService;
	}
	
	
	
	
	

}
