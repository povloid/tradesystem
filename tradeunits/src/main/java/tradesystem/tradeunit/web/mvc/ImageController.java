package tradesystem.tradeunit.web.mvc;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import pk.home.libs.combine.service.DefaultPathDateFileService;

/**
 * The Class ImageController.
 */
@Controller
public class ImageController {
	
	@Autowired
	private DefaultPathDateFileService defaultPathDateFileService;
	
	@RequestMapping(value = "/image/{prefix}/{yyyy}/{mm}/{dd}/{file:.*}", method = RequestMethod.GET)
	protected void image(@PathVariable String prefix,
			@PathVariable String yyyy, @PathVariable String mm,
			@PathVariable String dd, @PathVariable String file,
			HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		// get the filename from the "file" parameter
		if (file == null || file.equals("")) {
			throw new ServletException(
					"Invalid or non-existent file parameter in Image servlet.");
		}

		// Устранение уязвимости
		file = file.replace("../", "");

		// add the .pdf suffix if it doesn't already exist

		BufferedInputStream buf = null;
		FileInputStream in = null;
		OutputStream out = null;

		try {

			out = response.getOutputStream();
			File pdf = new File(defaultPathDateFileService.getBaseDirPath() 
					+ "/" + prefix 
					+ "/" + yyyy + "/" + mm + "/" + dd + "/" + file);

			// set response headers

			// Get the absolute path of the image
			ServletContext sc = request.getSession().getServletContext();
			String filename = sc.getRealPath(pdf.getAbsolutePath());

			//////////System.out.println(filename);

			// Get the MIME type of the image
			String mimeType = sc.getMimeType(filename.toLowerCase());

			//////////System.out.println(mimeType);

			//////////sc.log("MIME TYPE: " + mimeType);

			if (mimeType == null) {
				sc.log("Could not get MIME type of " + filename);
				response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
				return;
			}

			// Set content type
			response.setContentType(mimeType);

			// Это раскоментировать если сделать как атачмент
			// response.addHeader("Content-Disposition", "attachment; filename="
			// + fileName);

			response.setContentLength((int) pdf.length());

			FileInputStream input = new FileInputStream(pdf);
			buf = new BufferedInputStream(input);
			int readBytes = 0;

			// read from the file; write to the ServletOutputStream
			while ((readBytes = buf.read()) != -1)
				out.write(readBytes);

		} catch (IOException ioe) {
			System.out.println("ERROR: " + ioe.getMessage());

		} catch (Exception e) {
			System.out.println("ERROR: " + e.getMessage());

		} finally {

			if (out != null)
				out.close();

			if (in != null)
				in.close();

			if (buf != null)
				buf.close();
		}
	}
}
