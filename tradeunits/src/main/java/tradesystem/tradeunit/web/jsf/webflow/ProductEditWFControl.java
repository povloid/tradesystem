package tradesystem.tradeunit.web.jsf.webflow;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.primefaces.event.FileUploadEvent;

import pk.home.libs.combine.web.jsf.flow.AWFControl;
import tradesystem.tradeunit.domain.Image;
import tradesystem.tradeunit.domain.Price;
import tradesystem.tradeunit.domain.Product;
import tradesystem.tradeunit.domain.security.UserAccount;
import tradesystem.tradeunit.service.CountedAttributeService;
import tradesystem.tradeunit.service.PriceService;
import tradesystem.tradeunit.service.ProductGroupService;
import tradesystem.tradeunit.service.ProductService;
import tradesystem.tradeunit.web.jsf.security.TerminalCurrentUser;

/**
 * JSF edit control class for entity class: Product Product - продукт
 */
public class ProductEditWFControl extends AWFControl<Product, Long> implements
		Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Long productGroupId;
	
	private Price price;

	@Override
	public Product findEdited(Long id) throws Exception {
		Product product = getProductService().findLazy(id);
		
		// FIND PRICE
		this.price = getPriceService().findPrice(getTerminalCurrentUser().getDivision(), product);
		
		return product;
	}

	@Override
	public Product newEdited() throws Exception {
		
		Product product = new Product();
		
		if(productGroupId != null)
			product.setProductGroup(getProductGroupService().find(productGroupId));
		
		return product;
	}

	public PriceService getPriceService() {
		return findBean("priceService",PriceService.class);
	}
	
	public UserAccount getTerminalCurrentUser() {
		return findBean("terminalCurrentUser", TerminalCurrentUser.class).getUserAccount();
	}
	
	public ProductService getProductService() {
		return findBean("productService", ProductService.class);
	}
	
	public ProductGroupService getProductGroupService() {
		return findBean("productGroupService", ProductGroupService.class);
	}
	
	public CountedAttributeService getCountedAttributeService() {
		return findBean("countedAttributeService", CountedAttributeService.class);
	}

	@Override
	protected void confirmAddImpl() throws Exception {
		edited = getProductService().persist(edited);
		
		this.price = getPriceService().findPrice(getTerminalCurrentUser().getDivision(), edited);
	}

	@Override
	protected void confirmEditImpl() throws Exception {
		edited = getProductService().merge(edited);
		
		getPriceService().merge(price);
	}

	@Override
	protected void confirmDelImpl() throws Exception {
		getPriceService().remove(price);
		
		getProductService().remove(edited);
	}

	// init
	// ----------------------------------------------------------------------------------------------
	protected void init0() throws Exception {
		updateImages();
	}
		
	// PRODUCT GROUP BEGIN
	public void setProductGroup(String result, Long id) throws Exception {
		if(result.equals("ok"))
			edited.setProductGroup(getProductGroupService().find(id));
	}
	// PRODUCT GROUP END
	
	// COUNTED ATTRIBUTES BEGIN
	public void setCountedAttributes(String result, Long id) throws Exception {
		if(result.equals("ok"))
			edited.setCountedAttribute(getCountedAttributeService().find(id));
	}
	// COUNTRD ATTRIBUTES END

	// IMAGES BEGIN
	// -------------------------------------------------------------------------------------

	/** The selected image. */
	private Image selectedImage;

	/** The images. */
	private List<Image> images = new ArrayList<Image>();

	/**
	 * Update images.
	 */
	public void updateImages() {
		try {
			images.clear();
			if (edited != null && edited.getId() != null) {
				if (edited.getImages() != null) {
					images.addAll(edited.getImages());
					System.out.println(">>>Load images count: "
							+ edited.getImages().size());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Handle file upload.
	 * 
	 * @param event
	 *            the event
	 */
	public void handleFileUpload(FileUploadEvent event) {
		try {
			edited = getProductService().saveImage(edited,
					event.getFile().getFileName(),
					event.getFile().getInputstream());

			updateImages();

			FacesMessage msg = new FacesMessage("Succesful.", "File "
					+ event.getFile().getFileName() + " is uploaded.");
			FacesContext.getCurrentInstance().addMessage(null, msg);
		} catch (Exception e) {
			e.printStackTrace();
			FacesMessage error = new FacesMessage(
					"The files were not uploaded!");
			FacesContext.getCurrentInstance().addMessage(null, error);
		}

	}

	/**
	 * Delete image.
	 * 
	 * @throws Exception
	 *             the exception
	 */
	public void deleteImage() throws Exception {
		edited = getProductService().removeImage(edited, selectedImage);

		updateImages();
	}

	// IMAGES PROPERTIES

	public Image getSelectedImage() {
		return selectedImage;
	}

	public void setSelectedImage(Image selectedImage) {
		this.selectedImage = selectedImage;
	}

	public List<Image> getImages() {
		return images;
	}

	public void setImages(List<Image> images) {
		this.images = images;
	}


	// IMAGES END
	// -------------------------------------------------------------------------------------

	// gets and sets
	// ---------------------------------------------------------------------------------------------------

	public Long getProductGroupId() {
		return productGroupId;
	}
	
	public void setProductGroupId(Long productGroupId) {
		this.productGroupId = productGroupId;
	}

	public Price getPrice() {
		return price;
	}

	public void setPrice(Price price) {
		this.price = price;
	}
	
	
	
	
}
