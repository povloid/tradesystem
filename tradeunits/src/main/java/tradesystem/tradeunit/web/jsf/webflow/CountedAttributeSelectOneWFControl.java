package tradesystem.tradeunit.web.jsf.webflow;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.primefaces.model.SortOrder;

import pk.home.libs.combine.dao.ABaseDAO.SortOrderType;
import pk.home.libs.combine.dao.PredicatePair;
import pk.home.libs.combine.web.jsf.flow.AWFBasicControl;
import pk.home.libs.combine.web.jsf.flow.model.WFLazyDataModel;
import tradesystem.tradeunit.domain.CountedAttribute;
import tradesystem.tradeunit.domain.CountedAttribute_;
import tradesystem.tradeunit.service.CountedAttributeService;

/**
 * 
 * Class CountedAttributeSelectOneWFControl
 *
 * @author Kopychenko Pavel
 *
 * @date Jul 15, 2013
 *
 */
public class CountedAttributeSelectOneWFControl extends AWFBasicControl implements
		Serializable {

	public CountedAttributeService getCountedAttributeService() {
		return (CountedAttributeService) findBean("countedAttributeService");
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private CountedAttribute selected;

	private WFLazyDataModel<CountedAttribute> model = new WFLazyDataModel<CountedAttribute>() {

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		
		@Override
		protected int count(Map<String, String> filters) throws Exception {
			List<PredicatePair<CountedAttribute>> ppList = new ArrayList<>();
			ppList.add(new PredicatePair<>(CountedAttribute_.parent, null));
			
			return (int) getCountedAttributeService().countAdvanced(ppList);
		}

		@Override
		protected List<CountedAttribute> aload(int first, int pageSize,
				String sortField, SortOrder sortOrder,
				Map<String, String> filters) throws Exception {
			List<PredicatePair<CountedAttribute>> ppList = new ArrayList<>();
			ppList.add(new PredicatePair<>(CountedAttribute_.parent, null));
			
			return getCountedAttributeService()
					.getAllEntitiesAdvanced(ppList, false, first, pageSize, CountedAttribute_.keyName, SortOrderType.ASC);
		}

		@Override
		public CountedAttribute getRowData(String rowKey) {
			for (CountedAttribute rd : getDataList()) {
				if (rd.getId() == Long.parseLong(rowKey))
					return rd;
			}
			return null;
		}

		@Override
		public Object getRowKey(CountedAttribute object) {
			return object.getId();
		}
	};

	// actions
	// -----------------------------------------------------------------------------------------

	/**
	 * Select action 
	 * 
	 * @return
	 * @throws Exception
	 */
	public String select() throws Exception {

		if (selected == null) {
			FacesContext.getCurrentInstance().addMessage(
					null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: ",
							"Элемент небыл выбран"));
			throw new Exception("Элемент небыл выбран");
		}

		return "select";
	}

	// get's and set's
	// -------------------------------------------------------------------------------------------------

	public CountedAttribute getSelected() {
		return selected;
	}

	public void setSelected(CountedAttribute selected) {
		this.selected = selected;
	}

	public WFLazyDataModel<CountedAttribute> getModel() {
		return model;
	}

	public void setModel(WFLazyDataModel<CountedAttribute> model) {
		this.model = model;
	}

	public Long getSelectedId() {
		if (selected != null)
			return selected.getId();
		else
			return null;
	}

}
