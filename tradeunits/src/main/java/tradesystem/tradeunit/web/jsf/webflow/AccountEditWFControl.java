package tradesystem.tradeunit.web.jsf.webflow;

import java.io.Serializable;

import pk.home.libs.combine.web.jsf.flow.AWFControl;
import tradesystem.tradeunit.domain.Account;
import tradesystem.tradeunit.service.AccountService;

/**
 * JSF edit control class for entity class: Account
 * Account - счет
 */
public class AccountEditWFControl extends AWFControl<Account, Long> implements
		Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public Account findEdited(Long id) throws Exception {
		return getAccountService().find(id);
	}

	@Override
	public Account newEdited() throws Exception {
		return new Account();
	}

	public AccountService getAccountService() {
		return (AccountService) findBean("accountService");
	}

	@Override
	protected void confirmAddImpl() throws Exception {
		edited = getAccountService().persist(edited);
	}

	@Override
	protected void confirmEditImpl() throws Exception {
		edited = getAccountService().merge(edited);
	}

	@Override
	protected void confirmDelImpl() throws Exception {
		getAccountService().remove(edited);
	}

	// init
	// ----------------------------------------------------------------------------------------------
	protected void init0() throws Exception {
	}

	// gets and sets
	// ---------------------------------------------------------------------------------------------------

}
