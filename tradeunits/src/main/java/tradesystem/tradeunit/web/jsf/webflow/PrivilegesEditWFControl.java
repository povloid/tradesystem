package tradesystem.tradeunit.web.jsf.webflow;

import java.io.Serializable;

import pk.home.libs.combine.web.jsf.flow.AWFControl;
import tradesystem.tradeunit.domain.Privileges;
import tradesystem.tradeunit.service.PrivilegesService;

/**
 * JSF edit control class for entity class: Privileges
 * Privileges - привелегии
 */
public class PrivilegesEditWFControl extends AWFControl<Privileges, Long> implements
		Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public Privileges findEdited(Long id) throws Exception {
		return getPrivilegesService().find(id);
	}

	@Override
	public Privileges newEdited() throws Exception {
		return new Privileges();
	}

	public PrivilegesService getPrivilegesService() {
		return (PrivilegesService) findBean("privilegesService");
	}

	@Override
	protected void confirmAddImpl() throws Exception {
		edited = getPrivilegesService().persist(edited);
	}

	@Override
	protected void confirmEditImpl() throws Exception {
		edited = getPrivilegesService().merge(edited);
	}

	@Override
	protected void confirmDelImpl() throws Exception {
		getPrivilegesService().remove(edited);
	}

	// init
	// ----------------------------------------------------------------------------------------------
	protected void init0() throws Exception {
	}

	// gets and sets
	// ---------------------------------------------------------------------------------------------------

}
