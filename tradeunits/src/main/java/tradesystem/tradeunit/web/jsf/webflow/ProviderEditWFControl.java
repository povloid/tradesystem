package tradesystem.tradeunit.web.jsf.webflow;

import java.io.Serializable;

import pk.home.libs.combine.web.jsf.flow.AWFControl;
import tradesystem.tradeunit.domain.Provider;
import tradesystem.tradeunit.service.ProviderService;

/**
 * JSF edit control class for entity class: Provider
 * Provider - продукт
 */
public class ProviderEditWFControl extends AWFControl<Provider, Long> implements
		Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public Provider findEdited(Long id) throws Exception {
		return getProviderService().find(id);
	}

	@Override
	public Provider newEdited() throws Exception {
		return new Provider();
	}

	public ProviderService getProviderService() {
		return (ProviderService) findBean("providerService");
	}

	@Override
	protected void confirmAddImpl() throws Exception {
		edited = getProviderService().persist(edited);
	}

	@Override
	protected void confirmEditImpl() throws Exception {
		edited = getProviderService().merge(edited);
	}

	@Override
	protected void confirmDelImpl() throws Exception {
		getProviderService().remove(edited);
	}

	// init
	// ----------------------------------------------------------------------------------------------
	protected void init0() throws Exception {
	}

	// gets and sets
	// ---------------------------------------------------------------------------------------------------

}
