package tradesystem.tradeunit.web.jsf.webflow;

import java.io.Serializable;

import javax.persistence.metamodel.SingularAttribute;

import pk.home.libs.combine.web.jsf.flow.AWFBaseLazyLoadTableView;
import tradesystem.tradeunit.domain.Measure;
import tradesystem.tradeunit.domain.Measure_;
import tradesystem.tradeunit.service.MeasureService;

/**
 * JSF view control class for entity class: Measure
 * Measure - мера
 */
public class MeasureViewWFControl extends AWFBaseLazyLoadTableView<Measure> implements
		Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public MeasureService getMeasureService() {
		return (MeasureService) findBean("measureService");
	}

	@Override
	protected void aInit() throws Exception {
		
		SingularAttribute<Measure, ?> orderByAttribute = Measure_.id;
		if (csortField != null && csortField.equals("id")) {
			orderByAttribute = Measure_.id;
		} else if (csortField != null && csortField.equals("keyName")) {
			orderByAttribute = Measure_.keyName;
		}

		dataModel = getMeasureService().getAllEntities((page - 1) * rows, rows,
				orderByAttribute, getSortOrderType());
	}

	@Override
	protected long initAllRowsCount() throws Exception {		
		return getMeasureService().count();
	}
	
	
	public String add(){
		return "add";
	}
	
	public String edit(){
		return "edit";
	}
	
	public String del(){
		return "del";
	}
	
}
