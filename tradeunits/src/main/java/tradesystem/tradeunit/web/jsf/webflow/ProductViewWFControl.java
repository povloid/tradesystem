package tradesystem.tradeunit.web.jsf.webflow;

import java.io.Serializable;

import javax.persistence.metamodel.SingularAttribute;

import pk.home.libs.combine.service.tuple.TupleS;
import pk.home.libs.combine.web.jsf.flow.AWFBaseLazyLoadTableView;
import tradesystem.tradeunit.domain.Product;
import tradesystem.tradeunit.domain.ProductGroup;
import tradesystem.tradeunit.domain.Product_;
import tradesystem.tradeunit.domain.security.UserAccount;
import tradesystem.tradeunit.service.CommonInfoService;
import tradesystem.tradeunit.service.ProductService;
import tradesystem.tradeunit.web.jsf.security.TerminalCurrentUser;

/**
 * JSF view control class for entity class: Product Product - продукт
 */
public class ProductViewWFControl extends AWFBaseLazyLoadTableView<TupleS>
		implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private ProductGroup productGroup;
	
	private boolean groupView;

	public ProductService getProductService() {
		return (ProductService) findBean("productService");
	}
	
	public UserAccount getTerminalCurrentUser() {
		return findBean("terminalCurrentUser", TerminalCurrentUser.class).getUserAccount();
	}
	
	public CommonInfoService getCommonInfoService() {
		return (CommonInfoService) findBean("commonInfoService");
	}

	@Override
	protected void aInit() throws Exception {

		SingularAttribute<Product, ?> orderByAttribute = Product_.id;
		if (csortField != null && csortField.equals("id")) {
			orderByAttribute = Product_.id;
		} else if (csortField != null && csortField.equals("keyName")) {
			orderByAttribute = Product_.keyName;
		}

		
		//List<SelectionAtribute<Product>> selections = new ArrayList<>();
		//selections.add(new SelectionAtribute<>(Product_.id));
		//selections.add(new SelectionAtribute<>(Product_.barcode));
		//selections.add(new SelectionAtribute<>(Product_.mainImage));
		//selections.add(new SelectionAtribute<>(Product_.keyName));
		//selections.add(new SelectionAtribute<>(Product_.description));
		//selections.add(new SelectionAtribute<>(Product_.measure, Measure_.keyName));
		
		
		if (groupView){
			//List<PredicatePair<Product>> predicates = new ArrayList<>();
			//predicates.add(new PredicatePair<Product>(Product_.productGroup, productGroup));

			//dataModel = getProductService().getAllEntitiesAdvanced(predicates,
			//		false, (page - 1) * rows, rows, orderByAttribute,
			//		getSortOrderType());
			
			//dataModel = getProductService().getAllEntitiesAdvancedMultiselect(selections, predicates,
			//		false, (page - 1) * rows, rows, orderByAttribute,
			//		getSortOrderType());
			
			dataModel = getCommonInfoService().productsInDivision(getTerminalCurrentUser().getDivision(), productGroup, true,  
					false, 
					(page - 1) * rows, rows, orderByAttribute, getSortOrderType());
			
		} else {
			//dataModel = getProductService().getAllEntities((page - 1) * rows,
			//		rows, orderByAttribute, getSortOrderType());
		
			//dataModel = getProductService().getAllEntitiesAdvancedMultiselect(selections, null,
			//		false, (page - 1) * rows, rows, orderByAttribute,
			//		getSortOrderType());
			
			dataModel = getCommonInfoService().productsInDivision(getTerminalCurrentUser().getDivision(),null, false,  
					false, 
					(page - 1) * rows, rows, orderByAttribute, getSortOrderType());
		}

	}

	@Override
	protected long initAllRowsCount() throws Exception {
		if (groupView){
			//List<PredicatePair<Product>> predicates = new ArrayList<>();
			//predicates.add(new PredicatePair<Product>(Product_.productGroup, productGroup));

			//return getProductService().countAdvanced(predicates);
			
			return getCommonInfoService().productsInDivisionCount(getTerminalCurrentUser().getDivision(), productGroup, true);
			
		} else	
			return getCommonInfoService().productsInDivisionCount(getTerminalCurrentUser().getDivision(), null, false);
	}

	public String add() {
		return "addProduct";
	}

	public String edit() {
		return "editProduct";
	}

	public String del() {
		return "delProduct";
	}
	
	// get's and set's -------------------------------------------------------------------------------------

	public ProductGroup getProductGroup() {
		return productGroup;
	}

	public void setProductGroup(ProductGroup productGroup) {
		this.productGroup = productGroup;
	}

	public boolean isGroupView() {
		return groupView;
	}

	public void setGroupView(boolean groupView) {
		this.groupView = groupView;
	}
	
	

}
