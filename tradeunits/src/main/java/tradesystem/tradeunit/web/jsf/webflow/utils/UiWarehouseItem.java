package tradesystem.tradeunit.web.jsf.webflow.utils;

import java.io.Serializable;
import java.util.List;

import tradesystem.tradeunit.domain.Warehouse;

/**
 * 
 * Class WItems
 *
 * @author Kopychenko Pavel
 *
 * @date 04 дек. 2013 г.
 *
 */
public class UiWarehouseItem implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private List<UIItem> uiItems;
	
	private UIItem rootItem;
	
	private Warehouse warehouse;

	/**
	 * Instantiates a new ui warehouse item.
	 */
	public UiWarehouseItem() {
		super();
	}

	/**
	 * Instantiates a new ui warehouse item.
	 *
	 * @param warehouse the warehouse
	 * @param uiItems the ui items
	 */
	public UiWarehouseItem( Warehouse warehouse, List<UIItem> uiItems) {
		super();
		this.uiItems = uiItems;
		
		for (UIItem i : uiItems)
			if (i.getItem().getCountedAttribute() == null
					|| i.getItem().getCountedAttribute().getParent() == null) {
				this.rootItem = i;
				break;
			}
		
		this.warehouse = warehouse;
	}

	public List<UIItem> getUiItems() {
		return uiItems;
	}

	public void setUiItems(List<UIItem> uiItems) {
		this.uiItems = uiItems;
	}

	public UIItem getRootItem() {
		return rootItem;
	}

	public void setRootItem(UIItem rootItem) {
		this.rootItem = rootItem;
	}

	public Warehouse getWarehouse() {
		return warehouse;
	}

	public void setWarehouse(Warehouse warehouse) {
		this.warehouse = warehouse;
	}
	
	
	
	
}