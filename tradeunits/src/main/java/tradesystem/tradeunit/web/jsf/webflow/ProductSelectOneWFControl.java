package tradesystem.tradeunit.web.jsf.webflow;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.primefaces.model.SortOrder;

import pk.home.libs.combine.web.jsf.flow.AWFBasicControl;
import pk.home.libs.combine.web.jsf.flow.model.WFLazyDataModel;
import tradesystem.tradeunit.domain.Product;
import tradesystem.tradeunit.service.ProductService;

public class ProductSelectOneWFControl extends AWFBasicControl implements
		Serializable {

	public ProductService getProductService() {
		return (ProductService) findBean("productService");
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -6986222423369826350L;

	private Product selected;

	private WFLazyDataModel<Product> model = new WFLazyDataModel<Product>() {

		/**
		 * 
		 */
		private static final long serialVersionUID = 4970282896915525138L;
		
		@Override
		protected int count(Map<String, String> filters) throws Exception {
			return (int) getProductService().count();
		}

		@Override
		protected List<Product> aload(int first, int pageSize,
				String sortField, SortOrder sortOrder,
				Map<String, String> filters) throws Exception {
			return getProductService().getAllEntities(first, pageSize);
		}

		@Override
		public Product getRowData(String rowKey) {
			for (Product rd : getDataList()) {
				if (rd.getId() == Long.parseLong(rowKey))
					return rd;
			}
			return null;
		}

		@Override
		public Object getRowKey(Product object) {
			return object.getId();
		}
	};

	// actions
	// -----------------------------------------------------------------------------------------

	/**
	 * Выбор остановки
	 * 
	 * @return
	 * @throws Exception
	 */
	public String select() throws Exception {

		if (selected == null) {
			FacesContext.getCurrentInstance().addMessage(
					null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: ",
							"Элемент небыл выбран"));
			throw new Exception("Элемент небыл выбран");
		}

		return "select";
	}

	// get's and set's
	// -------------------------------------------------------------------------------------------------

	public Product getSelected() {
		return selected;
	}

	public void setSelected(Product selected) {
		this.selected = selected;
	}

	public WFLazyDataModel<Product> getModel() {
		return model;
	}

	public void setModel(WFLazyDataModel<Product> model) {
		this.model = model;
	}

	public Long getSelectedId() {
		if (selected != null)
			return selected.getId();
		else
			return null;
	}

}
