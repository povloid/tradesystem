/*
 * 
 */
package tradesystem.tradeunit.web.jsf.webflow.utils;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

import tradesystem.tradeunit.domain.Account;
import tradesystem.tradeunit.domain.Item;
import tradesystem.tradeunit.domain.Price;
import tradesystem.tradeunit.domain.Product;
import tradesystem.tradeunit.domain.Warehouse;

/**
 * 
 * Class UIProduct
 *
 * @author Kopychenko Pavel
 *
 * @date 28 нояб. 2013 г.
 *
 */
public class UIProduct implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	

	private Price price;
	private Product product;
	
	
	/** The ui warehouse item. */
	private List<UiWarehouseItem> uiWarehouseItems = new ArrayList<>();
	
	/**
	 * Instantiates a new uI product.
	 */
	public UIProduct() {
		super();
	}
	
	/**
	 * Instantiates a new uI product.
	 *
	 * @param product the product
	 */
	public UIProduct(Product product) {
		super();
		this.product = product;
	}

	/**
	 * Inits the ui product.
	 *
	 * @param price the price
	 */
	public void initUIProduct(Price price) {
		this.price = price;		
	}
	
	
	/**
	 * Adds the ui warehouse item.
	 *
	 * @param warehouse the warehouse
	 * @param items the items
	 * @param minus the minus
	 * @throws Exception the exception
	 */
	public void addUiWarehouseItem(Warehouse warehouse, List<Item> items, boolean minus) throws Exception{		
		uiWarehouseItems.add(new UiWarehouseItem(warehouse ,UIItem.createUIItemsList(items, minus)));
	}
	
	
	/**
	 * Gets the ui values sum.
	 *
	 * @return the ui values sum
	 */
	public BigDecimal getUiValuesSum(){
		
		BigDecimal r = new BigDecimal(0);
		
		for(UiWarehouseItem i : uiWarehouseItems)
			r = r.add(i.getRootItem().getUiValue());
		
		return r;
	}
	
	
	/**
	 * Gets the ui numerators sum.
	 *
	 * @return the ui numerators sum
	 */
	public BigDecimal getUiNumeratorsSum() {

		BigDecimal r = new BigDecimal(0);

		for (UiWarehouseItem i : uiWarehouseItems)
			r = r.add(i.getRootItem().getUiNumerator());

		return r;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public Price getPrice() {
		return price;
	}

	public void setPrice(Price price) {
		this.price = price;
	}

	public List<UiWarehouseItem> getUiWarehouseItems() {
		return uiWarehouseItems;
	}

	public void setUiWarehouseItems(List<UiWarehouseItem> uiWarehouseItems) {
		this.uiWarehouseItems = uiWarehouseItems;
	}

	@Override
	public int hashCode() {
		return product.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		
		// not set
		if (!(obj instanceof UIProduct)) {
			return false;
		}
		
		return product.equals(((UIProduct)obj).product);
	}
	
	
	
	// static 
	
	/**
	 * Select products items.
	 *
	 * @param uiProducts the ui products
	 * @return the list
	 * @throws Exception the exception
	 */
	public static List<Item> selectProductsItems(List<UIProduct> uiProducts) throws Exception {
		
		List<Item> items = new ArrayList<>();

		for (UIProduct up : uiProducts)
			for (UiWarehouseItem uwi : up.getUiWarehouseItems()){
				
				List<Item> upList = UIItem.createItemsList(uwi.getUiItems());
				
				for(Item i: upList){
					i.setTransientOwner(uwi.getWarehouse());
					i.setTransientCountsObject(up.getProduct());
					
					i.setActualPriceValue(up.getPrice().getValue());
					i.setActualPriceValueNumerator(up.getPrice().getValueNumrator());
					
					// TODO: пока оценка равна цене из прайса так как пока нет скидок
					i.setCostValue(up.getPrice().getValue());
					i.setCostValueNumerator(up.getPrice().getValueNumrator());
				}
				
				
				items.addAll(upList);
			}
		
		return items;
	}
	
	
	
	
}
