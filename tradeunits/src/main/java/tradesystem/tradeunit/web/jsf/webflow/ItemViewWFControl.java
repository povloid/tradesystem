package tradesystem.tradeunit.web.jsf.webflow;

import java.io.Serializable;

import javax.persistence.metamodel.SingularAttribute;

import pk.home.libs.combine.web.jsf.flow.AWFBaseLazyLoadTableView;
import tradesystem.tradeunit.domain.Item;
import tradesystem.tradeunit.domain.Item_;
import tradesystem.tradeunit.service.ItemService;

/**
 * JSF view control class for entity class: Item
 * Item - запись
 */
public class ItemViewWFControl extends AWFBaseLazyLoadTableView<Item> implements
		Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ItemService getItemService() {
		return (ItemService) findBean("itemService");
	}

	@Override
	protected void aInit() throws Exception {
		
		SingularAttribute<Item, ?> orderByAttribute = Item_.id;
		if (csortField != null && csortField.equals("id")) {
			orderByAttribute = Item_.id;
		}

		dataModel = getItemService().getAllEntities((page - 1) * rows, rows,
				orderByAttribute, getSortOrderType());
	}

	@Override
	protected long initAllRowsCount() throws Exception {		
		return getItemService().count();
	}
	
	
	public String add(){
		return "add";
	}
	
	public String edit(){
		return "edit";
	}
	
	public String del(){
		return "del";
	}
	
}
