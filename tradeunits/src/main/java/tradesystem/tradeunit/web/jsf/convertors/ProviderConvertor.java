package tradesystem.tradeunit.web.jsf.convertors;

import org.springframework.beans.factory.annotation.Autowired;

import pk.home.libs.combine.web.jsf.ABaseConverter;
import tradesystem.tradeunit.domain.Provider;
import tradesystem.tradeunit.service.ProviderService;

public class ProviderConvertor extends ABaseConverter{

	@Autowired
	private ProviderService providerService;
	
	@Override
	protected Object findObject(String key) throws Exception {
		return providerService.find(Long.parseLong(key));
	}

	@Override
	protected String getStringKey(Object value) throws Exception {
		return String.valueOf(((Provider) value).getId());
	}

}