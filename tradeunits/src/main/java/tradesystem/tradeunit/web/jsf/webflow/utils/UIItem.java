package tradesystem.tradeunit.web.jsf.webflow.utils;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import tradesystem.tradeunit.domain.CountedAttribute;
import tradesystem.tradeunit.domain.ICountsObject;
import tradesystem.tradeunit.domain.IOwner;
import tradesystem.tradeunit.domain.Item;


/**
 * 
 * Class UIItem
 *
 * @author Kopychenko Pavel
 *
 * @date Sep 25, 2013
 *
 */
public class UIItem implements Serializable{
	

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	private Item item;
	
	
	
	/**
	 * Instantiates a new uI item.
	 */
	public UIItem() {
		super();
	}
	
	/**
	 * Instantiates a new uI item.
	 *
	 * @param item the item
	 * @param minus the minus
	 */
	public UIItem(Item item, boolean minus) {
		super();
		this.item = item;
		this.minus = minus;
	}
	
	
	
	/**
	 * Instantiates a new uI item.
	 *
	 * @param minus the minus
	 */
	public UIItem(boolean minus) {
		super();
		this.item = new Item();
		this.minus = minus;
	}

	/**
	 * Instantiates a new uI item.
	 *
	 * @param minus the minus
	 * @param transientOwner the transient owner
	 * @param transientCountsObject the transient counts object
	 * @param value the value
	 * @param numerator the numerator
	 * @param countedAttribute the counted attribute
	 * @param description the description
	 */
	public UIItem(boolean minus, IOwner transientOwner, ICountsObject transientCountsObject ,BigDecimal value, BigDecimal numerator,
			CountedAttribute countedAttribute, String description) {
		this.item = new Item(transientOwner, transientCountsObject, value, numerator, countedAttribute, description);
		this.minus = minus;
	}

	/** The Constant minusOne. */
	final static BigDecimal minusOne = new BigDecimal(-1);
	
	/** The minus. */
	private boolean minus;
	
	/**
	 * Unsign.
	 *
	 * @param decimal the decimal
	 * @return the big decimal
	 */
	private BigDecimal unsign(BigDecimal decimal) {
		return minus ? decimal.multiply(minusOne) : decimal;
	}
	
	/**
	 * Sign.
	 *
	 * @param decimal the decimal
	 * @return the big decimal
	 * @throws Exception the exception
	 */
	private BigDecimal sign(BigDecimal decimal) throws Exception{
		
		// check
		if(decimal.signum() == -1)
			throw new Exception("decimal value is negative");
		
		// do
		return minus ? decimal.abs().multiply(minusOne) : decimal;
	}
	
	/**
	 * Gets the ui value.
	 *
	 * @return the ui value
	 */
	public BigDecimal getUiValue() {
		return unsign(item.getValue());
	}

	/**
	 * Sets the ui value.
	 *
	 * @param value the new ui value
	 * @throws Exception the exception
	 */
	public void setUiValue(BigDecimal value) throws Exception{
		item.setValue(sign(value));
	}

	/**
	 * Gets the ui numerator.
	 *
	 * @return the ui numerator
	 */
	public BigDecimal getUiNumerator() {
		return unsign(item.getNumerator());
	}

	/**
	 * Sets the ui numerator.
	 *
	 * @param numerator the new ui numerator
	 * @throws Exception the exception
	 */
	public void setUiNumerator(BigDecimal numerator) throws Exception{
		item.setNumerator(sign(numerator));
	}

	public Item getItem() {
		return item;
	}

	public void setItem(Item item) {
		this.item = item;
	}

	public boolean isMinus() {
		return minus;
	}

	public void setMinus(boolean minus) {
		this.minus = minus;
	}
	
	/**
	 * Creates the item list from ui item list.
	 *
	 * @param list the list
	 * @return the list
	 */
	public static List<Item> createItemsList(List<UIItem> list){
		List<Item> result = new ArrayList<>();
		
		for(UIItem i: list)
			result.add(i.getItem());
		
		return result;
	}
	
	/**
	 * Creates the ui items list.
	 *
	 * @param list the list
	 * @param minus the minus
	 * @return the list
	 */
	public static List<UIItem> createUIItemsList(List<Item> list, boolean minus){
		List<UIItem> result = new ArrayList<>();
		
		for(Item i: list)
			result.add(new UIItem(i, minus));
		
		return result;
	}
	
}
