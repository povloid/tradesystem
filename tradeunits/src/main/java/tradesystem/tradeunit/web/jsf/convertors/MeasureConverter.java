package tradesystem.tradeunit.web.jsf.convertors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import pk.home.libs.combine.web.jsf.ABaseConverter;
import tradesystem.tradeunit.domain.Measure;
import tradesystem.tradeunit.service.MeasureService;

/**
 * 
 * Class MeasureConverter
 *
 * @author Kopychenko Pavel
 *
 * @date Jun 28, 2013
 *
 */
@Component
public class MeasureConverter extends ABaseConverter{
	
	@Autowired
	private MeasureService measureService; 

	@Override
	protected Object findObject(String key) throws Exception {
		return measureService.find(Long.parseLong(key));
	}

	@Override
	protected String getStringKey(Object value) throws Exception {
		return String.valueOf(((Measure) value).getId());
	}
}
