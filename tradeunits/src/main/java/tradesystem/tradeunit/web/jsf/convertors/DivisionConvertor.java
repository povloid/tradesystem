package tradesystem.tradeunit.web.jsf.convertors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import pk.home.libs.combine.web.jsf.ABaseConverter;
import tradesystem.tradeunit.domain.Division;
import tradesystem.tradeunit.service.DivisionService;

/**
 * The Class DivisionConvertor.
 */
@Component
public class DivisionConvertor extends ABaseConverter{

	@Autowired
	private DivisionService divisionService;
	
	@Override
	protected Object findObject(String key) throws Exception {
		return divisionService.find(Long.parseLong(key));
	}

	@Override
	protected String getStringKey(Object value) throws Exception {
		return String.valueOf(((Division) value).getId());
	}

}
