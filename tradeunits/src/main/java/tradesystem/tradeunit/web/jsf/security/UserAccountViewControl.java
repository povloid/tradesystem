package tradesystem.tradeunit.web.jsf.security;

import java.io.Serializable;

import javax.persistence.metamodel.SingularAttribute;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import pk.home.libs.combine.web.jsf.ABaseLazyLoadTableView;
import tradesystem.tradeunit.domain.security.UserAccount;
import tradesystem.tradeunit.domain.security.UserAccount_;
import tradesystem.tradeunit.service.security.UserAccountService;

/**
 * Контроллер просмотра списка пользователей
 * 
 * @author povloid
 *
 */
@Scope("session")
@Component("userAccountViewControl")
public final class UserAccountViewControl extends ABaseLazyLoadTableView<UserAccount>
		implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Autowired
	private UserAccountService userAccountService;

	@Override
	protected void aInit() throws Exception {
		SingularAttribute<UserAccount, ?> orderByAttribute = UserAccount_.id;
		if (csortField != null && csortField.equals("id")) {
			orderByAttribute = UserAccount_.id;
		} else if (csortField != null && csortField.equals("username")) {
			orderByAttribute = UserAccount_.username;
		} 

		dataModel = userAccountService.getAllEntities((page - 1) * rows, rows,
				orderByAttribute, getSortOrderType());
	}

	@Override
	protected long initAllRowsCount() throws Exception {
		return userAccountService.count();
	}

}
