package tradesystem.tradeunit.web.jsf.webflow;

import java.io.Serializable;

import javax.persistence.metamodel.SingularAttribute;

import pk.home.libs.combine.web.jsf.flow.AWFBaseLazyLoadTableView;
import tradesystem.tradeunit.domain.Account;
import tradesystem.tradeunit.domain.Account_;
import tradesystem.tradeunit.service.AccountService;

/**
 * JSF view control class for entity class: Account
 * Account - счет
 */
public class AccountViewWFControl extends AWFBaseLazyLoadTableView<Account> implements
		Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public AccountService getAccountService() {
		return (AccountService) findBean("accountService");
	}

	@Override
	protected void aInit() throws Exception {
		
		SingularAttribute<Account, ?> orderByAttribute = Account_.id;
		if (csortField != null && csortField.equals("id")) {
			orderByAttribute = Account_.id;
		}

		dataModel = getAccountService().getAllEntities((page - 1) * rows, rows,
				orderByAttribute, getSortOrderType());
	}

	@Override
	protected long initAllRowsCount() throws Exception {		
		return getAccountService().count();
	}
	
	
	public String add(){
		return "add";
	}
	
	public String edit(){
		return "edit";
	}
	
	public String del(){
		return "del";
	}
	
}
