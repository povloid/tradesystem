package tradesystem.tradeunit.web.jsf.webflow;

import java.io.Serializable;

import pk.home.libs.combine.web.jsf.flow.AWFControl;
import tradesystem.tradeunit.domain.Measure;
import tradesystem.tradeunit.service.MeasureService;

/**
 * JSF edit control class for entity class: Measure
 * Measure - мера
 */
public class MeasureEditWFControl extends AWFControl<Measure, Long> implements
		Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public Measure findEdited(Long id) throws Exception {
		return getMeasureService().find(id);
	}

	@Override
	public Measure newEdited() throws Exception {
		return new Measure();
	}

	public MeasureService getMeasureService() {
		return (MeasureService) findBean("measureService");
	}

	@Override
	protected void confirmAddImpl() throws Exception {
		edited = getMeasureService().persist(edited);
	}

	@Override
	protected void confirmEditImpl() throws Exception {
		edited = getMeasureService().merge(edited);
	}

	@Override
	protected void confirmDelImpl() throws Exception {
		getMeasureService().remove(edited);
	}

	// init
	// ----------------------------------------------------------------------------------------------
	protected void init0() throws Exception {
	}

	// gets and sets
	// ---------------------------------------------------------------------------------------------------

}
