package tradesystem.tradeunit.web.jsf.webflow;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.persistence.NoResultException;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import pk.home.libs.combine.web.jsf.flow.AWFBasicControl;
import tradesystem.tradeunit.domain.Account;
import tradesystem.tradeunit.domain.Customer;
import tradesystem.tradeunit.domain.Item;
import tradesystem.tradeunit.domain.Measure.MeasureType;
import tradesystem.tradeunit.domain.Product;
import tradesystem.tradeunit.domain.Product_;
import tradesystem.tradeunit.domain.Warehouse;
import tradesystem.tradeunit.domain.WarehousePrivileges;
import tradesystem.tradeunit.domain.security.UserAccount;
import tradesystem.tradeunit.service.AccountService;
import tradesystem.tradeunit.service.CurrencyService;
import tradesystem.tradeunit.service.CustomerService;
import tradesystem.tradeunit.service.ItemService;
import tradesystem.tradeunit.service.OrderService;
import tradesystem.tradeunit.service.PriceService;
import tradesystem.tradeunit.service.PrivilegesService;
import tradesystem.tradeunit.service.ProductService;
import tradesystem.tradeunit.service.ProviderService;
import tradesystem.tradeunit.service.WarehouseService;
import tradesystem.tradeunit.web.jsf.security.TerminalCurrentUser;
import tradesystem.tradeunit.web.jsf.webflow.utils.UIItem;
import tradesystem.tradeunit.web.jsf.webflow.utils.UIProduct;
import tradesystem.tradeunit.web.jsf.webflow.utils.UiWarehouseItem;

/**
 * Product sale controller
 * 
 * 
 * Class OpProductSaleWFControl
 * 
 * @author Kopychenko Pavel
 * 
 * @date Oct 1, 2013
 * 
 */
public class OpProductSaleWFControl extends AWFBasicControl implements
		Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private static final Logger LOG = Logger.getLogger(OpProductSaleWFControl.class);

	
	// SERVICES
	
	private PriceService getPriceService() {
		return findBean("priceService", PriceService.class);
	}
	
	private OrderService getOrderService() {
		return findBean("orderService", OrderService.class);
	}

	private AccountService getAccountService() {
		return findBean("accountService", AccountService.class);
	}

	private WarehouseService getWarehouseService() {
		return findBean("warehouseService", WarehouseService.class);
	}

	private ProductService getProductService() {
		return findBean("productService", ProductService.class);
	}

	private ItemService getItemService() {
		return findBean("itemService", ItemService.class);
	}

	public UserAccount getTerminalCurrentUser() {
		return findBean("terminalCurrentUser", TerminalCurrentUser.class)
				.getUserAccount();
	}

	private ProviderService getProviderService() {
		return findBean("providerService", ProviderService.class);
	}

	private CustomerService getCustomerService() {
		return findBean("customerService", CustomerService.class);
	}
	
	private CurrencyService getCurrencyService() {
		return findBean("currencyService", CurrencyService.class);
	}
	
	private PrivilegesService getPrivilegesService() {
		return findBean("privilegesService", PrivilegesService.class);
	}

	// FIELDS


	private String barcode;

	private List<UIProduct> uiProducts;

	private Customer customer;

	private String description;
	
	private UIProduct selectedUiProduct;
	
	private BigDecimal toBalance;
	
	
	
	
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see pk.home.libs.combine.web.jsf.flow.AWFBasicControl#init0()
	 */
	@Override
	protected void init0() throws Exception {
		LOG.debug("Init form");
	}

	// Actions

	/**
	 * New op.
	 * 
	 * @return the string
	 * @throws Exception
	 *             the exception
	 */
	public void newOp() throws Exception {

		// Cleaning all elements
		
		LOG.debug("Clean all alements for NEW OP");

		barcode = "";

		uiProducts = new ArrayList<>();

		customer = null;

		description = "";
		
		toBalance = new BigDecimal(0);
	}

	/**
	 * Find product.
	 * 
	 * @throws Exception
	 *             the exception
	 */
	public String findProduct() throws Exception {

		LOG.debug("find product for barcode: " + this.barcode);

		if (StringUtils.isNotEmpty(this.barcode))
			try {

				// Find product for barcode
				Product product = getProductService().findAdvanced(
						Product_.barcode, barcode);
				
				LOG.debug("Finded product: " + product);
				
				UIProduct uiProduct = new UIProduct(product); 
				
				if (!uiProducts.contains(uiProduct)) {
					// IF new product
					
					// Old variant
					//uiProduct.initUIProduct( null
					//		, null, UIItem.createUIItemsList(
					//				getItemService().productItems(product),	true));
					
					
					UserAccount userAccount = getTerminalCurrentUser();
					
					LOG.debug(">>> user : " + userAccount);
					LOG.debug(">>> user division: " + userAccount.getDivision());
					
					// Initialize common elements
					uiProduct.initUIProduct(getPriceService().findPrice(userAccount.getDivision(), product));
					
					// Initialize warehouse elements
					List<Warehouse> wList = PrivilegesService
							.warehousesFromPrivileges(getPrivilegesService()
									.getPrivilegesForWarehouseType(userAccount,
											userAccount.getDivision(),
											WarehousePrivileges.PRODUCT_SALE));

					LOG.debug(">>> wList size: " + wList.size());
					
					// fill the uiWarehouseItem  
					for (Warehouse w : wList)
						uiProduct.addUiWarehouseItem(w,getItemService().productItems(w,product), true);
					
					// add new product
					uiProducts.add(uiProduct);
					
					this.selectedUiProduct = uiProduct;
					
					//uiProduct.getRootItem().setUiValue(new BigDecimal(1));
				} else {
					// IF product was founded
					
					int index = uiProducts.indexOf(uiProduct);
					this.selectedUiProduct = uiProducts.get(index);
					
					//UIItem rootUiItem = uiProducts.get(index).getRootItem();
					//rootUiItem.setUiValue(rootUiItem.getUiValue().add(new BigDecimal(1)));
				}
				
				
				// /Account account = getAccountService().findAccount(
				// / getTerminalCurrentUser().getDivision(), product,
				// getTerminalCurrentUser());

			} catch (NoResultException ex) {
				ex.printStackTrace();
				FacesContext.getCurrentInstance().addMessage(
						null,
						new FacesMessage(FacesMessage.SEVERITY_ERROR,
								"Error: ", ex.getMessage()));
				
				throw new Exception(ex);
			}
		else
			return "findProduct";
		
		return "setQuantity";

	}

	
	/**
	 * Delete product.
	 *
	 * @throws Exception the exception
	 */
	public void deleteProduct() throws Exception {
		uiProducts.remove(selectedUiProduct);
		selectedUiProduct = null;
	}
	
	
	
	/**
	 * All sum.
	 *
	 * @return the big decimal
	 */
	public BigDecimal allSum() {
		
		BigDecimal sum = new BigDecimal(0);
		
		for (UIProduct up : uiProducts) {
			
			sum = sum.add(
					up.getUiValuesSum().multiply(up.getPrice().getValue()));
			
			// Calculations for fraction
			LOG.debug("DENOMINATOR: " + up.getProduct().getDenominator());
			
			if (up.getProduct().getMeasure().getMeasureType() == MeasureType.FRACTION)
				sum = sum.add(up.getUiNumeratorsSum().multiply(up.getPrice().getValueNumrator()));
			
			LOG.debug("ALL SUM: " + sum);
		}
		
		return sum;
	}
	
	/**
	 * Sum to cash.
	 *
	 * @return the big decimal
	 */
	public BigDecimal toCach() {
		return allSum().subtract(toBalance);
	}
	
	// SELECT CUSTOMER
	
	/**
	 * Sets the customer.
	 *
	 * @param result the result
	 * @param id the id
	 * @throws Exception the exception
	 */
	public void setCustomer(String result, Long id) throws Exception {
		if (result.equals("ok"))
			this.customer = getCustomerService().find(id);
	}
	
	
	/**
	 * Delete customer.
	 *
	 * @throws Exception the exception
	 */
	public void delCustomer() throws Exception {
		this.customer = null;
		this.toBalance = new BigDecimal(0);
	}
	

	// SELECT CUSTOMER END
	
	
	// ENTER BEGIN 

	/**
	 * Enter.
	 *
	 * @return the string
	 * @throws Exception the exception
	 */
	public String enter() throws Exception {
		try {

			getOrderService().productSale(getTerminalCurrentUser(), getTerminalCurrentUser().getDivision(),

			UIProduct.selectProductsItems(uiProducts),

			new Item(getTerminalCurrentUser().getCash(), getCurrencyService().mainCurrency(), toCach(), new BigDecimal(0), null, description),

			customer == null ? null : new Item(customer, getCurrencyService().mainCurrency(), toBalance.negate(), new BigDecimal(0), null, description),

			description);

			newOp(); // Clean for new op

			return "enter";

		} catch (Exception ex) {
			ex.printStackTrace();
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: ", ex.getMessage()));

			throw new Exception(ex);
		}
	}
	
	
	// ENTER END
	

	// get's and set's
	// ---------------------------------------------------------------

	public String getBarcode() {
		return barcode;
	}

	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public List<UIProduct> getUiProducts() {
		return uiProducts;
	}

	public void setUiProducts(List<UIProduct> uiProducts) {
		this.uiProducts = uiProducts;
	}

	public UIProduct getSelectedUiProduct() {
		return selectedUiProduct;
	}

	public void setSelectedUiProduct(UIProduct selectedUiProduct) {
		this.selectedUiProduct = selectedUiProduct;
	}

	public BigDecimal getToBalance() {
		return toBalance;
	}

	public void setToBalance(BigDecimal toBalance) {
		this.toBalance = toBalance;
	}
	
	
	
	

}
