package tradesystem.tradeunit.web.jsf.webflow;

import java.io.Serializable;

import javax.persistence.metamodel.SingularAttribute;

import pk.home.libs.combine.web.jsf.flow.AWFBaseLazyLoadTableView;
import tradesystem.tradeunit.domain.Order;
import tradesystem.tradeunit.domain.Order_;
import tradesystem.tradeunit.service.OrderService;

/**
 * JSF view control class for entity class: Order
 * Order - ордер
 */
public class OrderViewWFControl extends AWFBaseLazyLoadTableView<Order> implements
		Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public OrderService getOrderService() {
		return (OrderService) findBean("orderService");
	}

	@Override
	protected void aInit() throws Exception {
		
		SingularAttribute<Order, ?> orderByAttribute = Order_.id;
		if (csortField != null && csortField.equals("id")) {
			orderByAttribute = Order_.id;
		} 

		dataModel = getOrderService().getAllEntities((page - 1) * rows, rows,
				orderByAttribute, getSortOrderType());
	}

	@Override
	protected long initAllRowsCount() throws Exception {		
		return getOrderService().count();
	}
	
	
	public String add(){
		return "add";
	}
	
	public String edit(){
		return "edit";
	}
	
	public String del(){
		return "del";
	}
	
}
