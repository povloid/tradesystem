package tradesystem.tradeunit.web.jsf.webflow;

import java.io.Serializable;

import pk.home.libs.combine.web.jsf.flow.AWFControl;
import tradesystem.tradeunit.domain.ProductGroup;
import tradesystem.tradeunit.service.ProductGroupService;

/**
 * JSF edit control class for entity class: ProductGroup
 * ProductGroup - группа продуктов
 */
public class ProductGroupEditWFControl extends AWFControl<ProductGroup, Long> implements
		Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public ProductGroup findEdited(Long id) throws Exception {
		return getProductGroupService().find(id);
	}
	
	/**
	 * Additional for tree functional.
	 * 
	 * @param parentId
	 *            the parent id
	 * @return the product group
	 * @throws Exception
	 *             the exception
	 */
	public ProductGroup newEdited(Object parentId) throws Exception {
		System.out.println(parentId);
		
		if (parentId != null) {
			ProductGroup productGroup = newEdited();
			productGroup.setParent(getProductGroupService().find(parentId));

			return productGroup;
		} else
			return newEdited();
	}

	@Override
	public ProductGroup newEdited() throws Exception {
		return new ProductGroup();
	}

	public ProductGroupService getProductGroupService() {
		return (ProductGroupService) findBean("productGroupService");
	}

	@Override
	protected void confirmAddImpl() throws Exception {
		edited = getProductGroupService().persist(edited);
	}

	@Override
	protected void confirmEditImpl() throws Exception {
		edited = getProductGroupService().merge(edited);
	}

	@Override
	protected void confirmDelImpl() throws Exception {
		getProductGroupService().remove(edited);
	}

	// init
	// ----------------------------------------------------------------------------------------------
	protected void init0() throws Exception {
	}

	// gets and sets
	// ---------------------------------------------------------------------------------------------------

}
