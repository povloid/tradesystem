package tradesystem.tradeunit.web.jsf.webflow;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.primefaces.model.SortOrder;

import pk.home.libs.combine.web.jsf.flow.AWFBasicControl;
import pk.home.libs.combine.web.jsf.flow.model.WFLazyDataModel;
import tradesystem.tradeunit.domain.Provider;
import tradesystem.tradeunit.service.ProviderService;

public class ProviderSelectOneWFControl extends AWFBasicControl implements
		Serializable {

	public ProviderService getProviderService() {
		return (ProviderService) findBean("providerService");
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -6986222423369826350L;

	private Provider selected;

	private WFLazyDataModel<Provider> model = new WFLazyDataModel<Provider>() {

		/**
		 * 
		 */
		private static final long serialVersionUID = 4970282896915525138L;
		
		@Override
		protected int count(Map<String, String> filters) throws Exception {
			return (int) getProviderService().count();
		}

		@Override
		protected List<Provider> aload(int first, int pageSize,
				String sortField, SortOrder sortOrder,
				Map<String, String> filters) throws Exception {
			return getProviderService().getAllEntities(first, pageSize);
		}

		@Override
		public Provider getRowData(String rowKey) {
			for (Provider rd : getDataList()) {
				if (rd.getId() == Long.parseLong(rowKey))
					return rd;
			}
			return null;
		}

		@Override
		public Object getRowKey(Provider object) {
			return object.getId();
		}
	};

	// actions
	// -----------------------------------------------------------------------------------------

	/**
	 * Выбор остановки
	 * 
	 * @return
	 * @throws Exception
	 */
	public String select() throws Exception {

		if (selected == null) {
			FacesContext.getCurrentInstance().addMessage(
					null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: ",
							"Элемент небыл выбран"));
			throw new Exception("Элемент небыл выбран");
		}

		return "select";
	}

	// get's and set's
	// -------------------------------------------------------------------------------------------------

	public Provider getSelected() {
		return selected;
	}

	public void setSelected(Provider selected) {
		this.selected = selected;
	}

	public WFLazyDataModel<Provider> getModel() {
		return model;
	}

	public void setModel(WFLazyDataModel<Provider> model) {
		this.model = model;
	}

	public Long getSelectedId() {
		if (selected != null)
			return selected.getId();
		else
			return null;
	}

}
