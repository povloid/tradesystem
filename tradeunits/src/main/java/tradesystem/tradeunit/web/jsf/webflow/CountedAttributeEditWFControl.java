package tradesystem.tradeunit.web.jsf.webflow;

import java.io.Serializable;

import pk.home.libs.combine.web.jsf.flow.AWFControl;
import tradesystem.tradeunit.domain.CountedAttribute;
import tradesystem.tradeunit.service.CountedAttributeService;

/**
 * JSF edit control class for entity class: CountedAttribute
 * СountedAttribute - подсчитываемый атрибут
 */
public class CountedAttributeEditWFControl extends AWFControl<CountedAttribute, Long> implements
		Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	private Long parentId;
	
	private CountedAttribute selectedSubAttribute;

	@Override
	public CountedAttribute findEdited(Long id) throws Exception {
		System.out.println("find "  + id);
		CountedAttribute countedAttribute = getCountedAttributeService().findZ(id);
		return countedAttribute;
	}

	@Override
	public CountedAttribute newEdited() throws Exception {
		CountedAttribute countedAttribute =  new CountedAttribute();
		
		if(parentId != null)
			countedAttribute.setParent(getCountedAttributeService().find(parentId));
		
		return countedAttribute;
	}

	public CountedAttributeService getCountedAttributeService() {
		return (CountedAttributeService) findBean("countedAttributeService");
	}

	@Override
	protected void confirmAddImpl() throws Exception {
		edited = getCountedAttributeService().persist(edited);
	}

	@Override
	protected void confirmEditImpl() throws Exception {
		edited = getCountedAttributeService().merge(edited);
	}

	@Override
	protected void confirmDelImpl() throws Exception {
		getCountedAttributeService().remove(edited);
	}

	// init
	// ---------------------------------------------------------------------------------------------------
	protected void init0() throws Exception {
	}
	
	
	/**
	 * Adds the sub attribute.
	 *
	 * @return the string
	 * @throws Exception the exception
	 */
	public String addSubAttribute() throws Exception {
		
		return "addSubAttribute";
	}
	
	/**
	 * Edits the sub attribute.
	 *
	 * @return the string
	 * @throws Exception the exception
	 */
	public String editSubAttribute() throws Exception {
		
		return "editSubAttribute";
	}


	/**
	 * Deletes sub attribute.
	 *
	 * @return the string
	 * @throws Exception the exception
	 */
	public String delSubAttribute() throws Exception {
	
		return "delSubAttribute";
	}


	// gets and sets
	// ---------------------------------------------------------------------------------------------------

	public CountedAttribute getSelectedSubAttribute() {
		return selectedSubAttribute;
	}
	
	public void setSelectedSubAttribute(CountedAttribute selectedSubAttribute) {
		this.selectedSubAttribute = selectedSubAttribute;
	}

	public Long getParentId() {
		return parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}
	
	
}
