package tradesystem.tradeunit.web.jsf.webflow;

import java.io.Serializable;

import pk.home.libs.combine.web.jsf.flow.AWFControl;
import tradesystem.tradeunit.domain.Warehouse;
import tradesystem.tradeunit.service.WarehouseService;

/**
 * JSF edit control class for entity class: Warehouse
 * Warehouse - склад
 */
public class WarehouseEditWFControl extends AWFControl<Warehouse, Long> implements
		Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public Warehouse findEdited(Long id) throws Exception {
		return getWarehouseService().find(id);
	}

	@Override
	public Warehouse newEdited() throws Exception {
		return new Warehouse();
	}

	public WarehouseService getWarehouseService() {
		return (WarehouseService) findBean("warehouseService");
	}

	@Override
	protected void confirmAddImpl() throws Exception {
		edited = getWarehouseService().persist(edited);
	}

	@Override
	protected void confirmEditImpl() throws Exception {
		edited = getWarehouseService().merge(edited);
	}

	@Override
	protected void confirmDelImpl() throws Exception {
		getWarehouseService().remove(edited);
	}

	// init
	// ----------------------------------------------------------------------------------------------
	protected void init0() throws Exception {
	}

	// gets and sets
	// ---------------------------------------------------------------------------------------------------

}
