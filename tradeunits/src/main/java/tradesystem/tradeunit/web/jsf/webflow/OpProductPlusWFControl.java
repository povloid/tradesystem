package tradesystem.tradeunit.web.jsf.webflow;

import java.io.Serializable;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.persistence.NoResultException;

import org.apache.commons.lang.StringUtils;

import pk.home.libs.combine.dao.ABaseDAO.SortOrderType;
import pk.home.libs.combine.web.jsf.flow.AWFBasicControl;
import tradesystem.tradeunit.domain.Account;
import tradesystem.tradeunit.domain.OrderType;
import tradesystem.tradeunit.domain.Product;
import tradesystem.tradeunit.domain.Product_;
import tradesystem.tradeunit.domain.Provider;
import tradesystem.tradeunit.domain.Warehouse;
import tradesystem.tradeunit.domain.Warehouse_;
import tradesystem.tradeunit.domain.security.UserAccount;
import tradesystem.tradeunit.service.AccountService;
import tradesystem.tradeunit.service.ItemService;
import tradesystem.tradeunit.service.OrderService;
import tradesystem.tradeunit.service.ProductService;
import tradesystem.tradeunit.service.ProviderService;
import tradesystem.tradeunit.service.WarehouseService;
import tradesystem.tradeunit.web.jsf.security.TerminalCurrentUser;
import tradesystem.tradeunit.web.jsf.webflow.utils.UIItem;

/**
 * Class OpProductPlusWFControl.
 * 
 * @author Kopychenko Pavel
 * @date Jul 31, 2013
 */
public class OpProductPlusWFControl extends AWFBasicControl implements
		Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private OrderService getOrderService() {
		return findBean("orderService", OrderService.class);
	}
	
	private AccountService getAccountService() {
		return findBean("accountService", AccountService.class);
	}
	
	private WarehouseService getWarehouseService() {
		return findBean("warehouseService", WarehouseService.class);
	}
	
	private ProductService getProductService() {
		return findBean("productService", ProductService.class);
	}
	
	private ItemService getItemService() {
		return findBean("itemService", ItemService.class);
	}
	
	public UserAccount getTerminalCurrentUser() {
		return findBean("terminalCurrentUser", TerminalCurrentUser.class).getUserAccount();
	}
	
	private ProviderService getProviderService() {
		return findBean("providerService", ProviderService.class);
	}
	
	
	private OrderType orderType;

	private String barcode;

	private Warehouse warehouse;
	private List<Warehouse> warehouses;

	private Account account;
	private Product product;
	private Provider provider;

	private UIItem mainItem;
	private List<UIItem> items;

	private String description;

	
	// PROVIDER
	public void setProvider(String result, Long id) throws Exception {
		if(result.equals("ok"))
			 this.provider = getProviderService().find(id);
	}
	// PROVIDER END
	
	/**
	 * Set order type
	 * 
	 * @param orderType
	 * @throws Exception
	 */
	public void setOrderType(String orderType) throws Exception {
		if(orderType.equals("ppl"))
			this.orderType = OrderType.PRODUCT_PLUS;
		else if(orderType.equals("pmn"))
			this.orderType = OrderType.PRODUCT_MINUS;
		else
			throw new Exception("Bad orderType");
	}
	
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see pk.home.libs.combine.web.jsf.flow.AWFBasicControl#init0()
	 */
	@Override
	protected void init0() throws Exception {
		this.warehouses = getWarehouseService().getAllEntitiesAdvanced(
				Warehouse_.division, getTerminalCurrentUser().getDivision(),
				true, -1, -1, Warehouse_.keyName, SortOrderType.ASC);
	}
	
	
	/**
	 * Find product.
	 *
	 * @throws Exception the exception
	 */
	public String findProduct() throws Exception {

		System.out.println("find product for barcode: " + this.barcode);

		if (StringUtils.isNotEmpty(this.barcode))
			try {

				this.product = getProductService().findAdvanced(
						Product_.barcode, barcode);
				
				items = UIItem.createUIItemsList(getItemService().productItems(warehouse, product), 
						orderType == OrderType.PRODUCT_MINUS);
				
				mainItem = items.get(0);				

			} catch (NoResultException ex) {
				this.product = null;
			}

		return "findProduct";
	}
	
	
	/**
	 * Enter operation
	 * 
	 * @return
	 * @throws Exception
	 */
	public String enter() throws Exception {
		try {

			getOrderService().productPlus(getTerminalCurrentUser(),
					getTerminalCurrentUser().getDivision(), orderType, UIItem.createItemsList(items), description, provider);
			
			barcode = "";
			product = null;
			account = null;
			
			
			FacesContext.getCurrentInstance().addMessage(
					null,
					new FacesMessage(FacesMessage.SEVERITY_INFO, "OK: ", "The operation was successful"));

		} catch (Exception e) {
			
			e.printStackTrace();
			FacesContext.getCurrentInstance().addMessage(
					null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: ", e
							.getMessage()));
		}
		
		// Clean
		for(UIItem i: items)
			i.getItem().setId(null);

		return "enter";
	}

	// get's and set's
	// ---------------------------------------------------------------
	public OrderType getOrderType() {
		return orderType;
	}

	public void setOrderType(OrderType orderType) {
		this.orderType = orderType;
	}

	public String getBarcode() {
		return barcode;
	}

	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public List<UIItem> getItems() {
		return items;
	}

	public void setItems(List<UIItem> items) {
		this.items = items;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public UIItem getMainItem() {
		return mainItem;
	}

	public void setMainItem(UIItem mainItem) {
		this.mainItem = mainItem;
	}

	public Warehouse getWarehouse() {
		return warehouse;
	}

	public void setWarehouse(Warehouse warehouse) {
		this.warehouse = warehouse;
	}

	public List<Warehouse> getWarehouses() {
		return warehouses;
	}

	public void setWarehouses(List<Warehouse> warehouses) {
		this.warehouses = warehouses;
	}

	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}

	public Provider getProvider() {
		return provider;
	}

	public void setProvider(Provider provider) {
		this.provider = provider;
	}
	
	
	

}
