package tradesystem.tradeunit.web.jsf.webflow;

import java.io.Serializable;

import javax.persistence.metamodel.SingularAttribute;

import pk.home.libs.combine.web.jsf.flow.AWFBaseLazyLoadTableView;
import tradesystem.tradeunit.domain.Provider;
import tradesystem.tradeunit.domain.Provider_;
import tradesystem.tradeunit.service.ProviderService;

/**
 * JSF view control class for entity class: Provider
 * Provider - продукт
 */
public class ProviderViewWFControl extends AWFBaseLazyLoadTableView<Provider> implements
		Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ProviderService getProviderService() {
		return (ProviderService) findBean("providerService");
	}

	@Override
	protected void aInit() throws Exception {
		
		SingularAttribute<Provider, ?> orderByAttribute = Provider_.id;
		if (csortField != null && csortField.equals("id")) {
			orderByAttribute = Provider_.id;
		} else if (csortField != null && csortField.equals("keyName")) {
			orderByAttribute = Provider_.keyName;
		}

		dataModel = getProviderService().getAllEntities((page - 1) * rows, rows,
				orderByAttribute, getSortOrderType());
	}

	@Override
	protected long initAllRowsCount() throws Exception {		
		return getProviderService().count();
	}
	
	
	public String add(){
		return "add";
	}
	
	public String edit(){
		return "edit";
	}
	
	public String del(){
		return "del";
	}
	
}
