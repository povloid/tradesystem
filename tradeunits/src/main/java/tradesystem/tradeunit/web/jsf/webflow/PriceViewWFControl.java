package tradesystem.tradeunit.web.jsf.webflow;

import java.io.Serializable;

import javax.persistence.metamodel.SingularAttribute;

import pk.home.libs.combine.web.jsf.flow.AWFBaseLazyLoadTableView;
import tradesystem.tradeunit.domain.Price;
import tradesystem.tradeunit.domain.Price_;
import tradesystem.tradeunit.service.PriceService;

/**
 * JSF view control class for entity class: Price
 * Price - цена
 */
public class PriceViewWFControl extends AWFBaseLazyLoadTableView<Price> implements
		Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public PriceService getPriceService() {
		return (PriceService) findBean("priceService");
	}

	@Override
	protected void aInit() throws Exception {
		
		SingularAttribute<Price, ?> orderByAttribute = Price_.id;
		if (csortField != null && csortField.equals("id")) {
			orderByAttribute = Price_.id;
		}// else if (csortField != null && csortField.equals("keyName")) {
		//	orderByAttribute = Price_.keyName;
		//}

		dataModel = getPriceService().getAllEntities((page - 1) * rows, rows,
				orderByAttribute, getSortOrderType());
	}

	@Override
	protected long initAllRowsCount() throws Exception {		
		return getPriceService().count();
	}
	
	
	public String add(){
		return "add";
	}
	
	public String edit(){
		return "edit";
	}
	
	public String del(){
		return "del";
	}
	
}
