package tradesystem.tradeunit.web.jsf.webflow;

import java.io.Serializable;

import javax.persistence.metamodel.SingularAttribute;

import org.primefaces.event.ToggleEvent;
import org.primefaces.model.Visibility;

import pk.home.libs.combine.web.jsf.flow.AWFBaseLazyLoadTableView;
import tradesystem.tradeunit.domain.ProductGroup;
import tradesystem.tradeunit.domain.ProductGroup_;
import tradesystem.tradeunit.service.ProductGroupService;

/**
 * JSF view control class for entity class: ProductGroup ProductGroup - группа
 * продуктов
 */
public class ProductGroupViewWFControl extends
		AWFBaseLazyLoadTableView<ProductGroup> implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;


	
	
	/** The parent entity. */
	private ProductGroup parent;

	public ProductGroupService getProductGroupService() {
		return (ProductGroupService) findBean("productGroupService");
	}

	
	/**
	 * For tree functional
	 * 
	 * @param parentId
	 * @return
	 * @throws Exception
	 */
	public ProductGroup findParent(Object parentId) throws Exception {
		return parentId != null ? getProductGroupService().find(parentId) : null;
	}
	
	// -----------------------------

	@Override
	protected void aInit() throws Exception {

		SingularAttribute<ProductGroup, ?> orderByAttribute = ProductGroup_.id;
		if (csortField != null && csortField.equals("id")) {
			orderByAttribute = ProductGroup_.id;
		} else if (csortField != null && csortField.equals("keyName")) {
			orderByAttribute = ProductGroup_.keyName;
		}

		dataModel = getProductGroupService().getChildrens(parent,
				ProductGroup_.parent, (page - 1) * rows, rows,
				orderByAttribute, getSortOrderType());
	}

	@Override
	protected long initAllRowsCount() throws Exception {
		return getProductGroupService().getChildrensCount(parent, ProductGroup_.parent);
	}

	// actions
	// -----------------------------------------------------------------------

	public String add() {
		return "add";
	}

	public String edit() {
		return "edit";
	}

	public String del() {
		return "del";
	}
	
	public String loadParent(){
		return "loadParent";
	}
	
	public String deleteParent(){
		return "deleteParent";
	}

	// Visual usability functional
	
	private boolean showTableGroup;
	
	public void handleToggleTableGroup(ToggleEvent event) {  
		showTableGroup = event.getVisibility() == Visibility.HIDDEN;
    }  
	
	
	
	// get's and set's
	// -----------------------------------------------------------------------

	public ProductGroup getParent() {
		return parent;
	}

	public void setParent(ProductGroup parent) {
		this.parent = parent;
	}
	
	public Object getParentId() {
		return parent != null ? parent.getId() : null; 
	}
	
	public Object getParentParentId() {
		return parent.getParent() != null ? parent.getParent().getId() : null; 
	}


	public boolean isShowTableGroup() {
		return showTableGroup;
	}


	public void setShowTableGroup(boolean showTableGroup) {
		this.showTableGroup = showTableGroup;
	}
	
	
	
}
