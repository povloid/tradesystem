package tradesystem.tradeunit.web.jsf.webflow;

import java.io.Serializable;

import javax.persistence.metamodel.SingularAttribute;

import pk.home.libs.combine.web.jsf.flow.AWFBaseLazyLoadTableView;
import tradesystem.tradeunit.domain.Division;
import tradesystem.tradeunit.domain.Division_;
import tradesystem.tradeunit.service.DivisionService;

/**
 * JSF view control class for entity class: Division
 * Division - отделение
 */
public class DivisionViewWFControl extends AWFBaseLazyLoadTableView<Division> implements
		Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public DivisionService getDivisionService() {
		return (DivisionService) findBean("divisionService");
	}

	@Override
	protected void aInit() throws Exception {
		
		SingularAttribute<Division, ?> orderByAttribute = Division_.id;
		if (csortField != null && csortField.equals("id")) {
			orderByAttribute = Division_.id;
		} else if (csortField != null && csortField.equals("keyName")) {
			orderByAttribute = Division_.keyName;
		}

		dataModel = getDivisionService().getAllEntities((page - 1) * rows, rows,
				orderByAttribute, getSortOrderType());
	}

	@Override
	protected long initAllRowsCount() throws Exception {		
		return getDivisionService().count();
	}
	
	
	public String add(){
		return "add";
	}
	
	public String edit(){
		return "edit";
	}
	
	public String del(){
		return "del";
	}
	
}
