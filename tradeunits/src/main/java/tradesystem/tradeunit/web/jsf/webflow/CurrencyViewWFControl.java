package tradesystem.tradeunit.web.jsf.webflow;

import java.io.Serializable;

import javax.persistence.metamodel.SingularAttribute;

import pk.home.libs.combine.web.jsf.flow.AWFBaseLazyLoadTableView;
import tradesystem.tradeunit.domain.Currency;
import tradesystem.tradeunit.domain.Currency_;
import tradesystem.tradeunit.service.CurrencyService;

/**
 * JSF view control class for entity class: Currency
 * Currency - валюта
 */
public class CurrencyViewWFControl extends AWFBaseLazyLoadTableView<Currency> implements
		Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public CurrencyService getCurrencyService() {
		return (CurrencyService) findBean("currencyService");
	}

	@Override
	protected void aInit() throws Exception {
		
		SingularAttribute<Currency, ?> orderByAttribute = Currency_.id;
		if (csortField != null && csortField.equals("id")) {
			orderByAttribute = Currency_.id;
		} else if (csortField != null && csortField.equals("keyName")) {
			orderByAttribute = Currency_.keyName;
		}

		dataModel = getCurrencyService().getAllEntities((page - 1) * rows, rows,
				orderByAttribute, getSortOrderType());
	}

	@Override
	protected long initAllRowsCount() throws Exception {		
		return getCurrencyService().count();
	}
	
	
	public String add(){
		return "add";
	}
	
	public String edit(){
		return "edit";
	}
	
	public String del(){
		return "del";
	}
	
}
