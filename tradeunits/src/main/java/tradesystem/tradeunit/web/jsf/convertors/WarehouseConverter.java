package tradesystem.tradeunit.web.jsf.convertors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import pk.home.libs.combine.web.jsf.ABaseConverter;
import tradesystem.tradeunit.domain.Warehouse;
import tradesystem.tradeunit.service.WarehouseService;

/**
 * 
 * @author povloid
 *
 */

@Component
public class WarehouseConverter extends ABaseConverter{
	
	@Autowired
	private WarehouseService warehouseService; 
	
	@Override
	protected Object findObject(String key) throws Exception {
		return warehouseService.find(Long.parseLong(key));
	}

	@Override
	protected String getStringKey(Object value) throws Exception {
		return String.valueOf(((Warehouse) value).getId());
	}

}
