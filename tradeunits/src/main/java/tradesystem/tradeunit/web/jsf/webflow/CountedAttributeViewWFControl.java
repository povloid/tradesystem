package tradesystem.tradeunit.web.jsf.webflow;

import java.io.Serializable;

import javax.persistence.metamodel.SingularAttribute;

import pk.home.libs.combine.web.jsf.flow.AWFBaseLazyLoadTableView;
import tradesystem.tradeunit.domain.CountedAttribute;
import tradesystem.tradeunit.domain.CountedAttribute_;
import tradesystem.tradeunit.service.CountedAttributeService;

/**
 * JSF view control class for entity class: CountedAttribute
 * СountedAttribute - подсчитываемый атрибут
 */
public class CountedAttributeViewWFControl extends AWFBaseLazyLoadTableView<CountedAttribute> implements
		Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public CountedAttributeService getCountedAttributeService() {
		return (CountedAttributeService) findBean("countedAttributeService");
	}

	@Override
	protected void aInit() throws Exception {
		
		SingularAttribute<CountedAttribute, ?> orderByAttribute = CountedAttribute_.id;
		if (csortField != null && csortField.equals("id")) {
			orderByAttribute = CountedAttribute_.id;
		} else if (csortField != null && csortField.equals("keyName")) {
			orderByAttribute = CountedAttribute_.keyName;
		}

		//dataModel = getCountedAttributeService().getAllEntities((page - 1) * rows, rows,
		//		orderByAttribute, getSortOrderType());
		dataModel =  getCountedAttributeService().getChildrens(null,
				CountedAttribute_.parent, (page - 1) * rows, rows,
				orderByAttribute, getSortOrderType());
	}

	@Override
	protected long initAllRowsCount() throws Exception {		
		return getCountedAttributeService().getChildrensCount(null, CountedAttribute_.parent);
	}
	
	
	public String add(){
		return "add";
	}
	
	public String edit(){
		return "edit";
	}
	
	public String del(){
		return "del";
	}
	
}
