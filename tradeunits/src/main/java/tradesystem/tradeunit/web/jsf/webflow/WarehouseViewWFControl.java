package tradesystem.tradeunit.web.jsf.webflow;

import java.io.Serializable;

import javax.persistence.metamodel.SingularAttribute;

import pk.home.libs.combine.web.jsf.flow.AWFBaseLazyLoadTableView;
import tradesystem.tradeunit.domain.Warehouse;
import tradesystem.tradeunit.domain.Warehouse_;
import tradesystem.tradeunit.service.WarehouseService;

/**
 * JSF view control class for entity class: Warehouse
 * Warehouse - склад
 */
public class WarehouseViewWFControl extends AWFBaseLazyLoadTableView<Warehouse> implements
		Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public WarehouseService getWarehouseService() {
		return (WarehouseService) findBean("warehouseService");
	}

	@Override
	protected void aInit() throws Exception {
		
		SingularAttribute<Warehouse, ?> orderByAttribute = Warehouse_.id;
		if (csortField != null && csortField.equals("id")) {
			orderByAttribute = Warehouse_.id;
		} else if (csortField != null && csortField.equals("keyName")) {
			orderByAttribute = Warehouse_.keyName;
		} else if (csortField != null && csortField.equals("division")) {
			orderByAttribute = Warehouse_.division;
		}

		dataModel = getWarehouseService().getAllEntities((page - 1) * rows, rows,
				orderByAttribute, getSortOrderType());
	}

	@Override
	protected long initAllRowsCount() throws Exception {		
		return getWarehouseService().count();
	}
	
	
	public String add(){
		return "add";
	}
	
	public String edit(){
		return "edit";
	}
	
	public String del(){
		return "del";
	}
	
}
