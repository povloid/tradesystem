package tradesystem.tradeunit.web.jsf.webflow;

import java.io.Serializable;

import pk.home.libs.combine.web.jsf.flow.AWFControl;
import tradesystem.tradeunit.domain.Item;
import tradesystem.tradeunit.service.ItemService;

/**
 * JSF edit control class for entity class: Item
 * Item - запись
 */
public class ItemEditWFControl extends AWFControl<Item, Long> implements
		Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public Item findEdited(Long id) throws Exception {
		return getItemService().find(id);
	}

	@Override
	public Item newEdited() throws Exception {
		return new Item();
	}

	public ItemService getItemService() {
		return (ItemService) findBean("itemService");
	}

	@Override
	protected void confirmAddImpl() throws Exception {
		edited = getItemService().persist(edited);
	}

	@Override
	protected void confirmEditImpl() throws Exception {
		edited = getItemService().merge(edited);
	}

	@Override
	protected void confirmDelImpl() throws Exception {
		getItemService().remove(edited);
	}

	// init
	// ----------------------------------------------------------------------------------------------
	protected void init0() throws Exception {
	}

	// gets and sets
	// ---------------------------------------------------------------------------------------------------

}
