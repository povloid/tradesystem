package tradesystem.tradeunit.web.jsf.webflow;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.primefaces.model.SortOrder;

import pk.home.libs.combine.web.jsf.flow.AWFBasicControl;
import pk.home.libs.combine.web.jsf.flow.model.WFLazyDataModel;
import tradesystem.tradeunit.domain.ProductGroup;
import tradesystem.tradeunit.service.ProductGroupService;

public class ProductGroupSelectOneWFControl extends AWFBasicControl implements
		Serializable {

	public ProductGroupService getProductGroupService() {
		return (ProductGroupService) findBean("productGroupService");
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -6986222423369826350L;

	private ProductGroup selected;

	private WFLazyDataModel<ProductGroup> model = new WFLazyDataModel<ProductGroup>() {

		/**
		 * 
		 */
		private static final long serialVersionUID = 4970282896915525138L;
		
		@Override
		protected int count(Map<String, String> filters) throws Exception {
			return (int) getProductGroupService().count();
		}

		@Override
		protected List<ProductGroup> aload(int first, int pageSize,
				String sortField, SortOrder sortOrder,
				Map<String, String> filters) throws Exception {
			return getProductGroupService().getAllEntities(first, pageSize);
		}

		@Override
		public ProductGroup getRowData(String rowKey) {
			for (ProductGroup rd : getDataList()) {
				if (rd.getId() == Long.parseLong(rowKey))
					return rd;
			}
			return null;
		}

		@Override
		public Object getRowKey(ProductGroup object) {
			return object.getId();
		}
	};

	// actions
	// -----------------------------------------------------------------------------------------

	/**
	 * Выбор остановки
	 * 
	 * @return
	 * @throws Exception
	 */
	public String select() throws Exception {

		if (selected == null) {
			FacesContext.getCurrentInstance().addMessage(
					null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: ",
							"Элемент небыл выбран"));
			throw new Exception("Элемент небыл выбран");
		}

		return "select";
	}

	// get's and set's
	// -------------------------------------------------------------------------------------------------

	public ProductGroup getSelected() {
		return selected;
	}

	public void setSelected(ProductGroup selected) {
		this.selected = selected;
	}

	public WFLazyDataModel<ProductGroup> getModel() {
		return model;
	}

	public void setModel(WFLazyDataModel<ProductGroup> model) {
		this.model = model;
	}

	public Long getSelectedId() {
		if (selected != null)
			return selected.getId();
		else
			return null;
	}

}
