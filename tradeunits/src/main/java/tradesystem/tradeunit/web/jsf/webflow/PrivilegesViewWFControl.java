package tradesystem.tradeunit.web.jsf.webflow;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.metamodel.SingularAttribute;

import pk.home.libs.combine.dao.PredicatePair;
import pk.home.libs.combine.web.jsf.flow.AWFBaseLazyLoadTableView;
import tradesystem.tradeunit.domain.Privileges;
import tradesystem.tradeunit.domain.Privileges_;
import tradesystem.tradeunit.domain.security.UserAccount;
import tradesystem.tradeunit.service.PrivilegesService;
import tradesystem.tradeunit.service.security.UserAccountService;

/**
 * JSF view control class for entity class: Privileges
 * Privileges - привелегии
 */
public class PrivilegesViewWFControl extends AWFBaseLazyLoadTableView<Privileges> implements
		Serializable {
	
    private UserAccount selectedUserAccount; 

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public PrivilegesService getPrivilegesService() {
		return findBean("privilegesService", PrivilegesService.class);
	}
		
	public UserAccountService getUserAccountService() {
		return findBean("userAccountService", UserAccountService.class);
	}
	

	@Override
	protected void aInit() throws Exception {
		
		List<PredicatePair<Privileges>> ppList = new ArrayList<>();
		
		ppList.add(new PredicatePair<>(Privileges_.userAccount, selectedUserAccount));
		
		SingularAttribute<Privileges, ?> orderByAttribute = Privileges_.id;
		if (csortField != null && csortField.equals("id")) {
			orderByAttribute = Privileges_.id;
		} else if (csortField != null && csortField.equals("division")) {
			orderByAttribute = Privileges_.division;
		} else if (csortField != null && csortField.equals("warehouse")) {
			orderByAttribute = Privileges_.warehouse;
		} else if (csortField != null && csortField.equals("cash")) {
			orderByAttribute = Privileges_.cash;
		}
		
		dataModel = getPrivilegesService().getAllEntitiesAdvanced(ppList, false, (page - 1) * rows, rows,
				orderByAttribute, getSortOrderType());
		
		//dataModel = getPrivilegesService().getAllEntities((page - 1) * rows, rows,
		//		orderByAttribute, getSortOrderType());
	}

	@Override
	protected long initAllRowsCount() throws Exception {
		
		List<PredicatePair<Privileges>> ppList = new ArrayList<>();
		
		ppList.add(new PredicatePair<>(Privileges_.userAccount, selectedUserAccount));
		
		return getPrivilegesService().countAdvanced(ppList);
		
		//return getPrivilegesService().count();
		
	}
	
	
	public void findUserAccount(Long id) throws Exception {
		this.selectedUserAccount = getUserAccountService().find(id);
	}
	
	
	public String add(){
		return "add";
	}
	
	public String edit(){
		return "edit";
	}
	
	public String del(){
		return "del";
	}

	
	// get's and set's ----------------------------------------------------------------------------------------------
	
	
	public UserAccount getSelectedUserAccount() {
		return selectedUserAccount;
	}

	public void setSelectedUserAccount(UserAccount selectedUserAccount) {
		this.selectedUserAccount = selectedUserAccount;
	}
	
	
}
