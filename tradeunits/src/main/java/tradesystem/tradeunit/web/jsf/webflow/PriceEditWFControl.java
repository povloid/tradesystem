package tradesystem.tradeunit.web.jsf.webflow;

import java.io.Serializable;

import pk.home.libs.combine.web.jsf.flow.AWFControl;
import tradesystem.tradeunit.domain.Price;
import tradesystem.tradeunit.service.PriceService;

/**
 * JSF edit control class for entity class: Price
 * Price - цена
 */
public class PriceEditWFControl extends AWFControl<Price, Long> implements
		Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public Price findEdited(Long id) throws Exception {
		return getPriceService().find(id);
	}

	@Override
	public Price newEdited() throws Exception {
		return new Price();
	}

	public PriceService getPriceService() {
		return (PriceService) findBean("priceService");
	}

	@Override
	protected void confirmAddImpl() throws Exception {
		edited = getPriceService().persist(edited);
	}

	@Override
	protected void confirmEditImpl() throws Exception {
		edited = getPriceService().merge(edited);
	}

	@Override
	protected void confirmDelImpl() throws Exception {
		getPriceService().remove(edited);
	}

	// init
	// ----------------------------------------------------------------------------------------------
	protected void init0() throws Exception {
	}

	// gets and sets
	// ---------------------------------------------------------------------------------------------------

}
