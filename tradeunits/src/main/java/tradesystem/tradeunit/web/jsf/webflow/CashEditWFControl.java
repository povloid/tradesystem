package tradesystem.tradeunit.web.jsf.webflow;

import java.io.Serializable;

import pk.home.libs.combine.web.jsf.flow.AWFControl;
import tradesystem.tradeunit.domain.Cash;
import tradesystem.tradeunit.service.CashService;

/**
 * JSF edit control class for entity class: Cash
 * Cash - касса
 */
public class CashEditWFControl extends AWFControl<Cash, Long> implements
		Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public Cash findEdited(Long id) throws Exception {
		return getCashService().find(id);
	}

	@Override
	public Cash newEdited() throws Exception {
		return new Cash();
	}

	public CashService getCashService() {
		return (CashService) findBean("cashService");
	}

	@Override
	protected void confirmAddImpl() throws Exception {
		edited = getCashService().persist(edited);
	}

	@Override
	protected void confirmEditImpl() throws Exception {
		edited = getCashService().merge(edited);
	}

	@Override
	protected void confirmDelImpl() throws Exception {
		getCashService().remove(edited);
	}

	// init
	// ----------------------------------------------------------------------------------------------
	protected void init0() throws Exception {
	}

	// gets and sets
	// ---------------------------------------------------------------------------------------------------

}
