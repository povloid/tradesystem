package tradesystem.tradeunit.web.jsf.webflow;

import java.io.Serializable;

import javax.persistence.metamodel.SingularAttribute;

import pk.home.libs.combine.web.jsf.flow.AWFBaseLazyLoadTableView;
import tradesystem.tradeunit.domain.Cash;
import tradesystem.tradeunit.domain.Cash_;
import tradesystem.tradeunit.service.CashService;

/**
 * JSF view control class for entity class: Cash
 * Cash - касса
 */
public class CashViewWFControl extends AWFBaseLazyLoadTableView<Cash> implements
		Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public CashService getCashService() {
		return (CashService) findBean("cashService");
	}

	@Override
	protected void aInit() throws Exception {
		
		SingularAttribute<Cash, ?> orderByAttribute = Cash_.id;
		if (csortField != null && csortField.equals("id")) {
			orderByAttribute = Cash_.id;
		} else if (csortField != null && csortField.equals("keyName")) {
			orderByAttribute = Cash_.keyName;
		} else if (csortField != null && csortField.equals("division")) {
			orderByAttribute = Cash_.division;
		}

		dataModel = getCashService().getAllEntities((page - 1) * rows, rows,
				orderByAttribute, getSortOrderType());
	}

	@Override
	protected long initAllRowsCount() throws Exception {		
		return getCashService().count();
	}
	
	
	public String add(){
		return "add";
	}
	
	public String edit(){
		return "edit";
	}
	
	public String del(){
		return "del";
	}
	
}
