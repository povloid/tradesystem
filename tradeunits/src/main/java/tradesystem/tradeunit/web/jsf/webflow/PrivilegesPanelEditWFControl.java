package tradesystem.tradeunit.web.jsf.webflow;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.apache.log4j.Logger;

import pk.home.libs.combine.dao.ABaseDAO.SortOrderType;
import pk.home.libs.combine.dao.PredicatePair;
import pk.home.libs.combine.web.jsf.flow.AWFBasicControl;
import tradesystem.tradeunit.domain.Cash;
import tradesystem.tradeunit.domain.CashPrivileges;
import tradesystem.tradeunit.domain.Cash_;
import tradesystem.tradeunit.domain.CommonPrivilegesTypes;
import tradesystem.tradeunit.domain.Division;
import tradesystem.tradeunit.domain.Privileges;
import tradesystem.tradeunit.domain.Privileges_;
import tradesystem.tradeunit.domain.Warehouse;
import tradesystem.tradeunit.domain.WarehousePrivileges;
import tradesystem.tradeunit.domain.Warehouse_;
import tradesystem.tradeunit.domain.security.UserAccount;
import tradesystem.tradeunit.service.CashService;
import tradesystem.tradeunit.service.DivisionService;
import tradesystem.tradeunit.service.PrivilegesService;
import tradesystem.tradeunit.service.WarehouseService;
import tradesystem.tradeunit.service.security.UserAccountService;

/**
 * JSF view control class for entity class: Privileges
 * Privileges - привелегии
 */
public class PrivilegesPanelEditWFControl extends AWFBasicControl implements
		Serializable {
	
	private static final Logger LOG = Logger.getLogger(PrivilegesPanelEditWFControl.class);
	
    private UserAccount selectedUserAccount;
    
    private List<Privileges> privileges;
    
    public class PanelDivisionBlock implements Serializable {

    	/**
    	 * 
    	 */
    	private static final long serialVersionUID = 1L;
    	
    	private Division division;
    	
    	private List<Privileges> commonPrivileges;
    	private List<Privileges> cashPrivileges;
    	private List<Privileges> warehousePrivileges;
    	
    	/**
    	 * Instantiates a new division block.
    	 */
    	public PanelDivisionBlock() {
    		super();
    	}
    	
    	/**
    	 * Instantiates a new division block.
    	 *
    	 * @param division the division
    	 * @param commonPrivileges the common privileges
    	 * @param cashPrivileges the cash privileges
    	 * @param warehousePrivileges the warehouse privileges
    	 */
    	public PanelDivisionBlock(Division division,
    			List<Privileges> commonPrivileges,
    			List<Privileges> cashPrivileges,
    			List<Privileges> warehousePrivileges) {
    		super();
    		this.division = division;
    		this.commonPrivileges = commonPrivileges;
    		this.cashPrivileges = cashPrivileges;
    		this.warehousePrivileges = warehousePrivileges;
    	}
    	
    	/**
	     * Warehouses.
	     *
	     * @return the list
	     */
	    public List<Warehouse> ws() {
    		List<Warehouse> list = new ArrayList<>(); 
    		
    		for(Privileges p: warehousePrivileges)
    			if(!list.contains(p.getWarehouse())) 
    					list.add(p.getWarehouse());
    	
    		return list;
    	}
	    
	    /**
    	 * Wps.
    	 *
    	 * @param warehouse the warehouse
    	 * @return the list
    	 */
    	public List<Privileges> wps(Warehouse warehouse) {
    		List<Privileges> list = new ArrayList<>(); 
    		
    		for(Privileges p: warehousePrivileges)
    			if(p.getWarehouse().equals(warehouse)) 
    					list.add(p);
    	
    		return list;
    	}
	    
	    /**
    	 * Cashes.
    	 *
    	 * @return the list
    	 */
    	public List<Cash> cs() {
    		List<Cash> list = new ArrayList<>(); 
    		
    		for(Privileges p: cashPrivileges)
    			if(!list.contains(p.getCash())) 
    					list.add(p.getCash());
   
    		return list;
    	}
    	
    	/**
	     * Cps.
	     *
	     * @param cash the cash
	     * @return the list
	     */
	    public List<Privileges> cps(Cash cash) {
    		List<Privileges> list = new ArrayList<>(); 
    		
    		for(Privileges p: cashPrivileges)
    			if(p.getCash().equals(cash)) 
    					list.add(p);
    	
    		return list;
    	}
    	

    	public Division getDivision() {
    		return division;
    	}

    	public void setDivision(Division division) {
    		this.division = division;
    	}

    	public List<Privileges> getCommonPrivileges() {
    		return commonPrivileges;
    	}

    	public void setCommonPrivileges(List<Privileges> commonPrivileges) {
    		this.commonPrivileges = commonPrivileges;
    	}

    	public List<Privileges> getCashPrivileges() {
    		return cashPrivileges;
    	}

    	public void setCashPrivileges(List<Privileges> cashPrivileges) {
    		this.cashPrivileges = cashPrivileges;
    	}

    	public List<Privileges> getWarehousePrivileges() {
    		return warehousePrivileges;
    	}

    	public void setWarehousePrivileges(List<Privileges> warehousePrivileges) {
    		this.warehousePrivileges = warehousePrivileges;
    	}
    }
    
    private List<PanelDivisionBlock> divisionsBlocks;
    

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public PrivilegesService getPrivilegesService() {
		return findBean("privilegesService", PrivilegesService.class);
	}
		
	public UserAccountService getUserAccountService() {
		return findBean("userAccountService", UserAccountService.class);
	}
	
	public DivisionService getDivisionService() {
		return findBean("divisionService", DivisionService.class);
	}
	
	public WarehouseService getWarehouseService() {
		return findBean("warehouseService", WarehouseService.class);
	}
	
	public CashService getCashService() {
		return findBean("cashService", CashService.class);
	}
	
	
	
	
	/**
	 * Find user account for id
	 * @param id
	 * @throws Exception
	 */
	public void findUserAccount(Long id) throws Exception {
		LOG.debug(">>> Select user for id=" + id);
		
		this.selectedUserAccount = getUserAccountService().find(id);
		
		LOG.debug(">>> Selected user " + selectedUserAccount.getUsername());
		
		List<PredicatePair<Privileges>> ppList = new ArrayList<>();
		ppList.add(new PredicatePair<>(Privileges_.userAccount, selectedUserAccount));
		
		privileges = getPrivilegesService().getAllEntitiesAdvanced(ppList, true, 0, 0, 
				Privileges_.division, SortOrderType.ASC);
		
		LOG.debug(">>> void findUserAccount(Long id) => privileges size is " + privileges.size());
		
		
		divisionsBlocks = new ArrayList<>();
		for(Division division: getDivisionService().getAllEntities()){
			divisionsBlocks.add(new PanelDivisionBlock( division,
					getCommonPrivileges(privileges, division, selectedUserAccount), 
					getPrivilegesForCashes(getCashService(), privileges, division, selectedUserAccount),
					getPrivilegesForWarehouses(getWarehouseService(), privileges, division, selectedUserAccount))); 
		}
		
	}
	
	
	/**
	 * Save.
	 *
	 * @return the string
	 * @throws Exception the exception
	 */
	public void voidSave() throws Exception {
		save();
	}
	
	
	/**
	 * Save.
	 *
	 * @return the string
	 * @throws Exception the exception
	 */
	public String save() throws Exception {
		
		try {
			
			List<Privileges> allList = new ArrayList<>();
			
			for(PanelDivisionBlock block: divisionsBlocks){
				allList.addAll(block.getCommonPrivileges());
				allList.addAll(block.getWarehousePrivileges());
				allList.addAll(block.getCashPrivileges());
			}

			getPrivilegesService().save(allList);
			
			
			FacesContext.getCurrentInstance().addMessage(
					null,
					new FacesMessage(FacesMessage.SEVERITY_INFO, "Info: ", "Привелегии сохранены"));
			
		} catch (Exception e) {
			e.printStackTrace();
			FacesContext.getCurrentInstance().addMessage(
					null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: ", e
							.getMessage()));
		}
		
		return "save";
	}
	
	/**
	 * Save and close.
	 *
	 * @return the string
	 * @throws Exception the exception
	 */
	public String saveAndClose() throws Exception {
		save();
		return "end";
	}
	
	
	/**
	 * Gets the common privileges.
	 *
	 * @param privileges the privileges
	 * @param division the division
	 * @param selectedUserAccount the selected user account
	 * @return the common privileges
	 * @throws Exception the exception
	 */
	private static List<Privileges> getCommonPrivileges(List<Privileges> privileges, Division division, UserAccount selectedUserAccount) throws Exception {
		
		List<Privileges> list = new ArrayList<>();
		
		for (CommonPrivilegesTypes cpt : CommonPrivilegesTypes.values()) {

			LOG.debug(">>> List<Privileges> getCommonPriveleges => loop cpt "
					+ cpt);
			LOG.debug(">>> List<Privileges> getCommonPriveleges => privileges size is "
					+ privileges.size());

			boolean added = false;
			for (Privileges p : privileges)
				if (p.getCommonPrivilegesTypes() != null
						&& p.getDivision().equals(division)
						&& p.getCommonPrivilegesTypes() == cpt) {
					list.add(p);
					added = true;
				}

			if (!added)
				list.add(new Privileges(division, selectedUserAccount, cpt,
						null, null, null, null, "..."));
		}

		LOG.debug(">>> List<Privileges> getCommonPriveleges => result list size is "
				+ list.size());

		return list;
	}
	
	
	
	
	/**
	 * Gets the privileges for warehouses.
	 *
	 * @param warehouseService the warehouse service
	 * @param privileges the privileges
	 * @param division the division
	 * @param selectedUserAccount the selected user account
	 * @return the privileges for warehouses
	 * @throws Exception the exception
	 */
	private static List<Privileges> getPrivilegesForWarehouses(WarehouseService warehouseService, 
			List<Privileges> privileges, Division division, UserAccount selectedUserAccount) throws Exception {
		
		List<Privileges> list = new ArrayList<>();
		
		List<PredicatePair<Warehouse>> ppList = new ArrayList<>();
		ppList.add(new PredicatePair<>(Warehouse_.division, division));
		
		for (Warehouse warehouse : warehouseService.getAllEntitiesAdvanced(
				ppList, true, 0, 0, Warehouse_.keyName, SortOrderType.ASC)) {

			LOG.debug(">>> List<Privileges> getPrivilegesForWarehouses => warehouse: "
					+ warehouse);
			LOG.debug(">>> List<Privileges> getPrivilegesForWarehouses => privileges size is "
					+ privileges.size());

			for (WarehousePrivileges wp : WarehousePrivileges.values()) {

				boolean added = false;
				for (Privileges p : privileges)
					if (p.getWarehouse() != null
							&& p.getDivision().equals(division)
							&& p.getWarehouse().equals(warehouse)
							&& p.getWarehousePrivileges() == wp) {
						list.add(p);
						added = true;
					}

				if (!added)
					list.add(new Privileges(division, selectedUserAccount,
							null, warehouse, wp, null, null, "..."));

				LOG.debug(">>> List<Privileges> warehouse => " + warehouse);
			}

		}

		LOG.debug(">>> List<Privileges> getPrivilegesForWarehouses => result list size is " + list.size());
		
		return list;
	}
	
	
	

	/**
	 * Gets the privileges for cashes.
	 *
	 * @param cashService the cash service
	 * @param privileges the privileges
	 * @param division the division
	 * @param selectedUserAccount the selected user account
	 * @return the privileges for cashes
	 * @throws Exception the exception
	 */
	private static List<Privileges> getPrivilegesForCashes(CashService cashService, 
			List<Privileges> privileges, Division division, UserAccount selectedUserAccount) throws Exception {
		
		List<Privileges> list = new ArrayList<>();
		
		List<PredicatePair<Cash>> ppList = new ArrayList<>();
		ppList.add(new PredicatePair<>(Cash_.division, division));
		
		
		for (Cash cash : cashService.getAllEntitiesAdvanced(ppList, true, 0, 0,
				Cash_.keyName, SortOrderType.ASC)) {

			LOG.debug(">>> List<Privileges> getPrivilegesForCashes => warehouse: "
					+ cash);
			LOG.debug(">>> List<Privileges> getPrivilegesForCashes => privileges size is "
					+ privileges.size());

			for (CashPrivileges cp : CashPrivileges.values()) {

				boolean added = false;
				for (Privileges p : privileges)
					if (p.getCash() != null && p.getDivision().equals(division)
							&& p.getCash().equals(cash)
							&& p.getCashPrivileges() == cp) {
						list.add(p);
						added = true;
					}

				if (!added)
					list.add(new Privileges(division, selectedUserAccount,
							null, null, null, cash, cp, "..."));
			}
		}

		LOG.debug(">>> List<Privileges> getPrivilegesForCashes => result list size is " + list.size());
		
		return list;
	}
	
	
	
	
	// get's and set's ----------------------------------------------------------------------------------------------
	
	public UserAccount getSelectedUserAccount() {
		return selectedUserAccount;
	}

	public void setSelectedUserAccount(UserAccount selectedUserAccount) {
		this.selectedUserAccount = selectedUserAccount;
	}

	public List<Privileges> getPrivileges() {
		return privileges;
	}

	public void setPrivileges(List<Privileges> privileges) {
		this.privileges = privileges;
	}

	public List<PanelDivisionBlock> getDivisionsBlocks() {
		return divisionsBlocks;
	}

	public void setDivisionsBlocks(List<PanelDivisionBlock> divisionsBlocks) {
		this.divisionsBlocks = divisionsBlocks;
	}

}
