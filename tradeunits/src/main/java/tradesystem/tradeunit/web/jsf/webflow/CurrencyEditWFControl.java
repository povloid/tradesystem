package tradesystem.tradeunit.web.jsf.webflow;

import java.io.Serializable;

import pk.home.libs.combine.web.jsf.flow.AWFControl;
import tradesystem.tradeunit.domain.Currency;
import tradesystem.tradeunit.service.CurrencyService;

/**
 * JSF edit control class for entity class: Currency
 * Currency - валюта
 */
public class CurrencyEditWFControl extends AWFControl<Currency, Long> implements
		Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public Currency findEdited(Long id) throws Exception {
		return getCurrencyService().find(id);
	}

	@Override
	public Currency newEdited() throws Exception {
		return new Currency();
	}

	public CurrencyService getCurrencyService() {
		return (CurrencyService) findBean("currencyService");
	}

	@Override
	protected void confirmAddImpl() throws Exception {
		edited = getCurrencyService().persist(edited);
	}

	@Override
	protected void confirmEditImpl() throws Exception {
		edited = getCurrencyService().merge(edited);
	}

	@Override
	protected void confirmDelImpl() throws Exception {
		getCurrencyService().remove(edited);
	}

	// init
	// ----------------------------------------------------------------------------------------------
	protected void init0() throws Exception {
	}

	// gets and sets
	// ---------------------------------------------------------------------------------------------------

}
