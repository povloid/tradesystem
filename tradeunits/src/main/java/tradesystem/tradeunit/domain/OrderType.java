package tradesystem.tradeunit.domain;

import java.io.Serializable;


/**
 * 
 * Class OrderType
 *
 * @author Kopychenko Pavel
 *
 * @date Oct 1, 2013
 *
 */
public enum OrderType implements Serializable {
	
	/** The cash plus. */
	CASH_PLUS,
	
	/** The cash minus. */
	CASH_MINUS,
	
	/** The product plus. */
	PRODUCT_PLUS, 
	
	/** The product minus. */
	PRODUCT_MINUS,
	
	/** The product sale. */
	PRODUCT_SALE, 
	
	/** The product return. */
	PRODUCT_RETURN
}
