package tradesystem.tradeunit.domain;

import java.math.BigDecimal;

/**
 * The Interface ICountsObject.
 *
 * @author Kopychenko Pavel
 * @date Jun 21, 2013
 */
public interface ICountsObject {


	/**
	 * Gets the value type.
	 *
	 * @return the value type
	 */
	Measure.MeasureType getValueType();
	
	/**
	 * The Enum Type.
	 */
	enum Type {
		MONEY, THING
	}
	
	Type getType();
	
	
	/**
	 * Gets the denominator.
	 *
	 * @return the denominator
	 */
	BigDecimal getDenominator();
	
	
	/**
	 * Gets the counted attribute.
	 *
	 * @return the counted attribute
	 */
	CountedAttribute getCountedAttribute();
	
	
	/**
	 * Gets the key name.
	 *
	 * @return the key name
	 */
	String getKeyName();

}
