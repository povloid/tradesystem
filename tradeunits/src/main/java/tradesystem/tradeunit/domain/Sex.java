package tradesystem.tradeunit.domain;


/**
 * Пол
 * @author povloid
 *
 */
public enum Sex {
	MAN, WOMAN
}
