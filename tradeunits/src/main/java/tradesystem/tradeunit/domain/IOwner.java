package tradesystem.tradeunit.domain;

/**
 * The Interface IOwner.
 *
 * @author povloid
 */
public interface IOwner {
	
	/**
	 * Gets the key name.
	 *
	 * @return the key name
	 */
	public String getKeyName();
	
	

}
