package tradesystem.tradeunit.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Index;
import org.hibernate.validator.constraints.Length;

import tradesystem.tradeunit.domain.Measure.MeasureType;

/**
 * Entity class: Product Product - продукт
 * 
 * @author Kopychenko Pavel
 * 
 * @date Jun 12, 2013
 * 
 */
@Entity
@Table(schema = "public", name = "product")
@NamedQueries({
		@NamedQuery(name = "Product.findAll", query = "select a from Product a order by a.id"),
		@NamedQuery(name = "Product.findByPrimaryKey", query = "select a from Product a where a.id = ?1") })
public class Product implements Serializable, ICountsObject {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The version. */
	@Version
	private int version;

	/** The product group. */
	@ManyToOne
	private ProductGroup productGroup;

	/** The id. */
	@Column(nullable = false)
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	/** The key name. */
	@NotNull
	@Length(max = 250)
	@Column(unique = true, nullable = false, length = 250)
	@Index(name = "product_keyname_idx")
	private String keyName;

	/** The barcode. */
	@NotNull
	@Length(max = 100)
	@Column(unique = true, nullable = false, length = 100)
	@Index(name = "product_barcode_idx")
	private String barcode;

	/** The description. */
	@Length(max = 3000)
	@Column(length = 3000)
	private String description;
	
	@NotNull
	@ManyToOne()
	private Measure measure;
	
	@NotNull
	@Column(nullable = false)
	private BigDecimal denominator;
	
	@Override
	public Type getType() {
		return Type.THING;
	}
	
	// COUNTED ATTRIBUTE
	@ManyToOne
	private CountedAttribute countedAttribute; 
	
	// IMAGE
	
	public static final String IMAGESTORE_PREFIX = "product";
	
	@ManyToMany(fetch = FetchType.LAZY)
	private Set<Image> images;
	
	private String mainImage;
	
	@OneToMany(mappedBy = "product", fetch = FetchType.EAGER)
	private Set<Price> prices;
	
	@OneToMany(mappedBy = "product", fetch = FetchType.LAZY)
	private Set<Account> accounts;
	
	
	/**
	 * Gets the price.
	 *
	 * @param division the division
	 * @return the price
	 */
	public Price getPrice(Division division){
		
		for(Price p: prices)
			if(p.getDivision().equals(division))
				return p;
		
		return null;
	}

	/**
	 * Instantiates a new product.
	 */
	public Product() {
		super();
	}

	
	/**
	 * Preparing persist or update.
	 * 
	 * @throws Exception
	 *             the exception
	 */
	@PrePersist
	@PreUpdate
	public void prePersistOrUpdate() throws Exception {		
		preparing();

		if(measure.getMeasureType() == MeasureType.FRACTION && denominator.compareTo(new BigDecimal(0)) <= 0)
			throw new Exception("Denominator mast be greater than zero!");
	}
	
	
	
	/**
	 * Preparing.
	 *
	 * @throws Exception the exception
	 */
	private void preparing() throws Exception{
		
		
		if(measure.getMeasureType() != MeasureType.FRACTION)
			denominator = new BigDecimal(0);
		
	}

	
	// get's and set's
	// -----------------------------------------------------------------------------------------------------------------------------

	public int getVersion() {
		return version;
	}

	public Measure.MeasureType getValueType() {
		return measure.getMeasureType();
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getKeyName() {
		return this.keyName;
	}

	public void setKeyName(String keyName) {
		System.out.println(keyName);
		this.keyName = keyName;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getBarcode() {
		return barcode;
	}

	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}

	public ProductGroup getProductGroup() {
		return productGroup;
	}

	public void setProductGroup(ProductGroup productGroup) {
		this.productGroup = productGroup;
	}

	public BigDecimal getDenominator() {
		return denominator;
	}

	public void setDenominator(BigDecimal denominator) {
		this.denominator = denominator;
	}

	public Measure getMeasure() {
		return measure;
	}


	public void setMeasure(Measure measure) {
		this.measure = measure;
	}

	public Set<Image> getImages() {
		return images;
	}


	public void setImages(Set<Image> images) {
		this.images = images;
	}

	public String getMainImage() {
		return mainImage;
	}


	public void setMainImage(String mainImage) {
		this.mainImage = mainImage;
	}

	@Override
	public CountedAttribute getCountedAttribute() {
		return countedAttribute;
	}


	public void setCountedAttribute(CountedAttribute countedAttribute) {
		this.countedAttribute = countedAttribute;
	}


	public Set<Price> getPrices() {
		return prices;
	}


	public void setPrices(Set<Price> prices) {
		this.prices = prices;
	}

	public Set<Account> getAccounts() {
		return accounts;
	}

	public void setAccounts(Set<Account> accounts) {
		this.accounts = accounts;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// not set
		if (!(object instanceof Product)) {
			return false;
		}
		Product other = (Product) object;
		if ((this.id == null && other.id != null)
				|| (this.id != null && !this.id.equals(other.id))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "tradesystem.tradeunit.domain.Product[ id=" + id + " ]";
	}



}
