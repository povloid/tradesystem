package tradesystem.tradeunit.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Index;

/**
 * 
 * Class Account
 *
 * @author Kopychenko Pavel
 *
 * @date Jun 20, 2013
 *
 */
@Entity
@Table(schema = "public", name = "Account", uniqueConstraints = @UniqueConstraint(columnNames = {
		"parentAccount_id", "countedAttribute_id" }))
@NamedQueries({
		@NamedQuery(name = "Account.findAll", query = "select a from Account a order by a.id"),
		@NamedQuery(name = "Account.findByPrimaryKey", query = "select a from Account a where a.id = ?1") })
public class Account implements Serializable {

	public final static int SCALE = 4; 
	public final static int PRECISION = 19;
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/** The version. */
	@Version
	private int version;

	@Column(nullable = false)
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private String description;

	// Specific ----------------------------------------------------------

	@Column(nullable = false)
	private boolean credit;

	@NotNull
	@ManyToOne
	private Division division;

	/** The product. */
	@ManyToOne
	private Product product;
	
	/** The product. */
	@ManyToOne
	private Currency currency;

	@NotNull
	@Column(nullable = false, scale = SCALE, precision = PRECISION)
	private BigDecimal value;

	@NotNull
	@Column(nullable = false, scale = 0)
	private BigDecimal numerator;


	// Owners ------------------------------------------------------------
	@ManyToOne
	private Cash cash;
	
	@ManyToOne
	private Customer customer;
	
	@ManyToOne
	private Warehouse warehouse;
	
	
	// SUB ACCOUNT
	@ManyToOne
	@JoinColumn(name = "parentAccount_id", referencedColumnName = "id")
	@Index(name = "account_parent_idx")
	private Account parentAccount;
	
	// TODO: tree graph checking
	
	@OneToMany(mappedBy = "parentAccount", fetch = FetchType.LAZY)
	private List<Account> subAccounts = new ArrayList<Account>();
	
	@ManyToOne
	private CountedAttribute countedAttribute; 
	

	/**
	 * Sets the owner.
	 *
	 * @param owner the new owner
	 */
	public void setOwner(IOwner owner) {
		cash = owner instanceof Cash? (Cash) owner : null;
		customer = owner instanceof Customer? (Customer) owner : null;
		warehouse = owner instanceof Warehouse? (Warehouse) owner : null;
	}
	
	/**
	 * Gets the owner.
	 *
	 * @return the owner
	 */
	public IOwner getOwner(){
		return cash != null ? cash : customer != null ? customer : warehouse != null ? warehouse : null; 
	}
	
	// Owners ------------------------------------------------------------
	
	
	/**
	 * Preparing persist or update.
	 * 
	 * @throws Exception
	 *             the exception
	 */
	@PrePersist
	@PreUpdate
	public void prePersistOrUpdate() throws Exception {
		checkOne();
		preparing();
		checkTwo();
	}

	/**
	 * Check.
	 * 
	 * @throws Exception
	 *             the exception
	 */
	public void checkOne() throws Exception {

		// Owners check
		checkOnlyOne("Customer or cash or warehouse required!",
				"Only one (customer or cash or warehouse) required!", cash, customer, warehouse);
		
		// Counters check
		checkOnlyOne("product or currency required!","Only one (product or currency) required!", product, currency);

		// Cash validation
		
		// value
		if(value == null)
			throw new Exception("The value is null!");
		
		if(numerator == null)
			throw new Exception("The numerator is null!");
		
		
		

	}

	/**
	 * Preparing.
	 * 
	 * @throws Exception
	 *             the exception
	 */
	public void preparing() throws Exception {
		
		// Logic states
		if(getOwner() instanceof Warehouse){
			
			if(getCountsObject() instanceof Product){
				credit = false;
			}
			
		} else if(getOwner() instanceof Cash){
			
			if(getCountsObject() instanceof Currency){
				credit = false;
			}
			
		} else if(getOwner() instanceof Customer){
			
			if(getCountsObject() instanceof Currency){
				credit = true;
			}
			
		} 
	}

	/**
	 * Check.
	 * 
	 * @throws Exception
	 *             the exception
	 */
	public void checkTwo() throws Exception {
		
		// Credit validation		
		if (!credit && (value.signum() == -1 || numerator.signum() == -1))
			throw new Exception(
					"In no credit account value or numerator mast have positive value!\n" +
					"id = " + id + 		
					" value = " + value + " numerator=" + numerator + ".  Owner:" + getOwner().getKeyName() 
					+ " CountsObject: " + getCountsObject().getKeyName());
		
		
		// TODO: написать тест
		if(cash != null && (product != null || currency == null))
			throw new Exception("Cash mast contain only currency counter.");
		// TODO: написать тест
		if(warehouse != null && (product == null || currency != null))
			throw new Exception("warehouse mast contain only product counter.");
		
		// sub account check
		// TODO: Проверку родителей на отрыв графа
		
		// TODO: написать тест
		if (countedAttribute != null) {
			Account p = this;
			int r = 0;

			while (p != null) {
				r += p.getCountedAttribute().equals(getCountsObject().getCountedAttribute()) ? 1 : 0;
				
				if(r == 1 && !(p.getParentAccount() == null)) 
					throw new Exception("Bad sub acounts structure! Roor account is not contain main counted attribute");
				
				p = p.getParentAccount();
			}

			if (r != 1)	throw new Exception("Bad sub acounts structure!");
		}
		
		
			
	}

	/**
	 * Gets the counts object.
	 *
	 * @return the counts object
	 */
	public ICountsObject getCountsObject() {
		return product != null ? product : currency != null ? currency : null;
	}
	
	/**
	 * Sets the counts object.
	 *
	 * @param owner the new owner
	 */
	public void setCountsObject(ICountsObject owner) {
		product = owner instanceof Product ? (Product) owner : null;
		currency = owner instanceof Currency ? (Currency) owner : null;
	}

	/**
	 * Check only one.
	 *
	 * @param errorMesage1 the error mesage1
	 * @param errorMesage2 the error mesage2
	 * @param objects the objects
	 * @throws Exception the exception
	 */
	private void checkOnlyOne(String errorMesage1, String errorMesage2,
			Object... objects) throws Exception {
		short checkN = 0;

		for (Object o : objects)
			checkN += o != null ? 1 : 0;

		if (checkN == 0)
			throw new Exception(errorMesage1);

		if (checkN > 1)
			throw new Exception(errorMesage2);

	}
	
	
	/**
	 * Instantiates a new account.
	 */
	public Account() {
		super();
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public Division getDivision() {
		return division;
	}

	public void setDivision(Division division) {
		this.division = division;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public BigDecimal getValue() {
		return value;
	}

	public void setValue(BigDecimal value) {
		this.value = value != null ? value.setScale(SCALE, RoundingMode.HALF_UP) : null;
	}

	public BigDecimal getNumerator() {
		return numerator;
	}

	public void setNumerator(BigDecimal numerator) {
		this.numerator = numerator;
	}

	public boolean getCredit() {
		return credit;
	}

	public void setCredit(boolean credit) {
		this.credit = credit;
	}

	public Currency getCurrency() {
		return currency;
	}

	public void setCurrency(Currency currency) {
		this.currency = currency;
	}

	public Cash getCash() {
		return cash;
	}

	public void setCash(Cash cash) {
		this.cash = cash;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public Warehouse getWarehouse() {
		return warehouse;
	}

	public void setWarehouse(Warehouse warehouse) {
		this.warehouse = warehouse;
	}

	public Account getParentAccount() {
		return parentAccount;
	}

	public void setParentAccount(Account parentAccount) {
		this.parentAccount = parentAccount;
	}

	public List<Account> getSubAccounts() {
		return subAccounts;
	}

	public void setSubAccounts(List<Account> subAccounts) {
		this.subAccounts = subAccounts;
	}

	public CountedAttribute getCountedAttribute() {
		return countedAttribute;
	}

	public void setCountedAttribute(CountedAttribute countedAttribute) {
		this.countedAttribute = countedAttribute;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// not set
		if (!(object instanceof Account)) {
			return false;
		}
		Account other = (Account) object;
		if ((this.id == null && other.id != null)
				|| (this.id != null && !this.id.equals(other.id))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "tradesystem.tradeunit.domain.Account[ id=" + id + " ]";
	}

	

}
