package tradesystem.tradeunit.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Index;

import tradesystem.tradeunit.domain.security.UserAccount;

/**
 * Entity class: Privileges
 *
 */
@Entity
@Table(schema = "public", name = "Privileges", uniqueConstraints = {
		@UniqueConstraint(columnNames = { "division_id", "userAccount_id" , "commonprivilegestypes"}),
		
		@UniqueConstraint(columnNames = { "division_id", "userAccount_id" , "warehouse_id", "warehousePrivileges" }),
		@UniqueConstraint(columnNames = { "division_id", "userAccount_id" , "cash_id", "cashPrivileges"}),
		
})

// Additional indexes
@org.hibernate.annotations.Table(appliesTo = "Privileges", indexes = {
        @Index(name = "privileges_common_idx", columnNames = {"division_id", "userAccount_id","commonprivilegestypes"}),
        @Index(name = "privileges_warehouse_idx", columnNames = {"division_id", "userAccount_id","warehouse_id"}),
        @Index(name = "privileges_cash_idx", columnNames = {"division_id", "userAccount_id","cash_id"}),
})

@NamedQueries({
	@NamedQuery(name = "Privileges.findAll", query = "select a from Privileges a order by a.id"),
	@NamedQuery(name = "Privileges.findByPrimaryKey", query = "select a from Privileges a where a.id = ?1")})
public class Privileges implements Serializable {

	   
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Column(nullable = false)
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	// ---- common -----
	
	@NotNull
	@ManyToOne
	//@Index(name = "privileges_common_idx1")
	private Division division;
	
	@NotNull
	@ManyToOne
	//@Index(name = "privileges_common_idx1")
	private UserAccount userAccount; 
	
	@Enumerated(EnumType.STRING)
	//@Index(name = "privileges_common_idx1")
	private CommonPrivilegesTypes commonPrivilegesTypes;
	
	// ----- specific -----
	
	@ManyToOne
	private Warehouse warehouse;
	
	@Enumerated(EnumType.STRING)
	private WarehousePrivileges warehousePrivileges;
	
	
	@ManyToOne
	private Cash cash;
	
	@Enumerated(EnumType.STRING)
	private CashPrivileges cashPrivileges;
	
	// --- all common fields ---
	
	boolean allow;
	
	private String description;
	

	@PrePersist
	@PreUpdate
	public void prePersistOrUpdate() throws Exception {	
		
		if(warehouse != null && !division.equals(warehouse.getDivision()))
			throw new Exception("Warehouse division in not equal division");
		
		if(cash != null && !division.equals(cash.getDivision()))
			throw new Exception("Cash division in not equal division");
	}
	
	
	/**
	 * Instantiates a new privileges.
	 */
	public Privileges() {
		super();
	}
	
	
	/**
	 * Instantiates a new privileges.
	 *
	 * @param division the division
	 * @param userAccount the user account
	 * @param commonPrivilegesTypes the common privileges types
	 * @param warehouse the warehouse
	 * @param warehousePrivileges the warehouse privileges
	 * @param cash the cash
	 * @param cashPrivileges the cash privileges
	 * @param description the description
	 */
	public Privileges(Division division, UserAccount userAccount,
			CommonPrivilegesTypes commonPrivilegesTypes, Warehouse warehouse, WarehousePrivileges warehousePrivileges,
			Cash cash, CashPrivileges cashPrivileges, String description) {
		super();
		this.division = division;
		this.userAccount = userAccount;
		this.commonPrivilegesTypes = commonPrivilegesTypes;
		this.warehouse = warehouse;
		this.warehousePrivileges = warehousePrivileges;
		this.cash = cash;
		this.cashPrivileges = cashPrivileges;
		this.description = description;
	}
	
	/**
	 * Instantiates a new privileges.
	 *
	 * @param division the division
	 * @param userAccount the user account
	 * @param commonPrivilegesTypes the common privileges types
	 * @param warehouse the warehouse
	 * @param warehousePrivileges the warehouse privileges
	 * @param cash the cash
	 * @param cashPrivileges the cash privileges
	 * @param allow the allow
	 * @param description the description
	 */
	public Privileges(Division division, UserAccount userAccount,
			CommonPrivilegesTypes commonPrivilegesTypes, Warehouse warehouse, WarehousePrivileges warehousePrivileges,
			Cash cash, CashPrivileges cashPrivileges, boolean allow, String description) {
		super();
		this.division = division;
		this.userAccount = userAccount;
		this.commonPrivilegesTypes = commonPrivilegesTypes;
		this.warehouse = warehouse;
		this.warehousePrivileges = warehousePrivileges;
		this.cash = cash;
		this.cashPrivileges = cashPrivileges;
		this.description = description;
		this.allow = allow;
	}


	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}   
		
	public UserAccount getUserAccount() {
		return userAccount;
	}
	
	public void setUserAccount(UserAccount userAccount) {
		this.userAccount = userAccount;
	}
	
	public Division getDivision() {
		return division;
	}
	
	public void setDivision(Division division) {
		this.division = division;
	}
	
	public Warehouse getWarehouse() {
		return warehouse;
	}
	
	public void setWarehouse(Warehouse warehouse) {
		this.warehouse = warehouse;
	}
	
	public Cash getCash() {
		return cash;
	}
	
	public void setCash(Cash cash) {
		this.cash = cash;
	}
	
	public CommonPrivilegesTypes getCommonPrivilegesTypes() {
		return commonPrivilegesTypes;
	}

	public void setCommonPrivelegesTypes(CommonPrivilegesTypes commonPrivilegesTypes) {
		this.commonPrivilegesTypes = commonPrivilegesTypes;
	}

	public CashPrivileges getCashPrivileges() {
		return cashPrivileges;
	}


	public void setCashPrivileges(CashPrivileges cashPrivileges) {
		this.cashPrivileges = cashPrivileges;
	}

	public WarehousePrivileges getWarehousePrivileges() {
		return warehousePrivileges;
	}


	public void setWarehousePrivileges(WarehousePrivileges warehousePrivileges) {
		this.warehousePrivileges = warehousePrivileges;
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public void setCommonPrivilegesTypes(CommonPrivilegesTypes commonPrivilegesTypes) {
		this.commonPrivilegesTypes = commonPrivilegesTypes;
	}

	public boolean isAllow() {
		return allow;
	}

	public void setAllow(boolean allow) {
		this.allow = allow;
	}


	@Override
	public int hashCode() {
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// not set
		if (!(object instanceof Privileges)) {
			return false;
		}
		Privileges other = (Privileges) object;
		if ((this.id == null && other.id != null)
				|| (this.id != null && !this.id.equals(other.id))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "tradesystem.tradeunit.domain.Privileges[ id=" + id + " ]";
	}
   
}
