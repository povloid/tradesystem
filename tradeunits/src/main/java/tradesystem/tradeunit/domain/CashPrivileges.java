package tradesystem.tradeunit.domain;

import java.io.Serializable;

/**
 * The Enum CashPrivileges.
 */
public enum CashPrivileges implements Serializable {
	
	/** The view. */
	VIEW,
	
	/** The cash plus. */
	CASH_PLUS,
	
	/** The cash minus. */
	CASH_MINUS,
	
	/** The product sale. */
	PRODUCT_SALE, 
	
	/** The product return. */
	PRODUCT_RETURN

}
