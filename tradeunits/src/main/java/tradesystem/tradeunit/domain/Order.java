package tradesystem.tradeunit.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Index;
import org.hibernate.validator.constraints.Length;

import tradesystem.tradeunit.domain.security.UserAccount;

/**
 * Entity class: Order Order - ордер
 * 
 */
@Entity
@Table(schema = "public", name = "Order")
@NamedQueries({
		@NamedQuery(name = "Order.findAll", query = "select a from Order a order by a.id"),
		@NamedQuery(name = "Order.findByPrimaryKey", query = "select a from Order a where a.id = ?1") })
public class Order implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Column(nullable = false)
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(nullable = false)
	@Index(name = "order_idx2")
	@NotNull
	private Date opTime;

	@ManyToOne
	@Index(name = "order_idx0")
	private Order previousOrder;

	@Length(max = 1000)
	@Column(length = 1000)
	private String description;

	@Enumerated(EnumType.STRING)
	@Column(nullable = false)
	@Index(name = "order_idx1")
	private OrderType orderType;

	@NotNull
	@ManyToOne
	@Index(name = "order_idx3")
	private Division division;

	@ManyToOne
	@Index(name = "order_idx4")
	@JoinColumn(nullable = false)
	@NotNull
	private UserAccount userAccount;

	@OneToMany(mappedBy = "order", fetch = FetchType.LAZY)
	private Set<Item> items;

	// Additional info

	@ManyToOne
	private Provider provider;
	
	

	/**
	 * Pre persist or update.
	 * 
	 * @throws Exception
	 *             the exception
	 */
	@PrePersist
	@PreUpdate
	public void prePersistOrUpdate() throws Exception {
		if(provider != null && orderType != OrderType.PRODUCT_PLUS && orderType != OrderType.PRODUCT_MINUS)
			throw new Exception("provider can be setted only if orderType is PRODUCT_PLUS or PRODUCT_MINUS");
			
			
	}

	/**
	 * Instantiates a new order.
	 *
	 * @param opTime the op time
	 * @param orderType the order type
	 * @param division the division
	 * @param userAccount the user account
	 * @param description the description
	 */
	public Order(Date opTime, OrderType orderType, Division division,
			UserAccount userAccount, String description) {
		super();
		this.orderType = orderType;
		this.opTime = opTime;
		this.division = division;
		this.userAccount = userAccount;
		this.description = description;
	}
	
	
	/**
	 * Instantiates a new order.
	 *
	 * @param opTime the op time
	 * @param orderType the order type
	 * @param division the division
	 * @param userAccount the user account
	 * @param description the description
	 * @param provider the provider
	 */
	public Order(Date opTime, OrderType orderType, Division division,
			UserAccount userAccount, String description, Provider provider) {
		super();
		this.orderType = orderType;
		this.opTime = opTime;
		this.division = division;
		this.userAccount = userAccount;
		this.description = description;
		this.provider = provider;
	}

	public Order() {
		super();
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Order getPreviousOrder() {
		return previousOrder;
	}

	public void setPreviousOrder(Order previousOrder) {
		this.previousOrder = previousOrder;
	}

	public OrderType getOrderType() {
		return orderType;
	}

	public void setOrderType(OrderType orderType) {
		this.orderType = orderType;
	}

	public Date getOpTime() {
		return opTime;
	}

	public void setOpTime(Date opTime) {
		this.opTime = opTime;
	}

	public Division getDivision() {
		return division;
	}

	public void setDivision(Division division) {
		this.division = division;
	}

	public UserAccount getUserAccount() {
		return userAccount;
	}

	public void setUserAccount(UserAccount userAccount) {
		this.userAccount = userAccount;
	}

	public Set<Item> getItems() {
		return items;
	}

	public void setItems(Set<Item> items) {
		this.items = items;
	}

	public Provider getProvider() {
		return provider;
	}

	public void setProvider(Provider provider) {
		this.provider = provider;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// not set
		if (!(object instanceof Order)) {
			return false;
		}
		Order other = (Order) object;
		if ((this.id == null && other.id != null)
				|| (this.id != null && !this.id.equals(other.id))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "tradesystem.tradeunit.domain.Order[ id=" + id + " ]";
	}

}
