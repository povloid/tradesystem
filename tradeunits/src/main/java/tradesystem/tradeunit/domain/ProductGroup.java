package tradesystem.tradeunit.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Index;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * Entity class: ProductGroup ProductGroup - группа продуктов
 * 
 * @author Kopychenko Pavel
 * 
 * @date Jun 12, 2013
 * 
 */
@Entity
@Table(schema = "public", name = "ProductGroup", uniqueConstraints = @UniqueConstraint(columnNames = {
		"parent_id", "keyName" }))
@NamedQueries({
		@NamedQuery(name = "ProductGroup.findAll", query = "select a from ProductGroup a order by a.id"),
		@NamedQuery(name = "ProductGroup.findByPrimaryKey", query = "select a from ProductGroup a where a.id = ?1") })
public class ProductGroup implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	@Column(nullable = false)
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	/** The parent group. */
	@ManyToOne
	@JoinColumn(name = "parent_id", referencedColumnName = "id")
	@Index(name = "product_group_parent_idx")
	private ProductGroup parent;

	/** The key name. */
	@NotNull
	@NotEmpty
	@Length(max = 250)
	@Column(nullable = false, length = 250)
	@Index(name = "product_group_keyname_idx")
	private String keyName;

	/** The description. */
	@Length(max = 3000)
	@Column(length = 3000)
	private String description;
	
	@Length(max = 1000)
	@Column(length = 1000)
	private String path;
	
	// TODO: Написать check и тесты
	
	
	
	
	/**
	 * Preparing persist or update.
	 * 
	 * @throws Exception
	 *             the exception
	 */
	@PrePersist
	@PreUpdate
	public void prePersistOrUpdate() throws Exception {	
		path = getTreePathAsString();
	}
	
	/**
	 * Gets the tree path.
	 *
	 * @return the tree path
	 */
	public List<ProductGroup> getTreePath() {
		
		List<ProductGroup> path = new ArrayList<>();
		
		ProductGroup parent = this;
		
		while(parent != null){
			path.add(parent);
			parent = parent.getParent();
		}
		
		Collections.reverse(path);
		
		return path; 
	}
	
	
	/**
	 * Gets the tree path as string.
	 *
	 * @return the tree path as string
	 */
	public String getTreePathAsString() {
		String path = "";
		
		for(ProductGroup pg: getTreePath())
			path += " / " + pg.keyName;
		
		return path.length() == 0 ? "/" : path;
	}

	public ProductGroup() {
		super();
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getKeyName() {
		return this.keyName;
	}

	public void setKeyName(String keyName) {
		System.out.println(keyName);
		this.keyName = keyName;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public ProductGroup getParent() {
		return parent;
	}

	public void setParent(ProductGroup parent) {
		this.parent = parent;
	}

	public String getPath() {
		return path;
	}


	public void setPath(String path) {
		this.path = path;
	}


	@Override
	public int hashCode() {
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// not set
		if (!(object instanceof ProductGroup)) {
			return false;
		}
		ProductGroup other = (ProductGroup) object;
		if ((this.id == null && other.id != null)
				|| (this.id != null && !this.id.equals(other.id))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "tradesystem.tradeunit.domain.ProductGroup[ id=" + id + " ]";
	}

}
