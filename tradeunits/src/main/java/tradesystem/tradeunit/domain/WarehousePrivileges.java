package tradesystem.tradeunit.domain;

import java.io.Serializable;


/**
 * The Enum WarehousePrivileges.
 */
public enum WarehousePrivileges implements Serializable {
	
	VIEW,
	/** The product plus. */
	PRODUCT_PLUS, 
	
	/** The product minus. */
	PRODUCT_MINUS,
	
	/** The product sale. */
	PRODUCT_SALE, 
	
	/** The product return. */
	PRODUCT_RETURN

}
