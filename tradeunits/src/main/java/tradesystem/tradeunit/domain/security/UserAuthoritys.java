package tradesystem.tradeunit.domain.security;

/**
 * Список ролей
 * 
 * @author povloid
 * 
 */
public enum UserAuthoritys {
	ROLE_SUPERUSER, // Суперпользователь
	ROLE_ADMIN, // Администратор

	ROLE_USER,

	ROLE_RB,

	ROLE_USER_META_EDITOR, // Редактор метатегов

	ROLE_VIEWER, // Просмотр
	
}
