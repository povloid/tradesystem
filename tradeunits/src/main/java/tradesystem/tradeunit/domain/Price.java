package tradesystem.tradeunit.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Index;

import tradesystem.tradeunit.domain.Measure.MeasureType;

/**
 * Entity class: Price
 * Price - цена
 *
 */
@Entity
@Table(schema = "public", name = "Price", uniqueConstraints = @UniqueConstraint(columnNames = {
		"division_id", "product_id" }))
@NamedQueries({
	@NamedQuery(name = "Price.findAll", query = "select a from Price a order by a.id"),
	@NamedQuery(name = "Price.findByPrimaryKey", query = "select a from Price a where a.id = ?1")})
public class Price implements Serializable {

	public final static int SCALE = 2; 
	public final static int PRECISION = 19;
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Column(nullable = false)
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
		
	@Column(scale = SCALE, precision = PRECISION)
	private BigDecimal value;
	
	@Column(scale = SCALE, precision = PRECISION)
	private BigDecimal valueNumrator;
	
	@NotNull
	@ManyToOne
	@Index(name = "price_division_product_idx")
	private Division division;
	
	@NotNull
	@ManyToOne
	@Index(name = "price_division_product_idx")
	private Product product;
	
	private String description;

	/**
	 * Pre persist or update.
	 * 
	 * @throws Exception
	 *             the exception
	 */
	@PrePersist
	@PreUpdate
	public void prePersistOrUpdate() throws Exception {
		
		if (value != null) {

			// TODO: Написать тест
			if (product.getMeasure().getMeasureType() == MeasureType.FRACTION && value != null && valueNumrator == null)
				throw new Exception("valueNumrator nast setted to if measure tupe is FRACTION");

			// TODO: Написать тест
			if (product.getMeasure().getMeasureType() == MeasureType.FRACTION && (valueNumrator == null || valueNumrator.compareTo(new BigDecimal(0)) <= 0))
				throw new Exception("valueNumrator mast have positive value");
		}
		
	}
	

	public Price() {
		super();
	}   
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}   
  
	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	public BigDecimal getValue() {
		return value;
	}

	public void setValue(BigDecimal value) {
		this.value = value != null ? value.setScale(SCALE, RoundingMode.HALF_UP) : null;
	}
	
	public Division getDivision() {
		return division;
	}
	
	public void setDivision(Division division) {
		this.division = division;
	}
	
	public Product getProduct() {
		return product;
	}
	
	public void setProduct(Product product) {
		this.product = product;
	}
	
	public BigDecimal getValueNumrator() {
		return valueNumrator;
	}
	
	public void setValueNumrator(BigDecimal valueNumrator) {
		this.valueNumrator = valueNumrator;
	}
	
	@Override
	public int hashCode() {
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// not set
		if (!(object instanceof Price)) {
			return false;
		}
		Price other = (Price) object;
		if ((this.id == null && other.id != null)
				|| (this.id != null && !this.id.equals(other.id))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "tradesystem.tradeunit.domain.Price[ id=" + id + " ]";
	}
   
}
