package tradesystem.tradeunit.domain;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import tradesystem.tradeunit.domain.Measure.MeasureType;

/**
 * Entity class: Currency Currency - валюта
 * 
 */
@Entity
@Table(schema = "public", name = "Currency")
@NamedQueries({
		@NamedQuery(name = "Currency.findAll", query = "select a from Currency a order by a.id"),
		@NamedQuery(name = "Currency.findByPrimaryKey", query = "select a from Currency a where a.id = ?1") })
public class Currency implements Serializable, ICountsObject {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Column(nullable = false)
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(unique = true)
	private Boolean main;
	
	@NotNull
	@Column(unique = true, nullable = false)
	private String keyName;

	private String description;
	
	
	@Override
	public CountedAttribute getCountedAttribute() {
		return null;
	}
	

	@Override
	public Measure.MeasureType getValueType() {
		return MeasureType.FLOAT;
	}

	@Override
	public Type getType() {
		return Type.MONEY;
	}

	@Override
	public BigDecimal getDenominator() {
		return new BigDecimal(0);
	}

	public Currency() {
		super();
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getKeyName() {
		return this.keyName;
	}

	public void setKeyName(String keyName) {
		System.out.println(keyName);
		this.keyName = keyName;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Boolean getMain() {
		return main;
	}

	public void setMain(Boolean main) {
		this.main = main;
	}


	@Override
	public int hashCode() {
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// not set
		if (!(object instanceof Currency)) {
			return false;
		}
		Currency other = (Currency) object;
		if ((this.id == null && other.id != null)
				|| (this.id != null && !this.id.equals(other.id))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "tradesystem.tradeunit.domain.Currency[ id=" + id + " ]";
	}



}
