package tradesystem.tradeunit.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * The Class Image.
 */
@Entity
@Table(schema = "public", name = "image")
@NamedQueries({
		@NamedQuery(name = "Image.findAll", query = "select a from Image a order by a.id"),
		@NamedQuery(name = "Image.findByPrimaryKey", query = "select a from Image a where a.id = ?1") })
public class Image implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id. */
	@Id
	@Column(nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	/**
	 * Instantiates a new image.
	 */
	public Image() {
		super();
	}

	/**
	 * Instantiates a new image.
	 * 
	 * @param filename
	 *            the filename
	 */
	public Image(String filename) {
		super();
		this.filename = filename;
	}

	/** The filename. */
	private String filename;

	/**
	 * Gets the filename.
	 * 
	 * @return the filename
	 */
	public String getFilename() {
		return filename;
	}

	/**
	 * Sets the filename.
	 * 
	 * @param filename
	 *            the new filename
	 */
	public void setFilename(String filename) {
		this.filename = filename;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// not set
		if (!(object instanceof Image)) {
			return false;
		}
		Image other = (Image) object;
		if ((this.id == null && other.id != null)
				|| (this.id != null && !this.id.equals(other.id))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "tradesystem.tradeunit.domain.Image[ id=" + id + " ]";
	}

}
