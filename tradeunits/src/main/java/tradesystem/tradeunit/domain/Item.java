package tradesystem.tradeunit.domain;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Index;
import org.hibernate.validator.constraints.Length;

import tradesystem.tradeunit.domain.Measure.MeasureType;

/**
 * Entity class: Item Item - запись
 * 
 */
@Entity
@Table(schema = "public", name = "Item", uniqueConstraints = @UniqueConstraint(columnNames = {
		"order_id", "account_id" }))
@NamedQueries({
		@NamedQuery(name = "Item.findAll", query = "select a from Item a order by a.id"),
		@NamedQuery(name = "Item.findByPrimaryKey", query = "select a from Item a where a.id = ?1") })
public class Item implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Column(nullable = false)
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotNull
	@ManyToOne
	@Index(name = "items_idx0")
	private Order order;

	@NotNull
	@ManyToOne
	@Index(name = "items_idx1")
	private Account account;

	@NotNull
	@Column(nullable = false, scale = Account.SCALE, precision = Account.PRECISION)
	private BigDecimal value;

	@NotNull
	@Column(nullable = false, scale = 0)
	private BigDecimal numerator;
	
	@ManyToOne
	private Measure measure;

	@Length(max = 250)
	@Column(length = 250)
	private String description;
	
	@ManyToOne
	private CountedAttribute countedAttribute;
	
	@Column(scale = Account.SCALE, precision = Account.PRECISION)
	private BigDecimal actualPriceValue;
	
	@Column(scale = Account.SCALE, precision = Account.PRECISION)
	private BigDecimal actualPriceValueNumerator;
	
	@Column(scale = Account.SCALE, precision = Account.PRECISION)
	private BigDecimal costValue;
	
	@Column(scale = Account.SCALE, precision = Account.PRECISION)
	private BigDecimal costValueNumerator;
	
	// Transient fields
	
	@Transient
	private IOwner transientOwner;
	
	@Transient
	private ICountsObject transientCountsObject; 
	
	/**
	 * Pre persist or update.
	 * 
	 * @throws Exception
	 *             the exception
	 */
	@PrePersist
	@PreUpdate
	public void prePersistOrUpdate() throws Exception {

		// TODO: написать тест
		if (account == null) 
			throw new Exception("account is null");
		
		// TODO: написать тест
		if (account.getCountedAttribute() != null 
				&& !account.getCountedAttribute().equals(countedAttribute))
			throw new Exception("countedAttribute in item and account mast be equal. ( " + countedAttribute + " != " + account.getCountedAttribute() + ")");

		// TODO: написать тест
		if (order == null)
			throw new Exception("order is null");

		// TODO: написать тест
		if (value == null)
			throw new Exception("value is null");

		// TODO: написать тест
		if (numerator == null)
			throw new Exception("numerator is null");
		
		

		// TODO: написать тест
		if (!account.getDivision().equals(order.getDivision()))
			throw new Exception("account division is not equal order division");
		
		// ---------------------------------------------------------------------------------------------------
		if (order.getOrderType() == OrderType.PRODUCT_PLUS) { // PRODUCT_PLUS

			// TODO: написать тест
			if (!(account.getOwner() instanceof Warehouse))
				throw new Exception("No product owner for PRODUCT_PLUS oderation");
			
			// TODO: написать тест
			if (value.compareTo(new BigDecimal(0)) < 0)
				throw new Exception("product value mast be >= 0 when order type is PRODUCT_PLUS");
			
			// TODO: написать тест
			if (numerator.compareTo(new BigDecimal(0)) < 0)
				throw new Exception("product numerator mast be >= 0 when order type is PRODUCT_PLUS");
			
			// TODO: написать тест
			if (value.compareTo(new BigDecimal(0)) == 0 && numerator.compareTo(new BigDecimal(0)) == 0)
				throw new Exception("Product value or numerator mast be >= 0 when order type is PRODUCT_PLUS");
			
			// TODO: написать тест
			if (measure == null || !measure.equals(account.getProduct().getMeasure()))
				throw new Exception("measure is null OR measure is not equal product measure");

		}
		// -----------------------------------------------------------------------------------------------
		else if (order.getOrderType() == OrderType.PRODUCT_MINUS) { // PRODUCT_MINUS

			// TODO: написать тест
			if (!(account.getOwner() instanceof Warehouse))
				throw new Exception("No product owner for PRODUCT_MINUS oderation");
			
			// TODO: написать тест
			if (value.compareTo(new BigDecimal(0)) > 0)
				throw new Exception("product value mast be <= 0 when order type is PRODUCT_MINUS");
			
			// TODO: написать тест
			if (numerator.compareTo(new BigDecimal(0)) > 0)
				throw new Exception("product numerator mast be <= 0 when order type is PRODUCT_MINUS");
			
			// TODO: написать тест
			if (value.compareTo(new BigDecimal(0)) == 0 && numerator.compareTo(new BigDecimal(0)) == 0)
				throw new Exception("Product value or numerator mast be <= 0 when order type is PRODUCT_MINUS");
			
			// TODO: написать тест
			if (measure == null || !measure.equals(account.getProduct().getMeasure()))
				throw new Exception("measure is null OR measure is not equal product measure");
		}
		// ---------------------------------------------------------------------------------------------------
		// TODO: написать тест
		else if (order.getOrderType() == OrderType.CASH_PLUS) { // CASH_PLUS

			// TODO: написать тест
			if (!(account.getOwner() instanceof Cash))
				throw new Exception("No cash owner for CASH_PLUS oderation");

			// TODO: написать тест
			if (value.compareTo(new BigDecimal(0)) <= 0)
				throw new Exception("cash value mast be > 0 when order type is CASH_PLUS");
			
		}  
		// ---------------------------------------------------------------------------------------------------
		// TODO: написать тест
		else if (order.getOrderType() == OrderType.CASH_MINUS) { // CASH_MINUS

			// TODO: написать тест
			if (!(account.getOwner() instanceof Cash))
				throw new Exception(
						"No cash owner for CASH_MINUS oderation");

			// TODO: написать тест
			if (value.compareTo(new BigDecimal(0)) >= 0)
				throw new Exception("Cash value mast be < 0 when order type is CASH_MINUS");
			
		} 
		// ---------------------------------------------------------------------------------------------------
		// TODO: написать тест
		else if (order.getOrderType() == OrderType.PRODUCT_SALE) { // PRODUCT_SALE
			
			// TODO: написать тест
			if (this.account.getOwner() instanceof Warehouse && this.account.getCountsObject() instanceof Product) { // PRODUCTS VALIDATION -------------------------
				
				// TODO: написать тест
				if (costValue == null)
					throw new Exception("The costValue must seted on warehouse items in PRODUCT_SALE operation");
				
				// TODO: написать тест
				if (costValue.compareTo(new BigDecimal(0)) < 0)
					throw new Exception("costValue mast be >= 0 when order type is PRODUCT_SALE");
				
				if(account.getProduct().getMeasure().getMeasureType() == MeasureType.FRACTION){
					
					// TODO: написать тест
					if (costValueNumerator == null)
						throw new Exception("The costValueNumerator must seted on warehouse items in PRODUCT_SALE operation");
					
					// TODO: написать тест
					if (costValueNumerator.compareTo(new BigDecimal(0)) < 0)
						throw new Exception("costValueNumerator mast be >= 0 when order type is PRODUCT_SALE");
				}
				

				// TODO: написать тест
				if (!(account.getOwner() instanceof Warehouse))
					throw new Exception("No product owner for PRODUCT_SALE oderation");
				
				// TODO: написать тест
				if (value.compareTo(new BigDecimal(0)) > 0)
					throw new Exception("Product value mast be <= 0 when order type is PRODUCT_SALE");
				
				// TODO: написать тест
				if (numerator.compareTo(new BigDecimal(0)) > 0)
					throw new Exception("Product numerator mast be <= 0 when order type is PRODUCT_SALE");
				
				// TODO: написать тест
				if (value.compareTo(new BigDecimal(0)) == 0 && numerator.compareTo(new BigDecimal(0)) == 0)
					throw new Exception("Product value or numerator mast be <= 0 when order type is PRODUCT_SALE");
				
				// TODO: написать тест
				if (measure == null || !measure.equals(account.getProduct().getMeasure()))
					throw new Exception("measure is null OR measure is not equal product measure");
				
			} else if (this.account.getOwner() instanceof Cash) { // CASH VALIDATION ------------------------- 
				// CASH VALIDATION
				
				// TODO: написать тест
				if (!(account.getOwner() instanceof Cash))
					throw new Exception("No cash owner for PRODUCT_SALE oderation");

				// TODO: написать тест
				if (value.compareTo(new BigDecimal(0)) < 0)
					throw new Exception("value mast be => 0 when order type is PRODUCT_SALE");
				
			}  else if (this.account.getOwner() instanceof Customer) { // CUSTOMER VALIDATION ----------------
				
				// TODO: написать тест
				if (value.compareTo(new BigDecimal(0)) > 0)
					throw new Exception("Customer value mast be <= 0 when order type is PRODUCT_SALE");
			} 
		}
		// ---------------------------------------------------------------------------------------------------
		// TODO: написать тест
		else if (order.getOrderType() == OrderType.PRODUCT_RETURN) { // PRODUCT_RETURN

			/*
			// TODO: написать тест
			if (this.account.getOwner() instanceof Warehouse && this.account.getCountsObject() instanceof Product) { // PRODUCTS
																														// VALIDATION
																														// -------------------------

				// TODO: написать тест
				if (costValue == null)
					throw new Exception("The costValue must seted on warehouse items in PRODUCT_RETURN operation");

				// TODO: написать тест
				if (costValue.compareTo(new BigDecimal(0)) < 0)
					throw new Exception("costValue mast be >= 0 when order type is PRODUCT_SALE");

				if (account.getProduct().getMeasure().getMeasureType() == MeasureType.FRACTION) {

					// TODO: написать тест
					if (costValueNumerator == null)
						throw new Exception("The costValueNumerator must seted on warehouse items in PRODUCT_SALE operation");

					// TODO: написать тест
					if (costValueNumerator.compareTo(new BigDecimal(0)) < 0)
						throw new Exception("costValueNumerator mast be >= 0 when order type is PRODUCT_SALE");
				}

				// TODO: написать тест
				if (!(account.getOwner() instanceof Warehouse))
					throw new Exception("No product owner for PRODUCT_SALE oderation");

				// TODO: написать тест
				if (value.compareTo(new BigDecimal(0)) > 0)
					throw new Exception("Product value mast be <= 0 when order type is PRODUCT_SALE");

				// TODO: написать тест
				if (numerator.compareTo(new BigDecimal(0)) > 0)
					throw new Exception("Product numerator mast be <= 0 when order type is PRODUCT_SALE");

				// TODO: написать тест
				if (value.compareTo(new BigDecimal(0)) == 0 && numerator.compareTo(new BigDecimal(0)) == 0)
					throw new Exception("Product value or numerator mast be <= 0 when order type is PRODUCT_SALE");

				// TODO: написать тест
				if (measure == null || !measure.equals(account.getProduct().getMeasure()))
					throw new Exception("measure is null OR measure is not equal product measure");

			} else if (this.account.getOwner() instanceof Cash) { // CASH
																	// VALIDATION
																	// -------------------------
				// CASH VALIDATION

				// TODO: написать тест
				if (!(account.getOwner() instanceof Cash))
					throw new Exception("No cash owner for PRODUCT_SALE oderation");

				// TODO: написать тест
				if (value.compareTo(new BigDecimal(0)) < 0)
					throw new Exception("value mast be => 0 when order type is PRODUCT_SALE");

			} else if (this.account.getOwner() instanceof Customer) { // CUSTOMER
																		// VALIDATION
																		// ----------------

				// TODO: написать тест
				if (value.compareTo(new BigDecimal(0)) > 0)
					throw new Exception("Customer value mast be <= 0 when order type is PRODUCT_SALE");
			}
			
			*/
		}
		
		
	}
	

	/**
	 * Instantiates a new item.
	 *
	 * @param account the account
	 */
	public Item(Account account) {
		super();
		this.account = account;
		this.countedAttribute = account.getCountedAttribute();
		this.transientOwner = account.getOwner();
		this.transientCountsObject = account.getCountsObject();
		
		// For product
		if(account.getProduct() != null && account.getCountsObject().equals(account.getProduct()))
			this.measure = account.getProduct().getMeasure();
	}


	/**
	 * Instantiates a new item.
	 *
	 * @param transientOwner the transient owner
	 * @param transientCountsObject the transient counts object
	 * @param value the value
	 * @param numerator the numerator
	 * @param countedAttribute the counted attribute
	 * @param description the description
	 */
	public Item(IOwner transientOwner, ICountsObject transientCountsObject,  BigDecimal value, BigDecimal numerator, CountedAttribute countedAttribute, String description) {
		super();
		this.transientOwner = transientOwner;
		this.transientCountsObject = transientCountsObject;
		this.value = value;
		this.numerator = numerator;
		this.countedAttribute = countedAttribute;
		this.description = description;
		
		// For product
		if(transientCountsObject instanceof Product)
			this.measure = ((Product)transientCountsObject).getMeasure();
	}
	
	

	/**
	 * Instantiates a new item.
	 */
	public Item() {
		super();
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Order getOrder() {
		return order;
	}

	public void setOrder(Order order) {
		this.order = order;
	}

	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}

	public BigDecimal getValue() {
		return value;
	}

	public void setValue(BigDecimal value) {
		this.value = value;
	}

	public BigDecimal getNumerator() {
		return numerator;
	}

	public void setNumerator(BigDecimal numerator) {
		this.numerator = numerator;
	}
	
	public CountedAttribute getCountedAttribute() {
		return countedAttribute;
	}

	public void setCountedAttribute(CountedAttribute countedAttribute) {
		this.countedAttribute = countedAttribute;
	}

	public BigDecimal getCostValue() {
		return costValue;
	}

	public void setCostValue(BigDecimal costValue) {
		this.costValue = costValue;
	}

	public BigDecimal getActualPriceValue() {
		return actualPriceValue;
	}

	public void setActualPriceValue(BigDecimal actualPriceValue) {
		this.actualPriceValue = actualPriceValue;
	}

	public IOwner getTransientOwner() {
		return transientOwner;
	}

	public void setTransientOwner(IOwner transientOwner) {
		this.transientOwner = transientOwner;
	}

	public ICountsObject getTransientCountsObject() {
		return transientCountsObject;
	}

	public void setTransientCountsObject(ICountsObject transientCountsObject) {
		this.transientCountsObject = transientCountsObject;
	}

	public BigDecimal getActualPriceValueNumerator() {
		return actualPriceValueNumerator;
	}

	public void setActualPriceValueNumerator(BigDecimal actualPriceValueNumerator) {
		this.actualPriceValueNumerator = actualPriceValueNumerator;
	}

	public BigDecimal getCostValueNumerator() {
		return costValueNumerator;
	}

	public void setCostValueNumerator(BigDecimal costValueNumerator) {
		this.costValueNumerator = costValueNumerator;
	}
	
	public Measure getMeasure() {
		return measure;
	}

	public void setMeasure(Measure measure) {
		this.measure = measure;
	}


	@Override
	public int hashCode() {
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// not set
		if (!(object instanceof Item)) {
			return false;
		}
		Item other = (Item) object;
		if ((this.id == null && other.id != null)
				|| (this.id != null && !this.id.equals(other.id))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "tradesystem.tradeunit.domain.Item[ id=" + id + " ]";
	}

}
