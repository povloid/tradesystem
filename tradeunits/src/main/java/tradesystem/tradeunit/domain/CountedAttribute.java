package tradesystem.tradeunit.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Index;



/**
 * Entity class: CountedAttribute
 * СountedAttribute - подсчитываемый атрибут
 *
 */
@Entity
@Table(schema = "public", name = "CountedAttribute" , uniqueConstraints = @UniqueConstraint(columnNames = {
		"parent_id", "keyName" }))
@NamedQueries({
	@NamedQuery(name = "CountedAttribute.findAll", query = "select a from CountedAttribute a order by a.id"),
	@NamedQuery(name = "CountedAttribute.findByPrimaryKey", query = "select a from CountedAttribute a where a.id = ?1")})
public class CountedAttribute implements Serializable {

	   
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Column(nullable = false)
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@NotNull
    @Column(nullable = false)
	private String keyName;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "parent_id", referencedColumnName = "id")
	@Index(name = "CountedAttribute_parent_idx")
	private CountedAttribute parent;
	
	@OneToMany(mappedBy = "parent", fetch = FetchType.LAZY)
	@OrderBy("keyName")
	//@OrderColumn
	private List<CountedAttribute> children = new ArrayList<CountedAttribute>();
	
	private int orId;
	
	private String description;
	
	// TODO: Проверку родителей на отрыв графа
	

	public CountedAttribute() {
		super();
	}   
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}   
	public String getKeyName() {
		return this.keyName;
	}

	public void setKeyName(String keyName) {
		System.out.println(keyName);
		this.keyName = keyName;
	}   
	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	public CountedAttribute getParent() {
		return parent;
	}
	
	public void setParent(CountedAttribute parent) {
		this.parent = parent;
	}
	
	public List<CountedAttribute> getChildren() {
		return children;
	}
	
	public void setChildren(List<CountedAttribute> children) {
		this.children = children;
	}
	
	public int getOrId() {
		return orId;
	}
	
	public void setOrId(int orId) {
		this.orId = orId;
	}
	
	@Override
	public int hashCode() {
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// not set
		if (!(object instanceof CountedAttribute)) {
			return false;
		}
		CountedAttribute other = (CountedAttribute) object;
		if ((this.id == null && other.id != null)
				|| (this.id != null && !this.id.equals(other.id))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "tradesystem.tradeunit.domain.CountedAttribute[ id=" + id + " ]";
	}
   
}
