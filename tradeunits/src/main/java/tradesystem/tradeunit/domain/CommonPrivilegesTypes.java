package tradesystem.tradeunit.domain;

import java.io.Serializable;

/**
 * 
 * Class CommonPrivelegesTypes
 *
 * @author Kopychenko Pavel
 *
 * @date 19 нояб. 2013 г.
 *
 */
public enum CommonPrivilegesTypes implements Serializable {
	EDIT_PRICE, MOVE_PRODUCT
}
